<style>
    #pano img {
        border: none !important;
        max-width: none !important;
    }
    .streetlink {
        text-decoration: underline;
        color: blue;
        font-weight: bold;
    }
    .pl-label {
        font-size: 14pt;
    }
    #Asunto {
        height: 30px;
    }
    #phone{
        height: 30px;
    }
    #email_user{
              height: 30px;
          }
    #modalContactForm{
        width: 400px;
    }
</style>
<script>
    function validateForm() {
        var error = false;
        if ($('#chkTerm').is(':checked')) {
            //var x = document.forms["myForm"]["name"].value;
            console.log(document.forms["myForm"]["promoCode"].value);
            if (document.forms["myForm"]["promoCode"].value != null && document.forms["myForm"]["promoCode"].value != '') {
                $.ajax({
                        url: '/paypal/check_promocode/' + document.forms["myForm"]["promoCode"].value,
                        global: false,
                        type: "POST",
                        async: false,
                        success: function(data){
                            if (data != 'true') {
                                alert(data);
                                document.forms["myForm"]["promoCode"].focus();
                                return false;
                            }
                        }
                    }
                );
                /*                $.post('paypal/check_promocode/' + document.forms["myForm"]["promoCode"].value)
                 .done(function (data) {
                 //  console.log(data);
                 if (data != 'true') {
                 alert(data);
                 document.forms["myForm"]["email"].focus();
                 return false;
                 }
                 });*/
            }
            if (document.forms["myForm"]["ddlClass"].value != null && document.forms["myForm"]["ddlClass"].value != '') {
                $.ajax({
                        url: '/paypal/check_available_class/' + document.forms["myForm"]["ddlClass"].value,
                        global: false,
                        type: "POST",
                        async: false,
                        success: function (data) {
                            if (data != 'true') {
                                alert(data);
                                error = true;
                                /*                                if (data == 'Class not contain any groups') {
                                 alert(data);
                                 error = true;
                                 }*/
                                document.forms["myForm"]["ddlClass"].focus();
                                return false;
                            }
                        }
                    }
                );
            }
            else
            {
                alert('Please choose available class');
                document.forms["myForm"]["ddlClass"].focus();
                return false;
            }

            if (document.forms["myForm"]["name"].value == null || document.forms["myForm"]["name"].value == "") {
                alert("First name must be filled out");
                document.forms["myForm"]["name"].focus();
                return false;
            }
            else if (document.forms["myForm"]["email"].value == null || document.forms["myForm"]["email"].value == "") {
                alert("Email must be filled out");
                document.forms["myForm"]["email"].focus();
                return false;
            }
        }
        else
        {
            alert('Tienes que marcar la casilla para aceptar los términos y condiciones de Mingles.es');
            return false;
        }
        if(error)
        {
            return false;
        }
    }

    $(function() {
        $('#sendPhoneContact').bind('click',function(e){
            e.preventDefault();

            if ($('#chkTerms').is(':checked')) {
                $.ajax({
                        url: '/main/sendphonecontact',
                        global: false,
                        type: "POST",
                        async: false,
                        data: {
                            phone: $('#phone').val(),
                            fname: $('#Asunto').val(),
                            email: $('#email_user').val()
                        },
                        success: function (data) {
                            if (data != 'true') {
                                alert(data);
                                return false;
                            }
                            else {
                                alert("Registro enviado correctamente!");
                            }
                        }
                    }
                );
            }
            else
            {
                alert("Tienes que aceptar las condiciones");
                return false;
            }
        });
        console.log( "ready!" );
    });
</script>

<link href="<?=base_url('assets/css/custom-theme/jquery-ui-1.10.3.custom.css')?>" rel="stylesheet">

<!--<link href="<?/*=base_url('assets/css/landing/bootstrap.css')*/?>" rel="stylesheet">
<link href="<?/*=base_url('assets/css/landing/bootstrap-responsive.css')*/?>" rel="stylesheet">
<link href="<?/*=base_url('assets/css/landing/bootstrap-select.css')*/?>" rel="stylesheet">-->
<!--<link href="<?/*=base_url('assets/css/landing/mystyles.css')*/?>" rel="stylesheet">
<link href="<?/*=base_url('assets/css/landing/mystyles_responsive.css')*/?>" rel="stylesheet">-->

<div class="top-setction">
    <h1 style="text-align: center;color:blue;"><?=$page['title']?></h1>
    <br>
    <div class="row" style="text-align: center;">
        <div class="wrapper">
        <div class="span1" style="text-align: center;">
            <p>&nbsp;</p>
        </div>
        <div style="float: left;">
            <img src="<?=base_url('pages/images/'.$page['page_id'].'.jpg')?>">
        </div>

        <div class="span4" style="text-align: left;float: left;padding-left: 40px;width: 400px;">
                <?=@$page['body']?>
        </div>
            <div class="" style="">
                <form id="modalContactForm">
                    <label class="pl-label" for="Asunto">
                        First name*:
                    </label>
                    <input type="text" name="Asunto" id="Asunto" required="">
                    <div class="clear">
                    </div>
                    <label class="pl-label" for="email">
                        Email*:</label>
                    <input type="text" value="" name="email_user" id="email_user" required="" >
                    <div class="clear">
                    </div>
                    <label class="pl-label" for="phone">
                        Phone (Optional):</label>
                    <input type="text" name="phone" id="phone">
                    <div class="clear">
                    </div>
                    <div class="radio">
                        <p>
                            <input type="checkbox" id="chkTerms" value="" name="">
                            <a id="informationConditionsTerms" style="color:#000" href="#modalInfoBar"><span>Acepto los Términos y Condiciones y Política de Privacidad</span></a>
                        </p>

                    </div>
                    <div class="clear">
                    </div>
                    <div class="clear">
                    </div>
                    <button aria-hidden="true" class="btn btn-info" id="sendPhoneContact">
                        Let's Mingle</button>
                </form>
            </div>
            </div>
        <input style="display: none" id="pubs_ids" value="<?=$page['pubs_ids']?>">
        <input style="display: none" id="landingpage" value="<?=$page['page_id']?>">
        <div class="clear" style="height: 30px">
        </div>
        <form name="myForm" action="<?=base_url('paypal')?>?sandbox=0" onsubmit="return validateForm()" method="post">
            <input type="hidden" name="action" value="process" />
            <input type="hidden" name="cmd" value="_cart" /> <?php // use _cart for cart checkout         ?>
            <input type="hidden" name="currency_code" value="EUR" />
            <input type="hidden" name="invoice" value="<?php echo date("His") . rand(1234, 9632); ?>" />
        <div class="blue-box">
        <div class="wrapper">
            <div class="regis-box1" id="option">
                <div class="number">
                    <img src="<?=base_url()?>assets/images/1.png"></div>
                <h2> Elige tu Ciudad </h2>
                <select name="ddlCity" id="ddlCity" class="selectbg" ></select>

                <input type="hidden" name="hndCityID" id="hndCityID">
                <h2 style="padding-top:10px;">
                    Selecciona la fecha de tu Mingles</h2>
                <div id="datepicker" class="date"></div>
                <div id="event"> </div>
                <!--  <div class="date">
                <img src="images/calender.png" alt=""></div>-->
                <!--                <div class="map" id="map_canvas" style="width:304px;height:250px;">
                                </div>-->
                <div class="form-text">
                    <a href="#" class="showall" style="display: none;">
                        Show all</a> <br>
                </div>
                <div  id="map_canvas" style="width:304px;height:250px;"></div>
                <div id="pano" style="display:none;width:304px;height:250px;"></div>

            </div>
            <div class="border">
                <img src="<?=base_url()?>assets/images/border.png" alt=""></div>
            <div class="regis-box1" id="option1">
                <div class="number">
                    <img src="<?=base_url()?>assets/images/2.png"></div>
                <h2>
                    Selecciona los datos:</h2>
                <div class="form">
                    <h3>
                        Mingles<br>
                        <!--                    <h2> <input type="label" name="selDate" id="selDate" value=""></h2><br>-->
                        Elige tu lugar Mingles:</h3>
                    <select name="ddlPub" id="ddlPub" class="selectbg" >
                    </select>
                    <!--                <input type="submit" name="info" class="submit_bt" value="Info">-->
                    <div class="height20">
                    </div>
                    <h3>Elige tu Sesi&oacute;n:</h3>
                    <select name="ddlClass" id="ddlClass" class="selectbg">
                    </select>
                    <h4>
                        Precio:</h4>
                    <div class="submit_bt" id="price">


                    </div>
                    <h3>
                        Elige tu Nivel:</h3>
                    <select name="ddlLevel"  id="ddlLevel" class="selectbg">

                    </select>
                    <div class="height20">
                    </div>
                    <h3>Idioma</h3>
                    <select name="ddlLanguage" id="ddlLanguage" class="selectbg">
                    </select>
                    <span id="InfoNivel" class="pointer submit_bt">Info Nivel</span> <span id="OnetoOne"
                                                                                           class="pointer submit_bt">one to one</span>
                </div>
            </div>
            <div class="border">
                <img src="<?=base_url()?>assets/images/border.png" alt=""></div>
            <div class="regis-box1" id="option2">
                <div class="number">
                    <img src="<?=base_url()?>assets/images/3.png"></div>
                <h2>
                    Introduce tus datos:</h2>
                <div class="form">
                    <input type="text" name="name" id="name" class="inputbg" placeholder="Nombre Completo" value="<?php
                    if ($this->session->userdata('user_id')) {
                        echo $this->session->userdata('user_fullname');
                    }
                    ?>" >
                    <input type="text" name="email"  id="email" class="inputbg" placeholder="Email" value="<?php
                    if ($this->session->userdata('user_id')) {
                        echo $this->session->userdata('email');
                    }
                    ?>" >
                    <input type="text" name="promoCode" id="promoCode" class="inputbg" placeholder="Codigo Promocional ">
                    <!--                    <h3> Selecciona la cantidad a pagar:</h3>
                                        <input type="hidden" name="ProductName" id="ProductName" value="" />
                                        <select name="ddlSession" id="ddlSession" class="selectbg">
                                        </select>-->
                    <div class="radio">
                        <p>
                            <input name="" type="checkbox" value="" id="chkTerm">
                            <a href="#modalInfoBar" style="color:#fff" id="informationConditionsTerms"><span>Acepto los T&eacute;rminos y Condiciones y Pol&iacute;tica de Privacidad</span></a>
                        </p>

                    </div>


                    <input type="submit" name="info" id="joinus" class="submit_bt" value="Apuntarme" >
                    <div class="height15">
                    </div>

                    <div class="form-text">
                        <a href="<?=base_url('bono')?>">
                            &iquest;QUIERES UN BONO MINGLES? (20% DESCUENTO)</a>
                    </div>
                </div>
            </div>
            <div class="height0">
            </div>
        </div>
            </div>
            </form>
    </div>

</div><!-- /.container -->
