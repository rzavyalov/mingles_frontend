<script>
    $(function()
    {
        latLng = new google.maps.LatLng(<?=$pub_info['Latitude']?>, <?=$pub_info['Longitude']?>);
        var map = new google.maps.Map(document.getElementById("map_canvas"), {
            center: latLng,
            zoom: 11,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });


        latLng = new google.maps.LatLng(<?=$pub_info['Latitude']?>, <?=$pub_info['Longitude']?>);
        var marker = new google.maps.Marker({
            position: latLng,
            map: map,
            title: '<?=$pub_info['PubName']?>'
        });

        var infowindow = new google.maps.InfoWindow({
            maxWidth: 150,
            maxHeight: 50
        });


         google.maps.event.addListener(marker, 'click', (function(marker) {
         return function() {
         infowindow.setContent("<div style=\"color:red;width:150px;height:50px;\">" + "<?=$pub_info['PubName']?>" + '<br/>' + "<?=$pub_info['PubAddress']?>" + "</div>");
         infowindow.open(map, marker);
         }
         })(marker));

    });
</script>
<style>
    hr {
        border: none;
        background-color: #808080;
        color: #808080;
        height: 2px;
    }
</style>


<div class="top-section">
    <h1 style="text-align: center;color:blue;">Detalles de tu sesi&oacute;n <?=$class_info['ClassName']?></h1>
    <br>

    <div class="row" style="text-align: center;">
    <div class="wrapper" style="text-align: center;">
        <div class="row">
        <div class="span1" style="text-align: center;">
            <p>&nbsp;</p>
        </div>


<!--        <div style="float: left;">
            <img src="<?/*=base_url('pages/images/'.$page['page_id'].'.jpg')*/?>">
        </div>-->

        <div class="span4" style="text-align: left; padding-left: 40px; width: 400px; padding-bottom: 20px; font-size: 14pt;">

            <div><strong>Fecha: <b><?=$class_info['ClassDate']?></b></strong></div>
            <div><strong>Lugar: <b><?=$pub_info['PubName']?></b></strong></div>
            <div><strong>Direcci&oacute;n: <b><?=$pub_info['PubAddress']?></b></strong></div>

        </div>
            <hr>
            <div style="float: left;padding-right: 20px;padding-top: 20px;">
            <h1 style="text-align: center;">Comparte con tus amigos</h1>
                <div class="shared_icons">
                    <a target="_blank" href="https://twitter.com/intent/tweet?original_referer=https%3A%2F%2Fabout.twitter.com%2Fresources%2Fbuttons&text=<?=urlencode("España va ha aprender #ingles de forma diferente con @MinglesSpain y yo me apunto #goLingual ")?>&tw_p=tweetbutton&url=<?=base_url()?>"><img src="<?=base_url('assets/images/social/Twitter.png')?>"></a>
                <a href="https://www.facebook.com/sharer/sharer.php?u=<?=base_url()?>" target="_blank">
                    <img src="<?=base_url('assets/images/social/FaceBook.png')?>">
                </a>
                <a href="https://www.linkedin.com/cws/share?url=http%3A%2F%2F<?=base_url()?>" target="_blank">
                    <img src="<?=base_url('assets/images/social/LinkedIn.png')?>"></a>
                    <img src="<?=base_url('assets/images/social/mail-icon.png')?>">
                </div>
            </div>
            <div  id="map_canvas" style="width:304px;height:250px;margin-top: 20px;float: right">
            </div>
            </div>
    </div>
        </div>


<!-- /.container -->

