<div class="height30">
</div>
<!--<start top-section> -->
<div class="inner-content">
    <div class="title_bg">
        <div class="wrapper">Disfruta con algunos de nuestro Videos</div>
    </div>
    <div class="wrapper">
        <div class="container marketing">
            <div class="row">

                <div class="video_box">
                    <h4 class="block-heading">Mingles según sus fundadores</h4>
                    <div class="block-content">
                        <p>Amanda, Gabriel y Andrés nos cuentan su visión de Mingles.</p>
                        <iframe width="100%" height="257" src="http://www.youtube.com/embed/cu5Mg4ae5jM" frameborder="0" allowfullscreen></iframe>
                        <span>Leka Leka (Madrid)</span>

                    </div>
                </div>

                <div class="video_box">
                    <h4 class="block-heading">Video presentacion de Mingles</h4>
                    <div class="block-content">
                        <p>
                            A través de este video Mingles nos muestra como funciona este proyecto innovador para aprender y mejorar idiomas. Let's Mingle!

                        </p>
                        <iframe width="100%" height="257" src="http://www.youtube.com/embed/KnVHkkT0Mo4?rel=0" frameborder="0" allowfullscreen></iframe>
                        <span>Diciembre 2013 - The Irish Rover (Madrid)</span>

                    </div>
                </div>

                <div class="clearfix"></div>

                <div class="video_box">
                    <h4 class="block-heading">Publireportaje Mingles</h4>
                    <div class="block-content">
                        <p>Los fundadores de Mingles y algunos alumnos nos cuentan en que consiste Mingles y su experiencia.</p>
                        <iframe width="100%" height="257" src="http://www.youtube.com/embed/c9Bb1Rxbncg" frameborder="0" allowfullscreen></iframe>
                        <span>Diciembre 2013 - Leka Leka (Madrid)</span>

                    </div>
                </div>

                <div class="video_box">
                    <h4 class="block-heading">Vocabulario de la semana - Thanksgiving</h4>
                    <div class="block-content">
                        <p>Reyes Magos...¡Papá Noel! Dia de todos los Santos...¡Halloween! Y esto de ¨Thanksgiving¨ ¿Qué és lo que és?  </p>
                        <iframe width="100%" height="257" src="http://www.youtube.com/embed/iVmR4wTAXVM" frameborder="0" allowfullscreen></iframe>
                        <span>Noviembre 2013 - En algún lugar de Madrid</span>

                    </div>
                </div>

                <div class="clearfix"></div>

                <div class="video_box">
                    <h4 class="block-heading">Vocabulario de la semana - GROSS</h4>
                    <div class="block-content">
                        <p>Aprende algunas nuevas palabras de vocabulario con Mingles de forma divertida.</p>
                        <iframe width="100%" height="257" src="http://www.youtube.com/embed/rLkeXDlMC5s?rel=0" frameborder="0" allowfullscreen></iframe>
                        <span>Noviembre 2013 - Lavapiés (Madrid)</span>

                    </div>
                </div>
            </div>
        </div>
    </div>