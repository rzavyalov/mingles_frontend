<div class="height30">
</div>
<!--<start top-section> -->
<div class="inner-content">
    <div class="title_bg">
        <div class="wrapper">Estos son nuestros Mingles - Premium Bars</div>
    </div>
    <div class="wrapper">
        <div class="container marketing">
            <div class="row">
                <div class="span4">
                    <div class="thumbnail">
                        <!-- <img data-src="js/holder/holder.js/300x200" alt="The Irish Rover">-->
                        <div class="img_inner fleft">
                            <img alt="The Irish Rover" src="<?=base_url()?>assets/images/irish1.jpg">
                        </div>
                        <div class="caption">
                            <address>
                                <strong>
                                    <h3><?=$this->lang->line('irish_rover_name')?></h3>
                                    Av. Brasil 7, Madrid</strong><br>
                            </address>
                            <p><?=$this->lang->line('irish_rover_description')?>
                            </p>
                            <p>
                                <a class="btn btn-primary" href="http://www.theirishrover.com" target="_blank">M&aacute;s Info</a><a
                                    class="btn btn-default" href="index.php#option">Ap&uacute;ntate</a>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="span4">
                    <div class="thumbnail">
                        <!-- <img data-src="js/holder/holder.js/300x200" alt="The Irish Rover">-->
                        <div class="img_inner fleft">
                            <img alt="Starbucks" src="<?=base_url()?>assets/images/starbucks.jpg">
                        </div>
                        <div class="caption">
                            <address>
                                <strong>
                                    <h3><?=$this->lang->line('starbucks_name')?></h3>
                                    Pedro Teixeira, 8, Madrid</strong><br>
                            </address>
                            <p><?=$this->lang->line('starbucks_description')?></p>
                            <p>
                                <a class="btn btn-primary" href="https://www.facebook.com/pages/Starbucks-Pedro-Teixeira/184290281602057" target="_blank">M&aacute;s Info</a><a
                                    class="btn btn-default" href="index.php#option">Ap&uacute;ntate</a>
                            </p>
                        </div>
                    </div>
                </div>
<div class="span4">
                    <div class="thumbnail">
                        <!-- <img data-src="js/holder/holder.js/300x200" alt="The Irish Rover">-->
                        <div class="img_inner fleft">
                            <img alt="Starbucks" src="<?=base_url()?>assets/images/prasad.jpg">
                        </div>
                        <div class="caption">
                            <address>
                                <strong>
                                    <h3><?=$this->lang->line('prasad_ocio_saludable_name')?></h3>
                                    Calle Guzman el Bueno, 14, Madrid</strong><br>
                            </address>
                            <p><?=$this->lang->line('prasad_ocio_saludable_description')?></p>
                            <p>
                                <a class="btn btn-primary" href="http://prasad-ociosaludable.com/" target="_blank">M&aacute;s Info</a><a
                                    class="btn btn-default" href="index.php#option">Ap&uacute;ntate</a>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="span4">
                    <div class="thumbnail">
                        <div class="img_inner fleft">
                            <img alt="Libreria Ocho y Medio Cafe" src="<?=base_url()?>assets/images/ochoymedio.jpg">
                        </div>
                        <div class="caption">
                            <address>
                                <strong>
                                    <h3><?=$this->lang->line('libreria_ocho_medio_cafe_name')?></h3>
                                    <a href="https://www.google.es/maps/preview#!data=!1m4!1m3!1d6075!2d-3.713486!3d40.424376!4m13!3m12!1m0!1m1!1sOcho+Y+Medio+Libros+de+Cine%2C+Madrid!3m8!1m3!1d1519!2d-3.713682!3d40.424543!3m2!1i1024!2i768!4f13.1"
                                       target="_blank">C\ Martin de los Heros 11</a>, Madrid</strong><br>
                            </address>
                            <p><?=$this->lang->line('libreria_ocho_medio_cafe_description')?></p>
                            <p>
                                <a class="btn btn-primary" href="http://www.ochoymedio.com/ochoymedio/quienes-somos"
                                   target="_blank">M&aacute;s Info</a><a class="btn btn-default" href="index.php#option">Ap&uacute;ntate</a>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="span4">
                    <div class="thumbnail">
                        <div class="img_inner fleft">
                            <img alt="Areia" src="<?=base_url()?>assets/images/areia.jpg">
                        </div>
                        <div class="caption">
                            <address>
                                <strong>
                                    <h3><?=$this->lang->line('areia_chillout_name')?></h3>
                                    C\ Hortaleza, 92, Madrid</strong><br>
                            </address>
                            <p><?=$this->lang->line('areia_chillout_description')?></p>
                            <br>
                            <p>
                                <a class="btn btn-primary" href="http://www.areiachillout.com/" target="_blank">M&aacute;s
                                    Info</a><a class="btn btn-default" href="index.php#option">Ap&uacute;ntate</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <br>
            <div class="row">
                <div class="span4">
                    <div class="thumbnail">
                        <div class="img_inner fleft">
                            <img alt="Leka Leka" src="<?=base_url()?>assets/images/leka_leka.jpg">
                        </div>
                        <div class="caption">
                            <address>
                                <strong>
                                    <h3><?=$this->lang->line('leka_leka_bar_name')?></h3>
                                    C\ San Bruno 3, Madrid</strong><br>
                            </address>
                            <p><?=$this->lang->line('leka_leka_bar_description')?></p>
                            <p>
                                <a class="btn btn-primary" href="https://www.facebook.com/LekaLekaBar" target="_blank">M&aacute;s Info</a><a class="btn btn-default" href="index.php#option">Ap&uacute;ntate</a>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="span4">
                    <div class="thumbnail">
                        <div class="img_inner fleft">
                            <img alt="Flash Flash" src="<?=base_url()?>assets/images/flashflash.jpg">
                        </div>
                        <div class="caption">
                            <address>
                                <strong>
                                    <h3><?=$this->lang->line('flash_flash_bar_name')?></h3>
                                    C\ Nu&ntilde;ez de Balboa 75, Madrid</strong><br>
                            </address>
                            <p><?=$this->lang->line('flash_flash_bar_description')?></p>
                            <br>
                            <p>
                                <a class="btn btn-primary" href="http://www.flashflashmadrid.com" target="_blank">M&aacute;s
                                    Info</a><a class="btn btn-default" href="index.php#option">Ap&uacute;ntate</a>
                            </p>
                        </div>
                    </div>
                </div>
                <!--<div class="span4">
                    <div class="thumbnail">
                        <div class="img_inner fleft">
                            <img alt="La Paca Bar" src="<?=base_url()?>assets/images/pub33.jpg">
                        </div>
                        <div class="caption">
                            <address>
                                <strong>
                                    <h3>The Thirty Three</h3>
                                    C/ Clara del Rey 33, Madrid</strong><br>
                            </address>
                            <p>
                                The Thirtythree est&aacute; ubicado en el coraz&oacute;n del barrio Prosperidad y es un lugar de ocio referente en nuestra ciudad. Cuenta con un amplio espacio distribuido en tres plantas. 
                                Se presenta como el lugar ideal para albergar al p&uacute;blico que desee disfrutar de un local singular en un contexto inigualable. The Pub 33 se une a la oferta de Mingles todos los 
                                jueves para practicar idiomas en un lugar preferencial.
                            </p>
                            <br>
                            <p>
                                <a class="btn btn-primary" href="http://www.thethirtythree.es/" target="_blank">M&aacute;s Info</a><a class="btn btn-default" href="index.php#option">Ap&uacute;ntate</a>
                            </p>
                        </div>
                    </div>
                </div>-->
                <div class="span4">
                    <div class="thumbnail">
                        <div class="img_inner fleft">
                            <img alt="La Bicicleta cafe" src="<?=base_url()?>assets/images/labicicleta.jpg">
                        </div>
                        <div class="caption">
                            <address>
                                <strong>
                                    <h3><?=$this->lang->line('la_bicicleta_cafe_name')?></h3>
                                    Plaza de San Ildefonso 9 (Malasa&ntilde;a), Madrid</strong><br>
                            </address>
                            <p><?=$this->lang->line('la_bicicleta_cafe_description')?></p>
                            <br>
                            <p>
                                <a class="btn btn-primary" href="http://www.labicicletacafe.com" target="_blank">M&aacute;s Info</a><a class="btn btn-default" href="index.php#option">Ap&uacute;ntate</a>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="span4">
                    <div class="thumbnail">
                        <div class="img_inner fleft">
                            <img alt="Ole Lola" src="<?=base_url()?>assets/images/olelola.jpg ">
                        </div>
                        <div class="caption">
                            <address>
                                <strong>
                                    <h3>Ol&eacute; Lola</h3>
                                    Calle San Mateo, 28, Madrid</strong><br>
                            </address>
                            <p>
                               Ol&eacute; Lola es un curioso maridaje de lo ca&ntilde;&iacute; con lo cosmopolita, la tradici&oacute;n con la modernidad y la elegancia, un bombardeo de est&iacute;mulos visuales, olfativos y auditivos que pretende reinventar el concepto tradicional de tasca. Adem&aacute;s es un espacio multifuncional, en continua renovaci&oacute;n, donde la decoraci&oacute;n juega un papel primordial, y en el que se puede disfrutar de una relajada sobremesa en torno a un caf&eacute;, vibrar con los mejores DJs del panorama nacional, celebrar un evento se&ntilde;alado, o participar en las diversas actividades que tienen lugar en Ol&eacute; Lola: monólogos, intercambios de idiomas, catas y degustaciones, etc. y todos los lunes las sesiones de Mingles
                            </p>
                            <br>
                            <p>
                                <a class="btn btn-primary" href="http://www.olelola.com" target="_blank">M&aacute;s Info</a><a class="btn btn-default" href="index.php#option">Ap&uacute;ntate</a>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="span4">
                    <div class="thumbnail">
                        <div class="img_inner fleft">
                            <img alt="Cups & Kids" src="<?=base_url()?>assets/images/cupsandkids.jpg">
                        </div>
                        <div class="caption">
                            <address>
                                <strong>
                                    <h3><?=$this->lang->line('caps_kids_name')?></h3>
                                    Calle Alameda, 18 (BARRIO DE LAS LETRAS), Madrid</strong><br>
                            </address>
                            <p><?=$this->lang->line('caps_kids_description')?></p>
                            <br>
                            <p>
                                <a class="btn btn-primary" href="http://cupsandkids.com/" target="_blank">M&aacute;s Info</a><a class="btn btn-default" href="index.php#option">Ap&uacute;ntate</a>
                            </p>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
</div>
