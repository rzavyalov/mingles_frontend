<!DOCTYPE html>
<html lang="es">
<head>
    <!--META-->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Jonathan Martinez">
    <meta name="reply-to" content="jon1baza@gmail.com">
    <meta name="Distribution" content="global"/>
    <meta name="description" content="Descripción de la página" />
    <meta name="Keywords" content="palabras clave separadas por comas"/>
    <META name="robots" content="NOINDEX,NOFOLLOW">
    <title>milingual</title>

    <!-- FUENTES -->
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,200,200italic,300,300italic,600,400italic,700,600italic,700italic,900,900italic' rel='stylesheet' type='text/css'>

    <!-- CSS -->
    <link rel="stylesheet" href="https://cdn.linearicons.com/free/1.0.0/icon-font.min.css">
    <link href="../../assets/css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../../assets/css/normalize.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/css/menu-hover.css" />
    <link href="../../assets/css/main.css" rel="stylesheet">

    <!--Datapicker-->
    <link href="../../assets/css/jquery-ui-1.10.1.css" rel="stylesheet">
    <link href="../../assets/css/melon.datepicker.css" rel="stylesheet">
</head>

<body>
<!-- ####### MENU PRINCIPAL######### -->
<div class="navbar-principal" role="navigation" id="navbar">
    <div class="container">
        <section class="color-7">
            <a href="#"><img src="../../assets/img/logo.png" alt="logo" id="logo-pc"></a>
            <nav class="cl-effect-21">
                <a href="#">Home</a>
                <a href="#">Bares</a>
                <a href="#">Blog</a>
                <a href="#">Colabora</a>
            </nav>
            <div id="menu2">
                <ul id="ul2">
                    <li id="liidioma"><a href="#" onclick="divlengua(); return false">Idioma <span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span></a></li>
                    <li id="licreditos"><a href="#" onclick="divcreditos(); return false">Créditos</a></li>
                    <li id="liapuntate"><a href="#" onclick="divapuntate(); return false">¡Apúntate!</a></li>
                </ul>
            </div>
        </section>
        <section class="mobile-menu">
            <a href="#"><img src="../../assets/img/logo.png" alt="logo" id="logo-pc"></a>
            <div id="menu2">
                <ul id="ul2">
                    <li id="liidioma"><a href="#" onclick="divlengua(); return false">Idioma <span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span></a></li>
                    <li id="liapuntate"><a href="#" onclick="divcreditos(); return false">Créditos</a></li>
                    <li id="lilogin"><a href="#" onclick="divapuntatem(); return false">¡Apuntate!</a></li>
                </ul>
            </div>
            <div id="btn-menu-responsive" class="btn-menu">
                <span></span>
            </div>

        </section>
    </div>
</div>

<!-- ####### CREDITOS ######### -->
<div class="navbar navbar-credito" id="navbar-credito">
        <div class="container">
            <section>
                <div id="txtcredito">
                    Ahora es mucho más sencillo pagar tus sesiones.<br>
                    <a href="#">Click aquí</a> para obtener más información.
                </div>
                <div id="txtcred">
                    30€
                </div>
                 <form method="post" action="" id="contadorcreditos">
                                <div class="numbers-row">
                                    <div class="inc button">+</div>
                                    <div class="dec button">-</div>
                                    <input type="text" name="creditos" id="creditos" value="1">
                                </div>
                                  <div class="buttons">
                                    <input type="submit" value="Comprar" id="submit-credito">
                       </div>
                  </form>
            </section>
        </div>
</div>

<!-- ####### APUNTA ######### -->
<div class="navbar-apunta" id="navbar-apunta">
    <div class="container">
        <section>
            <form id="apuntarse">
                  <input id="datepicker" placeholder="Fecha" />

                    <select id="espacio">
                        <optgroup label="Madrid">
                            <option value="espacio">Espacio</option>
                            <option value="ml1">Lugar1</option>
                            <option value="ml2">Lugar2</option>
                        </optgroup>
                        <optgroup label="Barcelona">
                            <option value="bl1">Lugar1</option>
                            <option value="bl2">Lugar2</option>
                        </optgroup>
                    </select>


                    <select id="nivel">
                        <option> Nivel/Precio </option>
                        <option> Precio1 </option>
                        <option> Precio2 </option>
                        <option> Precio3 </option>
                    </select>

                    <input type="text" name="fecha" value="Cod. promocional" id="codigopromo">
                    <input type="submit" value="Apuntarme" id="boton-apuntarme">
            </form>
        </section>
    </div>
 <!--  MAPA MENU OFF <section class="mp"> <iframe id="framemapa" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3036.3754594128054!2d-3.7028017999999885!3d40.444826199999994!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd4228f8750c1cdd%3A0xe88816b384b97c9!2sCalle+de+Sta+Engracia%2C+148%2C+28003+Madrid!5e0!3m2!1ses!2ses!4v1439411872171" width="100%" height="200%" frameborder="0" style="border:0" allowfullscreen></iframe> </section>  -->
</div>
<div class="navbar-apunta-m" id="navbar-apunta-m">
    <div class="container">
        <section>
            <div id="row menuapunta">
               <form id="apuntarse">
                  <input type="text" name="fecha" value="Fecha">
                  <div id="espacio">
                    <select>
                        <optgroup label="Madrid">
                            <option value="espacio">Espacio</option>
                            <option value="ml1">Lugar1</option>
                            <option value="ml2">Lugar2</option>
                        </optgroup>
                        <optgroup label="Barcelona">
                            <option value="bl1">Lugar1</option>
                            <option value="bl2">Lugar2</option>
                        </optgroup>
                    </select>
                </div>
                <div id="nivel">
                    <select>
                        <option> Nivel/Precio </option>
                        <option> Precio1 </option>
                        <option> Precio2 </option>
                        <option> Precio3 </option>
                    </select>
                </div>
                    <input type="text" name="fecha" value="Cod. promocional">
                    <input type="submit" value="Apuntarme" id="boton-apuntarme">
               </form>

            </div>

        </section>
    </div>
 <!--  MAPA MENU OFF <section class="mp"> <iframe id="framemapa" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3036.3754594128054!2d-3.7028017999999885!3d40.444826199999994!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd4228f8750c1cdd%3A0xe88816b384b97c9!2sCalle+de+Sta+Engracia%2C+148%2C+28003+Madrid!5e0!3m2!1ses!2ses!4v1439411872171" width="100%" height="200%" frameborder="0" style="border:0" allowfullscreen></iframe> </section>  -->
</div>

<!-- ####### CONTENEDOR PRINCIPAL (IMG) ######### -->
<div id="all-principal">
    <div id="intro" class="clearfix">
        <div class="item">
            <div class="container">
                <div class="row filalog">
                    <div class="col-md-3 col-md-push-9" id="languajes">
                        <ul id="otrosidiomas">
                            <li>Español</li>
                            <li>English</li>
                            <li>Deutsch</li>
                            <li>Portugues</li>
                        </ul>
                    </div>
                </div>
                <div class="row heiglog">
                    <!--Icono mouse scroll-->
                    <div class='icon-scroll'>
                        <div class="icon-scroll-screen"></div>
                    </div>
                    <div class="col-md-5" id="texto-portada">
                        <!--Texto home-->
                        <div id="changetxt">
                            Change the way<br>you practise languages<br><br>
                            <!--Botones-->
                            <div id="funcionamiento">
                                ¿Cómo funciona?
                            </div>
                            <div id="apuntate">
                                <a href="#" onclick="divapuntate(); return false">Apúntate</a>
                            </div>
                        </div>
                    </div>
                    <!--Pestañas login-->
                    <div class="col-md-4" id="logyreg">
                        <div class="tabs">
                            <!--pestaña 1 activa por defecto-->
                            <div class="tab">
                                <input type="radio" id="tab-1" name="tab-group-1" checked>
                                <label for="tab-1">Login</label>
                                <!--contenido de la pestaña 1-->
                                <div class="content">
                                    <p>
                                        <form action=" " id="form-pestaña-log">
                                            <input type="text" name="nombre" value="nombre" placeholder="Usuario"><br>
                                            <input type="password" name="pass" value="pass" placeholder="Contraseña"><br>
                                            <input type="checkbox" name="recordar" value="recordar"><span id="recor">Recordar contraseña</span><br>
                                            <input type="submit" value="Entrar"> <br>
                                            <a href="#">He olvidado mi contraseña</a> <br>
                                            <hr>
                                            <div id="face">Facebook</div><div id="gmail">GMail</div>


                                        </form>
                                    </p>
                                </div>
                            </div>
                            <!-- pestaña 2-->
                            <div class="tab">
                                <input type="radio" id="tab-2" name="tab-group-1">
                                <label for="tab-2">Registrate</label>
                                <div class="content">
                                    <!--contenido de la pestaña 2-->
                                    <p>
                                        <form action=" " id="form-pestaña-reg">
                                            <input type="text" name="nombre" value="nombre" placeholder="Usuario" id="user-reg"><br>
                                            <input type="text" name="email" value="email" placeholder="Email" id="email-reg"><br>
                                            <input type="password" name="pass" value="pass" placeholder="Contraseña" id="pass-reg"><br>
                                            <input type="password" name="pass2" value="pass2" placeholder="Repetir Contraseña" id="pass2-reg"><br>
                                            <input id="checkacep" type="checkbox" name="recordar" value="recordar"><span id="acept">Acepto las condiciones y Política de Privacidad</span><br>
                                            <input type="submit" value="Registrar" id="btnreg">



                                        </form>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- ####### LETS TALK ######### -->
<div id="lets-talk">
    Welcome to Milingual<br>
    <b>Let's talk!</b>
</div>

<!-- ####### QUE Y COMO ES MILINGUAL ######### -->
<div id="queycomo">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <img src="../../assets/img/profesional.png" alt="profesional" id="profesional">
                <div id="tituloprofesional">Profesional</div>
                <div id="txtprofesional">
                    Mingles es liderado por profesores nativos. El<br>
                    profesor corregirá a nuestros alumnos y<br>
                    presentará nuevo vocabulario y expresiones.<br>
                    Todo de forma muy amena y divertida.
                </div>
            </div>
            <div class="col-md-3 col-md-push-1">
                <img src="../../assets/img/flexible.png" alt="flexible" id="flexible">
                <div id="tituloflexible">Flexible</div>
                <div id="txtflexible">
                    ¡Cuando tú quieras! Apúntate a tus grupos de<br>
                    conversación reducidos (máximo cinco<br>
                    personas) por medio de nuestro calendario. Los<br>
                    alumnos y el profesor quedarán para disfrutar<br>
                    de su grupo de conversación.
                </div>
            </div>
            <div class="col-md-3 col-md-push-1">
                <img src="../../assets/img/social.png" alt="social" id="social">
                <div id="titulosocial">Social</div>
                <div id="txtsocial">
                    Los locales Mingles nos ofrecen una zona<br>
                    exclusiva para charlar cómodamente y las<br>
                    mejores consumiciones. Además, los grupos de<br>
                    conversacion giran alrededor de nuestros topics,<br>
                    temas interesantes.
                </div>
            </div>
        </div>
    </div>
</div>


    <!-- JAVASCRIPT-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="../../assets/js/bootstrap.js"></script>
    <script src="../../assets/js/modernizr.custom.js"></script>
    <script src="../../assets/js/main.js"></script>
   <script src="../../assets/js/jquery-ui-1.10.1.min.js"></script>
    <script>
        $(function() {
            $( "#datepicker" ).datepicker({
                inline: true,
                showOtherMonths: true
            })
            .datepicker('widget').wrap('<div class="ll-skin-melon"/>');
        });
    </script>
    <script>
$(document).ready(function() {
    $("#datepicker").datepicker(); });
</script>
</body>

</html>
