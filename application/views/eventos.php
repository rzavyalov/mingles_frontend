<div class="height30">
</div>
<!--<start top-section> -->
<div class="inner-content">
<div class="title_bg">
    <div class="wrapper">Eventos</div>
</div>
<div class="wrapper">
<div class="container marketing">
    <div class="row1">
    <div class="span5">
        <h2 class="block-title">
            <span>COOKING SUSHI IN ENGLISH (SATURDAY 7th MARCH)</span></h2>

        <div class="block-img">
            <a href="<?=base_url()?>assets/images/sushi.jpeg" target="_blank">
                <img src="<?=base_url()?>assets/images/sushi.jpeg" width="336" height="448" align="center" />
            </a>
        </div>
        <p>&nbsp;

        </p>
    </div>
    <!-- /.span6 -->
    <div class="span7">
        <h3 style="color: #FFFFFF;">&iquest;En qu&eacute; consiste?</h3>
        <p>
            El s&aacute;bado 7 de marzo vamos a aprender a preparar Sushi en ingl&eacute;s. Pasaremos un rato divertido
            con nuestra chef y profesora de ingl&eacute;s Aviva (NY) y luego nos comeremos nuestros platos. Todo mientras
            practicas tu ingl&eacute;s.
        </p>
        <h3 style="color: #FFFFFF;">&iquest;Cu&aacute;nto cuesta?</h3>
        <p>
            El precio es de 15 EUR con tu comida y consumici&oacute;n incluida
        </p>
        <h3>&iquest;Cu&aacute;ndo?</h3>
        <p>
            Sábado 7 de marzo de 17:00 a 19:00
        </p>
        <h3>&iquest;D&oacute;nde?</h3>
        <p>
            <a href="http://www.onnekas.es/" target="_blank" style="color: black; text-decoration: underline">Onnekas Moncloa</a>
        </p>
        <p>
            	Calle Andrés Mellado, 80, Madrid
        </p>
        <h3>&iquest;C&oacute;mo me apunto?</h3>
        <p>
            Ap&uacute;ntate en nuestro <a href="http://www.mingles.es/cooking" style="color: black; text-decoration: underline">calendario </a> , selecciona el s&aacute;bado 7 y realiza tu pago online.
        </p>
        <br>
        <br>
    </div>
    <!-- /.span4 -->
    <div class="clear"></div>
</div>
<!-- /.row -->
<div class="row1">
    <div class="span5">
        <h2 class="block-title">
            <span>COOKING WITH MINGLES (SATURDAY 31st JANUARY)</span></h2>

        <div class="block-img">
            <a href="<?=base_url()?>assets/images/peppers.jpg" target="_blank">
                <img src="<?=base_url()?>assets/images/peppers.jpg" width="336" height="448" align="center" />
            </a>
        </div>
        <p>&nbsp;

        </p>
    </div>
    <!-- /.span6 -->
    <div class="span7">
        <h3 style="color: #FFFFFF;">&iquest;En qu&eacute; consiste?</h3>
        <p>
            El s&aacute;bado 31 de enero organizamos una clases de cocina en ingl&eacute;s. Preparemos una deliciosa
            receta acompa&ntilde;ada del aut&eacute;ntico t&eacute; chai y luego la disfrutar&aacute;s mientr&aacute;s
            practicas tu ingl&eacute;s.
        </p>
        <h3 style="color: #FFFFFF;">&iquest;Cu&aacute;nto cuesta?</h3>
        <p>
            El precio es de 19 EUR con tu comida y consumici&oacute;n incluida
        </p>
        <h3>&iquest;Cu&aacute;ndo?</h3>
        <p>
            Sábado 31 de 14:00 a 16:00
        </p>
        <h3>&iquest;D&oacute;nde?</h3>
        <p>
            <a href="http://www.blackpepperco.com/" target="_blank" style="color: black; text-decoration: underline">Black Pepper & Co.</a>
        </p>
        <p>
            	Calle San Vicente Ferrer, 10, 28004 Madrid
        </p>
        <h3>&iquest;C&oacute;mo me apunto?</h3>
        <p>
            Ap&uacute;ntate en nuestro <a href="http://www.mingles.es/cooking" style="color: black; text-decoration: underline">calendario </a> , selecciona el s&aacute;bado 31 y realiza tu pago online.
        </p>
        <br>
        <br>
    </div>
    <!-- /.span4 -->
    <div class="clear"></div>
</div>
<!-- /.row -->
<div class="row1">
    <div class="span5">
        <h2 class="block-title">
            <span>MINGLES FOR PARENTS & KIDS (EVERY FRIDAY)</span></h2>

        <div class="block-img">
        <img src="<?=base_url()?>assets/images/cupsandkids.jpg" width="336" height="448" align="center" />
        </div>
        <p>&nbsp;

        </p>
    </div>
    <!-- /.span6 -->
    <div class="span7">
        <h3 style="color: #FFFFFF;">&iquest;En qu&eacute; consiste?</h3>
        <p>
            Cups & Kids se une a Mingles para ofrecer una forma muy divertida de practicar ingl&eacute;s tanto para los
            mayores como para los m&aacute;s peque&ntilde;os. En el coraz&oacute;n del barrio de las Letras, organizamos
            todos los viernes, grupos para adultos y para sus peques en las instalaciones de Cups&Kids.
        </p>
        <h3 style="color: #FFFFFF;">&iquest;Cu&aacute;nto cuesta?</h3>
        <p>
            Sesi&oacute;n adulto 10 EUR con una consumici&oacute;n incluida
        </p>
        <p>
            Sesi&oacute;n ni&ntilde;o 10 EUR con su merienda incluida
        </p>
        <p>
            Sesi&oacute;n adulto + ni&ntilde;o 18 EUR con su consumiciones
        </p>
        <h3>&iquest;Cu&aacute;ndo?</h3>
        <p>
            Todos los viernes a las 17:30 a las 18:30
        </p>
        <h3>&iquest;D&oacute;nde?</h3>
        <p>
          <a href="http://cupsandkids.com" target="_blank" style="color: black; text-decoration: underline">Cups & Kids</a>
        </p>
        <p>
            	Calle Alameda, 18, 28014 Madrid
        </p>
        <h3>&iquest;C&oacute;mo me apunto?</h3>
        <p>
            Ap&uacute;ntate en nuestro <a href="http://www.mingles.es/kids" style="color: black; text-decoration: underline">calendario </a> , selecciona el viernes que mejor te venga y tu lugar Mingles: "Cups&Kids" y la modalidad que m&aacute;s te guste.
        </p>
        <br>
        <br>
    </div>
    <!-- /.span4 -->
    <div class="clear"></div>
</div>
<!-- /.row -->
<div class="row1">
    <div class="span5">
        <h2 class="block-title">
            <span>MINGLES FOR ENTREPRENEURS (MARTES 01/07)</span></h2>

        <div class="block-img">
        <img src="<?=base_url()?>assets/images/flyer_emp2.jpg" width="336" height="448" align="center" />
        </div>
        <p>&nbsp;

        </p>
    </div>
    <!-- /.span6 -->
    <div class="span7">
        <h3 style="color: #FFFFFF;">&iquest;En qu&eacute; consiste?</h3>
        <p>
            Mingles celebra junto con AJE un evento especial para emprendedores. Organizamos grupos reducidos de máximo 5 personas liderados por un profesor nativo. Aprenderás todo el vocabulario de los emprendedores, las expresiones de los empresarios, el lenguaje específico para nuestro pitch y mucho más en la sesión de Mingles for entrepreneurs.
        </p>
        <h3 style="color: #FFFFFF;">&iquest;Cu&aacute;nto cuesta?</h3>
        <p>
            Este evento es gratuito e incluye una consumici&oacute;n.
        </p>
        <h3>&iquest;Cu&aacute;ndo?</h3>
        <p>
            Martes 1 de Julio
        </p>
        <h3>&iquest;D&oacute;nde?</h3>
        <p>
            AJE <a href="http://www.ajemadrid.es/" target="_blank" style="color: black; text-decoration: underline">garAJE Madrid</a>
        </p>
        <p>
            Calle de Matilde Diez, 11, Madrid
        </p>
        <h3>&iquest;C&oacute;mo me apunto?</h3>
        <p>
            Ap&uacute;ntate en nuestro <a href="http://www.mingles.es/#option" style="color: black; text-decoration: underline">calendario </a> , selecciona la fecha "1 de Julio" y tu lugar Mingles: "garAJE"
        </p>
        <br>
        <br>
    </div>
    <!-- /.span4 -->
    <div class="clear"></div>
</div>
<!-- /.row -->
<div class="row1">
    <div class="span5">
        <h2 class="block-title">
            <span>MAGIC MINGLES (MARTES 01/04)</span></h2>
        <div class="block-img">
            <img src="<?=base_url()?>assets/images/mingles_magia.jpg" width="336" height="448" align="center" />
        </div>
    </div>
    <!-- /.span6 -->
    <div class="span7">

        <h3>¿En qu&eacute; consiste?</h3>
        <p>
            Mingles celebra junto con la Hermandad de Magos de Madrid un evento mágico. Junto a tu profesor nativo tendrás un mago de alumno que te sorprenderá con sus trucos.
        </p>
        <h3 style="color: #FFFFFF;">&iquest;Cu&aacute;nto cuesta?</h3>
        <p>
            El precio es de 10 EUR con una consumici&oacute;n.
        </p>
        <h3 style="color: #FFFFFF;">&iquest;Cu&aacute;ndo?</h3>
        <p>
            Martes 1 de Abril
        </p>
        <h3 style="color: #FFFFFF;">&iquest;D&oacute;nde?</h3>
        <p>
            The Irish Rover <a href="http://www.theirishrover.com/" style="color: black; text-decoration: underline">The Irish Rover</a>
        </p>
        <p>
            Av. Brasil, 7, Madrid
        </p>
        <h3 style="color: #FFFFFF;">&iquest;C&oacute;mo me apunto?</h3>
        <p>
            Apúntate en nuestro <a href="http://www.mingles.es/#options" style="color: black; text-decoration: underline">calendario</a>
        </p>
        <br>
        <br>
    </div>
    <!-- /.span4 -->
    <div class="clear"></div>
</div>
<!-- /.row -->

<div class="row1">
    <div class="span5">
        <h2 class="block-title">
            <span>MINGLES DÍA DEL TEATRO</span></h2>
        <div class="block-img">
            <img src="<?=base_url()?>assets/images/areiateatro.jpg" />
        </div>
        <p></p>
    </div>
    <!-- /.span6 -->
    <div class="span7">

        <h3 style="color: #FFFFFF;">¿En qu&eacute; consiste?</h3>
        <p>
            Este jueves 27 de marzo a las 20:30 tendremos un Mingles especial para celebrar el d&iacute;a internacional del teatro. Habr&aacute; un mini pase de teatro en vivo que te sosprender&aacute; en el Areia Chillout además de una tem&aacute;tica relacionada.
        </p>
        <h3 style="color: #FFFFFF;">¿Cu&aacute;nto cuesta?</h3>
        <p>
            El precio es el de siempre, 10 EUR con una consumici&oacute;n. Recuerda que tambi&eacute;n tenemos bonos disponibles
        </p>
        <h3 style="color: #FFFFFF;">¿D&oacute;nde?</h3>
        <p>
            En uno de los locales más cool de Madrid <a href="http://www.areiachillout.com/"
                                                        style="color: black; text-decoration: underline">Areia Chillout</a>
        </p>
        <p>
            Calle Hortaleza, 92, Madrid
        </p>
        <h3 style="color: #FFFFFF;">¿C&oacute;mo me apunto?</h3>
        <p>
            Apúntate en nuestro <a href="http://www.mingles.es/#options" style="color: black; text-decoration: underline">calendario</a>
        </p>
        <h3 style="color: #FFFFFF;">¿Cu&aacute;ndo?</h3>
        <p>
            Jueves 27 a las 20:30
        </p>
    </div>
    <!-- /.span4 -->
    <div class="clear"></div>
</div>
<!-- /.row -->

<div class="row1">
    <div class="span5">
        <h2 class="block-title">
            <span>MULTIMINGLES (todos los martes)</span></h2>
        <div class="block-img">
            <img src="<?=base_url()?>assets/images/multimingles.jpg" width="336" height="448" align="center" />
        </div>

    </div>
    <!-- /.span6 -->
    <div class="span7">
        <h3>¿En qu&eacute; consiste?</h3>
        <p>La primera hora podrás disfrutar de un intercambio de ingles & networking con un profesor nativo animando la conversación. La segunda hora es nuestro Mingles, una hora en grupos reducidos (max. 5 personas) hablando sobre un topic divertido con un profesor nativo.</p>
        <h3>¿Cuanto cuesta?</h3>
        <p>La primera hora es gratuita y si te apetece quedarte a tu Mingles son 10 EUR con una consumici&oacute;n</p>
        <h3>¿Dónde?</h3>
        <p>En el pub irland&eacute;s referencia de Madrid  <a href="http://www.theirishrover.com/"                                                                          style="color: black; text-decoration: underline">The Irish Rover</a>                    </p>
        <h4>Av. Brasil, 7, Madrid </h4>
        <h3>¿Cómo me apunto?</h3>
        <p>Ap&uacute;ntate en nuestro <a href="http://www.mingles.es/#options" style="color: black; text-decoration: underline">calendario</a></p>


    </div>
    <!-- /.span4 -->
    <div class="clear"></div>
</div>
<!-- /.row -->


<div class="row1">
    <div class="span5">
        <h2 class="block-title"><span>LOS OSCARS llegan a Mingles (lunes 03/03)</span></h2>
        <div class="block-img">
            <img src="<?=base_url()?>assets/images/oscars.jpg" align="center" />

        </div>
        <!-- /.span6 -->
        <div class="span7">
            <h3>¿En qu&eacute; consiste?</h3>
            <p>
                Mingles celebra junto Ocho y Medio Librer&iacute;a de cine el evento de los Oscars. Podr&aacute;s participar en el Quizz de los Oscars junto a tu grupo Mingles liderado por un profesor nativo.
            </p>
            <h3>¿Cu&aacute;nto cuesta?</h3>
            <p>
                Este es un Mingles especial y el precio es de 15 EUR con una consumici&oacute;n + un picoteo de comida americana. Si pagas por adelantado tendr&aacute;s una copa de Cava en la recepción.
            </p>
            <h3>¿Cu&aacute;ndo?</h3>
            <p>Lunes 3 de Marzo</p>
            <h3>¿Dónde?</h3>
            <p>
                En Ocho y Medio Librería Café  <a href="http://www.ochoymedio.com/" style="color: black; text-decoration: underline">Ocho y Medio Libreria de Cine</a>
            </p>
            <p>
                Calle Martin de los Heros, 11, Madrid
            </p>
            <h3>¿Cómo me apunto?</h3>
            <p>
                Apúntate en nuestro <a href="http://www.mingles.es/#options" style="color: black; text-decoration: underline">calendario</a>
            </p>
        </div>
        <!-- /.span4 -->
        <div class="clear"></div>
    </div>
    <!-- /.row -->
</div>

<!-- Three columns of text below the carousel -->
<div class="row1">
    <div class="span5">
        <h2 class="block-title">
            <span>AREIA - SEXY VALENTIN (12 - Feb)</span></h2>
        <div class="block-img">
            <img src="<?=base_url()?>assets/images/areiaevento3.jpg" width="336" height="448" align="center" />
        </div>
    </div>
    <!-- /.span6 -->
    <div class="span7">
        <h3>¿En qué consiste?</h3>
        <p>
            Ven a celebrar Sexy Valentín con Mingles! Se organizar&aacute;n grupos reducidos de m&aacute;ximo cinco personas liderados por un profesor nativo y conversando sobre una tem&aacute;tica divertida.
        </p>
        <h3 style="color: #FFFFFF;">¿Cu&aacute;nto cuesta?</h3>
        <p>
            Este es un evento Mingles especial y el precio es de 15 EUR con tu consumici&oacute;n y dos tapas especiales de degustaci&oacute;n.
        </p>
        <h3 style="color: #FFFFFF;">¿D&oacute;nde?</h3>
        <p>
            En el pub Areia Chillout <a href="http://www.areiachillout.com/" style="color: black; text-decoration: underline">Areia ChillOut</a>
        </p>
        <p>
            Calle Hortaleza, 92, Madrid
        </p>
        <h3 style="color: #FFFFFF;">¿C&oacute;mo me apunto?</h3>
        <p>
            Apúntate en nuestro <a href="http://www.mingles.es/#options" style="color: black; text-decoration: underline">calendario</a>
        </p>
    </div>
    <!-- /.span4 -->
    <div class="clear"></div>
</div>
<!-- /.row -->



<div class="row1">
    <div class="span5">
        <h2 class="block-title">
            <span>EVENTO SOLIDARIO MINGLES (dia 18 Dic. a las 20:30)</span></h2>
        <div class="block-img">
            <img src="<?=base_url()?>assets/images/diapo10.png" width="450" height="640" />
        </div>
    </div>
    <!-- /.span6 -->
    <div class="span7">
        <br>
        <h3 style="color: #FFFFFF;">¿En qu&eacute; consiste?</h3>
        <p>
            Mingles organiza un evento solidario en el Turkana Bar, el primer restaurante solidario de Madrid. Presentaremos nuestro calendario solidario donde todos los fondos recaudados ir&aacute;n a CC ONG para un proyecto educativo en África. Se organizan grupos reducidos de máximo cuatro personas con un profesor nativo por mesa.
        </p>
        <br>
        <h3 style="color: #FFFFFF;">¿Cu&aacute;nto cuesta?</h3>
        <p>
            Este es un evento Mingles especial y el precio es de 15 EUR incluyendo tu clase Mingles, una consumición y tu cena.
        </p>
        <br>
        <h3 style="color: #FFFFFF;">¿D&oacute;nde?</h3>
        <p>
            En el bar <a href="http://www.turkanabar.es" style="color: black; text-decoration: underline">Turkana Bar</a>
        </p>
        <p>
            Calle Segovia 16, Madrid (La Latina)
        </p>
        <br>
        <h3 style="color: #FFFFFF;">¿C&oacute;mo me apunto?</h3>
        <p>
            Para reservar tu plaza ap&uacute;ntate en nuestro <a href="http://www.mingles.es/#options" style="color: black; text-decoration: underline">calendario</a>  y recibir&aacute;s tu confirmaci&oacute;n.
        </p>
        <br>
    </div>
    <!-- /.span4 -->
    <div class="clear"></div>
</div>

<div class="row1">
    <div class="span5">
        <h2 class="block-title">
            <span>MINGLES WITH INDIAN FOOD</span></h2>
        <div class="block-img">
            <img src="<?=base_url()?>assets/images/diapo6.jpg" width="336" height="448" />
        </div>
    </div>
    <!-- /.span6 -->
    <div class="span7">

        <h3 style="color: #FFFFFF;">¿En qu&eacute; consiste?</h3>
        <p>
            Grupos creados según tu nivel de ingl&eacute;s de hasta cinco personas con un profesor nativo. Disfrutando de una cata de comida india con nuestro topic preparado sobre la India. Mejorar&aacute;s tu ingl&eacute;s de forma amena y mucho m&aacute;s.
        </p>
        <br>
        <h3 style="color: #FFFFFF;">¿Cu&aacute;nto cuesta?</h3>
        <p>
            Este es un evento Mingles especial y el precio es de 15 EUR incluyendo tu clase Mingles, una consumici&oacute;n y una degustaci&oacute;n de comida India
        </p>
        <br>
        <h3 style="color: #FFFFFF;">¿D&oacute;nde?</h3>
        <p>
            En uno de los locales Chillout mas chic de Madrid: <a href="http://www.areiachillout.com/"
                                                                  style="color: black; text-decoration: underline">Areia</a>
        </p>
        <p>
            Calle Hortaleza, 92, Madrid
        </p>
        <br>
        <h3 style="color: #FFFFFF;">¿C&oacute;mo me apunto?</h3>
        <p>
            Para reservar tu plaza ap&uacute;ntate en nuestro  <a href="index.html#options" style="color: black; text-decoration: underline">calendario</a>  y te enviaremos los detalles de pago
        </p>
    </div>
    <!-- /.span4 -->
    <div class="clear"></div>
</div>



</div>
</div>
