<html>
<head>
    <meta http-equiv="refresh" content="3; url=<?=base_url()?>">
    <title>Payment Done Successfully</title>
    <style>.as_wrapper{
            font-family:Arial;
            color:#333;
            font-size:14px;
            padding:20px;
            border:2px dashed #17A3F7;
            width:600px;
            margin:0 auto;
        }</style>
</head>
<body>
<div class="as_wrapper">
    <h1>Has adquirido tu bono Mingles con &eacute;xito</h1>
    <h4>El cr&eacute;dito comprado ha quedado asociado a tu usuario (email). Accede
    a tu perfil para consultar los detalles. Consulta tu mail sino tienes tu password o cont&aacute;ctanos en
    <a href="mailto:info@mingles.es?subject='Contacto Web'">info@mingles.es</a> en caso de que tengas dudas.</h4>
    <h4>Use this below URL in paypal sandbox IPN Handler URL to complete the transaction</h4>
    <h3><?=base_url('paypal/bono_ipn')?></h3>
</div>
<p align="center"><img src="<?=base_url()?>assets/images/logo.png" alt=""></p>
</body>
</html>
