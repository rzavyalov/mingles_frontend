<div class="height30">
</div>
<!--<start top-section> -->
<div class="inner-content">
    <div class="title_bg">
        <div class="wrapper">Partners</div>
    </div>
    <div class="wrapper">
        <div class="container marketing">

            <div class="patner-title">
                <h2><em>Coworking & Entrepreneurship</em> <span></span></h2>
            </div>

            <div class="patner">
                <div class="row">
                    <ul>
                        <li><a href="http://www.aje.es">
                                <img src="<?=base_url()?>assets/images/partner5.png" alt="Logo AJE"></a></li>


                        <li><a href="http://www.ie.edu">
                                <img src="<?=base_url()?>assets/images/partner6.png" alt="Logo Area31"></a></li>

                        <li><a href="http://zincshower.com/category/showers-2014/">
                                <img src="<?=base_url()?>assets/images/partner9.png" alt="Logo ZicShower"></a></li>

                        <li><a href="http://www.diadeinternet.org/2014/?page=emp_index">
                                <img src="<?=base_url()?>assets/images/partner10.png" alt="Logo Dia de Internet"></a></li>
                    </ul>
                </div>

            </div>

            <div class="clear"></div>

            <div class="patner-title">
                <h2><em>Escuelas de Negocio y Universidades</em> <span></span></h2>
            </div>

            <div class="patner">
                <div class="row">
                    <ul>
                        <li><a href="http://www.ie.edu">
                                <img src="<?=base_url()?>assets/images/partner1.png" alt="Logo IE"></a></li>

                        <li><a href="http://www.upm.es">
                                <img src="<?=base_url()?>assets/images/partner3.png" alt="Logo UPM"></a></li>


                        <li><a href="http://www.eae.es">
                                <img src="<?=base_url()?>assets/images/partner2.png" alt="Logo EAE"></a></li>
                        <li><a href="http://www.exfera.org/">
                                <img src="<?=base_url()?>assets/images/partner4.png" alt="Logo Exfera"></a></li>
                    </ul>
                </div>
            </div>
            <div class="patner-title">
                <h2><em>Consumo Colaborativo</em> <span></span></h2>
            </div>

            <div class="patner">
                <div class="row">
                    <ul>
                        <li><a href="http://www.peers.com">
                                <img src="<?=base_url()?>assets/images/partner7.png" alt="Logo Peers"></a></li>
                        <li><a href="http://www.sala-mandra.es">
                                <img src="<?=base_url()?>assets/images/partner8.png" alt="Logo Salamandra"></a></li>
                    </ul>

                </div>

            </div>
        </div>
    </div>
</div>