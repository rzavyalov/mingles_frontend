<div class="inner-content">
    <div class="title_bg1">
        <div class="wrapper">Manage Level</div>
    </div>
    <div class="wrapper1">
        <p><div class="add-new-button">  <p><a href="#" data-url="<?=base_url('admin/managelevel/add')?>" class="editlevelmodal" title="Add New Level">Add New Level</a></p></div>
        <div class="clear"></div>
        </p>


        <table border='0' cellpadding='0' class='tablefrom' >
            <tr>
                <th>Level ID</th>
                <th>Level Name</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>

        <?php foreach ($levels as $level) : ?>
            <tr>
            <td><?=$level['LevelID']?></td>
            <td><?=$level['LevelName']?></td>
            <td><a href="#" data-id="<?=$level['LevelID']?>" data-url="<?=base_url('admin/managelevel/edit/'.$level['LevelID'])?>" class="editlevelmodal" title="Edit Level"> <img src="<?=base_url()?>assets/images/edit.png" style="height:24px;"></a></td>
            <td><a class="deletelevel" href="#" data-url="<?=base_url('admin/managelevel/delete/'.$level['LevelID'])?>" title="Delete Level"> <img src="<?=base_url()?>assets/images/delete.png" style="height:24px;" ></a></td>
            </tr>
        <?php endforeach; ?>
        </table>


        </div>
    </div>
<script>
    $('.editlevelmodal').click(function() {
        $.pgwModal({
            url: $(this).data('url'),
            loadingContent: '<span style="text-align:center">Loading in progress</span>',
            title: $(this).attr('title'),
            closable: true,
            titleBar: false
        });
    });

    $('.deletelevel').click(function(){
        var title_msg = $(this).attr('title');
        var url = $(this).data('url');
        var obj = $(this).parent().parent();
        dhtmlx.message({
            type:"confirm",
            text: "Do you want to delete the level?",
            title: title_msg,
            callback: function(e) {
                if(e)
                {
                    $.ajax({
                        url: url
                    }).done(function(e) {
                        obj.remove();
                    });
                }
            }
        });
    })
</script>
</body>
</html>