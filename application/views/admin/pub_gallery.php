<script src="<?=base_url('/assets/js/jquery.limit-1.2.source.js')?>"></script>
<script type='text/javascript'>


</script>

<style>
    .multiselect {
        width: 200px;
    }
    .selectBox {
        position: relative;
    }
    .selectBox select {
        width: 100%;
        font-weight: bold;
    }
    .overSelect {
        position: absolute;
        left: 0; right: 0; top: 0; bottom: 0;
    }
    #languages {
        display: none;
        border: 1px #dadada solid;
    }
    #languages label {
        display: block;
    }
    #languages label:hover {
        background-color: #1e90ff;
    }

    .rTable {
        display: block;
        width: 100%;
    }
    .rTableHeading, .rTableBody, .rTableFoot, .rTableRow{
        clear: both;
    }
    .rTableHead, .rTableFoot{
        background-color: #DDD;
        font-weight: bold;
    }
    .rTableCell, .rTableHead {
        border: 1px solid #999999;
        float: left;
        height: 17px;
        overflow: hidden;
        padding: 3px 1.8%;
        width: 28%;
    }
    .rTable:after {
        visibility: hidden;
        display: block;
        font-size: 0;
        content: " ";
        clear: both;
        height: 0;
    }
</style>

<div class="inner-content">
    <div class="title_bgd">
        <div class="wrapper">Bienvenido a tu panel Mingles</div>
    </div>

    <?php $this->load->view('admin/components/pubDashBoardLeft');?>

    <div class="profile-right">

    <h1>Gallery </h1>

        <form action="<?= base_url('admin/profile/update_pub_details') ?>" data-remote="true" accept-charset="UTF-8" method="POST">
            <input type="hidden" name="user_id" value="<?= $pub['user_id'] ?>"/>

        </form>
        <?php foreach($images as $img) : ?>

    <img src = "<?=base_url('admin/profile/getobj/'.$img['photo_url'])?>" >
    <?php endforeach; ?>

        <h2>Phone numbers</h2>
        <div class="rTable">
            <div class="rTableRow">
                <div class="rTableHead"><strong>Name</strong></div>
                <div class="rTableHead"><span style="font-weight: bold;">Telephone</span></div>
                <div class="rTableHead">&nbsp;</div>
            </div>
            <div class="rTableRow">
                <div class="rTableCell">John</div>
                <div class="rTableCell"><a href="tel:0123456785">0123 456 785</a></div>
                <div class="rTableCell"><img src="images/check.gif" alt="checked" /></div>
            </div>
            <div class="rTableRow">
                <div class="rTableCell">Cassie</div>
                <div class="rTableCell"><a href="tel:9876532432">9876 532 432</a></div>
                <div class="rTableCell"><img src="images/check.gif" alt="checked" /></div>
            </div>
        </div>
    </div>
</div>

</body>
