<div class="inner-content">
    <div class="title_bgd">
        <div class="wrapper">Bienvenido a tu panel Mingles</div>
    </div>

    <?php if($this->session->userdata('usertype_id') == 4) :?>
    <?php $this->load->view('admin/components/admin_teacherDashBoardLeft'); ?>
    <?php elseif($this->session->userdata('usertype_id') == 2): ?>
    <?php $this->load->view('admin/components/teacherDashBoardLeft'); ?>
    <?php endif?>
    <div class="profile-right">

        <h1>My Mingles</h1>
            <p>Click on the view icon to check the students of your session</p><br>
            <p>After your session:</p>
                <p>- Change the status of the students</p>
                <p>- Click on the student's picture in order to give them feedback</p>
                <p>- Remember also to confirm their level</p><br>
            </ul>
        <h1>Details of my Classes</h1>

        <table border='0' cellpadding='0' class='tablefrom'>
            <tr>
                <th>Group Name</th>
                <th>Class Name</th>
                <th>Class Date</th>
                <th>Class Time</th>
                <th>Pub Name</th>
                <th>City</th>
                <th>Language</th>
                <th>View</th>
            </tr>
        <?php foreach ($groups as $group) : ?>

                <tr>
                    <td><?= $group['GroupName'] ?></td>
                    <td><?= $group['ClassName'] ?></td>
                    <td><?= $group['ClassDate'] ?></td>
                    <td><?= $group['ClassStartTime'] . '-' . $group['ClassEndTime'] ?></td>
                    <td><?= $group['PubName'] ?></td>
                    <td><?= $group['CityName'] ?></td>
                    <td><?= $group['LanguageName'] ?></td>
                    <td><a href="<?= base_url() ?>admin/managegroup/details/<?= $group['GroupID'] ?>"> <img
                                src="<?= base_url() ?>assets/images/edit.png" style="height:24px;"></a></td>
                </tr>
                <?php $group_id = $group['GroupID'];
                $students = $group[$group_id]; ?>
<!--                <tr>
                    <td>
                        <table border='0' cellpadding='0' class='tablefrom'>
                            <tr>
                                <th>Student profile pic.</th>
                                <th>Student name</th>
                                <th>Level</th>
                                <th>Email</th>
                                <th>Status</th>
                            </tr>
                            <?php /*foreach ($students as $student) : */?>
                                <tr>
                                    <td><a href="<?/*=base_url('admin/student/profile/'.$student['user_id']) */?>">
                                        <?php /*if (!file_exists(FCPATH . 'users/profile/' . $student['user_id'] . '.jpg')) {
                                            */?>
                                            <img src="<?/*= base_url() */?>assets/images/defaultM.png" alt="" height="175px" width="175px">
                                        <?php /*} else { */?>
                                            <img src="<?/*= base_url() */?>users/profile/<?/*= $student['user_id'] */?>.jpg" height="175px"
                                                 width="175px"/>
                                        <?php /*} */?>
                                        </a>
                                    </td>
                                    <td><?/*= $student['user_fullname'] */?></td>
                                    <td><?/*= $student['level'] */?></td>
                                    <td>hidden</td>
                                    <td>Subscribed</td>
                                </tr>
                            <?php /*endforeach; */?>
                        </table>
                    </td>
                </tr>-->

        <?php endforeach; ?>
        </table>

    </div>
</div>

</body>
