<div class="inner-content">
    <div class="title_bg1">
        <div class="wrapper">Manage Promo Code</div>
    </div>
    <div class="title">
        <div class="wrapper">Classes promo codes</div>
    </div>
    <div class="wrapper1">
        <p><div class="add-new-button">  <p><a href="#" data-url="<?=base_url('admin/managepromocode/add')?>" class="addpromocodemodal" title="Add New Promo Code">Add New Promo Code</a></p></div>
        <div class="clear"></div>
        </p>




        <table border='0' cellpadding='0' class='tablefrom'>
            <tr>
                <th>Promo Code ID</th>
                <th>Promo Code </th>
                <th>Exp. Date</th>
                <th>Promo Code Amount</th>
                <th>Max Use</th>
                <th>Total Used</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>

        <?php foreach ($promocodes as $promocode) : ?>
            <tr>
            <td><?=$promocode['PromoCodeID']?></td>
            <td><?=$promocode['PromoCode']?></td>
            <td><?=$promocode['ExpDate']?></td>
            <td><?=$promocode['PromoCodeAmount']?></td>
            <td><?=$promocode['MaxUse']?></td>
            <td><?=$promocode['TotalUsed']?></td>
            <td><a href="#" data-id="<?=$promocode['PromoCodeID']?>" data-url="<?=base_url('admin/managepromocode/edit/'.$promocode['PromoCodeID'])?>" class="editpromocodemodal" title="Edit Language"> <img src="<?=base_url()?>assets/images/edit.png" style="height:24px;"></a></td>
            <td><a class="deletepromocode" href="#" data-url="<?=base_url('admin/managepromocode/delete/'.$promocode['PromoCodeID'])?>" title="Delete PromoCode"> <img src="<?=base_url()?>assets/images/delete.png" style="height:24px;" ></a></td>
            </tr>
        <?php endforeach; ?>
        </table>


        </div>
    </div>

<div class="inner-content">
    <div class="title">
        <div class="wrapper">Bones promo codes</div>
    </div>
    <div class="wrapper1">
        <p><div class="add-new-button">  <p><a href="#" data-url="<?=base_url('admin/managepromocode/add')?>" class="addbonespromocodemodal" title="Add New Promo Code">Add New Promo Code</a></p></div>
        <div class="clear"></div>
        </p>




        <table border='0' cellpadding='0' class='tablefrom'>
            <tr>
                <th>Promo Code ID</th>
                <th>Promo Code </th>
                <th>Exp. Date</th>
                <th>Promo Code Amount</th>
                <th>Max Use</th>
                <th>Total Used</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>

            <?php foreach ($bono_promocodes as $bono_promocode) : ?>
                <tr>
                    <td><?=$bono_promocode['PromoCodeID']?></td>
                    <td><?=$bono_promocode['PromoCode']?></td>
                    <td><?=$bono_promocode['ExpDate']?></td>
                    <td><?=$bono_promocode['PromoCodeAmount']?></td>
                    <td><?=$bono_promocode['MaxUse']?></td>
                    <td><?=$bono_promocode['TotalUsed']?></td>
                    <td><a href="#" data-id="<?=$bono_promocode['PromoCodeID']?>" data-url="<?=base_url('admin/managepromocode/edit/'.$bono_promocode['PromoCodeID'])?>" class="editpromocodemodal" title="Edit Language"> <img src="<?=base_url()?>assets/images/edit.png" style="height:24px;"></a></td>
                    <td><a class="deletepromocode" href="#" data-url="<?=base_url('admin/managepromocode/delete/'.$bono_promocode['PromoCodeID'])?>" title="Delete PromoCode"> <img src="<?=base_url()?>assets/images/delete.png" style="height:24px;" ></a></td>
                </tr>
            <?php endforeach; ?>
        </table>


    </div>
</div>
<script>

    $('.editpromocodemodal').click(function(e) {
        e.preventDefault();
        $.pgwModal({
            url: $(this).data('url'),
            loadingContent: '<span style="text-align:center">Loading in progress</span>',
            title: $(this).attr('title'),
            closable: true,
            titleBar: false
        });
    });

    $('.addpromocodemodal').click(function(e) {
        e.preventDefault();
        $.pgwModal({
            url: $(this).data('url'),
            loadingContent: '<span style="text-align:center">Loading in progress</span>',
            title: $(this).attr('title'),
            closable: true,
            titleBar: false
        });
        $(document).bind('PgwModal::PushContent', function() {
            $("#promotype [value='class']").attr("selected", "selected");
        });
    });

    $('.addbonespromocodemodal').bind('click',function(e)
    {
        e.preventDefault();
        $.pgwModal({
            url: $(this).data('url'),
            loadingContent: '<span style="text-align:center">Loading in progress</span>',
            title: $(this).attr('title'),
            closable: true,
            titleBar: false
        });

        $(document).bind('PgwModal::PushContent', function() {
            $("#promotype [value='bono']").attr("selected", "selected");
        });
    });

    $('.deletepromocode').click(function(e){
        e.preventDefault();
        var title_msg = $(this).attr('title');
        var url = $(this).data('url');
        var obj = $(this).parent().parent();
        dhtmlx.message({
            type:"confirm",
            text: "Do you want to delete the promocode?",
            title: title_msg,
            callback: function(e) {
                if(e)
                {
                    $.ajax({
                        url: url
                    }).done(function(e) {
                        obj.remove();
                    });
                }
            }
        });
    })
</script>
</body>
</html>