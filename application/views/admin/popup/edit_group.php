        <form action="<?=$action?>" data-remote="true" accept-charset="UTF-8" method="POST">
            <input type="hidden" name="GroupID" value="<?=$group['GroupID']?>" />
            <table  border="0" cellpadding="0" cellspacing="0" class="pop-table">
                <tr>
                    <td width="150"><label>Group Name<em>*</em></label></td>
                    <td>
                        <input  type="text" name="GroupName" value="<?=$group['GroupName']?>"/>
                    </td>
                </tr>
                <tr>
                    <td><label>Level Name<em>*</em></label></td>
                    <td>
                        <select id="ddlLevel" name="ddlLevel" >
                            <?php foreach ($levels as $level) : ?>
                                <option <?php

                                    if ($group['LevelID'] == $level['LevelID']) {
                                        ?> selected  <?php
                                        }
                                    ?>value="<?php echo $level['LevelID'] ?>"><?php echo $level['LevelName']; ?></option>
<?php endforeach; ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td><label>Teacher Name<em>*</em></label></td>
                    <td>
                        <select id="ddlTeacher" name="ddlTeacher" >
                            <option value="0">--Select Teacher--</option>
                            <?php foreach ($teachers_available as $teacher) { ?>
                                <option <?php
                                    if ($group['TeacherID'] == $teacher['teacher_id']) {
                                        ?> selected  <?php
                                        }
                                    ?>value="<?php echo $teacher['teacher_id'] ?>"><?php echo $teacher['TeacherName']; ?></option>
<?php } ?>
                        </select>
                    </td>
                </tr>


                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <input type="submit" <?php if (isset($_GET['id'])) { ?>value="Update"<?php } else { ?>value="Save"<?php } ?> class="btn btn-info">
                    </td>
                </tr>
                <tr align="Right">
                    <td colspan="2">&nbsp;</td>
                </tr>
            </table>
        </form>