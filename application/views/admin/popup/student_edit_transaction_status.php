<form action="<?=$action?>" data-remote="true" accept-charset="UTF-8" method="POST">
            <table border="0" cellpadding="0" cellspacing="0" class="pop-table">
                <tr>
                    <td><label>Status<em>*</em></label></td>
                    <td>
                        <select id="status_transaction" name="status_transaction">
                                <option <?=($status == 'Pending')?'selected':''?> value="Pending">Pending</option>
                                <option <?=($status == 'Paid')?'selected':''?> value="Paid">Paid</option>
                                <option <?=($status == 'completed')?'selected':''?> value="completed">Completed</option>
                        </select>
                    </td>
                </tr>

                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <input type="submit" value="Save" class="btn btn-info">
                    </td>
                </tr>
                <tr align="Right">
                    <td colspan="2">&nbsp;</td>
                </tr>
            </table>
        </form>
    </body>
</html>
