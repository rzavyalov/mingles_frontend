<link rel="stylesheet" href="<?=base_url();?>assets/css/jquery-ui.css" type="text/css" media="screen" charset="utf-8">
<link rel="stylesheet" href="<?=base_url();?>assets/css/elrte.min.css"                          type="text/css" media="screen" charset="utf-8">
<form action="<?=$action?>" data-remote="true" accept-charset="UTF-8" method="POST">
    <input type="hidden" name="email_id" value="<?=$email['email_id']?>" />
    <table border="0" cellpadding="0" cellspacing="0" class="pop-table">
        <tr>
            <td><label>Email Name<em>*</em></label></td>
            <td>
                <input  type="text" name="email_name" id="bone_name" value="<?=$email['email_name']?>"/>
            </td>
        </tr>
        <tr>
            <td><label>Subject<em>*</em></label></td>
            <td>
                <input  type="text" name="email_subject" id="email_name" value="<?=$email['email_subject']?>"/>
            </td>
        </tr>
        <tr>
            <td><label>Body<em>*</em></label></td>
            <td>
                <textarea style="width: 320px" class="input-xxlarge" name="email_message" id="email_message" rows="10"><?=@$email['email_message']; ?></textarea>
                <?php if($email['email_shortcodes']!="") : ?>
                <p class="help-block">You can use the following shortcodes: <?=$email['email_shortcodes']; ?></p>
                <?php endif;?>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <input type="submit" value="Save" class="btn btn-info">
            </td>
        </tr>
        <tr align="Right">
            <td colspan="2">&nbsp;</td>
        </tr>
    </table>
</form>
</body>
</html>
