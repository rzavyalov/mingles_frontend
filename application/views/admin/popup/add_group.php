        <form action="<?=$action?>" data-remote="true" accept-charset="UTF-8" method="POST">
            <table  border="0" cellpadding="0" cellspacing="0" class="pop-table">
                <tr>
                    <td width="150"><label>Group Name<em>*</em></label></td>
                    <td>
                        <input  type="text" name="GroupName" value=""/>
                    </td>
                </tr>
                <tr>
                    <td><label>Class Name<em>*</em></label></td>
                    <td>
                        <select id="ddlClass" name="ddlClass" >
                            <?php foreach ($classes as $class) : ?>
                                <option value="<?=$class['ClassID']?>"><?=$class['ClassName']. " - ".$class['ClassDate']?></option>
                            <?php endforeach; ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td><label>Level Name<em>*</em></label></td>
                    <td>
                        <select id="ddlLevel" name="ddlLevel" >
                            <?php foreach ($levels as $level) : ?>
                                <option value="<?=$level['LevelID']?>"><?=$level['LevelName']?></option>
                            <?php endforeach; ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td><label>One to One<em>*</em></label></td>
                    <td>
                        <input name="onetoone" value="yes" type="checkbox">
                    </td>
                </tr>
                <!--<tr>
                    <td><label>Teacher Name<em>*</em></label></td>
                    <td>
                        <select id="ddlTeacher" name="ddlTeacher" >
                            <?php /*foreach ($teachers as $teacher) { */?>
                                <option value="<?/*=$teacher['TeacherID']*/?>"><?/*=$teacher['TeacherName']*/?></option>
<?php /*} */?>
                        </select>
                    </td>
                </tr>-->


                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <input type="submit" value="Save" class="btn btn-info">
                    </td>
                </tr>
                <tr align="Right">
                    <td colspan="2">&nbsp;</td>
                </tr>
            </table>
        </form>