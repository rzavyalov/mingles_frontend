<form action="<?=$action?>" data-remote="true" accept-charset="UTF-8" method="POST">
            <table border="0" cellpadding="0" cellspacing="0" class="pop-table">
                <tr>
                    <td width="200px"><label>Session Payment Name<em>*</em></label></td>
                    <td>
                        <input  type="text" name="SessionPaymentName" value="<?=$session['SessionPaymentName']?>"/>
                    </td>
                </tr>
                <tr>
                    <td><label>Session Payment Amount<em>*</em></label></td>
                    <td>
                        <input  type="text" name="SessionPaymentAmount" value="<?=$session['SessionPaymentAmount']?>"/>
                    </td>
                </tr>
                <tr>
                    <td><label>Session Payment Credit<em>*</em></label></td>
                    <td><input  type="text" name="SessionPaymentCredit" value="<?=$session['SessionPaymentCredit']?>"/>
                    </td>
                </tr>
                <tr>
                    <td><label>Session Payment Description<em>*</em></label></td>
                    <td><input  type="text" name="SessionPaymentDescription" value="<?=$session['SessionPaymentDescription']?>"/>
                    </td>
                </tr>
                <tr>
                    <td><label>Payment For<em>*</em></label></td>
                    <td><select id="ddlPaymentFor" name="PaymentType" >
                            <option value="1" <?=$session['PaymentType']?'selected':''?>>Normal</option>
                            <option value="2" <?=$session['PaymentType']?'selected':''?>>Buno</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <input type="submit" value="Save" class="btn btn-info">
                    </td>
                </tr>
                <tr align="Right">
                    <td colspan="2">&nbsp;</td>
                </tr>
            </table>
        </form>
    </body>
</html>
