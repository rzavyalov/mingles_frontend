<form action="<?=$action?>" data-remote="true" accept-charset="UTF-8" method="POST">
            <table border="0" cellpadding="0" cellspacing="0" class="pop-table">
                <tr>
                    <td width="170px"><label>Pub Name<em>*</em></label></td>
                    <td width="300px">
                        <input  type="text" name="PubName" value=""/>
                    </td>

                </tr>
                <tr>
                    <td><label>Address<em>*</em></label></td>
                    <td>
                        <input  type="text" name="PubAddress" value=""/>
                    </td>
                </tr>

                <tr>
                    <td><label>City Name<em>*</em></label></td>
                    <td>
                        <select id="ddlcity" name="CityID" >
                            <option value="">--Select City--</option>
                            <?php foreach ($allcity as $row) { ?>
                                <option value="<?php echo $row['CityID'] ?>"><?php echo $row['CityName']; ?></option>
                            <?php } ?>
                        </select>
                    </td>
                </tr>

                <tr>
                    <td width="90"><label>Contact Person Name<em>*</em></label></td>
                    <td>
                        <input  type="text" name="ContactPersonName" value=""/>
                    </td>
                </tr>


                <tr>
                    <td><label>Phone Number<em>*</em></label></td>
                    <td><input  type="text" name="PubPhoneNumber" value=""/>
                    </td>
                </tr>

                <tr>
                    <td><label>Capacity for Mingles<em>*</em></label></td>
                    <td><input  type="text" name="CapacityforMingles" value=""/>
                    </td>
                </tr>

                <tr>
                    <td><label>Longitude<em>*</em></label></td>
                    <td><input  type="text" name="Longitude" value=""/>
                    </td>
                </tr>
                <tr>
                    <td><label>Latitude<em>*</em></label></td>
                    <td><input  type="text" name="Latitude" value=""/>
                    </td>
                </tr>

                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <input type="submit" value="Save" class="btn btn-info">
                    </td>
                </tr>
                <tr align="Right">
                    <td colspan="2">&nbsp;</td>
                </tr>
            </table>
        </form>
    </body>
</html>
