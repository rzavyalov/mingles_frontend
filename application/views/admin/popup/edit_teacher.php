<script type='text/javascript'>
    $(function() {
        $("#dob").datepicker({
            changeMonth: true,
            changeYear: true
        });
    });
</script>

<form action="<?=$action?>" data-remote="true" accept-charset="UTF-8" method="POST">
    <input type="hidden" name="TeacherID" value="<?=$teacher['TeacherID']?>" />
    <table  border="0" cellpadding="0" cellspacing="0" class="pop-table">
        <tr>
            <td width="150px"><label>Teacher Name<em>*</em></label></td>
            <td>
                <input  type="text" name="TeacherName" value="<?=$teacher['TeacherName']?>"/>
            </td>
        </tr>
        <tr>
            <td width="150px"><label>Teacher Email<em>*</em></label></td>
            <td>
                <input  type="text" name="email" value="<?=$teacher['email']?>"/>
            </td>
        </tr>
        <tr>
            <td width="150px"><label>Teacher Password<em>*</em></label></td>
            <td>
                <input  type="text" name="password" title="leave empty if you not want change current password" placeholder="leave empty if you not want change current password" value=""/>
            </td>
        </tr>
        <tr>
            <td><label>Teacher Phone<em>*</em></label></td>
            <td>
                <input  type="text" name="TeacherPhone" value="<?=$teacher['TeacherPhone']?>"/>
            </td>
        </tr>
        <tr>
            <td><label>Teacher Address<em>*</em></label></td>
            <td><input  type="text" name="TeacherAddress" value="<?=$teacher['TeacherAddress']?>"/>
            </td>
        </tr>
        <tr>
            <td><label>City Name<em>*</em></label></td>
            <td>
                <select id="ddlcity" name="CityID" >
                    <option value="">--Select City--</option>
                    <?php foreach ($allcity as $row) { ?>
                        <option <?php
                        if ($teacher['TeacherCityID'] == $row['CityID']) {
                        ?> selected  <?php
                        }
                        ?>value="<?php echo $row['CityID'] ?>"><?php echo $row['CityName']; ?></option>
                    <?php } ?>
                </select>
            </td>
        </tr>
        <tr>
            <td width="150px"><label>Date Of Birth<em>*</em></label></td>
            <td>
                <input  type="text" name="dob" id="dob" value="<?=$teacher['DateOfBirth']?>"/>
            </td>
        </tr>

        <tr>
            <td width="150px"><label>Gender</label></td>
            <td>
                <select id="ddlgender" name="Gender"  style="width: 150px">
                    <option value="Male">Male</option>
                    <option value="Female">Female</option>
                </select>
            </td>
        </tr>

        <tr>
            <td width="150px"><label>Nationality</label></td>
            <td>
                <input  type="text" name="Nationality" value="<?=$teacher['Nationality']?>"/>
            </td>
        </tr>

        <tr>
            <td width="150px"><label>NIF ID Number</label></td>
            <td>
                <input  type="text" name="NIF_ID_Number" value="<?=$teacher['NIF_ID_Number']?>"/>
            </td>
        </tr>

        <tr>
            <td width="150px"><label>Bank Account</label></td>
            <td>
                <input  type="text" name="BankAccount" value="<?=$teacher['BankAccount']?>"/>
            </td>
        </tr>

        <tr>
            <td width="150px"><label>Twitter</label></td>
            <td>
                <input  type="text" name="Twitter" value="<?=$teacher['Twitter']?>"/>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <input type="submit" <?php if (isset($_GET['id'])) { ?>value="Update"<?php } else { ?>value="Save"<?php } ?> class="btn btn-info" onclick="redirect_to_parent();">
            </td>
        </tr>
        <tr align="Right">
            <td colspan="2">&nbsp;</td>
        </tr>
    </table>
</form>
