<script type='text/javascript'>
    $(function() {
        $("#dob").datepicker({
            changeMonth: true,
            changeYear: true
        });
    });
</script>

<form action="<?=$action?>" data-remote="true" accept-charset="UTF-8" method="POST">
    <input type="hidden" name="TeacherID" value="" />
    <table  border="0" cellpadding="0" cellspacing="0" class="pop-table">
        <tr>
            <td width="150px"><label>Teacher Name<em>*</em></label></td>
            <td>
                <input  type="text" name="TeacherName" value=""/>
            </td>
        </tr>
        <tr>
            <td width="150px"><label>Teacher Email<em>*</em></label></td>
            <td>
                <input  type="text" name="email" value=""/>
            </td>
        </tr>
        <tr>
            <td width="150px"><label>Teacher Password<em>*</em></label></td>
            <td>
                <input  type="text" name="password" value=""/>
            </td>
        </tr>
        <tr>
            <td><label>Teacher Phone<em>*</em></label></td>
            <td>
                <input  type="text" name="TeacherPhone" value=""/>
            </td>
        </tr>
        <tr>
            <td><label>Teacher Address<em>*</em></label></td>
            <td><input  type="text" name="TeacherAddress" value=""/>
            </td>
        </tr>
        <tr>
            <td><label>City Name<em>*</em></label></td>
            <td>
                <select id="ddlcity" name="CityID" >
                    <option value="">--Select City--</option>
                    <?php foreach ($allcity as $row) { ?>
                        <option value="<?php echo $row['CityID'] ?>"><?php echo $row['CityName']; ?></option>
                    <?php } ?>
                </select>
            </td>
        </tr>
        <tr>
            <td width="150px"><label>Date Of Birth<em>*</em></label></td>
            <td>
                <input  type="text" name="dob" id="dob" value=""/>
            </td>
        </tr>

        <tr>
            <td width="150px"><label>Gender</label></td>
            <td>
                <select id="ddlgender" name="Gender"  style="width: 150px">
                    <option value="Male">Male</option>
                    <option value="Female">Female</option>
                </select>
            </td>
        </tr>

        <tr>
            <td width="150px"><label>Nationality</label></td>
            <td>
                <input  type="text" name="Nationality" value=""/>
            </td>
        </tr>

        <tr>
            <td width="150px"><label>NIF ID Number</label></td>
            <td>
                <input  type="text" name="NIF_ID_Number" value=""/>
            </td>
        </tr>

        <tr>
            <td width="150px"><label>Bank Account</label></td>
            <td>
                <input  type="text" name="BankAccount" value=""/>
            </td>
        </tr>

        <tr>
            <td width="150px"><label>Twitter</label></td>
            <td>
                <input  type="text" name="Twitter" value=""/>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <input type="submit" <?php if (isset($_GET['id'])) { ?>value="Update"<?php } else { ?>value="Save"<?php } ?> class="btn btn-info" onclick="redirect_to_parent();">
            </td>
        </tr>
        <tr align="Right">
            <td colspan="2">&nbsp;</td>
        </tr>
    </table>
</form>
