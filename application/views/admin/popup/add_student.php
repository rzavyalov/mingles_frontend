<script type='text/javascript'>
    $(function() {
        $("#dob").datepicker({
            changeMonth: true,
            changeYear: true
        });
    });
</script>
<form action="<?=$action?>" data-remote="true" accept-charset="UTF-8" method="POST">
    <input type="hidden" name="UserDetailsID" value="" />
    <table  border="0" cellpadding="0" cellspacing="0" class="pop-table">
        <tr>
            <td width="150px"><label>Full Name<em>*</em></label></td>
            <td>
                <input  type="text" name="UserFullName" value=""/>
            </td>
        </tr>
        <tr>
            <td width="150px"><label>Student Email<em>*</em></label></td>
            <td>
                <input  type="text" name="email" value=""/>
            </td>
        </tr>
        <tr>
            <td width="150px"><label>Student Password<em>*</em></label></td>
            <td>
                <input  type="text" name="password" title="leave empty if you want password auto-generated" placeholder="leave empty if you want password auto-generated" value=""/>
            </td>
        </tr>
        <tr>
            <td><label>Phone No<em>*</em></label></td>
            <td>
                <input  type="text" name="PhoneNo" value=""/>
            </td>
        </tr>
        <tr>
            <td><label>Address<em>*</em></label></td>
            <td><input  type="text" name="Address" value=""/>
            </td>
        </tr>
        <tr>
            <td><label>City Name<em>*</em></label></td>
            <td>
                <select id="ddlcity" name="CityID" >
                    <option value="">--Select City--</option>
                    <?php foreach ($allcity as $row) { ?>
                        <option value="<?php echo $row['CityID'] ?>"><?php echo $row['CityName']; ?></option>
                    <?php } ?>
                </select>
            </td>
        </tr>
        <tr>
            <td width="150px"><label>Date Of Birth<em>*</em></label></td>
            <td>
                <input  type="text" name="dob" id="dob" value=""/>
            </td>
        </tr>

        <tr>
            <td width="150px"><label>Gender</label></td>
            <td>
                <select id="ddlgender" name="Gender"  style="width: 150px">
                    <option value="Male">Male</option>
                    <option value="Female">Female</option>
                </select>
            </td>
        </tr>

        <tr>
            <td width="150px"><label>Twitter</label></td>
            <td>
                <input  type="text" name="Twitter" value=""/>
            </td>
        </tr>

        <tr>
            <td><label>Default Language<em>*</em></label></td>
            <td>
                <select id="ddlLanguage" name="LanguageID" >
                    <option value="">--Select Language--</option>
                    <?php foreach ($alllanguage as $row) { ?>
                        <option value="<?php echo $row['LanguageID'] ?>"><?php echo $row['LanguageName']; ?></option>
                    <?php } ?>
                </select>
            </td>
        </tr>

        <tr>
            <td>&nbsp;</td>
            <td>
                <input type="submit" <?php if (isset($_GET['id'])) { ?>value="Update"<?php } else { ?>value="Save"<?php } ?> class="btn btn-info" onclick="parent.parent.GB_hide();">
            </td>
        </tr>

        <tr align="Right">
            <td colspan="2">&nbsp;</td>
        </tr>
    </table>
</form>