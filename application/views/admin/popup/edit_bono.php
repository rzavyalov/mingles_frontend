<form action="<?=$action?>" data-remote="true" accept-charset="UTF-8" method="POST">
    <input type="hidden" name="bone_id" value="<?=$bono['bone_id']?>" />
    <table border="0" cellpadding="0" cellspacing="0" class="pop-table">
        <tr>
            <td><label>Bono Name<em>*</em></label></td>
            <td>
                <input  type="text" name="bone_name" id="bone_name" value="<?=$bono['bone_name']?>"/>
            </td>
        </tr>
        <tr>
            <td><label>Count Classes<em>*</em></label></td>
            <td>
                <input  type="text" name="count_classes" id="count_classes" value="<?=$bono['count_classes']?>"/>
            </td>
        </tr>
        <tr>
            <td><label>Amount<em>*</em></label></td>
            <td>
                <input  type="text" name="amount" id="amount" value="<?=$bono['amount']?>"/>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <input type="submit" value="Save" class="btn btn-info">
            </td>
        </tr>
        <tr align="Right">
            <td colspan="2">&nbsp;</td>
        </tr>
    </table>
</form>
</body>
</html>
