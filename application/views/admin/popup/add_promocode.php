<script>
    $(function() {
        $("#ExpDate").datepicker(
            {
                firstDay: 1,
                dateFormat: "yy-mm-dd"});
    });
</script>
<form action="<?=$action?>" data-remote="true" accept-charset="UTF-8" method="POST">
            <table border="0" cellpadding="0" cellspacing="0" class="pop-table">
                <tr>
                    <td width="150px"><label>Promo Code</label></td>
                    <td>
                        <select id="promotype" name="promotype">
                            <option value="class">Class</option>
                            <option value="bono">Bono</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td width="150px"><label>Promo Code</label></td>
                    <td> <input  type="text" name="PromoCode" value=""/>
                    </td>
                </tr>
                <tr>
                    <td><label>Exp Date<em>*</em></label></td>
                    <td>
                        <input  type="text" name="ExpDate" id="ExpDate" value=""/>
                    </td>
                </tr>
                <tr>
                    <td><label>Promo Code Amount<em>*</em></label></td>
                    <td>
                        <input  type="text" name="PromoCodeAmount" id="PromoCodeAmount" value=""/>
                    </td>
                </tr>

                <tr>
                    <td><label>Maximum Use<em>*</em></label></td>
                    <td>
                        <input  type="text" name="MaxUse" id="MaxUse" value=""/>
                    </td>
                </tr>

                <tr>
                    <td><label>Total Used<em>*</em></label></td>
                    <td>
                        <input  type="text" name="TotalUsed" id="TotalUsed" value=""/>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <input type="submit" value="Save" class="btn btn-info">
                    </td>
                </tr>
                <tr align="Right">
                    <td colspan="2">&nbsp;</td>
                </tr>
            </table>
        </form>
    </body>
</html>
