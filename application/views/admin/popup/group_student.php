<form action="<?=$action?>" data-remote="true" accept-charset="UTF-8" method="POST">
    <input type="hidden" name="ClassID" value="<?=$group_id?>" />
    <table border="0" cellpadding="0" cellspacing="0" class="pop-table">
        <tr>
            <td><label>Students <em>*</em></label></td>
            <td>
                <?php /*for($i=0;$i < count($student_in_group);$i++) :*/?>

                <?php foreach ($students as $student) :?>
                <select name="user_id[]" class="selectbg">
                    <option value="0">--Select Student--</option>
                    <option selected
                    value="<?php echo $student['user_id'] ?>"><?php echo $student['user_fullname']; ?></option>
                    <?php foreach ($all_students as $student) { ?>
                        <option value="<?php echo $student['UserDetailsID'] ?>"><?= $student['user_fullname']." | ".$student['level']; ?></option>
                    <?php } ?>
                </select>
                <?php endforeach; ?>
                <?php
                    if($group['group_type']) :
                        $other = 1-count($students);
                    else:
                        $other = 5-count($students);
                    endif;
                ?>
                <?php for($i=0;$i < $other; $i++) :?>
                <select name="user_id[]" class="selectbg">
                    <option value="0">--Select Student--</option>
                    <?php foreach ($all_students as $student) { ?>
                        <option value="<?php echo $student['UserDetailsID'] ?>"><?= $student['user_fullname']." | ".$student['level']; ?></option>
                    <?php } ?>
                </select>
                <?php endfor; ?>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <input type="submit" value="Save" class="btn btn-info" onclick="redirect_to_parent();">
            </td>
        </tr>
        <tr align="Right">
            <td colspan="2">&nbsp;</td>
        </tr>
    </table>
</form>