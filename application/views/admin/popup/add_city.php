<form action="<?=$action?>" data-remote="true" accept-charset="UTF-8" method="POST">
            <table border="0" cellpadding="0" cellspacing="0" class="pop-table">
                <tr>
                    <td width="90"><label>City Name</label></td>
                    <td> <input  type="text" name="CityName" value=""/>
                    </td>
                </tr>

                <tr>
                    <td><label>Country<em>*</em></label></td>
                    <td>
                        <select id="ddlCountry" name="ddlCountry">
                            <option value="">--Select Country--</option>
                            <?php foreach ($allCountry as $row) { ?>
                                <option value="<?= $row['CountryID'] ?>"><?= $row['CountryName']; ?></option>
                            <?php } ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td width="90"><label>Short Order</label></td>
                    <td> <input  type="text" name="ShortOrder" value=""/>
                    </td>
                </tr>

                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <input type="submit" value="Save" class="btn btn-info">
                    </td>
                </tr>
                <tr align="Right">
                    <td colspan="2">&nbsp;</td>
                </tr>
            </table>
        </form>
    </body>
</html>
