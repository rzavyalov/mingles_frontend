<script type='text/javascript'>
    $(function() {
        $("#ClassDate").datepicker(
            {
                changeMonth: true,
                changeYear: true,
                firstDay: 1,
                dateFormat: "yy-mm-dd"});
    });

</script>
<form action="<?=$action?>" data-remote="true" accept-charset="UTF-8" method="POST">
            <table border="0" cellpadding="0" cellspacing="0" class="pop-table">
                <tr>
                    <td width="90"><label>Class Name</label></td>
                    <td> <input  type="text" name="ClassName" value=""/>
                    </td>
                </tr>
                <tr>
                    <td><label>Class Date<em>*</em></label></td>
                    <td>
                        <input  type="text" name="ClassDate" id="ClassDate" value=""/>
                    </td>
                </tr>
                <tr>
                    <td><label>Class Time<em>*</em></label></td>
                    <td>

                        <select id="ddlst" name="cst" class="selectbg" style="width: 150px">
                              <?php foreach ($alltime as $row) { ?>
                                <option value="<?php echo $row['ClassTime'] ?>"><?php echo $row['ClassTime']; ?></option>
                                <?php } ?>
                        </select>

                        <select id="ddlst" name="cet" class="selectbg" style="width: 150px">
                            <?php foreach ($alltime as $row) { ?>
                                <option value="<?php echo $row['ClassTime'] ?>"><?php echo $row['ClassTime']; ?></option>
                                <?php } ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td><label>Pub Name<em>*</em></label></td>
                    <td>
                        <select id="ddlpub" name="PubID" class="selectbg">
                            <option value="0">--Select Pub--</option>
                            <?php foreach ($allpub as $row) { ?>
                                <option value="<?php echo $row['PubID'] ?>"><?php echo $row['PubName']; ?></option>
                                <?php } ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td><label>Language <em>*</em></label></td>
                    <td>
                        <select id="ddlLanguage" name="LanguageID" class="selectbg">
                            <option value="0">--Select Language--</option>
                            <?php foreach ($alllanguage as $row) { ?>
                                <option value="<?php echo $row['LanguageID'] ?>"><?php echo $row['LanguageName']; ?></option>
                                <?php } ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td><label>Session <em>*</em></label></td>
                    <td>
                        <select id="ddlSession" name="SessionID" class="selectbg">
                            <option value="0">--Select Session--</option>
                            <?php foreach ($sessions as $session) { ?>
                                <option value="<?php echo $session['SessionPaymentID'] ?>"><?php echo $session['SessionPaymentName']; ?></option>
                            <?php } ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td><label>One to One<em>*</em></label></td>
                    <td>
                        <input name="onetoone" value="yes" type="checkbox">
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <input type="submit" value="Save" class="btn btn-info">
                    </td>
                </tr>
                <tr align="Right">
                    <td colspan="2">&nbsp;</td>
                </tr>
            </table>
        </form>
    </body>
</html>
