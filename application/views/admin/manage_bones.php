<div class="inner-content">
    <div class="title_bg1">
        <div class="wrapper">Manage Bones</div>
    </div>
    <div class="wrapper1">
        <p><div class="add-new-button">  <p><a href="#" data-url="<?=base_url('admin/managebones/add')?>" class="bonopopup" title="Add New Bono">Add New Bono</a></p></div>
        <div class="clear"></div>
        </p>




        <table border='0' cellpadding='0' class='tablefrom'>
            <tr>
                <th>Bono Code ID</th>
                <th>Bono Name </th>
                <th>Count Classes</th>
                <th>Amount</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>

        <?php foreach ($bones as $bono) : ?>
            <tr>
            <td><?=$bono['bone_id']?></td>
            <td><?=$bono['bone_name']?></td>
            <td><?=$bono['count_classes']?></td>
            <td><?=$bono['amount']?></td>
            <td><a href="#" data-url="<?=base_url('admin/managebones/edit/'.$bono['bone_id'])?>" class="bonopopup" title="Edit Bono"> <img src="<?=base_url()?>assets/images/edit.png" style="height:24px;"></a></td>
            <td><a class="deletebono" href="#" data-url="<?=base_url('admin/managebones/delete/'.$bono['bone_id'])?>" title="Delete Bono"> <img src="<?=base_url()?>assets/images/delete.png" style="height:24px;" ></a></td>
            </tr>
        <?php endforeach; ?>
        </table>


        </div>
    </div>

<script>
    $('.bonopopup').click(function() {
        $.pgwModal({
            url: $(this).data('url'),
            loadingContent: '<span style="text-align:center">Loading in progress</span>',
            title: $(this).attr('title'),
            closable: true,
            titleBar: false
        });
    });

    $('.deletebono').click(function(){
        var title_msg = $(this).attr('title');
        var url = $(this).data('url');
        var obj = $(this).parent().parent();
        dhtmlx.message({
            type:"confirm",
            text: "Do you want to delete the bono?",
            title: title_msg,
            callback: function(e) {
                if(e)
                {
                    $.ajax({
                        url: url
                    }).done(function(e) {
                        obj.remove();
                    });
                }
            }
        });
    })
</script>
</body>
</html>