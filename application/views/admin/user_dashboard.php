<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDY0kkJiTPVd2U7aTOAwhc9ySH6oHxOIYM&sensor=false"></script>
<div class="inner-content">
    <div class="title_bgd">
        <div class="wrapper"><?=$this->lang->line('welcome_panel')?></div>
    </div>

    <?php if ($this->session->userdata('usertype_id') == 4) {
        $this->load->view('admin/components/admin_userDashBoardLeft');
    } else {
        $this->load->view('admin/components/userDashBoardLeft');
    }
    ?>


    <div class="profile-right">

        <h1>Mis Mingles</h1>
        <!--        <p><div class="add-new-button">  <p><a href="../index.php#option" title="Add New Class">Add New Class</a></p></div>
                <div class="clear"></div>
                </p>-->

        <!--<img src="<? /*=base_url()*/ ?>assets/images/steps2.png" alt="" height="55px" width="58%">-->
        <div class="price-box">
            <label><?=$this->lang->line('credit')?></label> <input type="text" name="price" value="<?= $user_info['credit'] ?>">
        </div>
        <br/>
        <br/>

        <p> <?=$this->lang->line('dashboard_student_description')?></p>
        <br/>

        <h1><?=$this->lang->line('details_sessions')?></h1>
        <div id="ajax_content">
        <table border='0' cellpadding='0' class='tablefrom'>
            <tr>
                <th><?=$this->lang->line('class')?></th>
                <th><?=$this->lang->line('date')?></th>
                <th><?=$this->lang->line('time')?></th>
                <th><?=$this->lang->line('place')?></th>
                <th><?=$this->lang->line('city')?></th>
                <th><?=$this->lang->line('language')?></th>
                <th><?=$this->lang->line('details')?></th>
                <th><?=$this->lang->line('cancel')?></th>
                <th><?=$this->lang->line('share')?></th>
            </tr>

            <?php foreach ($classes as $row) : ?>
                <tr>
                    <td><?= $row['ClassName'] ?></td>
                    <td><?= $row['ClassDate'] ?></td>
                    <td><?= $row['ClassStartTime'] . '-' . $row['ClassEndTime'] ?></td>
                    <td><?= $row['PubName'] ?></td>
                    <td><?= $row['CityName'] ?></td>
                    <td><?= $row['LanguageName'] ?></td>
                    <td>

                        <?php if($this->session->userdata('usertype_id') == 4): ?>
                            <a href="<?= base_url() ?>admin/student/userclass/<?= $row['ClassID'] ?>/<?=$user_info['user_id']?>"> <img
                                    src="<?= base_url() ?>assets/images/edit.png" style="height:24px;"></a>
                        <?php else: ?>
                        <a href="<?= base_url() ?>admin/userclass/<?= $row['ClassID'] ?>"> <img
                                src="<?= base_url() ?>assets/images/edit.png" style="height:24px;"></a>
                        <?php endif; ?>
                    </td>
                    <td><?php if ($row['can_cancel']) : ?><a class="cancelclass" href="#"
                                                             data-url="<?= base_url('admin/userclass/cancel/' . $row['ClassID']) ?>"
                            data-userid="<?=$row['UserDetailsID']?>"
                                                             title="Cancel class">
                            <img src="<?= base_url() ?>assets/images/delete.png" style="height:24px;">
                            </a><?php endif; ?></td>
                    <td><a href="#" class="shareclass" data-url="<?=base_url('admin/share/popup/' . $row['ClassID'])?>">Share</a></td>
                </tr>
            <?php endforeach; ?>

        </table>
            <span class="ajax_pag"><?=$this->pagination->create_links()?> </span>
            </div>
    </div>
</div>

<script src="/assets/js/pagination.js"></script>


<script src="/assets/js/student/share.js"></script>
<script src="/assets/js/cancelclass.js"></script>

<link rel="stylesheet" href="/assets/css/pagination.css">
</body>
