<!DOCTYPE html>

<html>
<head>
    <meta http-equiv="refresh" content="3; url=<?=base_url()?>">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <title>Mingles</title>
    <link href="<?=base_url()?>assets/css/style.css" type="text/css" rel="stylesheet" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
    <link rel="stylesheet" href="/resources/demos/style.css">

    <link href="<?=base_url()?>assets/css/admin/bootstrap-social-gh-pages/assets/css/font-awesome.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/css/admin/bootstrap-social-gh-pages/assets/css/docs.css" rel="stylesheet" >
    <link rel="stylesheet" href="<?=base_url()?>assets/css/admin/bootstrap-social-gh-pages/bootstrap-social.css">



    <script type='text/javascript'>
        $(function() {
            $("#dob").datepicker();
        });
    </script>
</head>
<body>
<div class="container">
    <!--<start header>-->
    <header>
        <div class="wrapper">
            <div class="logo"><a href="<?=base_url()?>"><img src="<?=base_url()?>assets/images/logo.png"></a></div>
            <div class="height15"></div>
        </div>
    </header>
    <!--<end header>-->
    <div class="height30">
    </div>
    <?= validation_errors()?>
    <!--<start top-section> -->
    <div class="inner-content">
        <div class="wrapper">
            <h2>Registration successfully. Please check email. Redirect will be an 3 seconds</h2>
        </div>

        <!--<start footer>-->
        <footer>
            <div class="footer">
                <div class="wrapper">
                    <div class="footer_lt">
                        MINGLES LANGUAGES<br>
                        La nueva forma de aprender Idiomas
                    </div>

                    <div class="footer_rt">
                        <img src="<?=base_url()?>assets/images/footer_logo.png" alt=""><br>
                        &copy; Mingles Languages S.L. Todos los derechos reservados.
                    </div>

                </div>

            </div>
        </footer>
        <!--<end footer>-->
    </div>
</body>
</html>

</html>