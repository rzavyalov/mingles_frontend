<div class="inner-content">
    <div class="title_bg1">
        <div class="wrapper">Premium cards list</div>
    </div>
    <div class="wrapper1">
        <p><div class="add-new-button">  <p><a href="#" data-url="<?=base_url('admin/pages/addpremiumcard')?>" class="newclass" title="Add New Page">Add New Page</a></p></div>
        <div class="clear"></div>
        </p>

        <table border='0' cellpadding='0' class='tablefrom'>
            <tr>
                <th>Page ID</th>
                <th>Title</th>
                <th>Main</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
        <?php foreach ($pages as $row) :?>
            <tr>
                <td><?=$row['page_id'] . '</td>'?>
                <td><?=$row['title'] . '</td>'?>
                <td><?=($row['is_main'])?'YES':'NO' . '</td>'?>
                <td><a href="#" class="editmodal" data-id="<?=$row['page_id']?>" data-url="<?=base_url('admin/pages/editpremiumcard/'.$row['page_id'])?>" title="Edit Class"> <img src="<?=base_url()?>assets/images/edit.png" style="height:24px;"></a></td>
                <td><a href="#" class="deletepage" data-name="<?=$row['page_id']?>" data-url="<?=base_url()?>admin/pages/delete/<?=$row['page_id']?>" title="Delete Page"> <img src="<?=base_url()?>assets/images/delete.png" style="height:24px;"></a></td>
                </tr>
         <?php endforeach;?>
        </table>
        </div>
    </div>
<script>
    $('.newclass').click(function() {
        $.pgwModal({
            url: $(this).data('url'),
            loadingContent: '<span style="text-align:center">Loading in progress</span>',
            title: 'Add page',
            closable: true,
            titleBar: false,
            maxWidth: 800
        });
    });
$('.editmodal').click(function() {
$.pgwModal({
    url: $(this).data('url'),
    loadingContent: '<span style="text-align:center">Loading in progress</span>',
    title: 'Edit page',
    closable: true,
    titleBar: false,
    maxWidth: 800
});
});

    $('.deletepage').click(function(){
        var title_msg = $(this).attr('title');
        var url = $(this).data('url');
        var obj = $(this).parent().parent();
        dhtmlx.message({
            type:"confirm",
            text: "Do you want to delete the page?",
            title: title_msg,
            callback: function(e) {
                if(e)
                {
                    $.ajax({
                        url: url
                    }).done(function(e) {
                        obj.remove();
                    });
                }
            }
        });
    })
</script>
</body>
</html>