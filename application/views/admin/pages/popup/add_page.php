<script src="<?=base_url('assets/js/ckeditor/ckeditor.js')?>"></script>

<script>
    CKEDITOR.replace( 'body' );

</script>

<script type='text/javascript'>
    $(function() {
        $("#ClassDate").datepicker(
            {
                changeMonth: true,
                changeYear: true,
                firstDay: 1,
                dateFormat: "yy-mm-dd"});
    });

</script>
<form action="<?=$action?>" data-remote="true" accept-charset="UTF-8" method="POST">
            <table border="0" cellpadding="0" cellspacing="0" class="pop-table">
                <tr>
                    <td width="90"><label>Title</label></td>
                    <td> <input type="text" name="title" value=""/>
                    </td>
                </tr>
                <tr>
                    <td width="90"><label>URL</label></td>
                    <td> <input type="text" name="url" value=""/>
                    </td>
                </tr>
                <tr>
                    <td><label>File<em>*</em></label></td>
                    <td> <input type="file" name="file"/>
                    </td>
                </tr>
                <tr>
                    <td><label>Body<em>*</em></label></td>
                    <td>
                        <textarea name="body" id="body"></textarea>
                    </td>
                </tr>
                <tr>
                    <td><label>Hidden<em></em></label></td>
                    <td>
                        <input type="checkbox" name="hidden">
                    </td>
                </tr>
                <tr>
                    <td><label>Pub Name<em>*</em></label></td>
                    <td>
                        <?php foreach ($allpub as $row) { ?>

                            <div>
                            <label><input name="pubs[]" value="<?=$row['PubID']?>" type="checkbox"/> <?=$row['PubName']?></label>
                            </div>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <input type="submit" <?php if (isset($_GET['id'])) { ?>value="Update"<?php } else { ?>value="Save"<?php } ?> class="btn btn-info" onclick="redirect_to_parent();">
                    </td>
                </tr>
                <tr align="Right">
                    <td colspan="2">&nbsp;</td>
                </tr>
            </table>
        </form>
    </body>
</html>
