<script src="<?=base_url('assets/js/ckeditor/ckeditor.js')?>"></script>
<!--<link href="<?/*=base_url('assets/css/landing/bootstrap.css')*/?>" rel="stylesheet">
<link href="<?/*=base_url('assets/css/landing/bootstrap-responsive.css')*/?>" rel="stylesheet">
<link href="<?/*=base_url('assets/css/landing/bootstrap-select.css')*/?>" rel="stylesheet">-->
<!--<link href="<?/*=base_url('assets/css/landing/mystyles.css')*/?>" rel="stylesheet">
<link href="<?/*=base_url('assets/css/landing/mystyles_responsive.css')*/?>" rel="stylesheet">-->
<script>
    CKEDITOR.replace( 'body' );
    CKEDITOR.replace( 'body_en' );
</script>
<style>
    .active{
        background: #a9a9a9;
        color: #f5f5f5;
        padding: 3px;
    }
</style>
<script type='text/javascript'>
    $(function() {
        $("#ClassDate").datepicker(
            {
                changeMonth: true,
                changeYear: true,
                firstDay: 1,
                dateFormat: "yy-mm-dd"});

        $('.lang_switch').bind('click',function(e){
            e.preventDefault();
            $('.lang_switch').removeClass('active');
            $(this).addClass('active');
            var language = $(this).data('lang_id')
            if(language == 'es')
            {
                $('#body_style').show();
                $("#body_en_style").hide();
                $('#body_en').hide();
            }
            else {
                if (language == 'en') {
                    $('#body_en_style').show();
                    $("#body_style").hide();
                    $('#body').hide();
                }
            }
        })
    });

</script>
<form action="<?=$action?>" enctype="multipart/form-data" data-remote="true" accept-charset="UTF-8" method="POST">
            <input type="hidden" name="page_id" value="<?=$page['page_id']?>" />
            <input type="hidden" name="language_id" id="language_id" value="es" />
            <table border="0" cellpadding="0" cellspacing="0" class="pop-table">
                <tr>
                    <td width="90"><label>Title</label></td>
                    <td> <input type="text" name="title" value="<?=$page['title']?>"/>
                    </td>
                </tr>
                <tr>
                    <td width="90"><label>URL</label></td>
                    <td> <input type="text" name="url" value="<?=$page['url']?>"/>
                    </td>
                </tr>
                <tr>
                    <td><label>File<em>*</em></label></td>
                    <td> <input type="file" name="userfile" size="20" />
                    </td>
                </tr>
                <tr>
                    <td><label>Language</label></td>
                    <td>
                        <a class="lang_switch active" data-lang_id="es" href="#">ES</a>
                        <a class="lang_switch" data-lang_id="en" href="#">EN</a>
                    </td>
                </tr>
                <tr id="body_style">
                    <td><label>Body<em>*</em></label></td>
                    <td>
                        <textarea cols="50" name="body" id="body"><?=$page['body']?></textarea>
                    </td>
                </tr>
                <tr id="body_en_style" style="display: none">
                    <td><label>Body<em>*</em></label></td>
                    <td>
                        <textarea cols="50" name="body_en" id="body_en"><?=$page['body_en']?></textarea>
                    </td>
                </tr>
                <tr>
                    <td><label>Hidden<em></em></label></td>
                    <td>
                        <input type="checkbox" name="hidden" <?= $page['hidden']?'checked="checked"':""?>>
                    </td>
                </tr>
                <tr>
                    <td><label>Pub Name<em>*</em></label></td>
                    <td>
                        <?php foreach ($allpub as $row) { ?>
                            <?php foreach ($pubs as $pub) {
                             if($pub == $row['PubID']){
                                 $checked = true;
                                 break;
                             }
                                else
                                {
                                    $checked = false;
                                }
                            }?>
                            <div>
                            <label><input name="pubs[]" value="<?=$row['PubID']?>" type="checkbox" <?= ($checked)?'checked':''?>/> <?=$row['PubName']?></label>
                            </div>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <input type="submit" <?php if (isset($_GET['id'])) { ?>value="Update"<?php } else { ?>value="Save"<?php } ?> class="btn btn-info">
                    </td>
                </tr>
                <tr align="Right">
                    <td colspan="2">&nbsp;</td>
                </tr>
            </table>
        </form>
    </body>
</html>
