<div class="inner-content">
    <div class="title_bg1">
        <div class="wrapper">Manage Group</div>
    </div>
    <div class="wrapper1">
        <form id="form1" name="form1" method="post" action="<?=base_url('admin/managegroup')?>">
            <table style="width:100%" class="pop-table-search">
                <tr>
                    <td width="80px">Pub Name: </td>
                    <td><!--<input  type="text" name="SPubName" value="<?/*=$this->input->post('SPubName')*/?>"/>--> </td>

                    <td><select name="SPubName">
                            <option value=""></option>
                            <?php foreach($pubs as $pub) :?>
                                <option <?=$filter_groups['pub_name']==$pub['PubName']?'selected':''?> value="<?=$pub['PubName']?>"><?=$pub['PubName']?></option>
                                <!--                                                <option value="Bono OneToOne">Bono OneToOne €112,50 EUR</option>-->
                            <?php endforeach;?>
                        </select>
                    </td>

                    <td width="80px">City Name:</td>

                    <td><select name="SCityName">
                            <option value=""></option>
                            <?php foreach($cities as $city) :?>
                                <option <?=$filter_groups['city_name']==$city['CityName']?'selected':''?> value="<?=$city['CityName']?>"><?=$city['CityName']?></option>
                                <!--                                                <option value="Bono OneToOne">Bono OneToOne €112,50 EUR</option>-->
                            <?php endforeach;?>
                        </select>
                    </td>
                    <td width="80px">Date:</td>
                    <td><select name="SDate">
                            <option value="all" <?=$filter_groups['date']=='All'?'selected':''?>><?= ($this->input->post('SDate') == 'all')?'selected':'' ?> All</option>
                            <option value="today" <?= ($filter_groups['date']=='today')?'selected':'' ?>>Today</option>
                            <option value="this_week" <?= ($filter_groups['date']=='this_week')?'selected':'' ?>>This week</option>
                            <option value="new" <?= ($filter_groups['date']=='new')?'selected':'' ?>>New</option>
                            <option value="old" <?= ($filter_groups['date']=='old')?'selected':'' ?>>Old</option>
                    </select></td>
                    <td><input type="submit" class="btns btn-info" value="Search" ></td>
                    <td style="text-align: right;"><p><div class="add-new-button">  <p><a href="#" data-url="<?=base_url('admin/managegroup/add')?>" class="addgroupmodal" title="Add New Group">Add New Group</a></p></div></td>
                </tr>
            </table>
        </form>
        <div class="clear"></div>



        <div id="ajax_content">
        <table border='0' cellpadding='0' class='tablefrom'>
            <tr>
                <th>Pub Name</th>
                <th>City</th>
                <th>Date</th>
                <th>Class Name</th>
                <th>Group Name</th>
                <th>Teacher Name</th>
                <th>Level Name</th>
                <th>Student Name</th>
                <th>Edit</th>
                <th>Manage Student</th>
                <th>Delete</th>
            </tr>

        <?php foreach ($groups->result_array() as $group) : ?>
            <tr>
            <td><?=$group['PubName']?></td>
            <td><?=$group['CityName']?></td>
            <td><?=date_format(date_create($group['ClassDate']), 'Y-m-d');?></td>
            <td><?=$group['CLassName']?></td>
            <td><a href="<?=base_url('admin/managegroup/details/'.$group['GroupID'])?>"><?=$group['GroupName']?></a></td>
            <td><?=$group['TeacherName']?></td>
            <td><?=$group['LevelName']?></td>
            <td <?php if($group['UserFullName'] != ''):?> style="background-color: #ffff00" <?php endif;?>><?=$group['UserFullName']?></td>

            <td><a href="#" data-id="<?=$group['GroupID']?>" data-url="<?=base_url('admin/managegroup/edit/'.$group['GroupID'])?>" class="editgroupmodal" title="Edit Group"> <img src="<?=base_url()?>assets/images/edit.png" style="height:24px;"></a></td>
            <td><a href="#" data-id="<?=$group['GroupID']?>" data-url="<?=base_url('admin/managegroup/students/'.$group['GroupID'])?>" class="managestudentsmodal"" title="Manage Student Group"> <img src="<?=base_url()?>assets/images/edit.png" style="height:24px;"></a></td>
            <td><a href="#" class="deletegroup" title="Delete Group" data-name="<?=$group['PubName']?>" data-url="<?=base_url('admin/managegroup/delete/'.$group['GroupID'])?>" title="Delete Group"> <img src="<?=base_url()?>assets/images/delete.png" style="height:24px;"></a></td>
            </tr>
        <?php endforeach; ?>
        </table>
        <span class="ajax_pag"><?=$this->pagination->create_links()?> </span>
            </div>

        </div>
    </div>

<script src="/assets/js/pagination.js"></script>

<link rel="stylesheet" href="/assets/css/pagination.css">

<script src="/assets/js/admin/deletegroup.js"></script>
<script src="/assets/js/admin/editgroup.js"></script>
<script src="/assets/js/admin/newgroup.js"></script>
<script src="/assets/js/admin/managestudents_modal.js"></script>
</body>
</html>