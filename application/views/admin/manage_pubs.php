<div class="inner-content">
    <div class="title_bg1">
        <div class="wrapper">Manage Pubs</div>
    </div>
    <div class="wrapper1">

        <form id="form1" name="form1" method="post" action="<?=base_url('admin/managepubs')?>">
            <table style="width:100%" class="pop-table-search">
                <tr>
                    <td width="80px">Pub Name: </td>
                    <td><input  type="text" name="SPubName" value="<?=$this->input->post('SPubName')?>"/> </td>
                    <td width="80px">City Name:</td>
                    <td><input  type="text" name="SCityName" value="<?=$this->input->post('SCityName')?>"/> </td>
                    <td><input type="submit" class="btns btn-info" value="Search" ></td>
                    <td style="text-align: right;"><p><div class="add-new-button">  <p><a href="#" data-url="<?=base_url('admin/managepubs/add')?>" class="editpubmodal" title="Add New Pub">Add New Pub</a></p></div></td>
                </tr>
            </table>
        </form>
        <div class="clear"></div>
        </p>




        <table border='0' cellpadding='0' class='tablefrom' >
            <tr>
                <th>Pub ID</th>
                <th>Pub Name</th>
                <th> Contact Person Name </th>
                <th>Address</th>
                <th>Phone Number</th>
                <th>Capacity for Mingles</th>
                <th>City Name</th>
                <th>Longitude</th>
                <th>Latitude</th>
                <th>Edit</th>
<!--                <th>View Profile</th>-->
                <th>Delete</th>
            </tr>

        <?php foreach ($pubs as $pub) : ?>
            <tr>
            <td><?=$pub['PubID']?></td>
            <td><?=$pub['PubName']?></td>
            <td><?=$pub['ContactPersonName']?></td>
            <td><?=$pub['PubAddress']?></td>
            <td><?=$pub['PubPhoneNumber']?></td>
            <td><?=$pub['CapacityforMingles']?></td>
            <td><?=$pub['CityName']?></td>
            <td><?=$pub['Longitude']?></td>
            <td><?=$pub['Latitude']?></td>
            <td><a href="#" data-id="<?=$pub['PubID']?>" data-url="<?=base_url('admin/managepubs/edit/'.$pub['PubID'])?>" class="editpubmodal" title="Edit Pub"> <img src="<?=base_url()?>assets/images/edit.png" style="height:24px;"></a></td>
            <!--<td><a href="<?/*=base_url('old/Pubs_New.php?id='.$pub['PubID'])*/?>" rel="gb_page_center[800, 425]" title="Edit Pub"> <img src="<?/*=base_url()*/?>assets/images/edit.png" style="height:24px;"></a></td>
            <td><a href="<?/*=base_url('old/barprofile.php?id='.$pub['PubID'])*/?>" > <img src="<?/*=base_url()*/?>assets/images/edit.png" style="height:24px;"></a></td>-->
            <td><a class="deletepub" href="#" data-url="<?=base_url('admin/managepubs/delete/'.$pub['PubID'])?>" title="Delete <?=$pub['PubName']?>"> <img src="<?=base_url()?>assets/images/delete.png" style="height:24px;"></a></td>
            </tr>
        <?php endforeach; ?>
        </table>


        </div>
    </div>
<script>
    $('.editpubmodal').click(function() {
        $.pgwModal({
            url: $(this).data('url'),
            loadingContent: '<span style="text-align:center">Loading in progress</span>',
            title: $(this).attr('title'),
            closable: true,
            titleBar: false
        });
    });

    $('.deletepub').click(function(){
        var title_msg = $(this).attr('title');
        var url = $(this).data('url');
        var obj = $(this).parent().parent();
        dhtmlx.message({
            type:"confirm",
            text: "Do you want to delete the pub?",
            title: title_msg,
            callback: function(e) {
                if(e)
                {
                    $.ajax({
                        url: url
                    }).done(function(e) {
                        obj.remove();
                    });
                }
            }
        });
    })
</script>
</body>
</html>