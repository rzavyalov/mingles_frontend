<div class="inner-content">
    <div class="title_bg1">
        <div class="wrapper">Manage Teacher</div>
    </div>
    <div class="wrapper1">
        <p><div class="add-new-button"><p><a class="addnewteachermodal" href="#" data-url="<?=base_url('admin/manageteacher/add')?>" title="Add New Teacher">Add New Teacher</a></p></div>

        <div>
            <a href="<?=base_url('admin/manageteacher/export')?>">Export</a>
        </div>
        <div class="clear"></div>
        <div>
            <p>Auto-mode</p>
            <form action="<?=base_url('admin/manageteacher/updatesettings')?>" method="post">
                <input type="checkbox" name="auto_teachers" <?= $auto_teachers?'checked="checked"':'' ?>>
                <button type="submit" class="btn btn-info">Save</button>
            </form>
        </div>
        <div class="clear"></div>
        </p>




        <table border='0' cellpadding='0' class='tablefrom'>
            <tr>
                <th>Teacher Name</th>
                <th>Teacher Phone</th>
                <th>Teacher Address</th>
                <th>Teacher City</th>
                <th>DOB</th>
                <th>Nationality</th>
                <th>NIF ID Number</th>
                <th>Gender</th>
                <th>Twitter</th>
                <th>Bank Account</th>
                <th>Edit</th>
                <th>View Profile</th>
                <th>Delete</th>
            </tr>

        <?php foreach ($teachers as $teacher) : ?>
            <tr>
            <td><?=$teacher['TeacherName']?></td>
            <td><?=$teacher['TeacherPhone']?></td>
            <td><?=$teacher['TeacherAddress']?></td>
            <td><?=$teacher['CityName']?></td>
            <td><?=$teacher['DateOfBirth']?></td>
            <td><?=$teacher['Nationality']?></td>
            <td><?=$teacher['NIF_ID_Number']?></td>
            <td><?=$teacher['Gender']?></td>
            <td><?=$teacher['Twitter']?></td>
            <td><?=$teacher['BankAccount']?></td>
            <td><a href="#" class="editteachermodal" data-url="<?=base_url('admin/manageteacher/edit/'.$teacher['user_id'])?>" title="Edit Teacher"> <img src="<?=base_url()?>assets/images/edit.png" style="height:24px;"></a></td>
            <td><a href="<?=base_url('admin/teacher/profile/'.$teacher['user_id'])?>"> <img src="<?=base_url()?>assets/images/edit.png" style="height:24px;"></a></td>
            <td><a class="deleteteacher" href="#" data-name="<?=$teacher['TeacherName']?>" title="Delete <?=$teacher['TeacherName']?>" data-url="<?=base_url('admin/manageteacher/delete/'.$teacher['TeacherID'])?>" > <img src="<?=base_url()?>assets/images/delete.png" style="height:24px;"></a></td>
            </tr>
        <?php endforeach; ?>
        </table>


        </div>
    </div>

<script>
    $('.addnewteachermodal').click(function() {
        $.pgwModal({
            url: $(this).data('url'),
            loadingContent: '<span style="text-align:center">Loading in progress</span>',
            title: $(this).attr('title'),
            closable: true,
            titleBar: false
        });
    });
    $('.editteachermodal').click(function() {
        $.pgwModal({
            url: $(this).data('url'),
            loadingContent: '<span style="text-align:center">Loading in progress</span>',
            title: $(this).attr('title'),
            closable: true,
            titleBar: false
        });
    });

    $('.deleteteacher').click(function(){
        var title_msg = $(this).attr('title');
        var url = $(this).data('url');
        var obj = $(this).parent().parent();
        dhtmlx.message({
            type:"confirm",
            text: "Do you want to delete the teacher?",
            title: title_msg,
            callback: function(e) {
                if(e)
                {
                    $.ajax({
                        url: url
                    }).done(function(e) {
                        obj.remove();
                    });
                }
            }
        });
    })
</script>
</body>
</html>