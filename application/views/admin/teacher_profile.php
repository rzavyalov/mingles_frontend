<script src="<?=base_url('/assets/js/jquery.limit-1.2.source.js')?>"></script>
<script type='text/javascript'>
    $(function() {
        $("#dob").datepicker(
            {
                firstDay: 1,
                dateFormat: "yy-mm-dd",
                changeYear: true,
                minDate: new Date(1950, 1, 25),
                maxDate: new Date(2004, 11, 31)
            });

        $('.delete_teacher').click(function(e){
            e.preventDefault();
            var title_msg = $(this).attr('title');
            var url = $(this).data('url');
            dhtmlx.message({
                type:"confirm",
                text: "Do you want to delete profile? This action cannot rollback!",
                title: title_msg,
                callback: function(e) {
                    if(e)
                    {
                        $.ajax({
                            url: url
                        }).done(function(resoponse) {
                            Jsonobj = JSON.parse(resoponse);

                            window.location.href = Jsonobj.url;
                        });
                    }
                }
            });
        })
        $('#description').limit('500','#charsLeft');
    });
    var expanded = false;
    function showCheckboxes() {
        var languages = document.getElementById("languages");
        if (!expanded) {
            languages.style.display = "block";
            expanded = true;
        } else {
            languages.style.display = "none";
            expanded = false;
        }
    }

</script>

<style>
    .multiselect {
        width: 200px;
    }
    .selectBox {
        position: relative;
    }
    .selectBox select {
        width: 100%;
        font-weight: bold;
    }
    .overSelect {
        position: absolute;
        left: 0; right: 0; top: 0; bottom: 0;
    }
    #languages {
        display: none;
        border: 1px #dadada solid;
    }
    #languages label {
        display: block;
    }
    #languages label:hover {
        background-color: #1e90ff;
    }
</style>

<div class="inner-content">
    <div class="title_bgd">
        <div class="wrapper">Bienvenido a tu panel Mingles</div>
    </div>

    <?php $this->load->view('admin/components/admin_teacherDashBoardLeft');?>

    <div class="profile-right">

    <h1>My Profile </h1>

        <form action="<?= base_url('admin/profile/update_teacher_details') ?>" data-remote="true" accept-charset="UTF-8" method="POST">
            <input type="hidden" name="user_id" value="<?= $teacher['user_id'] ?>"/>
            <table border="0" cellpadding="0" cellspacing="0" class="pop-table">
                <tr>
                    <td width="150px"><label>Teacher Name<em>*</em></label></td>
                    <td>
                        <input  type="text" name="TeacherName" style="width: 350px!important;" value="<?=$teacher['TeacherName'];?>"/>
                    </td>
                </tr>
                <tr>
                    <td><label>Teacher Phone<em>*</em></label></td>
                    <td>
                        <input  type="text" name="TeacherPhone" style="width: 350px!important;" value="<?=$teacher['TeacherPhone'];?>"/>
                    </td>
                </tr>
                <tr>
                    <td><label>Teacher Address<em>*</em></label></td>
                    <td><input  type="text" name="TeacherAddress" style="width: 350px!important;" value="<?=$teacher['TeacherAddress']?>"/>
                    </td>
                </tr>
            <tr>
                <td><label>City Name<em>*</em></label></td>
                <td>
                    <select id="ddlcity" name="city_id" style="width: 365px!important;">
                        <option value="">--Select City--</option>
                        <?php foreach ($cities as $row) { ?>
                            <option <?php
                            if ($teacher['TeacherCityID'] == $row['CityID']) {
                            ?> selected <?php
                            }
                            ?>value="<?php echo $row['CityID'] ?>"><?php echo $row['CityName']; ?></option>
                        <?php } ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td width="150px"><label>Date Of Birth</label></td>
                <td>
                    <input type="text" name="DOB" id="dob" style="width: 350px!important;"
                           value="<?= $teacher['DateOfBirth'] ?>"/>
                </td>
            </tr>
            <tr>
                <td width="150px"><label>Gender</label></td>
                <td>
                    <select id="ddlgender" name="gender" style="width: 150px">
                        <option value="m" <?php if ($teacher['Gender'] == 'm') echo 'selected'; ?>>Male
                        </option>
                        <option value="f" <?php if ($teacher['Gender'] == 'f') echo 'selected'; ?>>
                            Female
                        </option>
                    </select>
                </td>
            </tr>

                <tr>
                    <td width="150px"><label>Nationality</label></td>
                    <td>
                        <input  type="text" name="Nationality" style="width: 350px!important;" value="<?=$teacher['Nationality']?>"/>
                    </td>
                </tr>

                <tr>
                    <td width="150px"><label>NIF ID Number</label></td>
                    <td>
                        <input  type="text" name="NIF_ID_Number" style="width: 350px!important;" value="<?=$teacher['NIF_ID_Number']?>"/>
                    </td>
                </tr>

                <tr>
                    <td width="150px"><label>Bank Account</label></td>
                    <td>
                        <input  type="text" name="BankAccount" style="width: 350px!important;" value="<?=$teacher['BankAccount']?>"/>
                    </td>
                </tr>

                <tr>
                    <td width="150px"><label>Twitter</label></td>
                    <td>
                        <input  type="text" name="Twitter" style="width: 350px!important;" value="<?=$teacher['Twitter']?>"/>
                    </td>
                </tr>
                <tr>
                    <td width="150px"><label>TEFL or others</label></td>
                    <td>
                        <input  type="checkbox" name="tefl" <?= $teacher['tefl']?'checked':''?>/>
                    </td>
                </tr>

                <tr>
                    <td width="150px"><label>Workshop completed</label></td>
                    <td>
                        <input  type="checkbox" name="workshop" <?= $teacher['workshop']?'checked':''?>/>
                    </td>
                </tr>
                <tr>
                    <td width="150px"><label>Languages</label></td>
                    <td>
                        <div class="multiselect" style="width: 365px!important;">
                            <div class="selectBox" onclick="showCheckboxes()">
                                <select>
                                    <option>Select language(s)</option>
                                </select>
                                <div class="overSelect"></div>
                            </div>
                            <div id="languages">
                                <?php foreach($all_languages as $language): ?>
                                    <label for="<?=$language['LanguageName']?>">
                                        <input type="checkbox" value="<?=$language['LanguageID']?>" name="languages[]"
                                               <?= (in_array($language['LanguageID'],$languages))?' checked ':''?>
                                            /><?=$language['LanguageName']?>
                                    </label>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td width="150px"><label>Description</label></td>
                    <td>
                        <textarea id="description" style="width: 350px!important;" name="description""><?=$teacher['description']?></textarea>
                        <span id="charsLeft"></span> characters left.
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <input type="submit" name="update_profile" class="btn btn-info" value="Save"/>
                    </td>
                </tr>
                <tr align="Right">
                    <td colspan="2">&nbsp;</td>
                </tr>
            </table>

            <table border="0" cellpadding="0" cellspacing="0" class="pop-table">
                <tr>
                    <td width="150px"><label>Email Address<em>*</em></label></td>
                    <td>
                        <input type="text" name="email" style="width: 350px!important;"
                               value="<?= $teacher['email'] ?>"/>
                    </td>
                </tr>
                <tr>
                    <td><label>Current Password<em>*</em></label></td>
                    <td><input type="text" name="CurrentPassword" style="width: 350px!important;" value=""/>
                    </td>
                </tr>

                <tr>
                    <td><label>New Password<em>*</em></label></td>
                    <td><input type="text" name="CurrentPassword" style="width: 350px!important;" value=""/>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <input type="submit" value="Change" class="btn btn-info">
                        <input type="button" value="Delete" class="delete_teacher btn btn-danger" data-url="<?=base_url('admin/teacher/delete/'.$teacher['user_id'])?>">
                    </td>
                </tr>
            </table>
        </form>

    </div>
</div>

</body>
