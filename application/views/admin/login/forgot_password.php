<html>
<head>
    <title>Forgot Password</title>
</head>
<body>
<p><a href="<?php echo site_url('admin/login/forgot'); ?>">Login</a></p>
<?php
if($success){
    echo '<p>Thank you. We have sent you an email with further instructions on how to reset your password. You will be redirected after 5 seconds</p>';
    echo '<meta http-equiv="refresh" content="5; url='.site_url('admin/login').'">';
} else {
    echo form_open();
    echo form_label('Email Address', 'email') .'<br />';
    echo form_input(array('name' => 'email', 'value' => set_value('email'))) .'<br />';
    echo form_error('email');
    echo form_submit(array('type' => 'submit', 'value' => 'Reset Password'));
    echo form_close();
}
?>
</body>
</html>