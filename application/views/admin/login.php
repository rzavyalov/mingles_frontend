<!DOCTYPE html>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <title>Mingles</title>
    <link href="<?=base_url()?>assets/css/style.css" type="text/css" rel="stylesheet" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
    <link rel="stylesheet" href="/resources/demos/style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery.gritter.css" />

    <link href="<?=base_url()?>assets/css/admin/bootstrap-social-gh-pages/assets/css/font-awesome.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/css/admin/bootstrap-social-gh-pages/assets/css/docs.css" rel="stylesheet" >
    <link rel="stylesheet" href="<?=base_url()?>assets/css/admin/bootstrap-social-gh-pages/bootstrap-social.css">


    <!--page specific plugin scripts-->
    <script src="<?php echo base_url(); ?>assets/js/jquery.gritter.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script>
    <script type='text/javascript'>
        $(function() {
            $('.forgot-btn').on('click',function(e){
                e.preventDefault();
                $('#login_panel').hide();
                $('#forgot_panel').show();
            })
        });
    </script>
</head>
<body>
<div class="container">
    <!--<start header>-->
    <header>
        <div class="wrapper">
            <div class="logo"><a href="<?=base_url()?>"><img src="<?=base_url()?>assets/images/logo.png"></a></div>
            <div class="height15"></div>
        </div>
    </header>
    <!--<end header>-->
    <div class="height30">
    </div>
    <!--<start top-section> -->


    <div id="infoMessage"><?php echo $this->session->flashdata('notification');?></div>

    <div class="inner-content">
        <div class="wrapper">
            <div class="patner-title">
                <h2><em>Register or Sign In</em> <span></span></h2>
            </div>
            <div class="login_box">
                <div id="login_panel" class="login_panel">
                    <form id="form1" name="form1" method="post" action="<?=base_url()?>admin/login">
                        <!--<div class="login-title"> Login to Mingles Admin</div>-->
                        <div class="login_icon"><img src="<?=base_url()?>assets/images/login.png" width="123" height="124"></div>
                        <div>
                            <input type="text" name="username" value="" placeholder="Username or Email">                             </div>
                        <div>
                            <input type="password" name="password" value="" placeholder="Password">
                        </div>
                        <input name="login" class="submit_bt2" value="Login" type="submit">
                        <a class="submit_bt2 btn forgot-btn" href="#">Forgot password</a>
                    </form>
                </div>
                <div id="forgot_panel" class="login_panel">
                    <form id="form1w" name="form1w" method="post" action="<?=base_url('admin/login/forgot')?>">
                        <!--<div class="login-title"> Login to Mingles Admin</div>-->
                        <div class="login_icon"><img src="<?=base_url()?>assets/images/login.png" width="123" height="124"></div>
                        <div>
                            <input type="text" name="email" value="" placeholder="Username or Email">
                        </div>
                        <input name="forgot" class="submit_bt2" value="Forgot" type="submit">
                    </form>
                </div>
                <div class="register_panel">
                    <form id="frmRegister" name="frmRegister" method="post" action="<?=base_url()?>admin/login/register">

                        <div>
                            <input name="user_fullname" value="" placeholder="Nombre" type="text">
                        </div>
                        <div>
                            <input name="email" value="" placeholder="Email" type="text">
                        </div>
                        <div>
                            <input name="password" value="" placeholder="Password" type="password">
                        </div>
                        <div>
                            <input type="password" name="ConfirmPassword" value="" placeholder="Confirma Password"></p>
                        </div>
                        <div>
                            <select id="ddlUserTypr" name="usertype_id" class="selectbg">
                                <?php foreach ($user_types as $user_type) { ?>
                                    <option value="<?php echo $user_type['usertype_id'] ?>"><?php echo $user_type['usertype_name']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <input type="submit" name="commit" value="Register" class="submit_bt2">
                    </form>
                </div>


                <div class="clear"></div>
            </div>
            <div class="col-sm-4 social-buttons">
                <a class="btn btn-block btn-social btn-google-plus" href="<?=$google ?>">
                    <i class="fa fa-google-plus"></i> Sign in with Google
                </a>
                <a class="btn btn-block btn-social btn-facebook" href="<?=$facebook ?>">
                    <i class="fa fa-facebook"></i> Sign in with Facebook
                </a>
            </div>
        </div>

        <!--<start footer>-->
        <footer>
            <div class="footer">
                <div class="wrapper">
                    <div class="footer_lt">
                        MINGLES LANGUAGES<br>
                        La nueva forma de aprender Idiomas
                    </div>

                    <div class="footer_rt">
                        <img src="<?=base_url()?>assets/images/footer_logo.png" alt=""><br>
                        &copy; Mingles Languages S.L. Todos los derechos reservados.
                    </div>

                </div>

            </div>
        </footer>
        <!--<end footer>-->
    </div>
    <?php if($this->session->flashdata('notification-type') != "") { ?>
        <script>
            $.gritter.add({
                // (string | mandatory) the heading of the notification
                title: "<?php echo $this->session->flashdata('notification-title'); ?>",
                // (string | mandatory) the text inside the notification
                text: "<?php echo $this->session->flashdata('notification-text'); ?>",
                // (bool | optional) if you want it to fade out on its own or just sit there
                sticky: false,
                // (int | optional) the time you want it to be alive for before fading out
                time: '7500',
                // (string | optional) the class name you want to apply to that specific message
                class_name: "<?php echo $this->session->flashdata('notification-type'); ?>"
            });
        </script>
    <?php } ?>
</body>
</html>

</html>
