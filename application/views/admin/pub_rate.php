<script>
    $(document).ready(function() {
        $(".rateit").mousemove(function (eventObject) {

            $(this).next(".tooltip")
                .css({
                    "top": eventObject.pageY + 5,
                    "left": eventObject.pageX + 5
                })
                .show();

        }).mouseout(function () {

            $(".tooltip").hide()
                .css({
                    "top": 0,
                    "left": 0
                });
        });
        $(".blockquote").mousemove(function (eventObject) {

            $(this).next(".tooltip")
                .css({
                    "top": eventObject.pageY + 5,
                    "left": eventObject.pageX + 5
                })
                .show();

        }).mouseout(function () {

            $(".tooltip").hide()
                .css({
                    "top": 0,
                    "left": 0
                });
        });
    });
</script>

<div class="inner-content">
    <div class="title_bgd">
        <div class="wrapper">Bienvenido a tu panel Mingles</div>
    </div>

    <?php $this->load->view('admin/components/pubDashBoardLeft');?>

    <div class="profile-right">
        <h1>My Rate Details</h1>

        <div class="price-box">
            <label>Total rate</label>

            <div class="rateit" data-rateit-value="<?= $total_rate ?>" min="0" max="5" data-rateit-readonly="true"
                 style="padding-left: 50px;"></div>
        </div>
        <br/>
        <br/>





        <div class="text_container_r">
<div class="comment-description">Comments of the students:</div>

        <div class="text_container_l">
            <?php foreach ($groups as $group) : ?>
                <?php $students = $group['students']; ?>
                <?php foreach($students as $student) : ?>
                <div class="text_container_r">
                    <div class="text_l">

                        <?php if (!file_exists(FCPATH.'users/profile/'.$student['user_id'].'.jpg')) {
                            ?>
                            <img class="profile-pic-comment" src="<?=base_url()?>assets/images/defaultM.png" alt="" height="50px" width="50px">
                        <?php } else { ?>
                            <img class="profile-pic-comment" src="<?=base_url()?>users/profile/<?=$student['user_id']?>.jpg"  height="50px" width="50px" />
                        <?php } ?>
                        <div><?= $student['user_fullname'] ?></div>
                        <div class="rateit" data-rateit-value="<?= $group[$student['user_id']]['avg_rate'] ?>" min="0" max="5" data-rateit-readonly="true"></div>
                    </div>
                    <div class="text_r">
                        <div class="blockquote"><?=$group[$student['user_id']]['comment']?></div>
                        <div class="tooltip">
                            <p>Student: <?=$student['user_fullname']?></p>
                            <p>Date: <?=$group['ClassDate']?></p>
                            <p>Pub: <?=$group['PubName']?></p>
                        </div>
                    </div>
                </div>
                    <?php endforeach;?>
        <?php endforeach; ?>
        </div>
            </div>
    </div>
</div>

</body>
