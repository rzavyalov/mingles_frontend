<div class="profile-right">

<h1>Mis Mingles</h1>
<!--        <p><div class="add-new-button">  <p><a href="../index.php#option" title="Add New Class">Add New Class</a></p></div>
        <div class="clear"></div>
        </p>-->
<?php if ($this->session->userdata('usertype_id') == 4) : ?>
    <a href="<?= base_url('admin/student/dashboard/'.$user_info['user_id']) ?>" class="btn btn-info">Back</a>
<?php else: ?>
<a href="<?= base_url('admin/dashboard') ?>" class="btn btn-info">Back</a>
<?php endif;?>
<!--<img src="<? /*= base_url() */ ?>assets/images/steps2.png" alt="" height="55px" width="58%">-->

<br/>
<br/>

<p> Puedes ver qui&eacute;n es tu profesor de la sesi&oacute;n y los otros Minglers, los grupos son muy flexibles as&iacute;
    que esto puede cambiar hasta el &uacute;ltimo momento. Despu&eacute;s de tu sesi&oacute;n puedes puntuar y dejar tu
    opni&oacute;n sobre el local y sobre tu profesor, as&iacute; como de tu experiencia en general &iexcl;Seguimos
    mejorando por y par ti!</p>
<br/>

<h1><?=$this->lang->line('details_sessions')?></h1>

<p>&nbsp;</p>

<div style="padding-top: 20px;font-size: 16px; float: left; width: 100%;">
    <p><strong><?=$this->lang->line('class')?>: </strong><?= $class_details['ClassName'] ?></p>

    <p><strong><?=$this->lang->line('date')?>: </strong><?= $class_details['ClassDate'] ?></p>

    <p><strong><?=$this->lang->line('time')?>: </strong><?= $class_details['ClassStartTime'] ?></p>

    <p><strong><?=$this->lang->line('place')?>: </strong><?= $class_details['PubName'] ?></p>

    <p><strong><?=$this->lang->line('city')?>: </strong><?= $class_details['CityName'] ?></p>

    <p><strong><?=$this->lang->line('language')?>: </strong><?= $class_details['LanguageName'] ?></p>



    <?php if ($can_cancel) : ?>
        <a class="btn btn-info cancelclass" href="#" data-name="<?= base_url('admin/userclass/' . $id) ?>" data-url="<?= base_url('admin/userclass/cancel/' . $id) ?>">Cancelar</a>
    <?php endif ?>
</div>




<p>&nbsp;</p>
    <div class="student_link active"><a href="#">Students</a></div>
    <div class="teacher_link"><a href="#">Teachers</a></div>
    <div class="student_table">
<table border='0' cellpadding='0' class='tablefrom'>
    <tr>
        <th>Foto perfil</th>
        <th>Mingler</th>
    </tr>

    <?php if(!isset($no_group)):?>
    <?php foreach ($students as $student) : ?>
        <tr>
            <td>
                <?php if (!file_exists(FCPATH . 'users/profile/' . $student['uid'] . '.jpg')) {
                    ?>
                    <img src="<?= base_url() ?>assets/images/defaultM.png" alt="" height="50px" width="50px">
                <?php } else { ?>
                    <img src="<?= base_url() ?>users/profile/<?= $student['uid'] ?>.jpg" height="50px"
                         width="50px"/>
                <?php } ?>
            </td>
            <td><?= $student['user_fullname'] ?></td>
        </tr>
    <?php endforeach; ?>
    <?php endif;?>

</table>
        </div>

    <div class="teacher_table">
        <table border='0' cellpadding='0' class='tablefrom'>
            <tr>
                <th>Foto perfil</th>
                <th>Mingler</th>
            </tr>

            <?php if(!isset($no_group)):?>
                <?php foreach ($teachers as $teacher) : ?>
                    <tr>
                        <td>
                            <?php if (!file_exists(FCPATH . 'users/profile/' . $student['uid'] . '.jpg')) {
                                ?>
                                <img src="<?= base_url() ?>assets/images/defaultM.png" alt="" height="50px" width="50px">
                            <?php } else { ?>
                                <img src="<?= base_url() ?>users/profile/<?= $teacher['uid'] ?>.jpg" height="175px"
                                     width="50px"/>
                            <?php } ?>
                        </td>
                        <td><?= $teacher['user_fullname'] ?></td>
                    </tr>
                <?php endforeach; ?>
            <?php endif;?>

        </table>
    </div>


</div>
</div>

<script src="<?=base_url('/assets/js/jquery.limit-1.2.source.js')?>"></script>


<script>
    $('.teacher_link').click(function(){
        $('.student_link').removeClass('active');
        $(this).addClass('active');
        $('.teacher_table').show();
        $('.student_table').hide();
    });

    $('.student_link').click(function(){
        $('.teacher_link').removeClass('active');
        $(this).addClass('active');
        $('.student_table').show();
        $('.teacher_table').hide();
    });
</script>
<script>
$(function(){
    $('#text_comment').limit('700','#charsLeft');
    $('#pub_comment').limit('700','#PubCharsLeft');

    $('.change_status').click(function() {
        $.pgwModal({
            url: $(this).data('url'),
            loadingContent: '<span style="text-align:center">Loading in progress</span>',
            title: 'Edit class',
            closable: true,
            titleBar: false
        });
    });
})




    $('.cancelclass').click(function () {
        var title_msg = $(this).attr('title');
        var url = $(this).data('url');

        var obj = $(this).data('name');
        dhtmlx.message({
            type: "confirm",
            text: "¿Estás seguro que quieres cancelar tu sesión Mingles?",
            title: title_msg,
            callback: function (e) {
                if (e) {
                    $.ajax({
                        url: url
                    }).done(function (e) {
                        window.location = obj;
                    });
                }
            }
        });
    })

    $('#ratebutton').click(function(e) {


            var title_msg = $(this).attr('title');
            var url = $(this).data('url');
            var obj = $(this).parent().parent();
            dhtmlx.message({
                type:"confirm",
                text: "Quieres guardar los comentarios sobre tu profesor y el local?",
                title: title_msg,
                callback: function(e) {
                    if(e)
                    {
                        if($('#rate_form').length) {
                            $.ajax({
                                url: '/admin/teacher/updaterate',
                                data: $('#rate_form').serialize(),
                                type: 'post',
                                async: false
                            });
                        }
                        if($('#pub_form').length) {
                            $.ajax({
                                url: '/admin/pub/updaterate',
                                data: $('#pub_form').serialize(),
                                type: 'post',
                                async: false
                            }).done(function () {
                                window.location.href = "/admin/userclass/" + $('#class_id').val();
                            });
                        }
                    }
                }
            });



    })
</script>
<link rel="stylesheet" href="/assets/css/pub_class.css">
</body>
