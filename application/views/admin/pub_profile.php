<script src="<?=base_url('/assets/js/jquery.limit-1.2.source.js')?>"></script>
<script type='text/javascript'>


</script>

<style>
    .multiselect {
        width: 200px;
    }
    .selectBox {
        position: relative;
    }
    .selectBox select {
        width: 100%;
        font-weight: bold;
    }
    .overSelect {
        position: absolute;
        left: 0; right: 0; top: 0; bottom: 0;
    }
    #languages {
        display: none;
        border: 1px #dadada solid;
    }
    #languages label {
        display: block;
    }
    #languages label:hover {
        background-color: #1e90ff;
    }
</style>

<div class="inner-content">
    <div class="title_bgd">
        <div class="wrapper">Bienvenido a tu panel Mingles</div>
    </div>

    <?php $this->load->view('admin/components/pubDashBoardLeft');?>

    <div class="profile-right">

    <h1>My Profile </h1>

        <form action="<?= base_url('admin/profile/update_pub_details') ?>" data-remote="true" accept-charset="UTF-8" method="POST">
            <input type="hidden" name="user_id" value="<?= $pub['user_id'] ?>"/>
            <table border="0" cellpadding="0" cellspacing="0" class="pop-table">
                <tr>
                    <td width="150px"><label>Pub Name<em>*</em></label></td>
                    <td>
                        <input  type="text" name="PubName" style="width: 350px!important;" value="<?=$pub['PubName'];?>"/>
                    </td>
                </tr>
                <tr>
                    <td width="150px"><label>Pub Name<em>*</em></label></td>
                    <td>
                        <input  type="text" name="ContactPersonName" style="width: 350px!important;" value="<?=$pub['ContactPersonName'];?>"/>
                    </td>
                </tr>
                <tr>
                    <td width="150px"><label>Pub Address<em>*</em></label></td>
                    <td>
                        <input  type="text" name="PubAddress" style="width: 350px!important;" value="<?=$pub['PubAddress'];?>"/>
                    </td>
                </tr>

                <tr>
                    <td><label>Pub Phone<em>*</em></label></td>
                    <td>
                        <input  type="text" name="PubPhoneNumber" style="width: 350px!important;" value="<?=$pub['PubPhoneNumber'];?>"/>
                    </td>
                </tr>
                <tr>
                    <td><label>Longitude<em>*</em></label></td>
                    <td>
                        <input  type="text" name="longitude" style="width: 350px!important;" value="<?=$pub['Longitude'];?>"/>
                    </td>
                </tr>
                <tr>
                    <td><label>Latitude<em>*</em></label></td>
                    <td>
                        <input  type="text" name="latitude" style="width: 350px!important;" value="<?=$pub['Latitude'];?>"/>
                    </td>
                </tr>


            <tr>
                <td><label>City Name<em>*</em></label></td>
                <td>
                    <select id="ddlcity" name="city_id" style="width: 365px!important;">
                        <option value="">--Select City--</option>
                        <?php foreach ($cities as $row) { ?>
                            <option <?php
                            if ($pub['CityID'] == $row['CityID']) {
                            ?> selected <?php
                            }
                            ?>value="<?php echo $row['CityID'] ?>"><?php echo $row['CityName']; ?></option>
                        <?php } ?>
                    </select>
                </td>
            </tr>
                <tr>
                    <td width="150px"><label>Drink price</label></td>
                    <td>
                        <input type="text" id="drink_price" style="width: 350px!important;" name="drink_price" value="<?=$pub['drink_price']?>">
                    </td>
                </tr>
                <tr>
                    <td width="150px"><label>Description</label></td>
                    <td>
                        <textarea id="description" style="width: 350px!important;" name="description"><?=$pub['description']?></textarea>
                        <span id="charsLeft"></span> characters left.
                    </td>
                </tr>

<!--                <tr>
                    <td width="150px"><label>Description</label></td>
                    <td>
                        <textarea id="description" style="width: 350px!important;" name="description""><?/*=$teacher['description']*/?></textarea>
                        <span id="charsLeft"></span> characters left.
                    </td>
                </tr>-->
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <input type="submit" name="update_profile" class="btn btn-info" value="Save"/>
                    </td>
                </tr>
                <tr align="Right">
                    <td colspan="2">&nbsp;</td>
                </tr>
            </table>

            <table border="0" cellpadding="0" cellspacing="0" class="pop-table">
                <tr>
                    <td width="150px"><label>Email<em>*</em></label></td>
                    <td>
                        <input type="text" name="email" style="width: 350px!important;"
                               value="<?= $pub['email'] ?>"/>
                    </td>
                </tr>
                <tr>
                    <td><label><?=$this->lang->line('actual_password')?><em>*</em></label></td>
                    <td><input type="text" name="CurrentPassword" style="width: 350px!important;" value=""/>
                    </td>
                </tr>

                <tr>
                    <td><label><?=$this->lang->line('new_password')?><em>*</em></label></td>
                    <td><input type="text" name="NewPassword" style="width: 350px!important;" value=""/>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <input type="submit" value="Change" class="btn btn-info">
                    </td>
                </tr>
            </table>
        </form>

    </div>
</div>

</body>
