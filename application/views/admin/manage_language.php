<div class="inner-content">
    <div class="title_bg1">
        <div class="wrapper">Manage Language</div>
    </div>
    <div class="wrapper1">
        <p><div class="add-new-button"> <p><a href="#" data-url="<?=base_url('admin/managelanguage/add')?>" class="editlanguagemodal" title="Add New Language">Add New Language</a></p></div>
        <div class="clear"></div>
        </p>




        <table border='0' cellpadding='0' class='tablefrom'>
            <tr>
                <th>Language ID</th>
                <th>Language Name</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>

        <?php foreach ($languages as $language) : ?>
            <tr>
            <td><?=$language['LanguageID']?></td>
            <td><?=$language['LanguageName']?></td>
            <td><a href="#" data-id="<?=$language['LanguageID']?>" data-url="<?=base_url('admin/managelanguage/edit/'.$language['LanguageID'])?>" class="editlanguagemodal" title="Edit Language"> <img src="<?=base_url()?>assets/images/edit.png" style="height:24px;"></a></td>
            <td><a class="deletelanguage" href="#" data-url="<?=base_url()?>admin/managelanguage/delete/<?=$language['LanguageID']?>" title="Delete Language"> <img src="<?=base_url()?>assets/images/delete.png" style="height:24px;" ></a></td>
            </tr>
        <?php endforeach; ?>
        </table>


        </div>
    </div>
<script>
    $('.editlanguagemodal').click(function() {
        $.pgwModal({
            url: $(this).data('url'),
            loadingContent: '<span style="text-align:center">Loading in progress</span>',
            title: $(this).attr('title'),
            closable: true,
            titleBar: false
        });
    });

    $('.deletelanguage').click(function(){
        var title_msg = $(this).attr('title');
        var url = $(this).data('url');
        var obj = $(this).parent().parent();
        dhtmlx.message({
            type:"confirm",
            text: "Do you want to delete the Language?",
            title: title_msg,
            callback: function(e) {
                if(e)
                {
                    $.ajax({
                        url: url
                    }).done(function(e) {
                        obj.remove();
                    });
                }
            }
        });
    })
</script>
</body>
</html>