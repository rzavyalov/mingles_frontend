<div class="inner-content">
    <div class="title_bg1">
        <div class="wrapper">Manage Emails</div>
    </div>
    <div class="wrapper1">

        <table border='0' cellpadding='0' class='tablefrom'>
            <tr>
                <th>Email Code ID</th>
                <th>Email Name </th>
                <th>ON</th>
                <th>Edit</th>
            </tr>

        <?php foreach ($emails as $email) : ?>
            <tr>
            <td><?=$email['email_id']?></td>
            <td><?=$email['email_name']?></td>
            <td>
                <input type="checkbox" name="on" class="email_on" data-id="<?=$email['email_id']?>"
                    <?php if($email['email_on']) :?> checked <?php endif?>>
            </td>
            <td><a href="#" data-url="<?=base_url('admin/email/edit/'.$email['email_id'])?>" class="bonopopup" title="Edit Email"> <img src="<?=base_url()?>assets/images/edit.png" style="height:24px;"></a></td>
            </tr>
        <?php endforeach; ?>
        </table>


        </div>
    </div>

<script>
    $('.email_on').click(function() {
        var id = $(this).data('id');
        if($(this).attr('checked') == 'checked')
        {
            $.ajax({
                type: "POST",
                url: "<?=base_url('admin/email/enable/')?>",
                data: {'id':id}
            });
        }
        else
        {
            $.ajax({
                type: "POST",
                url: "<?=base_url('admin/email/disable/')?>",
                data: {'id':id}
            });
        }
    });
    $('.bonopopup').click(function() {
        $.pgwModal({
            url: $(this).data('url'),
            loadingContent: '<span style="text-align:center">Loading in progress</span>',
            title: $(this).attr('title'),
            closable: true,
            titleBar: false
        });
    });
</script>
</body>
</html>