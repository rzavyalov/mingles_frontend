<script>
    $(document).ready(function() {
        $(".rateit").mousemove(function (eventObject) {

            $(this).next(".tooltip")
                .css({
                    "top": eventObject.pageY + 5,
                    "left": eventObject.pageX + 5
                })
                .show();

        }).mouseout(function () {

            $(".tooltip").hide()
                .css({
                    "top": 0,
                    "left": 0
                });
        });
        $(".blockquote").mousemove(function (eventObject) {

            $(this).next(".tooltip")
                .css({
                    "top": eventObject.pageY + 5,
                    "left": eventObject.pageX + 5
                })
                .show();

        }).mouseout(function () {

            $(".tooltip").hide()
                .css({
                    "top": 0,
                    "left": 0
                });
        });
    });
</script>
<div class="inner-content">
    <div class="title_bgd">
        <div class="wrapper"><?=$this->lang->line('welcome_panel')?></div>
    </div>

    <?php if ($this->session->userdata('usertype_id') == 1) : ?>
        <?php $this->load->view('admin/components/userDashBoardLeft'); ?>
    <?php elseif ($this->session->userdata('usertype_id') == 4) : ?>
        <?php $this->load->view('admin/components/admin_userDashBoardLeft'); ?>
    <?php
    elseif ($this->session->userdata('usertype_id') == 2) : ?>
        <?php $this->load->view('admin/components/teacherDashBoardLeft'); ?>
    <?php endif; ?>

    <div class="profile-right">
        <?php if(!$user_info['disable_rate']) :?>
        <h1><?=$this->lang->line('evaluation_details')?></h1>
        <div class="price-box">
            <label><?=$this->lang->line('punctuation_total')?></label>
            <div class="rateit" data-rateit-value="<?=$total_rate?>" min="0" max="5" data-rateit-readonly="true" style="padding-left: 50px;"></div>
        </div>
        <br/>
        <br/>
        <div style="width:300px; float:right">
            <p><strong>Listening</strong></p>
            <div class="rateit" data-rateit-value="<?=$rate_listening?>" min="0" max="5" data-rateit-readonly="true" style="padding-left: 50px;"></div>
            <p><strong>Pronunciation</strong></p>
            <div class="rateit" data-rateit-value="<?=$rate_pronuncation?>" min="0" max="5" data-rateit-readonly="true" style="padding-left: 50px;"></div>
            <p><strong>Rate Speaking</strong></p>
            <div class="rateit" data-rateit-value="<?=$rate_speaking?>" min="0" max="5" data-rateit-readonly="true" style="padding-left: 50px;"></div>
            <p><strong>Vocabulary</strong></p>
            <div class="rateit" data-rateit-value="<?=$rate_vocabulary?>" min="0" max="5" data-rateit-readonly="true" style="padding-left: 50px;"></div>
        </div>

        <div class="text_container_r">
            <div class="comment-description">Comments of the teachers:</div>

            <div class="text_container_l">
                <?php foreach ($groups as $group) : ?>


                    <div class="text_container_r">
                        <div class="text_l">

                            <?php if (!file_exists(FCPATH.'users/profile/'.$group['user_id'].'.jpg')) {
                                ?>
                                <img class="profile-pic-comment" src="<?=base_url()?>assets/images/defaultM.png" alt="" height="50px" width="50px">
                            <?php } else { ?>
                                <img class="profile-pic-comment" src="<?=base_url()?>users/profile/<?=$group['user_id']?>.jpg"  height="50px" width="50px" />
                            <?php } ?>
                            <div><?= $group['user_fullname'] ?></div>
                            <div class="rateit" data-rateit-value="<?= @$group[$group['group_id']]['avg_rate'] ?>" min="0" max="5" data-rateit-readonly="true"></div>
                            <div class="tooltip">
                                <p>Listening: <?=$cat_rates[$group['group_id']][1]?></p>
                                <p>Pronunciation: <?=$cat_rates[$group['group_id']][2]?></p>
                                <p>Rate Speaking: <?=$cat_rates[$group['group_id']][3]?></p>
                                <p>Vocabulary: <?=$cat_rates[$group['group_id']][4]?></p>
                            </div>
                        </div>
                        <div class="text_r">
                            <div class="blockquote"><?=$group[$group['group_id']]['comment']?></div>
                            <div class="tooltip">
                                <p>Teacher: <?=$group['user_fullname']?></p>
                                <p>Date: <?=$group['ClassDate']?></p>
                                <p>Pub: <?=$group['PubName']?></p>
                            </div>
                        </div>
                    </div>




                <?php endforeach; ?>
            </div>
        </div>
        <?php else :?>
            <h1>You need activate rating in <a href="<?=base_url('admin/profile')?>">Editar mi perfil</a> page</h1>
        <?php endif;?>
    </div>
</div>

</body>
