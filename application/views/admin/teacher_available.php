<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery.gritter.css" />
<script src="<?php echo base_url(); ?>assets/js/jquery.gritter.min.js"></script>

<script>
    function validateForm() {
        var error = false;
        if ($('#chkTerm').is(':checked')) {
            $.ajax({
                url: "/json/dateclass/availableteachers_slots",
                data: "class_id=" + $('#ddlClass').val(),
                cache: false,
                global: false,
                type: "POST",
                async: false,
                success: function (data) {
                    if (data != 'true') {
                        alert(data);
                        error = true;
                        return false;
                    }
                }
            });
        }
        else
        {
            alert('Tienes que marcar la casilla para aceptar los términos y condiciones de Mingles.es');
            return false;
        }

        if(error)
        {
            return false;
        }
    }


</script>

<?php if($this->session->flashdata('notification-type') != "") { ?>
    <script>
        $.gritter.add({
            // (string | mandatory) the heading of the notification
            title: "<?php echo $this->session->flashdata('notification-title'); ?>",
            // (string | mandatory) the text inside the notification
            text: "<?php echo $this->session->flashdata('notification-text'); ?>",
            // (bool | optional) if you want it to fade out on its own or just sit there
            sticky: false,
            // (int | optional) the time you want it to be alive for before fading out
            time: '7500',
            // (string | optional) the class name you want to apply to that specific message
            class_name: "<?php echo $this->session->flashdata('notification-type'); ?>"
        });
    </script>
<?php } ?>

<div class="inner-content">
    <div class="title_bgd">
        <div class="wrapper">Bienvenido a tu panel Mingles</div>
    </div>

    <?php $this->load->view('admin/components/admin_teacherDashBoardLeft');?>

    <div class="profile-right">
        <h1>My Schedule</h1>
        <br/>
        <br/>
        <table border='0' cellpadding='0' class='tablefrom'>
            <tr>
                <th>Class Name</th>
                <th>Class Date</th>
                <th>Class start time</th>
                <th>Class end time</th>
                <th>Pub</th>
                <th>Status</th>
                <th></th>
            </tr>

            <?php foreach ($subscribes_classes as $subscribes_class) : ?>

                <tr>
                    <td><?=$subscribes_class['ClassName']?></td>
                    <td><?=$subscribes_class['ClassDate']?></td>
                    <td><?=$subscribes_class['ClassStartTime']?></td>
                    <td><?=$subscribes_class['ClassEndTime']?></td>
                    <td><?=$subscribes_class['PubName']?></td>
                    <td><?=$subscribes_class['status']?></td>
                    <td><?php if($subscribes_class['status'] != 'assigned') :?>
                        <a class="unsubscribe_btn" href="<?=base_url('admin/profile/unsubscribe/'.$subscribes_class['teacher_available_id'])?>"><img style="height:24px;" src="<?=base_url('assets/images/undo.png')?>"></a>
                    <?php endif;?>
                    </td>
                </tr>
            <?php endforeach;?>
        </table>
        <div style="margin-top: 50px;"></div>
        <form name="myForm" action="<?=base_url('admin/profile/availability')?>" onsubmit="return validateForm()" method="post">
            <input type="hidden" name="user_id" value="<?=$this->session->userdata('user_id')?>" />
        <div class="blue-box">
            <div class="wrapper">
                <div class="regis-box1" id="option">
                    <div class="number">
                        <img src="<?=base_url()?>assets/images/1.png"></div>
                    <h2> Elige tu Ciudad </h2>
                    <select name="ddlCity" id="ddlCity" class="selectbg" ></select>

                    <input type="hidden" name="hndCityID" id="hndCityID">
                    <h2 style="padding-top:10px;">
                        Selecciona la fecha de tu Mingles</h2>
                    <div id="datepicker" class="date"></div>
                    <div id="event"> </div>
                    <!--  <div class="date">
                    <img src="images/calender.png" alt=""></div>-->
                    <!--                <div class="map" id="map_canvas" style="width:304px;height:250px;">
                                    </div>-->
                    <div  id="map_canvas" style="width:304px;height:250px;">
                    </div>
                </div>
                <div class="border">
                    <img src="<?=base_url()?>assets/images/border.png" alt=""></div>
                <div class="regis-box1" id="option1">
                    <div class="number">
                        <img src="<?=base_url()?>assets/images/2.png"></div>
                    <h2>
                        Selecciona los datos:</h2>
                    <div class="form">
                        <h3>
                            Mingles<br>
                            <!--                    <h2> <input type="label" name="selDate" id="selDate" value=""></h2><br>-->
                            Elige tu lugar Mingles:</h3>
                        <select name="ddlPub" id="ddlPub" class="selectbg" >
                        </select>
                        <!--                <input type="submit" name="info" class="submit_bt" value="Info">-->
                        <div class="height20">
                        </div>
                        <h3>Elige tu Sesi&oacute;n:</h3>
                        <select name="ddlClass" id="ddlClass" class="selectbg">
                        </select>

                        <h3>Idioma</h3>
                        <select name="ddlLanguage" id="ddlLanguage" class="selectbg">
                        </select>

                    </div>
                </div>
                <div class="border">
                    <img src="<?=base_url()?>assets/images/border.png" alt=""></div>
                <div class="regis-box1" id="option2">
                    <div class="number">
                        <img src="<?=base_url()?>assets/images/3.png"></div>
                    <h2>
                        Introduce tus datos:</h2>
                    <div class="form">
                        <div class="radio">
                            <p>
                                <input name="" type="checkbox" value="" id="chkTerm">
                                Acepto los Términos y Condiciones y Política de Privacidad</p>
                        </div>
                        <input type="submit" name="info" id="joinus" class="submit_bt" value="Apuntarme" >
                        <div class="height15">
                        </div>

                    </div>
                </div>
                <div class="height0">
                </div>
            </div>
        </div>



    </div>
</div>

</body>
