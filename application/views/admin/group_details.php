<div class="inner-content">
    <div class="title_bg1">
        <div class="wrapper">Group Details: <?=$group['GroupName']?></div>
    </div>
    <div class="wrapper1">
        <p>

        <!--<div class="add-new-button"><p><a href="<?/*= base_url('admin/managegroup/add') */?>" rel="gb_page_center[600, 300]"
                                          title="Add New Group">Add New Group</a></p></div>-->
        <div class="clear"></div>

        </p>
        <div style="float: left; padding-right: 20px">
            <div class="span3 center profile-pic">
												<span id="user_avatar_container"
                                                      <?php if (@$teacher['user_id'] != TRUE) { ?>style="display:none;"<?php } ?>>
                                                                    <?php if (!file_exists(FCPATH . 'users/profile/' . $teacher['user_id'] . '.jpg')) {
                                                                        ?>
                                                                        <img
                                                                            src="<?= base_url() ?>assets/images/defaultM.png"
                                                                            alt="" height="175px" width="175px">
                                                                    <?php } else { ?>
                                                                        <img
                                                                            src="<?= base_url() ?>users/profile/<?= $teacher['user_id'] ?>.jpg"
                                                                            height="175px" width="175px"/>
                                                                    <?php } ?>
												</span>
            </div>
            <h2 style="color: #34a6fb;font-size: 17px;text-align: center"><?= @$teacher['TeacherName'] ?></h2>
        </div>
        <div style="padding-top: 20px;font-size: 16px">
            <p><strong>Pub Name: </strong><?= $group['PubName'] ?></p>
            <p><strong>City: </strong><?= $group['CityName'] ?></p>
            <p><strong>Class Name: </strong><?= $group['ClassName'] ?></p>
            <p><strong>Level Name: </strong><?= $group['LevelName'] ?></p>
            <p><strong>Date: </strong><?= $group['ClassDate'] ?></p>

<!---->
<!--            <p><strong>Class date: </strong>--><?//= $class_details['ClassDate'] ?><!--</p>-->
<!---->
<!--            <p><strong>Class Time: </strong>--><?//= $class_details['ClassStartTime'] ?><!--</p>-->
<!---->
<!--            <p><strong>Pub Name: </strong>--><?//= $class_details['PubName'] ?><!--</p>-->
<!---->
<!--            <p><strong>City: </strong>--><?//= $class_details['CityName'] ?><!--</p>-->
<!---->
<!--            <p><strong>Language: </strong>--><?//= $class_details['LanguageName'] ?><!--</p>-->
<!---->
<!--            <p><strong>Level: </strong>--><?//= $level['LevelName'] ?><!--</p>-->
<!--            <p><strong>Status: </strong><strong style="color:green">--><?//=($is_payed) ? 'Payed' : 'not payed'?><!--</strong></p>-->
        </div>

        <a href="<?=$back_button?>" class="btn btn-info">Back</a>
        <table border='0' cellpadding='0' class='tablefrom'>
            <tr>
                <th>Student profile pic.</th>
                <th>Student name</th>
                <?php if($this->session->userdata('usertype_id') == 4) :?>
                <th>Email</th>
                <?php endif; ?>
                <th>Level</th>
                <th>Payment status</th>
                <th>Status</th>
            </tr>

            <?php foreach ($students as $student) : ?>
                <tr>
                    <td><a href="<?=base_url('admin/managegroup/student/'.$student['user_id']."/".$group['GroupID']) ?>">
                            <?php if (!file_exists(FCPATH . 'users/profile/' . $student['user_id'] . '.jpg')) {
                                ?>
                                <img src="<?= base_url() ?>assets/images/defaultM.png" alt="" height="175px" width="175px">
                            <?php } else { ?>
                                <img src="<?= base_url() ?>users/profile/<?= $student['user_id'] ?>.jpg" height="175px"
                                     width="175px"/>
                            <?php } ?>
                        </a>
                    </td>
                    <td><?= $student['user_fullname'] ?></td>
                    <?php if($this->session->userdata('usertype_id') != 4) :?>

                    <?php elseif($this->session->userdata('usertype_id') == 4) :?>
                    <td><?= $student['email'] ?></td>
                    <?php endif;?>
                    <td><?= $student['level'] ?></td>
                    <td><?= $student['status'] ?></td>
                    <td>
                        <form action="<?= base_url('admin/managegroup/update_student_status/'.$group['GroupID']) ?>" data-remote="true" accept-charset="UTF-8"
                              method="POST">
                            <input type="hidden" name="user_id" value="<?= $student['user_id'] ?>"/>
                            <table border="0" cellpadding="0" cellspacing="0" class="table">
                                <tr>
                                    <td>
                                        <select id="studentstatus" name="studentstatus" style="width: 365px!important;">
                                            <option value="1">SUBSCRIBED</option>
                                            <?php foreach ($statuses as $status) { ?>
                                                <?php if($status['user_status_id'] == 1): continue; endif;?>
                                                <option <?php
                                                if ($student['StudentStatus'] == $status['user_status_id']) {
                                                ?> selected <?php
                                                }
                                                ?>value="<?php echo $status['user_status_id'] ?>"><?php echo $status['user_status_name']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </td>

                                    <td>
                                        <input type="submit" name="update_profile" <?php if (isset($_GET['id'])) { ?>value="Update"
                                               <?php } else { ?>value="Save"<?php } ?> class="btn btn-info"
                                               onclick="parent.parent.GB_hide();">
                                    </td>
                                </tr>
                            </table>
                        </form>
                    </td>
                </tr>
            <?php endforeach; ?>

        </table>


    </div>
</div>

</body>
</html>