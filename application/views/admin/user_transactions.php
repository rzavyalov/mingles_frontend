<div class="inner-content">
    <div class="title_bgd">
        <div class="wrapper"><?=$this->lang->line('welcome_panel')?></div>
    </div>

    <?php if ($this->session->userdata('usertype_id') == 4) {
        $this->load->view('admin/components/admin_userDashBoardLeft');
    } else {
        $this->load->view('admin/components/userDashBoardLeft');
    }
    ?>

    <div class="profile-right">
        <h1>Detalles de mis Pagos</h1>
        <div class="price-box">
            <label>Importe total recibido</label> <input type="text" name="price">
        </div>
        <br/>
        <br/>




        <table border='0' cellpadding='0' class='tablefrom'>
           <tr>
               <th>Clase</th>
               <th>Fecha</th>
               <th>Cantidad Total</th>
               <th>C&oacute;digo Promo</th>
               <th>Precio</th>
               <th>Cantidad Pagada</th>
               <th>Estado Pago</th>
               <th>PayPal ID-Transaci&oacute;n</th>
               <th>Fecha Transacci&oacute;n</th>
           </tr>

                <?php foreach ($transactions as $transaction) : ?>

                <tr>
                <td><?=$transaction['ClassName']?></td>
                <td><?=$transaction['ClassDate']?></td>
                <td><?=$transaction['TotalAmount']?></td>
                <td><?=$transaction['PromoCode']?></td>
                <td><?=$transaction['BillAmount']?></td>
                <td><?=$transaction['PaymentAmount']?></td>
                <td><?=$transaction['PaymentStatus']?></td>
                <td><?=$transaction['PayPalTransactionID']?></td>
                <td><?=$transaction['TransactionDate']?></td>
                </tr>
            <?php endforeach;?>
            </table>
    </div>

</div>


</body>
</html>
