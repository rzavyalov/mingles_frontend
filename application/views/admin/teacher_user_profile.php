<div class="inner-content">
    <div class="title_bgd">
        <div class="wrapper">Bienvenido a tu panel Mingles</div>
    </div>

    <?php $this->load->view('admin/components/userDashBoardLeft');?>

    <div class="profile-right">
    <h1>My Profile </h1>


    <form action="<?=base_url('admin/profile/update_profile_details')?>" data-remote="true" accept-charset="UTF-8" method="POST">
        <input type="hidden" name="user_id" value="<?=$user_info['user_id']?>" />
        <table  border="0" cellpadding="0" cellspacing="0" class="pop-table">
            <tr>
                <td width="150px"><label>Full Name<em>*</em></label></td>
                <td>
                    <input  type="text" name="user_fullname" style="width: 350px!important;" value="<?=$user_info['user_fullname']?>"/>
                </td>
            </tr>
            <tr>
                <td><label>Phone No</label></td>
                <td>
                    <input  type="text" name="phone" style="width: 350px!important;" value="<?=$user_info['phone']?>"/>
                </td>
            </tr>
            <tr>
                <td><label>Address</label></td>
                <td>
                    <input  type="text" name="address" style="width: 350px!important;" value="<?=$user_info['address']?>"/>
                </td>
            </tr>
            <tr>
                <td><label>City Name<em>*</em></label></td>
                <td>
                    <select id="ddlcity" name="city_id" style="width: 365px!important;" >
                        <option value="">--Select City--</option>
                        <?php foreach ($cities as $row) { ?>
                            <option <?php
                            if ($user_info['city_id'] == $row['CityID']) {
                            ?> selected  <?php
                            }
                            ?>value="<?php echo $row['CityID'] ?>"><?php echo $row['CityName']; ?></option>
                        <?php } ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td width="150px"><label>Date Of Birth</label></td>
                <td>
                    <input  type="text" name="DOB" id="dob" style="width: 350px!important;"value="<?=$user_info['DOB']?>"/>
                </td>
            </tr>

            <tr>
                <td width="150px"><label>Gender</label></td>
                <td>
                    <select id="ddlgender" name="gender"  style="width: 150px">
                        <option value="m" <?php if ($user_info['gender'] == 'm') echo 'selected'; ?>>Male</option>
                        <option value="f" <?php if ($user_info['gender'] == 'f') echo 'selected'; ?>>Female</option>
                    </select>
                </td>
            </tr>

            <tr>
                <td width="150px"><label>Twitter</label></td>
                <td>
                    <input  type="text" name="twitter" style="width: 350px!important;"value="<?=$user_info['twitter']?>"/>
                </td>
            </tr>

            <tr>
                <td><label>I want to learn</label></td>
                <td>
                    <select id="ddlLanguage" name="language_id" style="width: 365px!important;" >
                        <option value="">--Select Language--</option>
                        <?php foreach ($languages as $row) { ?>
                            <option <?php
                            if ($user_info['language_id'] == $row['LanguageID']) {
                            ?> selected  <?php
                            }
                            ?>value="<?php echo $row['LanguageID'] ?>"><?php echo $row['LanguageName']; ?></option>
                        <?php } ?>
                    </select>
                </td>
            </tr>

            <tr>
                <td>&nbsp;</td>
                <td>
                    <input type="submit" name="update_profile" <?php if (isset($_GET['id'])) { ?>value="Update"<?php } else { ?>value="Save"<?php } ?> class="btn btn-info" onclick="parent.parent.GB_hide();">
                </td>
            </tr>

            <tr align="Right">
                <td colspan="2">&nbsp;</td>
            </tr>
        </table>
        <h1>Change Password</h1>

        <table  border="0" cellpadding="0" cellspacing="0" class="pop-table">
            <tr>
                <td width="150px"><label>Email Address<em>*</em></label></td>
                <td >
                    <input  type="text" name="email" style="width: 350px!important;" value="<?=$user_info['email']?>"/>
                </td>
            </tr>
            <tr>
                <td><label>Current Password<em>*</em></label></td>
                <td><input  type="text" name="CurrentPassword" style="width: 350px!important;" value=""/>
                </td>
            </tr>

            <tr>
                <td><label>New Password<em>*</em></label></td>
                <td><input  type="text" name="CurrentPassword" style="width: 350px!important;" value=""/>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input type="submit" value="Change" class="btn btn-info">
                </td>
            </tr>
        </table>
    </form>
    </div>
</div>

</body>
