<div class="inner-content">
    <div class="title_bg1">
        <div class="wrapper">Manage Users</div>
    </div>

    <div class="wrapper1">
        <p><div class="add-new-button"><a class="addnewusermodal" href="#" data-url="<?=base_url('admin/managestudent/add')?>" Title="Add New Student">Add New Student</a></div>
        <div>
            <a href="<?=base_url('admin/managestudent/export')?>">Export</a>
        </div>
        <div>
            <p>Auto-mode</p>
            <form action="<?=base_url('admin/managestudent/updatesettings')?>" method="post">
                <input type="checkbox" name="auto_students" <?= $auto_students?'checked="checked"':'' ?>>
                <button type="submit" class="btn btn-info">Save</button>
            </form>
        </div>
        <div class="clear"></div>
        </p>




        <table border='0' cellpadding='0' class='tablefrom' >
            <tr>
                <th>Full Name</th>
                <th>Address</th>
                <th>City</th>
                <th>DOB</th>
                <th>Gender</th>
                <th>Phone No.</th>
                <th>Default Language</th>
                <th>Twitter</th>
                <th>Profile</th>
                <th>View Class Details</th>
                <th>Delete</th>
            </tr>

        <?php foreach ($students as $student) : ?>
            <tr>
            <td><?=$student['user_fullname']?></td>
            <td><?=$student['address']?></td>
            <td><?=$student['CityName']?></td>
            <td><?=$student['DOB']?></td>
            <td><?=$student['gender']?></td>
            <td><?=$student['phone']?></td>
            <td><?=$student['LanguageName']?></td>
            <td><?=$student['twitter']?></td>
            <td><a href="<?=base_url('admin/student/profile/'.$student['user_id'])?>" title="View Student Profile"> <img src="<?=base_url()?>assets/images/edit.png" style="height:24px;"></a></td>
            <td><a href="<?=base_url('admin/student/dashboard/'.$student['user_id'])?>" Title="View Class Details"> <img src="<?=base_url()?>assets/images/edit.png" style="height:24px;"></a></td>

            <td><a class="deletestudent" href="#" data-url="<?=base_url('admin/managestudent/delete/'.$student['user_id'])?>" title="Delete Student"> <img src="<?=base_url()?>assets/images/delete.png" style="height:24px;" ></a></td>
            </tr>
        <?php endforeach; ?>
        </table>


        </div>
    </div>

<script>
    $('.addnewusermodal').click(function() {
        $.pgwModal({
            url: $(this).data('url'),
            loadingContent: '<span style="text-align:center">Loading in progress</span>',
            title: $(this).attr('title'),
            closable: true,
            titleBar: false
        });
    });

    $('.deletestudent').click(function(e){
        e.preventDefault();
        var title_msg = $(this).attr('title');
        var url = $(this).data('url');
        var obj = $(this).parent().parent();
        dhtmlx.message({
            type:"confirm",
            text: "Do you want to delete student?",
            title: title_msg,
            callback: function(e) {
                if(e)
                {
                    $.ajax({
                        url: url
                    }).done(function(e) {
                        obj.remove();
                    });
                }
            }
        });
    })
</script>

</body>
</html>