<link rel="stylesheet" href="<?=base_url()?>assets/css/admin/rate_day.css" type="text/css" />
<script src="<?=base_url('assets/js/admin/rate_day.js')?>"></script>


<div class="inner-content">
    <div class="title_bgd">
        <div class="wrapper"><?=$this->lang->line('welcome_panel')?></div>
    </div>

    <div class="profile-right">
<input id="date" style="display: none" value="<?=date("Y-m-d")?>">
        <h1>Group of the day (<?=date('m.d.Y')?>)</h1>
        <div class="pubcontainerlinks">
            <div class="box">
<?php foreach($pubs as $pub) :?>
        <div><a data-id="<?=$pub->PubID?>" class="publinkinfoday" href="#"><?=$pub->PubName?></a></div>
        <?php endforeach; ?>
                </div>
            </div>

        <div class="text_container">
            <div class="classes"></div>
        </div>
    </div>
</div>

</body>
