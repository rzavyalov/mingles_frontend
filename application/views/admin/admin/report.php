<link rel="stylesheet" href="<?=base_url()?>assets/css/admin/rate_day.css" type="text/css" />
<script src="<?=base_url('assets/js/admin/rate_day.js')?>"></script>


<div class="inner-content">
    <div class="title_bgd">
        <div class="wrapper"><?=$this->lang->line('welcome_panel')?></div>
    </div>

    <div class="profile-right">
<input id="date" style="display: none" value="<?=date("Y-m-d")?>">
        <h1>Reports</h1>
        <div>
            <h2>generate report</h2>
            <form>
                <select name="period">
                    <option value="month">Month</option>
                    <option value="week">Week</option>
                    <option value="year">Year</option>
                </select>
                <input name="year" value="<?=$year?>">
                <button type="submit">Create report</button>
            </form>
        </div>
        <div class="">
            <div class="">
<?php foreach($files as $file) :?>
        <div><a data-id="<?=$file?>" class="" href="#"><?=$file?></a></div>
        <?php endforeach; ?>
                </div>
            </div>

        <div class="text_container">
            <div class="classes"></div>
        </div>
    </div>
</div>

</body>
