<div class="inner-content">
    <div class="title_bg1">
        <div class="wrapper">Manage Payment Session</div>
    </div>
    <div class="wrapper1">
        <p><div class="add-new-button">  <p><a href="#" title="Add New Session" data-url="<?=base_url('admin/managesession/add')?>" class="editsessionmodal">Add New Session</a></p></div>
        <div class="clear"></div>
        </p>




        <table border='0' cellpadding='0' class='tablefrom'>
            <tr>
                <th>Session Payment ID</th>
                <th>Session Payment Name</th>
                <th>Payment Amount</th>
                <th>Credit Amount</th>
                <th>Session Payment Description</th>
                <th>Payment Type</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>

        <?php foreach ($sessions as $session) : ?>
            <tr>
                <td><?=$session['SessionPaymentID']?></td>
                <td><?=$session['SessionPaymentName']?></td>
                <td><?=$session['SessionPaymentAmount']?></td>
                <td><?=$session['SessionPaymentCredit']?></td>
                <td><?=$session['SessionPaymentDescription']?></td>
                <td><?=$session['PaymentType']?></td>
                <td><a href="#" data-id="<?=$session['SessionPaymentID']?>" data-url="<?=base_url('admin/managesession/edit/'.$session['SessionPaymentID'])?>" class="editsessionmodal" title="Edit Session"> <img src="<?=base_url()?>assets/images/edit.png" style="height:24px;"></a></td>
                <td><a class="deletesession" href="#" data-url="<?=base_url('admin/managesession/delete/'.$session['SessionPaymentID'])?>" title="Delete Session"> <img src="<?=base_url()?>assets/images/delete.png" style="height:24px;" ></a></td>
            </tr>
        <?php endforeach; ?>
        </table>


        </div>
    </div>
<script>
    $('.editsessionmodal').click(function() {
        $.pgwModal({
            url: $(this).data('url'),
            loadingContent: '<span style="text-align:center">Loading in progress</span>',
            title: $(this).attr('title'),
            closable: true,
            titleBar: false
        });
    });

    $('.deletesession').click(function(){
        var title_msg = $(this).attr('title');
        var url = $(this).data('url');
        var obj = $(this).parent().parent();
        dhtmlx.message({
            type:"confirm",
            text: "Do you want to delete the session?",
            title: title_msg,
            callback: function(e) {
                if(e)
                {
                    $.ajax({
                        url: url
                    }).done(function(e) {
                        obj.remove();
                    });
                }
            }
        });
    })
</script>
</body>
</html>