<script type='text/javascript'>
    $(function () {
        $("#dob").datepicker(
            {
                firstDay: 1,
                dateFormat: "yy-mm-dd",
                changeYear: true,
                minDate: new Date(1950, 1, 25),
                maxDate: new Date(2004, 11, 31)
            });
    });

</script>
<link href="<?= base_url('assets/css/rateit.css') ?>" rel="stylesheet" type="text/css">
<input id="student_id" style="display: none" value="<?=$student_id?>">
<input id="group_id" style="display: none" value="<?=$group_id?>">
<div class="inner-content">
<div class="title_bgd">
    <div class="wrapper">Bienvenido a tu panel Mingles</div>
</div>
<?php if ($this->session->userdata('usertype_id') == 4) : ?>
    <?php $this->load->view('admin/components/admin_userDashBoardLeft'); ?>
<?php
elseif ($this->session->userdata('usertype_id') == 2) : ?>
    <?php $this->load->view('admin/components/teacherDashBoardLeft'); ?>
<?php endif; ?>
<div class="profile-right">

    <h1>User Profile: <?= $user_info['user_fullname'] ?></h1>

<?php if ($this->session->userdata('usertype_id') == 2) : ?>
    <form id='level_form' action="<?= base_url('admin/student/updatelevel') ?>" data-remote="true" accept-charset="UTF-8"
          method="POST">
        <input type="hidden" name="user_id" value="<?= $user_info['user_id'] ?>"/>
        <table border="0" cellpadding="0" cellspacing="0" class="pop-table">
            <tr>
                <td width="150px"><label>Level</label></td>
                <td>
                    <select id="ddllevel" name="level_id" style="width: 365px!important;">
                        <option value="">--Select Level--</option>
                        <?php foreach ($levels as $level) { ?>
                            <option <?php
                            if ($user_info['level_id'] == $level['LevelID']) {
                            ?> selected <?php
                            }
                            ?>value="<?php echo $level['LevelID'] ?>"><?php echo $level['LevelName']; ?></option>
                        <?php } ?>
                    </select>
                </td>
            </tr>
<!--            <tr>
                <td>&nbsp;</td>
                <td>
                    <input type="submit" name="update_profile" <?php /*if (isset($_GET['id'])) { */?>value="Update"
                           <?php /*} else { */?>value="Save"<?php /*} */?> class="btn btn-info"
                           onclick="parent.parent.GB_hide();">
                </td>
            </tr>-->
        </table>
    </form>
    <?php if(!$user_info['disable_rate']) :?>
    <?php if (!$has_rate) : ?>
        <form id='rate_form' action="<?= base_url('admin/student/updaterate') ?>" data-remote="true" accept-charset="UTF-8"
              method="POST">
            <input type="hidden" name="user_id" value="<?= $user_info['user_id'] ?>"/>
            <input type="hidden" name="group_id" value="<?= $group_info['GroupID'] ?>"/>
            <div style="width: 300px;float: left">
                <p>Speaking & Participation</p>
                <input type="range" value="1" step="1" name="rate_speaking" id="backing1">

                <div class="rateit" data-rateit-backingfld="#backing1" data-rateit-resetable="false"
                     data-rateit-ispreset="true"
                     data-rateit-min="0" data-rateit-max="5">
                </div>
                <p>Pronunciation</p>
                <input type="range" value="1" step="1" name="pronunciation" id="backing2">

                <div class="rateit" data-rateit-backingfld="#backing2" data-rateit-resetable="false"
                     data-rateit-ispreset="true"
                     data-rateit-min="0" data-rateit-max="5">
                </div>
                <p>Vocabulary</p>
                <input type="range" value="1" step="1" name="vocabulary" id="backing3">

                <div class="rateit" data-rateit-backingfld="#backing3" data-rateit-resetable="false"
                     data-rateit-ispreset="true"
                     data-rateit-min="0" data-rateit-max="5">
                </div>
                <p>Listening</p>
                <input type="range" value="1" step="1" name="listening" id="backing4">

                <div class="rateit" data-rateit-backingfld="#backing4" data-rateit-resetable="false"
                     data-rateit-ispreset="true"
                     data-rateit-min="0" data-rateit-max="5">
                </div>

            </div>
            <div>
                <p>Comment:</p>
                <textarea name="comment" cols="45" rows="10" style="border: 1px solid;"></textarea>
            </div>
<!--            <div>
                <input type="submit" class="btn btn-info" value="Save" name="update_profile">
            </div>-->
        </form>

    <?php else: ?>
        <div style="width: 300px;float: left">
            <p><strong>Speaking & Participation</strong></p>

            <div class="rateit" data-rateit-value="<?= $rate_speaking ?>" min="0" max="5"
                 data-rateit-readonly="true" style="padding-left: 50px;"></div>
            <p><strong>Pronunciation</strong></p>

            <div class="rateit" data-rateit-value="<?= $rate_pronucation ?>" min="0" max="5" data-rateit-readonly="true"
                 style="padding-left: 50px;"></div>
            <p><strong>Vocabulary</strong></p>

            <div class="rateit" data-rateit-value="<?= $rate_vocabulary ?>" min="0" max="5" data-rateit-readonly="true"
                 style="padding-left: 50px;"></div>
            <p><strong>Listening</strong></p>

            <div class="rateit" data-rateit-value="<?= $rate_listening ?>" min="0" max="5" data-rateit-readonly="true"
                 style="padding-left: 50px;"></div>
        </div>

        <?php if($has_comment): ?>
<div>
    <p>Comment:</p>
    </div>
            <div><b><?=$comment?></b></div>
            <?php else: ?>
            <form action="<?= base_url('admin/student/updaterate') ?>" data-remote="true" accept-charset="UTF-8"
                  method="POST">
                <input type="hidden" name="user_id" value="<?= $user_info['user_id'] ?>"/>
                <input type="hidden" name="group_id" value="<?= $group_info['GroupID'] ?>"/>
                <div>
                    <p><strong>Comment:</strong></p>
                    <textarea name="comment" cols="45" rows="10" style="border: 1px solid;"></textarea>
                </div>
                <div>
                    <input type="submit" class="btn btn-info" value="Save" name="update_profile">
                </div>
            </form>
            <?php endif; ?>
    <?php endif; ?>
    <?php endif; ?>
    <div>
        <button id="ratebutton" class="btn btn-info">Save</button>
    </div>
<?php endif; ?>

<?php if ($this->session->userdata('usertype_id') == 1 || $this->session->userdata('usertype_id') == 4) : ?>
    <form action="<?= base_url('admin/profile/update_profile_details') ?>" data-remote="true" accept-charset="UTF-8"
          method="POST">
        <!--            <form action="<? /*= current_url() */ ?>" data-remote="true"
                  accept-charset="UTF-8" method="POST">-->
        <input type="hidden" name="user_id" value="<?= $user_info['user_id'] ?>"/>
        <table border="0" cellpadding="0" cellspacing="0" class="pop-table">
            <tr>
                <td width="150px"><label>Nombre Completo<em>*</em></label></td>
                <td>
                    <input type="text" name="user_fullname" style="width: 350px!important;"
                           value="<?= $user_info['user_fullname'] ?>"/>
                </td>
            </tr>
            <tr>
                <td><label>Tel&eacute;fono</label></td>
                <td>
                    <input type="text" name="phone" style="width: 350px!important;"
                           value="<?= $user_info['phone'] ?>"/>
                </td>
            </tr>
            <tr>
                <td><label>Direcci&oacute;n</label></td>
                <td>
                    <input type="text" name="address" style="width: 350px!important;"
                           value="<?= $user_info['address'] ?>"/>
                </td>
            </tr>
            <tr>
                <td><label>Ciudad<em>*</em></label></td>
                <td>
                    <select id="ddlcity" name="city_id" style="width: 365px!important;">
                        <option value="">--Selecciona t&uacute; Ciudad--</option>
                        <?php foreach ($cities as $row) { ?>
                            <option <?php
                            if ($user_info['city_id'] == $row['CityID']) {
                            ?> selected <?php
                            }
                            ?>value="<?php echo $row['CityID'] ?>"><?php echo $row['CityName']; ?></option>
                        <?php } ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td width="150px"><label>Fecha Nacimiento</label></td>
                <td>
                    <input type="text" name="DOB" id="dob" style="width: 350px!important;"
                           value="<?= $user_info['DOB'] ?>"/>
                </td>
            </tr>

            <tr>
                <td width="150px"><label>G&eacute;nero</label></td>
                <td>
                    <select id="ddlgender" name="gender" style="width: 150px">
                        <option value="m" <?php if ($user_info['gender'] == 'm') echo 'selected'; ?>>Hombre
                        </option>
                        <option value="f" <?php if ($user_info['gender'] == 'f') echo 'selected'; ?>>
                            Mujer
                        </option>
                    </select>
                </td>
            </tr>

            <tr>
                <td width="150px"><label>Twitter</label></td>
                <td>
                    <input type="text" name="twitter" style="width: 350px!important;"
                           value="<?= $user_info['twitter'] ?>"/>
                </td>
            </tr>

            <tr>
                <td><label>Quiero aprender</label></td>
                <td>
                    <select id="ddlLanguage" name="language_id" style="width: 365px!important;">
                        <option value="">--Selecciona un Idioma--</option>
                        <?php foreach ($languages as $language) : ?>
                            <option <?php
                            if ($user_info['language_id'] == $language['LanguageID']) :?>
                                selected
                            <?php endif; ?>
                                value="<?= $language['LanguageID'] ?>"><?= $language['LanguageName'] ?></option>
                        <?php endforeach; ?>
                    </select>
                </td>
            </tr>

<!--            <tr>
                <td>&nbsp;</td>
                <td>
                    <input type="submit" name="update_profile" value="Save" class="btn btn-info"
                           onclick="parent.parent.GB_hide();">
                </td>
            </tr>-->

            <tr align="Right">
                <td colspan="2">&nbsp;</td>
            </tr>
        </table>
        <h1>Cambiar Password</h1>

        <table border="0" cellpadding="0" cellspacing="0" class="pop-table">
            <tr>
                <td width="150px"><label>Email<em>*</em></label></td>
                <td>
                    <input type="text" name="email" style="width: 350px!important;"
                           value="<?= $user_info['email'] ?>"/>
                </td>
            </tr>
            <tr>
                <td><label>Password Actual<em>*</em></label></td>
                <td><input type="text" name="CurrentPassword" style="width: 350px!important;" value=""/>
                </td>
            </tr>

            <tr>
                <td><label>Password Nuevo<em>*</em></label></td>
                <td><input type="text" name="CurrentPassword" style="width: 350px!important;" value=""/>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input type="submit" value="Change" class="btn btn-info">
                </td>
            </tr>
        </table>
    </form>
<?php endif; ?>
</div>
</div>
<script src="<?= base_url('assets/js/jquery.rateit.min.js') ?>" type="text/javascript"></script>

<script>
    $(function(){


$('#ratebutton').click(function(e) {


    var title_msg = $(this).attr('title');
    var url = $(this).data('url');
    var obj = $(this).parent().parent();
    dhtmlx.message({
        type: "confirm",
        text: "¿Deseas salvar los comentarios y rating?",
        title: title_msg,
        callback: function (e) {
            if (e) {
                if($('#level_form').length) {
                    $.ajax({
                        url: '/admin/student/updatelevel',
                        data: $('#level_form').serialize(),
                        type: 'post',
                        async: false
                    });
                }
                if($('#rate_form').length) {
                    $.ajax({
                        url: '/admin/student/updaterate',
                        data: $('#rate_form').serialize(),
                        type: 'post',
                        async: false
                    }).done(function () {
                        window.location.href = "/admin/managegroup/student/" + $('#student_id').val()+"/"+$('#group_id').val();
                    });
                }
            }
        }
    });
});
    });
</script>
</body>
