<div class="inner-content">
    <div class="title_bg1">
        <div class="wrapper">Manage City</div>
    </div>
    <div class="wrapper1">
        <p><div class="add-new-button"><a href="#" data-url="<?=base_url('admin/managecity/add')?>" class="editcitymodal" title="Add New City"> Add New City</a></div>
        <div class="clear"></div>
        </p>




        <table border='0' cellpadding='0' cellspacing='0' class='tablefrom'>
            <tr>
                <th>City ID</th>
                <th>City Name</th>
                <th>Country</th>
                <th>ShortOrder</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>

        <?php foreach ($cities as $city) : ?>
            <tr>
            <td><?=$city['CityID']?></td>
            <td><?=$city['CityName']?></td>
            <td><?=$city['CountryName']?></td>
            <td><?=$city['ShortOrder']?></td>
            <td><a href="#" data-id="<?=$city['CityID']?>" data-url="<?=base_url('admin/managecity/edit/'.$city['CityID'])?>" class="editcitymodal" title="Edit City"> <img src="<?=base_url()?>assets/images/edit.png" style="height:24px;"></a></td>
            <td><a class="deletecity" href="#" data-url="<?=base_url('admin/managecity/delete/'.$city['CityID'])?>" title="Delete City"> <img src="<?=base_url()?>assets/images/delete.png" style="height:24px;" ></a></td>
            </tr>
        <?php endforeach; ?>
        </table>


        </div>
    </div>

<script>
    $('.editcitymodal').click(function() {
        $.pgwModal({
            url: $(this).data('url'),
            loadingContent: '<span style="text-align:center">Loading in progress</span>',
            title: $(this).attr('title'),
            closable: true,
            titleBar: false
        });
    });

    $('.deletecity').click(function(){
        var title_msg = $(this).attr('title');
        var url = $(this).data('url');
        var obj = $(this).parent().parent();
        dhtmlx.message({
            type:"confirm",
            text: "Do you want to delete the city?",
            title: title_msg,
            callback: function(e) {
                if(e)
                {
                    $.ajax({
                        url: url
                    }).done(function(e) {
                        obj.remove();
                    });
                }
            }
        });
    })
</script>

</body>
</html>