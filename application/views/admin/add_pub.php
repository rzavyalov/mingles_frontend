<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
    <title>Insert Records</title>
    <link href="/assets/css/style.css" rel="stylesheet" type="text/css" />
    <script type='text/javascript'>
        function redirect_to_parent() {
            parent.parent.window.location = "http://google.com";
            parent.parent.GB_hide();
        }
    </script>
</head>
<body onunload="redirect_to_parent();">
<?php echo @$msg; ?>
<form action="" data-remote="true" accept-charset="UTF-8" method="POST">
    <input type="hidden" name="PubID" value="<?php
    if (isset($_GET['id'])) {
        echo $pubs['PubID'];
    }
    ?>" />
    <table  border="0" cellpadding="0" cellspacing="0" class="pop-table">
        <tr>
            <td width="170px"><label>Pub Name<em>*</em></label></td>
            <td width="300px">
                <input  type="text" name="PubName" value="<?php
                if (isset($_GET['id'])) {
                    echo $pubs['PubName'];
                }
                ?>"/>
            </td>

        </tr>
        <tr>
            <td><label>Address<em>*</em></label></td>
            <td>
                <input  type="text" name="PubAddress" value="<?php
                if (isset($_GET['id'])) {
                    echo $pubs['PubAddress'];
                }
                ?>"/>
            </td>
        </tr>

        <tr>
            <td><label>City Name<em>*</em></label></td>
            <td>
                <select id="ddlcity" name="CityID" >
                    <option value="">--Select City--</option>
                    <?php foreach ($allcity as $row) { ?>
                        <option value="<?php echo $row['CityID'] ?>"><?php echo $row['CityName']; ?></option>
                    <?php } ?>
                </select>
            </td>
        </tr>

        <tr>
            <td width="90"><label>Contact Person Name<em>*</em></label></td>
            <td>
                <input  type="text" name="ContactPersonName" value=""/>
            </td>
        </tr>




        <tr>
            <td><label>Phone Number<em>*</em></label></td>
            <td><input  type="text" name="PubPhoneNumber" value=""/>
            </td>
        </tr>

        <tr>
            <td><label>Capacity for Mingles<em>*</em></label></td>
            <td><input  type="text" name="CapacityforMingles" value=""/>
            </td>
        </tr>

        <tr>
            <td><label>Longitude<em>*</em></label></td>
            <td><input  type="text" name="Longitude" value=""/>
            </td>
        </tr>
        <tr>
            <td><label>Latitude<em>*</em></label></td>
            <td><input  type="text" name="Latitude" value=""/>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <input type="submit" <?php if (isset($_GET['id'])) { ?>value="Update"<?php } else { ?>value="Save"<?php } ?> class="btn btn-info" onclick="parent.parent.GB_hide();">
            </td>
        </tr>

        <tr align="Right">
            <td colspan="2">&nbsp;</td>
        </tr>
    </table>
    <!--                <td valign="top">
                        <table  border="0" cellpadding="0" cellspacing="0" class="pop-table">
                            <tr>

                                <td width="170px"><img src="../images/20minutos.jpg"></td>
                            </tr>
                        </table>-->

    </td>
    </tr>
    </table>



</form>
</body>
</html>