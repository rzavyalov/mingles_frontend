<style>
    input[type='file'] {
        color: transparent;
    }
</style>
<link href="<?=base_url('assets/css/rateit.css')?>" rel="stylesheet" type="text/css">

<div class="wrapper2">

    <script type="text/javascript">
        $(document).ready(function() {
/*            if (window.webkitURL)
                $('input[type="file"]').attr('title', ' ');
            else
                $('input[type="file"]').attr('title', '');*/

            $('#ChangeProfilePicture').click(function() {
                $(this).parent().find('#uploadpicture').slideToggle();
                $(this).parent().find("#ChangeProfilePicture").hide();
            });
        });
    </script>

    <div class="profile-leftN">
        <div class="profile-details">
            <div class="profile-pic">
                <?php if (!file_exists(FCPATH.'users/profile/'.$this->session->userdata('user_id').'.jpg')) {
                    ?>
                    <img src="<?=base_url()?>assets/images/defaultM.png" alt="" height="175px" width="175px">
                <?php } else { ?>
                    <img src="<?=base_url()?>users/profile/<?=$this->session->userdata('user_id')?>.jpg"  height="175px" width="175px" />
                <?php } ?>
            </div>
            <h2><?=$this->session->userdata('user_fullname')?> </h2>
            <h2><?=$this->session->userdata('level_name')?></h2>
            <div class="rateit" data-rateit-value="<?=$this->session->userdata('total_rate')?>" min="0" max="5" data-rateit-readonly="true" style="padding-left: 50px;"></div>
            <!--<img src="<?/*=base_url()*/?>assets/images/ratting.jpg" alt=""  height="20px" width="75px" style="padding-left: 50px;">-->
            <br/>
            <br/>
            <div class="edit">
                <form  method="post" enctype="multipart/form-data" action="<?=base_url('admin/profile/changepic')?>">
                    <div id="uploadpicture" style="display:none;">
                        <input style="display:none;" name="current_page" value="<?=uri_string()?>">
                        <input type="file" name="file"/>
                        <br/>
                        <input type="submit" value="Upload" style="width:180px!important;" class="btn btn-info" name="update_profileimage"/>
                    </div>
                </form>
                <a href="#" id="ChangeProfilePicture" ><?=$this->lang->line('change_profile_picture')?></a>
            </div>
            <!--            <div class="edit1"></div>-->
            <div class="clear"></div>
        </div>
        <ul class="profile-list">
                <li><a href="<?=base_url('admin/dashboard')?>"><?=$this->lang->line('main_page')?></a></li>
                <li><a href="<?=base_url('admin/profile')?>"><?=$this->lang->line('edit_profile')?></a></li>
                <li><a href="<?=base_url('admin/profile/transactions')?>"><?=$this->lang->line('my_payments')?></a></li>
                <!--<li><a href="studentpass.php?id=<?php echo trim($_GET['id']); ?>">My Pass</a></li>-->
                <li><a href="<?=base_url('admin/profile/rate')?>"><?=$this->lang->line('my_rating')?></a></li>
                <li><a href="<?=base_url('admin/profile/rate_day')?>">Group of the day</a></li>

                <!--<li><a href="<?/*=base_url('#option')*/?>">APÚNTARME</a></li>-->

                <li><a href="<?=base_url('admin/dashboard/logout')?>">Logout </a></li>
            <div class="button">
                <a href="<?=base_url('#option')?>">
                    <img alt="" src="<?=base_url('assets/images/register_bt_en.png')?>"></a></div>
        </ul>
    </div>

    <script src="<?=base_url('assets/js/jquery.rateit.min.js')?>" type="text/javascript"></script>
