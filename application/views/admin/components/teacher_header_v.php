<!DOCTYPE html>
<html lang="en">
<head>
    <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=UTF-8">
    <META name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
    <META name="apple-mobile-web-app-capable" content="yes" />
    <title><?php echo $page_info['page_title'] ?></title>
    <meta name="description" content="<?php echo $page_info['page_description'] ?>">
    <meta name="keywords" content="<?php echo $page_info['pageKeywords'] ?>">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css' />
    <!--        <link href="css/style.css" type="text/css" rel="stylesheet" />-->
    <link href="<?=base_url()?>assets/css/style.css?<?php echo time(); ?>" type="text/css" rel="stylesheet" />
    <link href="<?=base_url()?>assets/css/popup.css" type="text/css" rel="stylesheet" />

    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
    <script src="<?=base_url('assets/js/jquery-1.10.2.js')?>"></script>
    <script src="<?=base_url('assets/js/jquery-ui.js')?>"></script>
    <!--<script src="//code.jquery.com/ui/1.11.0/jquery-ui.js"></script>-->
    <!--        <script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>-->
    <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDY0kkJiTPVd2U7aTOAwhc9ySH6oHxOIYM&sensor=false">
    </script>
    <link rel="stylesheet" href="/resources/demos/style.css">

    <link rel="stylesheet" href="<?=base_url()?>assets/js/jquery.bxslider.css" type="text/css" />
    <script type="text/javascript" src="<?=base_url()?>assets/js/jquery.bxslider.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/js/scripts.js"></script>

    <script type="text/javascript">
        var loggedIn = <?php if ($this->session->userdata('user_id')) {
    echo $this->session->userdata('user_id');
} ?>;

    </script>

    <script type="text/javascript">

        function DropDown(el) {

            this.dd = el;

            this.initEvents();

        }

        DropDown.prototype = {
            initEvents: function() {

                var obj = this;



                obj.dd.on('click', function(event) {

                    $(this).toggleClass('active');

                    event.stopPropagation();

                });

            }

        }

        $(function() {



            var dd = new DropDown($('#dd'));



            $(document).click(function() {

                // all dropdowns

                $('.wrapper-dropdown-2').removeClass('active');

            });



        });

    </script>

    <script src="<?=base_url()?>assets/js/teacher_available.js"></script>


</head>
    <body class="home" data-twttr-rendered="true">
    <div class="container">
        <!--<start header>-->
        <header>
            <div class="wrapper">

                <div class="logo"><a href="<?=base_url()?>">
                        <img src="<?=base_url()?>assets/images/logo.png"></a></div>
                <div class="header_rt">
                    <div class="top-menu">

                        <?php if ($this->session->userdata['usertype_id'] == '4') { ?>
                            <nav>
                                <ul>
                                    <li><a href="<?=base_url()?>">Home</a></li>
                                    <li><a href="#">Manage Master</a>
                                        <ul class="sub-menu">
                                            <li><a href="<?=base_url('admin/managecity')?>">Manage Cities</a></li>
                                            <li><a href="<?=base_url('admin/managelevel')?>">Manage Level</a></li>
                                            <li><a href="<?=base_url('admin/managelanguage')?>">Manage Language</a></li>
                                            <li><a href="<?=base_url('admin/managesession')?>">Manage Session</a></li>
                                            <li><a href="<?=base_url('admin/managepromocode')?>">Manage Promo Code</a></li>
                                        </ul>
                                    </li>

                                    <li><a href="#">Administration</a>
                                        <ul class="sub-menu">
                                            <li><a href="<?=base_url('admin/managepubs')?>">Manage Pubs</a> </li>
                                            <li><a href="<?=base_url('admin/manageteacher')?>">Manage Teacher</a></li>
                                            <li><a href="<?=base_url('admin/managestudent')?>">Manage Student</a></li>
                                            <!--                                                <li><a href="ManageUsers.php">Manage Users</a></li>-->
                                            <li><a href="<?=base_url('admin/manageclass')?>">Manage Class</a></li>
                                            <li><a href="<?=base_url('admin/managegroup')?>">Manage Group</a></li>
                                            <!--     <li><a href="GroupandStudentDetails.php"> Group & Student </a></li>-->
                                            <!--<li><a href="StudentPaymentDetails.php">Payment Details</a></li>-->
                                        </ul>
                                    </li>
                                    <!--   <li><a href="#"><?php echo $this->session->userdata('user_fullname') ?></a>
                                                <ul class="sub-menu">
                                                    <li><a href="users_New.php?id=<?php echo $_SESSION['ses_UserID'] ?>" rel="gb_page_center[700, 400]" title="Manage Profile">Profile</a></li>
                                                    <li><a href="changePassword.php?id=<?php echo $_SESSION['ses_UserID'] ?>" rel="gb_page_center[600, 300]" title="Change Password">Change Password</a></li>
                                                </ul>
                                            </li>-->
                                    <li><a href="<?=base_url()?>admin/dashboard/logout">Logout</a></li>
                                </ul>
                            </nav>

                        <?php } ?>

                        <div class="height20"></div>
                    </div>
                    <div class="height15"></div>
                    <div class="social_icon"><a href="http://www.facebook.com/MinglesSpain" target="_blank">
                            <img src="<?=base_url()?>assets/images/facebook.png"></a><a href="http://twitter.com/MinglesSpain" target="_blank"><img src="<?=base_url()?>assets/images/twitter.png"></a><a href="http://www.youtube.com/channel/UCJN5c55Ik3iozb1HYZMqVIw" target="_blank"><img src="<?=base_url()?>assets/images/youtube.png"></a><a href="http://www.linkedin.com/company/mingles?trk=company_name" target="_blank"><img src="<?=base_url()?>assets/images/linkedin.png"></a><a href="http://gplus.to/Mingles" target="_blank"><img src="<?=base_url()?>assets/images/google_plus.png"></a></div>
                    <div class="height0"></div>
                    <nav>
                        <ul>
                            <li><a href="#">IDIOMAS</a>
                                                <ul class="sub-menu">
                                                <li><a href="<?=base_url('whatsmingles')?>">¿Qué es?</a></li>
                                                <!--<li><a href="<?/*=base_url('metodo')*/?>">Nuestro Método</a> </li>-->
                                                <li><a href="http://www.mingles.es/page/spanish">#Spanish</a> </li>
                                                <li><a href="http://www.mingles.es/page/frenchmingles">#French</a> </li>
                                                <li><a href="http://www.mingles.es/page/portuguese">#Portuguese</a> </li>
                                                </ul>
                            </li>
                            <li><a href="<?=base_url('premiumcards')?>">PREMIUM CARD</a></li>
                            <li><a href="#">Los Espacios</a>
                                <ul class="sub-menu">
                                    <li><a href="<?=base_url('bares')?>">Locales Mingles</a></li>
                                    <li><a href="http://www.mingles.es/page/empresa">Empresas y Coworkings</a></li>
                                    <li><a href="http://www.mingles.es/page/womensmingles">En la Radio</a></li>
                                    <li><a href="http://www.mingles.es/page/kids">Kids Mingles</a></li>
                                    <li><a href="<?=base_url('eventos')?>">Eventos</a></li>
                                </ul>

                            </li>
                            <li><a href="<?=base_url('bono')?>">BONO MINGLES</a></li>
                        </ul>


                    </nav>
                </div>
        </header>
        <div class="height30">
        </div>
