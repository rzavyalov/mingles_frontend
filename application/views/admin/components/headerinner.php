<?php require 'common/config.php'; ?>

<!doctype html>
<html>
    <head>
        <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=UTF-8">
        <META name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
        <META name="apple-mobile-web-app-capable" content="yes" />
        <title><?php echo $pageTitle ?></title>
        <meta name="description" content="<?php echo $pageDescription ?>">
        <meta name="keywords" content="<?php echo $pageKeywords ?>">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css' />
        <link href="css/style.css" type="text/css" rel="stylesheet" />
        <link href="css/popup.css" type="text/css" rel="stylesheet" />
        <link rel="stylesheet" href="js/jquery.bxslider.css" type="text/css" />
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery.bxslider.js"></script>
        <script type="text/javascript" src="js/scripts.js"></script>

        <script type="text/javascript">
            window.onload = function() {
                document.getElementById("tuOption").onclick = function() {
                    var overlay = document.getElementById("overlay");
                    var popup = document.getElementById("popupContactus");

                    overlay.style.display = "block";
                    popup.style.display = "block";

                    var Asunto = document.getElementById("Asunto");
                    Asunto.value = "Tu Opinión nos Importa";
                    document.getElementById("Asunto").disabled = true;
                }

                document.getElementById("btnclosemodalContactForm").onclick = function() {
                    var overlay = document.getElementById("overlay");
                    var popup = document.getElementById("popupContactus");
                    overlay.style.display = "none";
                    popup.style.display = "none";
                }


                document.getElementById("aboutus").onclick = function() {
                    var overlay = document.getElementById("overlay");
                    var popup = document.getElementById("popupAboutus");
                    overlay.style.display = "block";
                    popup.style.display = "block";
                }


                document.getElementById("CloseBtn").onclick = function() {
                    var overlay = document.getElementById("overlay");
                    var popup = document.getElementById("popupAboutus");
                    overlay.style.display = "none";
                    popup.style.display = "none";
                }

                document.getElementById("contactUS").onclick = function() {
                    var overlay = document.getElementById("overlay");
                    var popup = document.getElementById("popupContactus");
                    overlay.style.display = "block";
                    popup.style.display = "block";
                }

                document.getElementById("teacher").onclick = function() {
                    var overlay = document.getElementById("overlay");
                    var popup = document.getElementById("popupContactus");

                    overlay.style.display = "block";
                    popup.style.display = "block";

                    var Asunto = document.getElementById("Asunto");
                    Asunto.value = "Contacto de Profesor";
                    document.getElementById("Asunto").disabled = true;
                }


                document.getElementById("bar").onclick = function() {
                    var overlay = document.getElementById("overlay");
                    var popup = document.getElementById("popupContactus");

                    overlay.style.display = "block";
                    popup.style.display = "block";

                    var Asunto = document.getElementById("Asunto");
                    Asunto.value = "Contacto de Bar";
                    document.getElementById("Asunto").disabled = true;
                }

                document.getElementById("Company").onclick = function() {
                    var overlay = document.getElementById("overlay");
                    var popup = document.getElementById("popupContactus");

                    overlay.style.display = "block";
                    popup.style.display = "block";

                    var Asunto = document.getElementById("Asunto");
                    Asunto.value = "Quiero Mingles en mi ciudad";
                    document.getElementById("Asunto").disabled = true;
                }
            };
        </script>

    </head>
    <body class="home" data-twttr-rendered="true">
        <div class="container">
            <!--<start header>-->
            <header>
                <div class="wrapper">

                    <div class="logo"><a href="index.php">
                            <img src="images/logo.png"></a></div>
                    <div class="header_rt">
                        <div class="login"><a href="admin/">Login / Register</a></div>
                        <div class="top-menu"><a href="#">Espa&ntilde;ol - (ES)</a></div>
                        <div class="select-city">
                            <select name="ddlCityHeader">
                                <option>Select City</option>
                                <?php
                                $Query = mysqli_query($connection, "SELECT * FROM lupCity", true)
                                        or die(mysqli_error($connection));
                                while ($result = mysqli_fetch_assoc($Query)) {
                                    ?>
                                    <option value="<?php echo $result['CityID']; ?>"><?php echo $result['CityName']; ?></option>
                                    <?php
                                }
                                ?>
                            </select>

                        </div>

                        <div class="height20"></div>
                        <div class="social_icon"><a href="http://www.facebook.com/MinglesSpain" target="_blank">
                                <img src="images/facebook.png"></a><a href="http://twitter.com/MinglesSpain" target="_blank"><img src="images/twitter.png"></a><a href="http://www.youtube.com/channel/UCJN5c55Ik3iozb1HYZMqVIw" target="_blank"><img src="images/youtube.png"></a><a href="http://www.linkedin.com/company/mingles?trk=company_name" target="_blank"><img src="images/linkedin.png"></a><a href="http://gplus.to/Mingles" target="_blank"><img src="images/google_plus.png"></a></div>
                        <div class="height0"></div>
                        <nav>
                        <div class="responsiveMenuSelect">



             <div class="wrapper-demo">

				<!-----start-wrapper-dropdown-2---->

					<div id="dd" class="wrapper-dropdown-2" tabindex="1">menu<span><img src="images/icon-menu.png"/></span>

							<ul class="dropdown">
                                <li><a href="#">IDIOMAS</a>
                                                <ul class="sub-menu">
                                                <li><a href="<?=base_url('whatsmingles')?>">¿Qué es?</a></li>
                                                <!--<li><a href="<?/*=base_url('metodo')*/?>">Nuestro Método</a> </li>-->
                                                <li><a href="http://www.mingles.es/page/spanish">#Spanish</a> </li>
                                                <li><a href="http://www.mingles.es/page/frenchmingles">#French</a> </li>
                                                <li><a href="http://www.mingles.es/page/portuguese">#Portuguese</a> </li>
                                                </ul>
                                            </li>
                                </li>
                                <li><a href="<?=base_url('premiumcards')?>">PREMIUM CARD</a></li>
                                <li><a href="#">Los Espacios</a>
                                    <ul class="sub-menu">
                                        <li><a href="bares.php">Locales Mingles</a></li>
                                        <li><a href="http://www.mingles.es/page/empresa">Empresas y Coworkings</a></li>
                                        <li><a href="http://www.mingles.es/page/womensmingles">En la Radio</a></li>
                                        <li><a href="http://www.mingles.es/page/kids">Kids Mingles</a></li>
                                        <li><a href="eventos.php">Eventos</a></li>
                                    </ul>

                                </li>
                                <li><a href="bono.php">BONO MINGLES</a></li>

                            </ul>
					</div>

			</div>

			<!-----end-wrapper-dropdown-2---->

			<!-----start-script---->

			<script type="text/javascript">

							function DropDown(el) {

								this.dd = el;

								this.initEvents();

							}

							DropDown.prototype = {

								initEvents : function() {

									var obj = this;



									obj.dd.on('click', function(event){

										$(this).toggleClass('active');

										event.stopPropagation();

									});

								}

							}

							$(function() {



								var dd = new DropDown( $('#dd') );



								$(document).click(function() {

									// all dropdowns

									$('.wrapper-dropdown-2').removeClass('active');

								});



							});

			</script>

              </div>

                            <ul>
                                <li><a href="#">IDIOMAS</a>
                                                <ul class="sub-menu">
                                                <li><a href="<?=base_url('whatsmingles')?>">¿Qué es?</a></li>
                                                <!--<li><a href="<?/*=base_url('metodo')*/?>">Nuestro Método</a> </li>-->
                                                <li><a href="http://www.mingles.es/page/spanish">#Spanish</a> </li>
                                                <li><a href="http://www.mingles.es/page/frenchmingles">#French</a> </li>
                                                <li><a href="http://www.mingles.es/page/portuguese">#Portuguese</a> </li>
                                                </ul>
                                            </li>
                                <li><a href="<?=base_url('premiumcards')?>">PREMIUM CARD</a></li>
                                <li><a href="#">Los Espacios</a>
                                    <ul class="sub-menu">
                                         <li><a href="bares.php">Los Locales</a></li>
                                        <li><a href="http://www.mingles.es/page/empresa">Empresas y Coworkings</a></li>
                                         <li><a href="http://www.mingles.es/page/womensmingles">En la Radio</a></li>
                                         <li><a href="http://www.mingles.es/page/kids">Kids Mingles</a></li>
                                         <li><a href="eventos.php">Eventos</a></li>
                                    </ul>

                                </li>
                                <li><a href="bono.php">BONO MINGLES</a></li>
                            </ul>
                        </nav>
                    </div>
                    <div class="height15"></div>
                </div>
            </header>
            <!--<end header>-->
