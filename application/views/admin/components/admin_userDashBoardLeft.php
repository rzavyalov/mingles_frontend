<style>
    input[type='file'] {
        color: transparent;
    }
</style>
<link href="<?=base_url('assets/css/rateit.css')?>" rel="stylesheet" type="text/css">

<div class="wrapper2">

    <script type="text/javascript">
        $(document).ready(function() {
            /*            if (window.webkitURL)
             $('input[type="file"]').attr('title', ' ');
             else
             $('input[type="file"]').attr('title', '');*/

            $('#ChangeProfilePicture').click(function() {
                $(this).parent().find('#uploadpicture').slideToggle();
                $(this).parent().find("#ChangeProfilePicture").hide();
            });
        });
    </script>

    <div class="profile-leftN">
        <div class="profile-details">
            <div class="profile-pic">
                <?php if (!file_exists(FCPATH.'users/profile/'.$user_info['user_id'].'.jpg')) {
                    ?>
                    <img src="<?=base_url()?>assets/images/defaultM.png" alt="" height="175px" width="175px">
                <?php } else { ?>
                    <img src="<?=base_url()?>users/profile/<?=$user_info['user_id']?>.jpg"  height="175px" width="175px" />
                <?php } ?>
            </div>
            <h2><?=$user_info['user_fullname']?> </h2>
            <h2><?=$user_info['level_name']?></h2>
            <div class="rateit" data-rateit-value="<?=$user_info['total_rate']?>" min="0" max="5" data-rateit-readonly="true" style="padding-left: 50px;"></div>
            <br/>
            <br/>
            <div class="edit">
                <form  method="post" enctype="multipart/form-data" action="<?=base_url('admin/profile/changepic')?>">
                    <div id="uploadpicture" style="display:none;">
                        <input style="display:none;" name="current_page" value="<?=uri_string()?>">
                        <input type="file" name="file"/>
                        <br/>
                        <input type="submit" value="Upload" style="width:180px!important;" class="btn btn-info" name="update_profileimage"/>
                    </div>
                </form>
                <a href="#" id="ChangeProfilePicture" >Cambiar Foto Perfil</a>
            </div>
            <!--            <div class="edit1"></div>-->
            <div class="clear"></div>
        </div>
        <ul class="profile-list">
                <li><a href="<?=base_url('admin/student/dashboard/'.$user_info['user_id'])?>">Principal</a></li>
                <li><a href="<?=base_url('admin/student/profile/'.$user_info['user_id'])?>">Editar mi perfil</a></li>
                <li><a href="<?=base_url('admin/student/transactions/'.$user_info['user_id'])?>">Mis Pagos</a></li>

                <li><a href="<?=base_url('admin/student/rate/'.$user_info['user_id'])?>">Mi Rating </a></li>
                <li><a href="<?=base_url('admin/dashboard/logout')?>">Logout </a></li>
        </ul>
    </div>

    <script src="<?=base_url('assets/js/jquery.rateit.min.js')?>" type="text/javascript"></script>
