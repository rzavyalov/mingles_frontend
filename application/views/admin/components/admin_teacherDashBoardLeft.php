<link href="<?=base_url('assets/css/rateit.css')?>" rel="stylesheet" type="text/css">
<script type="text/javascript">
    $(document).ready(function() {
        $('#ChangeProfilePicture').click(function() {
            $(this).parent().find('#uploadpicture').slideToggle();
            $(this).parent().find("#ChangeProfilePicture").hide();
        });
    });
</script>

<div class="wrapper2">
    <div class="profile-leftN">
        <div class="profile-details">
            <div class="profile-pic">
                <!--  <img src="../images/defaultM.png" alt="" height="175px" width="175px">-->
                <?php if (!file_exists(FCPATH.'users/profile/'.$teacher['user_id'].'.jpg')) {
                    ?>
                    <img src="<?=base_url()?>assets/images/defaultM.png" alt="" height="175px" width="175px">
                <?php } else { ?>
                    <img src="<?=base_url()?>users/profile/<?=$teacher['user_id']?>.jpg"  height="175px" width="175px" />
                <?php } ?>
            </div>
            <h2><?=$teacher['TeacherName']?> </h2>
            <div class="rateit" data-rateit-value="<?=$teacher['total_rate']?>" min="0" max="5" data-rateit-readonly="true" style="padding-left: 50px;"></div>
            <br/>
            <br/>
            <div class="edit">
                <form  method="post" enctype="multipart/form-data" action="<?=base_url('admin/profile/changepic')?>">
                    <div id="uploadpicture" style="display:none;">
                        <input style="display:none;" name="current_page" value="<?=uri_string()?>">
                        <input type="file" name="file"/>
                        <br/>
                        <input type="submit" value="Upload" style="width:180px!important;" class="btn btn-info" name="update_profileimage"/>
                    </div>
                </form>
                <a href="#" id="ChangeProfilePicture" >Change Profile Image</a>
            </div>
            <!--            <div class="edit1"></div>-->
            <div class="clear"></div>
        </div>
        <ul class="profile-list">
                <li><a href="<?=base_url('admin/teacher/dashboard/'.$teacher['user_id'])?>">Main Page</a></li>
                <li><a href="<?=base_url('admin/teacher/profile/'.$teacher['user_id'])?>">Manage My Profile</a></li>
                <li><a href="<?=base_url('admin/teacher/profile/availability/'.$teacher['user_id'])?>">Availability</a></li>
                <li><a href="<?=base_url('admin/teacher/rate/'.$teacher['user_id'])?>">My Ratings </a></li>
                <li><a href="<?=base_url('admin/dashboard/logout')?>">Logout </a></li>
        </ul>
    </div>
    <script src="<?=base_url('assets/js/jquery.rateit.min.js')?>" type="text/javascript"></script>
