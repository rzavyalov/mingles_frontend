<!--<start footer>-->
<footer>
    <div class="footer">
        <div class="wrapper">
            <div class="footer_lt">
                MINGLES LANGUAGES<br>
                La nueva forma de aprender Idiomas
            </div>

            <div class="footer_rt">
                <img src="<?=base_url()?>assets/images/footer_logo.png" alt=""><br>
                &copy; Mingles Languages S.L. Todos los derechos reservados.
            </div>

        </div>
    </div>
</footer>
<!--<end footer>-->
</div>

<!--End Info Nivel-->
</body>
</html>
