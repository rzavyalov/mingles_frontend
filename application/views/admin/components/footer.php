<!--<start bottom-logo>-->
<div class="bottom-logo">
    <div class="wrapper">

         <a href="https://www.youtube.com/watch?v=g297nBgypdg&feature=youtu.be" target="_blank">
            <img src="http://www.mingles.es/assets/images/bottom_logo13.png" alt="Minles en TVE"></a>

        <a href="http://goo.gl/citLc8" target="_blank">
            <img src="http://www.mingles.es/assets/images/bottom_logo11.jpg" alt=""></a>

        <a href="http://elpais.com/elpais/2013/10/14/eps/1381753903_217752.html"                                                  target="_blank">
            <img src="http://www.mingles.es/assets/images/bottom_logo2.jpg" alt=""></a>

        <a href="http://blogs.20minutos.es/capeando-la-crisis/2014/04/15/conversacion-en-ingles-en-tu-bar-favorito/" target="_blank">
            <img src="http://www.mingles.es/assets/images/bottom_logo3.jpg" alt=""></a>

        <a href="http://goo.gl/BAcDyV" target="_blank">
            <img src="http://www.mingles.es/assets/images/bottom_logo8.jpg" alt="Mingles en la cope"></a>

        <a href="https://www.youtube.com/watch?v=u02HHsGxQeM" target="_blank">
            <img src="http://www.mingles.es/assets/images/bottom_logo7.png" alt="Minles en la2"></a>

        <a href="http://cadenaser.com/emisora/2014/07/02/audios/1404262582_660215.html" target="_blank">
            <img src="http://www.mingles.es/assets/images/bottom_logo9.jpg" alt="Mingles en la cadena SER"></a>

        <a href="https://www.youtube.com/watch?v=LFh0YhLvcwQ" target="_blank">
            <img src="http://www.mingles.es/assets/images/bottom_logo10.jpg" alt="Mingles se va de bartes"></a>

    </div>
</div>
<!--<end bottom-logo>-->

<!--<start bottom>-->
<div class="bottom">
    <div class="wrapper">
        <div class="information">
            <h2>Información</h2>
            <span id="aboutus" class="pointer">Sobre nosotros</span><br>
            <span class="pointer"><a href="<?=base_url('medios')?>">Prensa</a></span>
            <span><a href="http://www.mingles.es/blog">Blog</a></span><br>
            <span><a href="<?=base_url('media')?>">Videos</a></span><br>
            <span><a href="<?=base_url('partners')?>">Partners</a></span><br>
        </div>
        <div class="information">
            <h2>Contacto</h2>
            <span id="contactUS" class="pointer">Contacta con nosotros</span>
            <a class="typeform-share button" href="https://mingles.typeform.com/to/mFxBD4" data-mode="1" target="_blank">&iquest;Qu&eacute; te parece la web?</a>

<script>(function(){var qs,js,q,s,d=document,gi=d.getElementById,ce=d.createElement,gt=d.getElementsByTagName,id='typef_orm',b='https://s3-eu-west-1.amazonaws.com/share.typeform.com/';if(!gi.call(d,id)){js=ce.call(d,'script');js.id=id;js.src=b+'share.js';q=gt.call(d,'script')[0];q.parentNode.insertBefore(js,q)}id=id+'_';if(!gi.call(d,id)){qs=ce.call(d,'link');qs.rel='stylesheet';qs.id=id;qs.href=b+'share-button.css';s=gt.call(d,'head')[0];s.appendChild(qs,s)}})()</script>
        </div>
        <div class="yellow_box">
            <span id="teacher" class="pointer">¿ERES UN PROFESOR?
                <p>
                    APÚNTATE AQUÍ
                </p>
            </span>
        </div>
        <div class="pink_box">
            <span id="bar" class="pointer">¿ERES UN BAR/PUB?
                <p>
                    APÚNTATE AQUÍ
                </p>
            </span>
        </div>
        <div class="blue_box">
            <span id="Company" class="pointer">¿QUIERES MINGLES EN TU CIUDAD?
            </span>
        </div>
    </div>
</div>
<!--<end bottom>-->
<!--<start footer>-->
<footer>
    <div class="footer">
        <div class="wrapper">
            <div class="footer_lt">
                MINGLES LANGUAGES<br>
                La nueva forma de aprender Idiomas
            </div>

            <div class="footer_rt">
                <img src="images/footer_logo.png" alt=""><br>
                &copy;Mingles Languages S.L. Todos los derechos reservados.
            </div>
        </div>
    </div>
</footer>
<!--<end footer>-->
</div>
<!--Section For Popup-->
<div style="display: none;" id="overlay">
</div>
<div style="display: none;" id="popupAboutus" class="popupBody">
    <h2>Sobre Nosotros</h2>
    <div class="popupBodytext">
        <p>
            Somos unos jóvenes con ganas de emprender en este novedoso proyecto. Mingles es una comunidad de personas interesadas en mejorar el conocimiento de idiomas. Nos encanta conocer gente nueva, los ambientes internacionales y multiculturales y promovemos el consumo colaborativo LET’s MINGLE!!!
        </p>
        <img src="images/team.jpg" alt="Sobre Nosotros">
    </div>
<!--    <button class="btn btn-info">
        Close</button>-->
    <button id="CloseBtn" class="CloseBtn">
        <img src="images/button_cancel.png" alt=""></button>
</div>
<!-- Start Contact US-->
<div style="display: none;" id="popupContactus" class="popupBody">
    <h2>
        Contacto</h2>
    <h3>Nos vas a enviar un mensaje &iexcl;Let's Mingle!</h3>
    <p>
    </p>
    <div class="popupBodytext">
        <form id="modalContactForm">
            <label for="Asunto" class="pl-label">
                Asunto:
            </label>
            <input required="" id="Asunto" name="Asunto" type="text">
            <div class="clear">
            </div>
            <label for="email" class="pl-label">
                Email:</label>
            <input required="" id="email" name="email" type="text">
            <div class="clear">
            </div>
            <label for="phone" class="pl-label">
                Nombre:</label>
            <input required="" id="phone" name="phone" type="text" />
            <div class="clear">
            </div>
            <label for="message" class="pl-label">
                Mensaje:</label>
            <textarea required="" id="address" name="address" type="text"></textarea>
            <div class="clear">
            </div>
            <button id="sendEmailContact" class="btn btn-info" aria-hidden="true">
                Send Email</button>
            <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">
                Close</button>
        </form>
    </div>
    <button id="btnclosemodalContactForm" class="CloseBtn">
        <img src="images/button_cancel.png" alt=""></button>
</div>
<!-- END Contact US-->
<!-- Start Info Nivel-->
<div style="display: none;" id="popupInfoNivel" class="popupBody">
    <h2>Información sobre los niveles:</h2>
    <div class="popupBodytext">
        <p>
        <ul>
            <li><b>Beginner (Usuario b&aacute;sico):</b> Es capaz de comprender y utilizar expresiones cotidianas de uso muy frecuente as&iacute; como frases sencillas destinadas a satisfacer necesidades de tipo inmediato. Puede presentarse a s&iacute; mismo y a otros, pedir y dar información personal b&aacute;sica sobre su domicilio, sus pertenencias y las personas que conoce. Puede relacionarse de forma elemental siempre que su interlocutor hable despacio y con claridad y esté dispuesto a cooperar.</li>
            <li><b>Intermediate (Intermedio):</b>  Es capaz de comprender los puntos principales de textos claros y en lengua est&aacute;ndar si tratan sobre cuestiones que le son conocidas, ya sea en situaciones de trabajo, de estudio o de ocio. Sabe desenvolverse en la mayor parte de las situaciones que pueden surgir durante un viaje por zonas donde se utiliza la lengua. Es capaz de producir textos sencillos y coherentes sobre temas que le son familiares o en los que tiene un inter&eacute;s personal. Puede describir experiencias, acontecimientos, deseos y aspiraciones, así como justificar brevemente sus opiniones o explicar sus planes.</li>
            <li><b>Upper Intermediate (Intermedio Alto):</b> Es capaz de entender las ideas principales de textos complejos que traten de temas tanto concretos como abstractos, incluso si son de car&aacute;cter t&eacute;cnico siempre que est&eacute;n dentro de su campo de especialización. Puede relacionarse con hablantes nativos con un grado suficiente de fluidez y naturalidad de modo que la comunicaci&oacute;n se realice sin esfuerzo por parte de ninguno de los interlocutores. Puede producir textos claros y detallados sobre temas diversos así como defender un punto de vista sobre temas generales indicando los pros y los contras de las distintas opciones.</li>
            <li><b>Advance (Avanzado):</b> Es capaz de comprender una amplia variedad de textos extensos y con cierto nivel de exigencia, así como reconocer en ellos sentidos impl&iacute;citos. Sabe expresarse de forma fluida y espont&aacute;nea sin muestras muy evidentes de esfuerzo para encontrar la expresi&oacute;n adecuada. Puede hacer un uso flexible y efectivo del idioma para fines sociales, acad&eacute;micos y profesionales. Puede producir textos claros, bien estructurados y detallados sobre temas de cierta complejidad, mostrando un uso correcto de los mecanismos de organizaci&oacute;n, articulaci&oacute;n y cohesi&oacute;n del texto.</li>
        </ul>
        </p>
    </div>
    <!--    <button class="btn btn-info">
            Cerrar</button>-->
    <button id="btnClosepopupInfoNivel" class="CloseBtn">
        <img src="images/button_cancel.png" alt=""></button>
</div>
<!--End Info Nivel-->
<!-- Start Info Nivel-->
<div style="display: none;" id="popupOnetoOne" class="popupBody">
    <h2>Plan Especial Mingles One To One</h2>
    <div class="popupBodytext">
        <p>
            Welcome to Mingles!
        </p>
        <p>
            Mingles también ofrece la opción de una clase one to one (tu solo con el profesor). Te apuntas también a través del calendario y nosotros te preparamos un profesor para ti. El precio es de 25 EUR por sesión o adquirir un bono con un 10% de descuento (112.5 EUR).
        </p>
        <p>Podrás seleccionar los topics de nuestro blog o incluso preparar una entrevista</p>
        <p>
            LET's MINGLE!!!
        </p>
    </div>
    <!--    <button class="btn btn-info">
            Close</button>-->
    <button id="btnClosepopupOnetoOne" class="CloseBtn">
        <img src="images/button_cancel.png" alt=""></button>
</div>
<!--End Info Nivel-->
<!-- Start Info Nivel-->
<div style="display: none;" id="popupInformationConditionsTerms" class="popupBody">
    <h2>Información sobre los niveles:</h2>

    <div class="popupBodytext">
        <p>1.- TITULARIDAD<br>
            El titular del Sitio Web http://www.mingles.es  (en adelante, la Web o Sitio Web) es la Sociedad "MINGLES LANGUAGES SL" (en adelante, MINGLES), con domicilio social en la Calle Velázquez 15, 1º (28001) Madrid, teléfonos de contacto (+34) 630358735 y (+34) 605606603 y dirección de correo electrónico: info@mingles.es, provista de N.I.F. núm. ESB86931722 e inscrita en el Registro Mercantil de Madrid al tomo 32022, folio 12, hoja número M576174, inscripción 1ª.
            <br>2.- CONDICIONES DE ACCESO Y USO DEL SITIO WEB<br>
            La utilización de esta Web implica la atribución de la condición de "Usuario" y, en su caso, la aceptación plena de todas las disposiciones incluidas en esta "Información Legal" desde el mismo momento en que se accede a él como tal Usuario. Asimismo el Usuario también se compromete a utilizar los contenidos y servicios disponibles a través de la Web de acuerdo a lo señalado en el apartado de "Política de Privacidad y Protección de Datos".:
        </p><p>MINGLES podrá alterar en cualquier momento y sin previo aviso el diseño, presentación y/o configuración del sitio web.</p>
        <p>Las presentes condiciones son las vigentes desde la fecha de su última actualización. MINGLES se reserva el derecho a modificarlas en cualquier momento y sin preaviso, en cuyo caso, entrarán en vigor desde su publicación en el sitio web, siendo solamente aplicables a los Usuarios que se den de alta a partir de dicha publicación.</p>
        <p>La información relativa a los servicios que se ofrecen en esta Web (contratación, características, referencias a precios, impuestos aplicables, gastos de envío, entre otros), se encuentra contenida en la misma página en donde dicho servicio se ofrece, y en particular en las Condiciones Generales de Contratación de MINGLES.</p>
        <p>MINGLES no es el titular de los otros sitios de la red a los que se pueda acceder mediante la utilización de links o hipervínculos que estén disponibles dentro de su sitio Web. En consecuencia, el Usuario accederá bajo su exclusiva responsabilidad a sus respectivos contenidos y condiciones de uso.</p>
        <p>No obstante, se autoriza al Usuario a realizar enlaces o links que conduzcan al Sitio Web de MINGLES siempre y cuando:</p>
        <ol>
            <li>Quede constancia de la identidad de su propietario.</li>
            <li>No se utilice la marca MINGLES o cualquier otro signo distintivo que pueda confundir sobre la propiedad del sitio Web que establezca el link.</li>
            <li>Las páginas o sitios de la red a través de los cuales se realice el enlace no deben atentar a la moral, al orden público y las buenas costumbres y han de ser respetuosos con los principios de no discriminación y el respeto a la dignidad de la persona, la protección a la juventud y la infancia y, en general, todos aquellos recogidos en el artículo 8 de la Ley de Servicios de la Sociedad de la Información y del Comercio Electrónico.</li>
            <li>La página web en la que se establezca el enlace no contendrá informaciones o contenidos falsos, inexactos o ilícitos.</li>
        </ol>
        <p>El establecimiento del enlace no implica en ningún caso la existencia de relaciones entre MINGLES y el propietario de la página Web en la que se establezca, ni la aceptación ni aprobación por parte de MINGLES de sus contenidos o servicios por lo que no se declarará ni se dará a entender que MINGLES ha autorizado el enlace o que ha supervisado o asumido los servicios puestos a disposición de la página web en la que se establece el enlace.</p>
        <p>MINGLES no se hace responsable bajo ningún concepto de los daños que pudieran dimanar del uso ilegal o indebido del presente Sitio Web. Al mismo tiempo, MINGLES queda eximida de cualquier responsabilidad por los daños y perjuicios que se pudieran ocasionar como consecuencia del acceso, reproducción, captación y transmisión de los contenidos y servicios de la página Web facilitados por terceros.</p>
        <p>MINGLES se reserva el derecho de modificar la presente "Información Legal", incluida su política de privacidad y comercial, con el objeto de adaptarla a las novedades legislativas o jurisprudenciales, o por criterios empresariales.</p>
        <p>El uso del presente sitio web deberá ser acorde a la ley eximiéndose MINGLES de cualquier responsabilidad que se pudiera dimanar por un uso contrario a éste.</p>
        <p>MINGLES se reserva el derecho de modificar la presente "Información Legal", incluida su política de privacidad y comercial, con el objeto de adaptarla a las novedades legislativas o jurisprudenciales, o por criterios empresariales.</p>
        <br>3.- OBJETO<br>
        <p>El presente Sitio Web ha sido diseñado para dar acceso a todo tipo de personas interesadas en practicar idiomas con profesores nativos, con el objetivo de mejorar su dicción y uso de los idiomas.</p>
        <p>En MINGLES también se puede acceder a soluciones informáticas y ofimáticas, al objeto de que a través de ella, puedan por si mismos o mediante la contratación de nuestros servicios. Este servicio se presta al cliente utilizando el sitio web www.mingles.es como medio de comunicación.</p>
        <br>4.- GARANTÍA DEL SITIO WEB<br>
        <p>MINGLES se exime de cualquier tipo de responsabilidad derivada de la información publicada en su Sitio Web, www.mingles.es o cualesquiera otros dependientes de este, siempre que esta información haya sido manipulada o introducida por un tercero ajeno a la organización.</p>
        <p>Este Sitio Web pretende reducir al mínimo los problemas ocasionados por errores de carácter técnico. No obstante, algunos datos o informaciones contenidos en las páginas pueden haber sido creados o estructurados en archivos o formatos no exentos de errores, por lo que no se puede garantizar que el servicio no quede interrumpido temporalmente o se vea afectado de cualquier otra forma por tales problemas técnicos. MINGLES no asume responsabilidad alguna respecto de dichos fallos o errores técnicos que se presenten resultado de la consulta de las páginas de www.mingles.es, o de aquellas otras externas objeto de reenvío de la página principal.</p>
        <p>En cualquier momento y sin necesidad de previo aviso al Usuario, MINGLES podrá llevar a cabo la suspensión temporal de servicios en su Sitio Web por problemas técnicos o causas de fuerza mayor. Asimismo, MINGLES se reserva el derecho a interrumpir de forma temporal o definitiva los servicios facilitados a través de su Sitio Web, advirtiéndolo, cuando así sea posible, mediante un anuncio en la propia Web.</p>
        <br>5.- PROPIEDAD INTELECTUAL<br>
        <p>El contenido del Sitio Web es una obra original y protegida por el Derecho de Propiedad Intelectual con todos los derechos reservados para MINGLES, salvo establecimiento expreso de otro tipo de licencia en los contenidos, no permitiéndose la reproducción ni transmisión en forma alguna ni por ningún medio, de parte o de todo el contenido del material de este Sitio Web, incluyendo sin ser limitativo, texto, gráficos, código de diseño, etc., sin previa autorización del autor. En ningún momento podrá ser objeto de copia, modificación o descompilación del código fuente mediante ingeniería inversa o cualquier otra técnica al efecto.</p>
        <p>Todos los logotipos y marcas de MINGLES, se encuentran registrados como marcas en los registros públicos correspondientes, contando de esta manera con el beneficio de la fe pública registral ante cualquier tipo de controversia en este sentido.</p>
        <p>Los logotipos ajenos a MINGLES que pudieran aparecer en el sitio Web, pertenecen a sus respectivos propietarios, quienes son en todo caso los responsables de cualesquier posible controversia que pudiera darse respecto a ellos. La colocación de dichos logotipos en el Sitio Web de MINGLES ha sido en todo caso, previa comunicación y aceptación por los propietarios.</p>
        <p>El diseño del Sitio Web es propiedad exclusiva de MINGLES desde su programación, edición, compilación y demás elementos necesarios para su funcionamiento, por tanto, será necesaria la autorización expresa y por escrito de MINGLES para su reproducción parcial o total, así como para su explotación, distribución y comercialización en general.</p>
        <p>Las reclamaciones que pudieran interponerse por los Usuarios en relación con posibles incumplimientos de los derechos de propiedad intelectual o industrial sobre cualesquiera de los Contenidos de este Sitio Web deberán dirigirse a la siguiente dirección de correo electrónico: info@mingles.com</p>
        <br>6.- CONDICIONES GENERALES DE CONTRATACIÓN DEL SERVICIO MINGLES<br>
        <p>MINGLES es la organización de grupos de charla dirigidos por un profesor nativo o profesor con facultades para la comunicación y enseñanza de idiomas.</p>
        <p>La duración de las jornadas MINGLES es de una 1 hora de conversación con un profesor por grupo con grupos máximo de 5 alumnos. La asistencia a las reuniones Let´s Mingles dará derecho a una consumición (refresco, cerveza, vino o café), no siendo MINGLES responsable de las consumiciones que pudieran realizar menores de edad si los hay en los grupos MINGLES.</p>
        <p>MINGLES se reserva el derecho de no aceptar las solicitudes de contratación de servicios realizadas mediante su Sitio Web para algunos Usuarios, por considerar que no son viables técnica o económicamente para alguna de las partes, por carecer de la buena fe necesaria para el uso correcto del servicio y su continuidad, o por cualquier otra causa que estime.</p>
        <br>7.- PRECIOS DE LOS SERVICIOS<br>
        <p>Los precios de los servicios de MINGLES incluyen los impuestos indirectos vigentes e IVA salvo indicación expresa en contrario y en función de que sean aplicables o no.</p>
        <p>El precio por alumno para acceder a cada sesión es de 10 euros para sesión única o bono de 40 euros que dará derecho al acceso a 5 sesiones MINGLES.</p>
        <p>MINGLES se reserva la facultad de cambiar los precios especificados en cualquier momento de acuerdo con su política comercial. En el caso de los contratos de prestación de servicios continuada, estos cambios de precios se comunicarán a nuestros clientes y usuario con antelación de 30 días.</p>
        <br>7.1- Política de Promociones<br>
        <p>MINGLES podrá realizar promociones y ofertas especiales para usuarios y grupos mediante códigos de descuento. Estos códigos de promoción no son válidos para los eventos Mingles, ni para los Mingles “one to one” (particulares)</p>
        <p>El código de promoción es de uso exclusivo para una sesión Mingles. Una persona no puede usar diferentes código de promoción en más de una sesión, reservándose MINGLES la capacidad de rechazar aquellos códigos que no hayan sido emitidos por la propia empresa.</p>
        <p>Mingles se reserva el derecho de cancelar alguna sesión anunciada en la web. La política de cancelación de sesiones dará derecho al usuario a un BONO de asistencia gratuita para otra sesión, sin devolución del importe pagado para la primera sesión.</p>
        <p>El alumno podr&aacute; cancelar la sesi&oacute;n hasta cinco horas antes de su inicio. En caso de no hacerlo perder&aacute; el cr&eacute;dito equivalente a esa sesi&oacute;n.</p>
        <p>Mingles se reserva el derecho de admisión a las sesiones en aquellos casos de usuarios que no cumplan las condiciones generales y particulares de cada una de las sesiones.</p>
        <p>Las ofertas no son acumulables a otros promociones Mingles </p>
        <br>7.2- Cancelación de asistencia a la sesión por parte del usuario<br>
        <p>El usuario puede cancelar su asistencia a la sesión MINGLES con 24 horas de antelación de la fecha y hora prevista para la sesión. Si la cancelación ocurre antes de las 24 horas de la celebración, el usuario tendrá derecho a la asistencia a otra sesión MINGLES sin posibilidad de devolución del importe pagado. </p>
        <p>Si el USUARIO no cancela la sesión pero tampoco asiste y así lo notifica, tendrá derecho a un descuento del 50% para la siguiente sesión que desee asistir pero no tendrá opción de devolución de la cantidad pagada para la sesión que ha perdido.</p>
        <br>Formas de pago aceptadas<br>
        <p>MINGLES cobra de manera anticipada la asistencia a las sesiones usando como medios de pago admitidos Paypal y TPV virtual que admite la mayoría de tarjetas de crédito y débito emitidas. MINGLES se reserva el derecho de anular cualquier tipo de operación de venta que detecte posibilidad de fraude, uso ilegítimo de tarjetas o usurpación de personalidad mediante medios de pago, poniendo en conocimiento de las autoridades, toda la información que el sistema recopila para procesar dicho pago.</p>
        <p>MINGLES NO ALMACENA en su WEB datos de las tarjetas de crédito ni credenciales de acceso a Paypal.</p>
        <br>USO DE "COOKIES" Y DEL FICHERO DE ACTIVIDAD<br>
        <p>MINGLES por su cuenta o por medio de un tercero contratado para la prestación de servicios de medición, pueden utilizar Cookies cuando un usuario navega por las páginas del Sitio Web. Las Cookies son ficheros enviados a un navegador por medio de un servidor Web para registrar las actividades del usuario en el Sitio Web.</p>
        <p>Las Cookies utilizadas por www.mingles.es o por el tercero que actúe en su nombre, sólo registran las direcciones IP y los nombres de dominio utilizados por los Usuarios, pero nunca información personal, como su dirección de correo electrónico o sus datos personales. Por otro lado, nunca se puede extraer la información contenida en el disco duro de un Usuario mediante una Cookie. Las Cookies suelen tener una duración temporal, y el Usuario puede configurar su navegador para ser avisado en pantalla de la recepción de Cookies y para impedir su instalación en su disco duro, así como para poder conocer el servidor que utiliza las Cookies. Para éstos casos, el Usuario deberá consultar las instrucciones de uso de su navegador para ampliar esta información.</p>
        <p>El Sitio Web de MINGLES, utiliza las Cookies para los análisis estadísticos de su utilización y para mejorar el servicio prestado al Usuario. Gracias a las Cookies, resulta posible que el servidor de www.mingles.es o el tercero que actúe en su nombre, reconozca el navegador del ordenador utilizado por el usuario, con la finalidad de que la navegación sea más sencilla, permitiendo por ejemplo, el acceso a los usuarios que se hayan registrado previamente, acceder a las áreas, servicios, promociones o concursos reservados exclusivamente para ellos sin tener que registrarse en cada visita, etc. Se utilizan también para medir la audiencia y parámetros del tráfico, controlar el progreso y el número de entradas.</p>
        <p>Para utilizar el Sitio Web de MINGLES, no resulta necesario que el usuario permita la instalación de las Cookies que él envía, o que envía el tercero que actúe en su nombre, pero entonces puede que sea necesario que el usuario haga la actividad de inicio de sesión en cada uno de los servicios cuya prestación requiera el registro previo o "login".</p>
        <p>Los servidores Web de www.mingles.com detectan de manera automática la dirección IP y el nombre de dominio utilizados por el usuario. Esta información es registrada en un fichero de actividad del servidor, que permite el posterior procesamiento de los datos para los fines estadísticos y de servicio especificados: obtener mediciones estadísticas, conocer el número de impresiones de páginas, el número de visitas realizadas a los servicios web, etc.</p>
        <br>9.- CAPACIDAD INFORMÁTICA MÍNIMA NECESARIA<br>
        <ul>
            <li>Navegador Estándar (Explorer ó Firefox) </li>
            <li>Adobe Acrobat Reader</li>
        </ul>
        <p>El ordenador/tablet/movil también deberá tener acceso a correo electrónico. La resolución esta optimizada para una pantalla de 1024 x 768.</p>
        <br>10 DERECHOS DE ACCESO, RECTIFICACION, CANCELACION Y OPOSION<br>
        <p>El usuario de MINGLES tiene usted derecho a ejercitar sus derechos de acceso, rectificación o cancelación de sus datos u oponerse al tratamiento de sus datos personales, comunicándolo por escrito a MINGLES LANGUAGES SL, en C/ Velázquez 15, 1º 28001 Madrid (Madrid), España, adjuntando una fotocopia de su D.N.I. por ambas caras, la petición en que se concreta su solicitud, fechada y firmada, así como su domicilio a efectos de notificaciones.</p>
        <p>También puede ejercitar dichos derechos mediante correo electrónico a la dirección info@mingles.es </p>
        <p>MINGLES tiene sus ficheros de CLIENTES, USUARIOS y PROVEEDORES inscritos en la AEPD con el número de registro 142822/2014</p>
        <br>11.- LEY APLICABLE Y JURISDICCIÓN<br>
        <p>Para toda cuestión litigiosa que incumba al Sitio Web de www.MINGLES.es o cualquiera de los que de él dependa, será de aplicación la legislación española, siendo competentes para la resolución de todos los conflictos derivados o relacionados con el uso del presente sitio Web, los Juzgados y Tribunales de Madrid (España).</p>
        <p>Se pone a disposición de los Usuarios los siguientes medios donde podrán dirigir sus peticiones, cuestiones y reclamaciones:</p>
        <ul>
            <li>Envío por correo postal a la siguiente dirección: Velázquez 15, 1º (28021)-Madrid.</li>
            <li>Comunicación por medio de una llamada telefónica al número de teléfono: (+34) 630358735</li>
        </ul>

    </div>
    <!--    <button class="btn btn-info">
            Cerrar</button>-->
    <button id="btnClosepopupInfoNivel" class="CloseBtn">
        <img src="<?=$base_url('assets/images/button_cancel.png')?>" alt=""></button>
</div>
<!--End Info Nivel-->
=======
<!--<start bottom-logo>-->
<div class="bottom-logo">
    <div class="wrapper">

        <a href="https://www.youtube.com/watch?v=g297nBgypdg&feature=youtu.be" target="_blank">
            <img src="http://www.mingles.es/assets/images/bottom_logo13.png" alt="Minles en TVE"></a>

        <a href="http://goo.gl/citLc8" target="_blank">
            <img src="http://www.mingles.es/assets/images/bottom_logo11.jpg" alt=""></a>

        <a href="http://elpais.com/elpais/2013/10/14/eps/1381753903_217752.html"                                                  target="_blank">
            <img src="http://www.mingles.es/assets/images/bottom_logo2.jpg" alt=""></a>

        <a href="http://blogs.20minutos.es/capeando-la-crisis/2014/04/15/conversacion-en-ingles-en-tu-bar-favorito/" target="_blank">
            <img src="http://www.mingles.es/assets/images/bottom_logo3.jpg" alt=""></a>

        <a href="http://goo.gl/BAcDyV" target="_blank">
            <img src="http://www.mingles.es/assets/images/bottom_logo8.jpg" alt="Mingles en la cope"></a>

        <a href="https://www.youtube.com/watch?v=u02HHsGxQeM" target="_blank">
            <img src="http://www.mingles.es/assets/images/bottom_logo7.png" alt="Minles en la2"></a>

        <a href="http://cadenaser.com/emisora/2014/07/02/audios/1404262582_660215.html" target="_blank">
            <img src="http://www.mingles.es/assets/images/bottom_logo9.jpg" alt="Mingles en la cadena SER"></a>

        <a href="https://www.youtube.com/watch?v=LFh0YhLvcwQ" target="_blank">
            <img src="http://www.mingles.es/assets/images/bottom_logo10.jpg" alt="Mingles se va de bartes"></a>

    </div>
</div>
<!--<end bottom-logo>-->


<!--<start bottom>-->
<div class="bottom">
    <div class="wrapper">
        <div class="information">
            <h2>Información</h2>
            <span id="aboutus" class="pointer">Sobre nosotros</span><br>
            <span class="pointer"><a href="<?=base_url('medios')?>">Prensa</a></span>
            <span><a href="http://www.mingles.es/blog">Blog</a></span><br>
            <span><a href="<?=base_url('media')?>">Videos</a></span><br>
            <span><a href="<?=base_url('partners')?>">Partners</a></span><br>
        </div>
        <div class="information">
            <h2>Contacto</h2>
            <span id="contactUS" class="pointer">Contacta con nosotros</span>
            <a class="typeform-share button" href="https://mingles.typeform.com/to/SVLWGZ" data-mode="1" target="_blank">German Mingles</a>
<a class="typeform-share button" href="https://mingles.typeform.com/to/mFxBD4" data-mode="1" target="_blank">&iquest;Qu&eacute; te parece la web?</a>
<script>(function(){var qs,js,q,s,d=document,gi=d.getElementById,ce=d.createElement,gt=d.getElementsByTagName,id='typef_orm',b='https://s3-eu-west-1.amazonaws.com/share.typeform.com/';if(!gi.call(d,id)){js=ce.call(d,'script');js.id=id;js.src=b+'share.js';q=gt.call(d,'script')[0];q.parentNode.insertBefore(js,q)}id=id+'_';if(!gi.call(d,id)){qs=ce.call(d,'link');qs.rel='stylesheet';qs.id=id;qs.href=b+'share-button.css';s=gt.call(d,'head')[0];s.appendChild(qs,s)}})()</script>
        </div>
        <div class="yellow_box">
            <span id="teacher" class="pointer">¿ERES UN PROFESOR?
                <p>
                    APÚNTATE AQUÍ
                </p>
            </span>
        </div>
        <div class="pink_box">
            <span id="bar" class="pointer">¿ERES UN BAR/PUB?
                <p>
                    APÚNTATE AQUÍ
                </p>
            </span>
        </div>
        <div class="blue_box">
            <span id="Company" class="pointer">¿QUIERES MINGLES EN TU CIUDAD?
            </span>
        </div>
    </div>
</div>
<!--<end bottom>-->
<!--<start footer>-->
<footer>
    <div class="footer">
        <div class="wrapper">
            <div class="footer_lt">
                MINGLES LANGUAGES<br>
                La nueva forma de aprender Idiomas
            </div>

            <div class="footer_rt">
                <img src="images/footer_logo.png" alt=""><br>
                &copy;Mingles Languages S.L. Todos los derechos reservados.
            </div>
        </div>
    </div>
</footer>
<!--<end footer>-->
</div>
<!--Section For Popup-->
<div style="display: none;" id="overlay">
</div>
<div style="display: none;" id="popupAboutus" class="popupBody">
    <h2>Sobre Nosotros</h2>
    <div class="popupBodytext">
        <p>
            Somos unos jóvenes con ganas de emprender en este novedoso proyecto. Mingles es una comunidad de personas interesadas en mejorar el conocimiento de idiomas. Nos encanta conocer gente nueva, los ambientes internacionales y multiculturales y promovemos el consumo colaborativo LET’s MINGLE!!!
        </p>
        <img src="images/team.jpg" alt="Sobre Nosotros">
    </div>
<!--    <button class="btn btn-info">
        Close</button>-->
    <button id="CloseBtn" class="CloseBtn">
        <img src="images/button_cancel.png" alt=""></button>
</div>
<!-- Start Contact US-->
<div style="display: none;" id="popupContactus" class="popupBody">
    <h2>
        Contacto</h2>
    <h3>Nos vas a enviar un mensaje &iexcl;Let's Mingle!</h3>
    <p>
    </p>
    <div class="popupBodytext">
        <form id="modalContactForm">
            <label for="Asunto" class="pl-label">
                Asunto:
            </label>
            <input required="" id="Asunto" name="Asunto" type="text">
            <div class="clear">
            </div>
            <label for="email" class="pl-label">
                Email:</label>
            <input required="" id="email" name="email" type="text">
            <div class="clear">
            </div>
            <label for="phone" class="pl-label">
                Nombre:</label>
            <input required="" id="phone" name="phone" type="text" />
            <div class="clear">
            </div>
            <label for="message" class="pl-label">
                Mensaje:</label>
            <textarea required="" id="address" name="address" type="text"></textarea>
            <div class="clear">
            </div>
            <button id="sendEmailContact" class="btn btn-info" aria-hidden="true">
                Send Email</button>
            <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">
                Close</button>
        </form>
    </div>
    <button id="btnclosemodalContactForm" class="CloseBtn">
        <img src="images/button_cancel.png" alt=""></button>
</div>
<!-- END Contact US-->
<!-- Start Info Nivel-->
<div style="display: none;" id="popupInfoNivel" class="popupBody">
    <h2>Información sobre los niveles:</h2>
    <div class="popupBodytext">
        <p>
        <ul>
            <li><b>Beginner (Usuario b&aacute;sico):</b> Es capaz de comprender y utilizar expresiones cotidianas de uso muy frecuente as&iacute; como frases sencillas destinadas a satisfacer necesidades de tipo inmediato. Puede presentarse a s&iacute; mismo y a otros, pedir y dar información personal b&aacute;sica sobre su domicilio, sus pertenencias y las personas que conoce. Puede relacionarse de forma elemental siempre que su interlocutor hable despacio y con claridad y esté dispuesto a cooperar.</li>
            <li><b>Intermediate (Intermedio):</b>  Es capaz de comprender los puntos principales de textos claros y en lengua est&aacute;ndar si tratan sobre cuestiones que le son conocidas, ya sea en situaciones de trabajo, de estudio o de ocio. Sabe desenvolverse en la mayor parte de las situaciones que pueden surgir durante un viaje por zonas donde se utiliza la lengua. Es capaz de producir textos sencillos y coherentes sobre temas que le son familiares o en los que tiene un inter&eacute;s personal. Puede describir experiencias, acontecimientos, deseos y aspiraciones, así como justificar brevemente sus opiniones o explicar sus planes.</li>
            <li><b>Upper Intermediate (Intermedio Alto):</b> Es capaz de entender las ideas principales de textos complejos que traten de temas tanto concretos como abstractos, incluso si son de car&aacute;cter t&eacute;cnico siempre que est&eacute;n dentro de su campo de especialización. Puede relacionarse con hablantes nativos con un grado suficiente de fluidez y naturalidad de modo que la comunicaci&oacute;n se realice sin esfuerzo por parte de ninguno de los interlocutores. Puede producir textos claros y detallados sobre temas diversos así como defender un punto de vista sobre temas generales indicando los pros y los contras de las distintas opciones.</li>
            <li><b>Advance (Avanzado):</b> Es capaz de comprender una amplia variedad de textos extensos y con cierto nivel de exigencia, así como reconocer en ellos sentidos impl&iacute;citos. Sabe expresarse de forma fluida y espont&aacute;nea sin muestras muy evidentes de esfuerzo para encontrar la expresi&oacute;n adecuada. Puede hacer un uso flexible y efectivo del idioma para fines sociales, acad&eacute;micos y profesionales. Puede producir textos claros, bien estructurados y detallados sobre temas de cierta complejidad, mostrando un uso correcto de los mecanismos de organizaci&oacute;n, articulaci&oacute;n y cohesi&oacute;n del texto.</li>
        </ul>
        </p>
    </div>
    <!--    <button class="btn btn-info">
            Cerrar</button>-->
    <button id="btnClosepopupInfoNivel" class="CloseBtn">
        <img src="images/button_cancel.png" alt=""></button>
</div>
<!--End Info Nivel-->
<!-- Start Info Nivel-->
<div style="display: none;" id="popupOnetoOne" class="popupBody">
    <h2>Plan Especial Mingles One To One</h2>
    <div class="popupBodytext">
        <p>
            Welcome to Mingles!
        </p>
        <p>
            Mingles también ofrece la opción de una clase one to one (tu solo con el profesor). Te apuntas también a través del calendario y nosotros te preparamos un profesor para ti. El precio es de 25 EUR por sesión o adquirir un bono con un 10% de descuento (112.5 EUR).
        </p>
        <p>Podrás seleccionar los topics de nuestro blog o incluso preparar una entrevista</p>
        <p>
            LET's MINGLE!!!
        </p>
    </div>
    <!--    <button class="btn btn-info">
            Close</button>-->
    <button id="btnClosepopupOnetoOne" class="CloseBtn">
        <img src="images/button_cancel.png" alt=""></button>
</div>
<!--End Info Nivel-->
<!-- Start Info Nivel-->
<div style="display: none;" id="popupInformationConditionsTerms" class="popupBody">
    <h2>Información sobre los niveles:</h2>

    <div class="popupBodytext">
        <p>1.- TITULARIDAD<br>
            El titular del Sitio Web http://www.mingles.es  (en adelante, la Web o Sitio Web) es la Sociedad "MINGLES LANGUAGES SL" (en adelante, MINGLES), con domicilio social en la Calle Velázquez 15, 1º (28001) Madrid, teléfonos de contacto (+34) 630358735 y (+34) 605606603 y dirección de correo electrónico: info@mingles.es, provista de N.I.F. núm. ESB86931722 e inscrita en el Registro Mercantil de Madrid al tomo 32022, folio 12, hoja número M576174, inscripción 1ª.
            <br>2.- CONDICIONES DE ACCESO Y USO DEL SITIO WEB<br>
            La utilización de esta Web implica la atribución de la condición de "Usuario" y, en su caso, la aceptación plena de todas las disposiciones incluidas en esta "Información Legal" desde el mismo momento en que se accede a él como tal Usuario. Asimismo el Usuario también se compromete a utilizar los contenidos y servicios disponibles a través de la Web de acuerdo a lo señalado en el apartado de "Política de Privacidad y Protección de Datos".:
        </p><p>MINGLES podrá alterar en cualquier momento y sin previo aviso el diseño, presentación y/o configuración del sitio web.</p>
        <p>Las presentes condiciones son las vigentes desde la fecha de su última actualización. MINGLES se reserva el derecho a modificarlas en cualquier momento y sin preaviso, en cuyo caso, entrarán en vigor desde su publicación en el sitio web, siendo solamente aplicables a los Usuarios que se den de alta a partir de dicha publicación.</p>
        <p>La información relativa a los servicios que se ofrecen en esta Web (contratación, características, referencias a precios, impuestos aplicables, gastos de envío, entre otros), se encuentra contenida en la misma página en donde dicho servicio se ofrece, y en particular en las Condiciones Generales de Contratación de MINGLES.</p>
        <p>MINGLES no es el titular de los otros sitios de la red a los que se pueda acceder mediante la utilización de links o hipervínculos que estén disponibles dentro de su sitio Web. En consecuencia, el Usuario accederá bajo su exclusiva responsabilidad a sus respectivos contenidos y condiciones de uso.</p>
        <p>No obstante, se autoriza al Usuario a realizar enlaces o links que conduzcan al Sitio Web de MINGLES siempre y cuando:</p>
        <ol>
            <li>Quede constancia de la identidad de su propietario.</li>
            <li>No se utilice la marca MINGLES o cualquier otro signo distintivo que pueda confundir sobre la propiedad del sitio Web que establezca el link.</li>
            <li>Las páginas o sitios de la red a través de los cuales se realice el enlace no deben atentar a la moral, al orden público y las buenas costumbres y han de ser respetuosos con los principios de no discriminación y el respeto a la dignidad de la persona, la protección a la juventud y la infancia y, en general, todos aquellos recogidos en el artículo 8 de la Ley de Servicios de la Sociedad de la Información y del Comercio Electrónico.</li>
            <li>La página web en la que se establezca el enlace no contendrá informaciones o contenidos falsos, inexactos o ilícitos.</li>
        </ol>
        <p>El establecimiento del enlace no implica en ningún caso la existencia de relaciones entre MINGLES y el propietario de la página Web en la que se establezca, ni la aceptación ni aprobación por parte de MINGLES de sus contenidos o servicios por lo que no se declarará ni se dará a entender que MINGLES ha autorizado el enlace o que ha supervisado o asumido los servicios puestos a disposición de la página web en la que se establece el enlace.</p>
        <p>MINGLES no se hace responsable bajo ningún concepto de los daños que pudieran dimanar del uso ilegal o indebido del presente Sitio Web. Al mismo tiempo, MINGLES queda eximida de cualquier responsabilidad por los daños y perjuicios que se pudieran ocasionar como consecuencia del acceso, reproducción, captación y transmisión de los contenidos y servicios de la página Web facilitados por terceros.</p>
        <p>MINGLES se reserva el derecho de modificar la presente "Información Legal", incluida su política de privacidad y comercial, con el objeto de adaptarla a las novedades legislativas o jurisprudenciales, o por criterios empresariales.</p>
        <p>El uso del presente sitio web deberá ser acorde a la ley eximiéndose MINGLES de cualquier responsabilidad que se pudiera dimanar por un uso contrario a éste.</p>
        <p>MINGLES se reserva el derecho de modificar la presente "Información Legal", incluida su política de privacidad y comercial, con el objeto de adaptarla a las novedades legislativas o jurisprudenciales, o por criterios empresariales.</p>
        <br>3.- OBJETO<br>
        <p>El presente Sitio Web ha sido diseñado para dar acceso a todo tipo de personas interesadas en practicar idiomas con profesores nativos, con el objetivo de mejorar su dicción y uso de los idiomas.</p>
        <p>En MINGLES también se puede acceder a soluciones informáticas y ofimáticas, al objeto de que a través de ella, puedan por si mismos o mediante la contratación de nuestros servicios. Este servicio se presta al cliente utilizando el sitio web www.mingles.es como medio de comunicación.</p>
        <br>4.- GARANTÍA DEL SITIO WEB<br>
        <p>MINGLES se exime de cualquier tipo de responsabilidad derivada de la información publicada en su Sitio Web, www.mingles.es o cualesquiera otros dependientes de este, siempre que esta información haya sido manipulada o introducida por un tercero ajeno a la organización.</p>
        <p>Este Sitio Web pretende reducir al mínimo los problemas ocasionados por errores de carácter técnico. No obstante, algunos datos o informaciones contenidos en las páginas pueden haber sido creados o estructurados en archivos o formatos no exentos de errores, por lo que no se puede garantizar que el servicio no quede interrumpido temporalmente o se vea afectado de cualquier otra forma por tales problemas técnicos. MINGLES no asume responsabilidad alguna respecto de dichos fallos o errores técnicos que se presenten resultado de la consulta de las páginas de www.mingles.es, o de aquellas otras externas objeto de reenvío de la página principal.</p>
        <p>En cualquier momento y sin necesidad de previo aviso al Usuario, MINGLES podrá llevar a cabo la suspensión temporal de servicios en su Sitio Web por problemas técnicos o causas de fuerza mayor. Asimismo, MINGLES se reserva el derecho a interrumpir de forma temporal o definitiva los servicios facilitados a través de su Sitio Web, advirtiéndolo, cuando así sea posible, mediante un anuncio en la propia Web.</p>
        <br>5.- PROPIEDAD INTELECTUAL<br>
        <p>El contenido del Sitio Web es una obra original y protegida por el Derecho de Propiedad Intelectual con todos los derechos reservados para MINGLES, salvo establecimiento expreso de otro tipo de licencia en los contenidos, no permitiéndose la reproducción ni transmisión en forma alguna ni por ningún medio, de parte o de todo el contenido del material de este Sitio Web, incluyendo sin ser limitativo, texto, gráficos, código de diseño, etc., sin previa autorización del autor. En ningún momento podrá ser objeto de copia, modificación o descompilación del código fuente mediante ingeniería inversa o cualquier otra técnica al efecto.</p>
        <p>Todos los logotipos y marcas de MINGLES, se encuentran registrados como marcas en los registros públicos correspondientes, contando de esta manera con el beneficio de la fe pública registral ante cualquier tipo de controversia en este sentido.</p>
        <p>Los logotipos ajenos a MINGLES que pudieran aparecer en el sitio Web, pertenecen a sus respectivos propietarios, quienes son en todo caso los responsables de cualesquier posible controversia que pudiera darse respecto a ellos. La colocación de dichos logotipos en el Sitio Web de MINGLES ha sido en todo caso, previa comunicación y aceptación por los propietarios.</p>
        <p>El diseño del Sitio Web es propiedad exclusiva de MINGLES desde su programación, edición, compilación y demás elementos necesarios para su funcionamiento, por tanto, será necesaria la autorización expresa y por escrito de MINGLES para su reproducción parcial o total, así como para su explotación, distribución y comercialización en general.</p>
        <p>Las reclamaciones que pudieran interponerse por los Usuarios en relación con posibles incumplimientos de los derechos de propiedad intelectual o industrial sobre cualesquiera de los Contenidos de este Sitio Web deberán dirigirse a la siguiente dirección de correo electrónico: info@mingles.com</p>
        <br>6.- CONDICIONES GENERALES DE CONTRATACIÓN DEL SERVICIO MINGLES<br>
        <p>MINGLES es la organización de grupos de charla dirigidos por un profesor nativo o profesor con facultades para la comunicación y enseñanza de idiomas.</p>
        <p>La duración de las jornadas MINGLES es de una 1 hora de conversación con un profesor por grupo con grupos máximo de 5 alumnos. La asistencia a las reuniones Let´s Mingles dará derecho a una consumición (refresco, cerveza, vino o café), no siendo MINGLES responsable de las consumiciones que pudieran realizar menores de edad si los hay en los grupos MINGLES.</p>
        <p>MINGLES se reserva el derecho de no aceptar las solicitudes de contratación de servicios realizadas mediante su Sitio Web para algunos Usuarios, por considerar que no son viables técnica o económicamente para alguna de las partes, por carecer de la buena fe necesaria para el uso correcto del servicio y su continuidad, o por cualquier otra causa que estime.</p>
        <br>7.- PRECIOS DE LOS SERVICIOS<br>
        <p>Los precios de los servicios de MINGLES incluyen los impuestos indirectos vigentes e IVA salvo indicación expresa en contrario y en función de que sean aplicables o no.</p>
        <p>El precio por alumno para acceder a cada sesión es de 10 euros para sesión única o bono de 40 euros que dará derecho al acceso a 5 sesiones MINGLES.</p>
        <p>MINGLES se reserva la facultad de cambiar los precios especificados en cualquier momento de acuerdo con su política comercial. En el caso de los contratos de prestación de servicios continuada, estos cambios de precios se comunicarán a nuestros clientes y usuario con antelación de 30 días.</p>
        <br>7.1- Política de Promociones<br>
        <p>MINGLES podrá realizar promociones y ofertas especiales para usuarios y grupos mediante códigos de descuento. Estos códigos de promoción no son válidos para los eventos Mingles, ni para los Mingles “one to one” (particulares)</p>
        <p>El código de promoción es de uso exclusivo para una sesión Mingles. Una persona no puede usar diferentes código de promoción en más de una sesión, reservándose MINGLES la capacidad de rechazar aquellos códigos que no hayan sido emitidos por la propia empresa.</p>
        <p>Mingles se reserva el derecho de cancelar alguna sesión anunciada en la web. La política de cancelación de sesiones dará derecho al usuario a un BONO de asistencia gratuita para otra sesión, sin devolución del importe pagado para la primera sesión.</p>
        <p>El alumno podr&aacute; cancelar la sesi&oacute;n hasta cinco horas antes de su inicio. En caso de no hacerlo perder&aacute; el cr&eacute;dito equivalente a esa sesi&oacute;n.</p>
        <p>Mingles se reserva el derecho de admisión a las sesiones en aquellos casos de usuarios que no cumplan las condiciones generales y particulares de cada una de las sesiones.</p>
        <p>Las ofertas no son acumulables a otros promociones Mingles </p>
        <br>7.2- Cancelación de asistencia a la sesión por parte del usuario<br>
        <p>El usuario puede cancelar su asistencia a la sesión MINGLES con 24 horas de antelación de la fecha y hora prevista para la sesión. Si la cancelación ocurre antes de las 24 horas de la celebración, el usuario tendrá derecho a la asistencia a otra sesión MINGLES sin posibilidad de devolución del importe pagado. </p>
        <p>Si el USUARIO no cancela la sesión pero tampoco asiste y así lo notifica, tendrá derecho a un descuento del 50% para la siguiente sesión que desee asistir pero no tendrá opción de devolución de la cantidad pagada para la sesión que ha perdido.</p>
        <br>Formas de pago aceptadas<br>
        <p>MINGLES cobra de manera anticipada la asistencia a las sesiones usando como medios de pago admitidos Paypal y TPV virtual que admite la mayoría de tarjetas de crédito y débito emitidas. MINGLES se reserva el derecho de anular cualquier tipo de operación de venta que detecte posibilidad de fraude, uso ilegítimo de tarjetas o usurpación de personalidad mediante medios de pago, poniendo en conocimiento de las autoridades, toda la información que el sistema recopila para procesar dicho pago.</p>
        <p>MINGLES NO ALMACENA en su WEB datos de las tarjetas de crédito ni credenciales de acceso a Paypal.</p>
        <br>USO DE "COOKIES" Y DEL FICHERO DE ACTIVIDAD<br>
        <p>MINGLES por su cuenta o por medio de un tercero contratado para la prestación de servicios de medición, pueden utilizar Cookies cuando un usuario navega por las páginas del Sitio Web. Las Cookies son ficheros enviados a un navegador por medio de un servidor Web para registrar las actividades del usuario en el Sitio Web.</p>
        <p>Las Cookies utilizadas por www.mingles.es o por el tercero que actúe en su nombre, sólo registran las direcciones IP y los nombres de dominio utilizados por los Usuarios, pero nunca información personal, como su dirección de correo electrónico o sus datos personales. Por otro lado, nunca se puede extraer la información contenida en el disco duro de un Usuario mediante una Cookie. Las Cookies suelen tener una duración temporal, y el Usuario puede configurar su navegador para ser avisado en pantalla de la recepción de Cookies y para impedir su instalación en su disco duro, así como para poder conocer el servidor que utiliza las Cookies. Para éstos casos, el Usuario deberá consultar las instrucciones de uso de su navegador para ampliar esta información.</p>
        <p>El Sitio Web de MINGLES, utiliza las Cookies para los análisis estadísticos de su utilización y para mejorar el servicio prestado al Usuario. Gracias a las Cookies, resulta posible que el servidor de www.mingles.es o el tercero que actúe en su nombre, reconozca el navegador del ordenador utilizado por el usuario, con la finalidad de que la navegación sea más sencilla, permitiendo por ejemplo, el acceso a los usuarios que se hayan registrado previamente, acceder a las áreas, servicios, promociones o concursos reservados exclusivamente para ellos sin tener que registrarse en cada visita, etc. Se utilizan también para medir la audiencia y parámetros del tráfico, controlar el progreso y el número de entradas.</p>
        <p>Para utilizar el Sitio Web de MINGLES, no resulta necesario que el usuario permita la instalación de las Cookies que él envía, o que envía el tercero que actúe en su nombre, pero entonces puede que sea necesario que el usuario haga la actividad de inicio de sesión en cada uno de los servicios cuya prestación requiera el registro previo o "login".</p>
        <p>Los servidores Web de www.mingles.com detectan de manera automática la dirección IP y el nombre de dominio utilizados por el usuario. Esta información es registrada en un fichero de actividad del servidor, que permite el posterior procesamiento de los datos para los fines estadísticos y de servicio especificados: obtener mediciones estadísticas, conocer el número de impresiones de páginas, el número de visitas realizadas a los servicios web, etc.</p>
        <br>9.- CAPACIDAD INFORMÁTICA MÍNIMA NECESARIA<br>
        <ul>
            <li>Navegador Estándar (Explorer ó Firefox) </li>
            <li>Adobe Acrobat Reader</li>
        </ul>
        <p>El ordenador/tablet/movil también deberá tener acceso a correo electrónico. La resolución esta optimizada para una pantalla de 1024 x 768.</p>
        <br>10 DERECHOS DE ACCESO, RECTIFICACION, CANCELACION Y OPOSION<br>
        <p>El usuario de MINGLES tiene usted derecho a ejercitar sus derechos de acceso, rectificación o cancelación de sus datos u oponerse al tratamiento de sus datos personales, comunicándolo por escrito a MINGLES LANGUAGES SL, en C/ Velázquez 15, 1º 28001 Madrid (Madrid), España, adjuntando una fotocopia de su D.N.I. por ambas caras, la petición en que se concreta su solicitud, fechada y firmada, así como su domicilio a efectos de notificaciones.</p>
        <p>También puede ejercitar dichos derechos mediante correo electrónico a la dirección info@mingles.es </p>
        <p>MINGLES tiene sus ficheros de CLIENTES, USUARIOS y PROVEEDORES inscritos en la AEPD con el número de registro 142822/2014</p>
        <br>11.- LEY APLICABLE Y JURISDICCIÓN<br>
        <p>Para toda cuestión litigiosa que incumba al Sitio Web de www.MINGLES.es o cualquiera de los que de él dependa, será de aplicación la legislación española, siendo competentes para la resolución de todos los conflictos derivados o relacionados con el uso del presente sitio Web, los Juzgados y Tribunales de Madrid (España).</p>
        <p>Se pone a disposición de los Usuarios los siguientes medios donde podrán dirigir sus peticiones, cuestiones y reclamaciones:</p>
        <ul>
            <li>Envío por correo postal a la siguiente dirección: Velázquez 15, 1º (28021)-Madrid.</li>
            <li>Comunicación por medio de una llamada telefónica al número de teléfono: (+34) 630358735</li>
        </ul>

    </div>
    <!--    <button class="btn btn-info">
            Cerrar</button>-->
    <button id="btnClosepopupInfoNivel" class="CloseBtn">
        <img src="<?=$base_url('assets/images/button_cancel.png')?>" alt=""></button>
</div>
<!--End Info Nivel-->
