<?php
require 'common/config.php';
session_start();

if (isset($_SESSION['ses_UserDetailsID']) && (trim($_SESSION['ses_UserDetailsID']) != '')) {

    $dsn = "mysql:host=$server;dbname=$db";
    $pdo = new PDO($dsn, $user, $pass);
    $pdo->beginTransaction();
    try {
        $statement = $pdo->prepare("SELECT LD.UserID,LD.UserDetailsID, LD.EmailAddress, LD.UserPassword, LD.UserTypeID, UT.UserTypeName,UD.UserFullName
                                    FROM logindetails as LD 
                                    LEFT JOIN  USERDetails AS UD ON UD.`UserDetailsID`=LD.`UserDetailsID` 
                                    LEFT JOIN  USERTYPE AS UT ON UT.UserTypeID=LD.UserTypeID 
                                    WHERE LD.`UserDetailsID`='" . trim($_SESSION['ses_UserDetailsID']) . "'");


        $statement->execute();
        $count = $statement->rowCount();
        if ($count != 0) {
            $UserDetails = $statement->fetch();
        }
    } catch (PDOException $e) {
        
    }

    unset($pdo);
}
?>

<!doctype html>
<html>
    <head>
        <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=UTF-8">
        <META name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
        <META name="apple-mobile-web-app-capable" content="yes" />
        <title><?php echo $pageTitle ?></title>
        <meta name="description" content="<?php echo $pageDescription ?>">
        <meta name="keywords" content="<?php echo $pageKeywords ?>">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css' />
        <!--        <link href="css/style.css" type="text/css" rel="stylesheet" />-->
        <link href="css/style.css?<?php echo time(); ?>" type="text/css" rel="stylesheet" />
        <link href="css/popup.css" type="text/css" rel="stylesheet" />

        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
        <script src="//code.jquery.com/jquery-1.10.2.js"></script>
        <script src="//code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
<!--        <script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>-->
        <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDY0kkJiTPVd2U7aTOAwhc9ySH6oHxOIYM&sensor=false">
        </script>
        <link rel="stylesheet" href="/resources/demos/style.css">

        <link rel="stylesheet" href="js/jquery.bxslider.css" type="text/css" />
        <script type="text/javascript" src="js/jquery.bxslider.js"></script>
        <script type="text/javascript" src="js/scripts.js"></script>

        <script type="text/javascript">
            var loggedIn = <?php if ($this->session->userdata('usertype_id') == 1) {
    echo $this->session->userdata('user_id');
} else{ ?> ""; <?php }?>;

        </script>
        
        <script type="text/javascript">

                                    function DropDown(el) {

                                        this.dd = el;

                                        this.initEvents();

                                    }

                                    DropDown.prototype = {
                                        initEvents: function() {

                                            var obj = this;



                                            obj.dd.on('click', function(event) {

                                                $(this).toggleClass('active');

                                                event.stopPropagation();

                                            });

                                        }

                                    }

                                    $(function() {



                                        var dd = new DropDown($('#dd'));



                                        $(document).click(function() {

                                            // all dropdowns

                                            $('.wrapper-dropdown-2').removeClass('active');

                                        });



                                    });

                                </script>

        <script src="js/main.js"></script>	

        <script type="text/javascript">
            window.onload = function() {
                document.getElementById("aboutus").onclick = function() {
                    var overlay = document.getElementById("overlay");
                    var popup = document.getElementById("popupAboutus");
                    overlay.style.display = "block";
                    popup.style.display = "block";
                };

                document.getElementById("CloseBtn").onclick = function() {
                    var overlay = document.getElementById("overlay");
                    var popup = document.getElementById("popupAboutus");
                    overlay.style.display = "none";
                    popup.style.display = "none";
                }

                document.getElementById("contactUS").onclick = function() {
                    alert(1);
                    var overlay = document.getElementById("overlay");
                    var popup = document.getElementById("popupContactus");
                    overlay.style.display = "block";
                    popup.style.display = "block";
                };


                document.getElementById("btnclosemodalContactForm").onclick = function() {
                    var overlay = document.getElementById("overlay");
                    var popup = document.getElementById("popupContactus");
                    overlay.style.display = "none";
                    popup.style.display = "none";
                }

                document.getElementById("InfoNivel").onclick = function() {
                    var overlay = document.getElementById("overlay");
                    var popup = document.getElementById("popupInfoNivel");
                    overlay.style.display = "block";
                    popup.style.display = "block";
                };


                document.getElementById("btnClosepopupInfoNivel").onclick = function() {
                    var overlay = document.getElementById("overlay");
                    var popup = document.getElementById("popupInfoNivel");
                    overlay.style.display = "none";
                    popup.style.display = "none";
                }

                document.getElementById("OnetoOne").onclick = function() {
                    var overlay = document.getElementById("overlay");
                    var popup = document.getElementById("popupOnetoOne");
                    overlay.style.display = "block";
                    popup.style.display = "block";
                };


                document.getElementById("btnClosepopupOnetoOne").onclick = function() {
                    var overlay = document.getElementById("overlay");
                    var popup = document.getElementById("popupOnetoOne");
                    overlay.style.display = "none";
                    popup.style.display = "none";
                }

                document.getElementById("tuOption").onclick = function() {
                    var overlay = document.getElementById("overlay");
                    var popup = document.getElementById("popupContactus");

                    overlay.style.display = "block";
                    popup.style.display = "block";

                    var Asunto = document.getElementById("Asunto");
                    Asunto.value = "Tu Opinión nos Importa";
                    document.getElementById("Asunto").disabled = true;
                }

                document.getElementById("teacher").onclick = function() {
                    var overlay = document.getElementById("overlay");
                    var popup = document.getElementById("popupContactus");

                    overlay.style.display = "block";
                    popup.style.display = "block";

                    var Asunto = document.getElementById("Asunto");
                    Asunto.value = "Contacto de Profesor";
                    document.getElementById("Asunto").disabled = true;
                }


                document.getElementById("bar").onclick = function() {
                    var overlay = document.getElementById("overlay");
                    var popup = document.getElementById("popupContactus");

                    overlay.style.display = "block";
                    popup.style.display = "block";

                    var Asunto = document.getElementById("Asunto");
                    Asunto.value = "Contacto de Bar";
                    document.getElementById("Asunto").disabled = true;
                }

                document.getElementById("Company").onclick = function() {
                    var overlay = document.getElementById("overlay");
                    var popup = document.getElementById("popupContactus");

                    overlay.style.display = "block";
                    popup.style.display = "block";

                    var Asunto = document.getElementById("Asunto");
                    Asunto.value = "Quiero Mingles en mi ciudad";
                    document.getElementById("Asunto").disabled = true;
                }
            };


        </script>

    </head>
    <body class="home" data-twttr-rendered="true">
        <div class="container">
            <!--<start header>-->
            <header>
                <div class="wrapper">

                    <div class="logo"><a href="index.php">
                            <img src="images/logo.png"></a></div>
                    <div class="header_rt">
                        <div class="login">
                            <?php
                            if ($this->session->userdata('user_id') === FALSE) {
                                ?>
                                <a href="admin/">Login / Register</a>
                                <?php
                            } else {?>
                                <a href="<?=base_url('admin')?>">Perfil</a> | <a href="<?=base_url('admin')?>">Logout</a>
                            <?php }?>


                        </div>
                        <div class="top-menu"><a href="#">Espa&ntilde;ol - (ES)</a></div>

                        <div class="select-city">
                            <select name="ddlCityHeader" id="ddlCityHeader">
                            </select>	
                        </div>

                        <div class="height20"></div>
                        <div class="social_icon"><a href="http://www.facebook.com/MinglesSpain" target="_blank">
                                <img src="images/facebook.png"></a><a href="http://twitter.com/MinglesSpain" target="_blank"><img src="images/twitter.png"></a><a href="http://www.youtube.com/channel/UCJN5c55Ik3iozb1HYZMqVIw" target="_blank"><img src="images/youtube.png"></a><a href="http://www.linkedin.com/company/mingles?trk=company_name" target="_blank"><img src="images/linkedin.png"></a><a href="http://gplus.to/Mingles" target="_blank"><img src="images/google_plus.png"></a></div>
                        <div class="height0"></div>
                        <nav>
                            <div class="responsiveMenuSelect">



                                <div class="wrapper-demo">

                                    <!-----start-wrapper-dropdown-2---->

                                    <div id="dd" class="wrapper-dropdown-2" tabindex="1">menu<span><img src="images/icon-menu.png"/></span>

                                        <ul class="dropdown">
                                            <li><a href="#">IDIOMAS</a>
                                                <ul class="sub-menu">
                                                <li><a href="<?=base_url('whatsmingles')?>">¿Qué es?</a></li>
                                                <!--<li><a href="<?/*=base_url('metodo')*/?>">Nuestro Método</a> </li>-->
                                                <li><a href="http://www.mingles.es/page/spanish">#Spanish</a> </li>
                                                <li><a href="http://www.mingles.es/page/frenchmingles">#French</a> </li>
                                                <li><a href="http://www.mingles.es/page/portuguese">#Portuguese</a> </li>
                                                </ul>
                                            </li>
                                            <li><a href="<?=base_url('premiumcards')?>">PREMIUM CARD</a></li>
                                            <li><a href="#">Los Espacios</a>
                                                <ul class="sub-menu">
                                                    <li><a href="bares.php">Locales Mingles</a></li>
                                                    <li><a href="http://www.mingles.es/page/empresa">Empresas y Coworkings</a></li>
                                                    <li><a href="http://www.mingles.es/page/womensmingles">La Radio</a></li>
                                                    <li><a href="http://www.mingles.es/page/kids">Kids Mingles</a></li>
                                                    <li><a href="eventos.php">Eventos</a></li>
                                                </ul>

                                            </li>
                                            <li><a href="bono.php">BONO MINGLES</a></li>

                                        </ul>
                                    </div>

                                </div>

                                <!-----end-wrapper-dropdown-2---->

                                <!-----start-script---->

 

                            </div>


                            <ul>
                                            <li><a href="#">IDIOMAS</a>
                                                <ul class="sub-menu">
                                                <li><a href="<?=base_url('whatsmingles')?>">¿Qué es?</a></li>
                                                <!--<li><a href="<?/*=base_url('metodo')*/?>">Nuestro Método</a> </li>-->
                                                <li><a href="http://www.mingles.es/page/spanish">#Spanish</a> </li>
                                                <li><a href="http://www.mingles.es/page/frenchmingles">#French</a> </li>
                                                <li><a href="http://www.mingles.es/page/portuguese">#Portuguese</a> </li>
                                                </ul>
                                            </li>
                                <li><a href="<?=base_url('premiumcards')?>">PREMIUM CARD</a></li>
                                <li><a href="#">Los Espacios</a>
                                    <ul class="sub-menu">
                                        <li><a href="bares.php">Locales Mingles</a></li>
                                        <li><a href="http://www.mingles.es/page/empresa">Empresas y Coworkings</a></li>
                                        <li><a href="http://www.mingles.es/page/womensmingles">En la Radio</a></li>
                                        <li><a href="http://www.mingles.es/page/kids">Kids Mingles</a></li>
                                        <li><a href="eventos.php">Eventos</a></li>
                                    </ul>

                                </li>
                                <li><a href="bono.php">BONO MINGLES</a></li>

                            </ul>


                        </nav>

                    </div>

                    <div class="height15"></div>
                </div>

                <div class="banner">

                    <ul class="bxslider">
                        <li>
                            <!--<img src="images/banner/banner.jpg" alt="" id="Img1" class="bgwidth"></li>-->
                            <img src="assets/images/banner/MINGLES_INGLES_1.png" alt="" id="Img1" class="bgwidth"></li>
                        <li>
                            <!--<img src="images/banner/banner1.jpg" alt="" id="Img2" class="bgwidth"></li>-->
                            <img src="assets/images/banner/MINGLES_INGLES_2.png" alt="" id="Img2" class="bgwidth"></li>
                        <li>
                            <img src="assets/images/banner/banner2.jpg" alt="" id="Img3" class="bgwidth"></li>
                    </ul>

                </div>
            </header>
            <!--<end header>-->
