<!DOCTYPE html>
<html lang="en">
	<head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <title>Mingles</title>
        <link href="<?=base_url()?>assets/css/style.css" type="text/css" rel="stylesheet" />
        <link href="<?=base_url()?>assets/css/popup.css" type="text/css" rel="stylesheet" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery.gritter.css" />
        <link rel="stylesheet" type="text/css" href="<?=base_url('assets/css/message/themes/message_default.css')?>">
        <script type="text/javascript" src="<?=base_url('assets/js/message.js')?>"></script>
        <script type="text/javascript">
            var GB_ROOT_DIR = "<?=base_url()?>assets/greybox/";
        </script>

        <script type="text/javascript" src="<?=base_url()?>assets/greybox/AJS.js"></script>
        <script type="text/javascript" src="<?=base_url()?>assets/greybox/AJS_fx.js"></script>
        <script type="text/javascript" src="<?=base_url()?>assets/greybox/gb_scripts.js"></script>
        <link href="<?=base_url()?>assets/greybox/gb_styles.css" rel="stylesheet" type="text/css" media="all" />
        <link rel="stylesheet" href="<?=base_url('assets/css/jquery-ui.css')?>">
<!--        <script src="<?/*=base_url('assets/js/jquery-1.10.2.js')*/?>"></script>-->
        <script src="<?=base_url('assets/js/jquery-1.11.1.min.js')?>"></script>
        <script src="<?=base_url('assets/js/jquery-migrate-1.2.1.min.js')?>"></script>


        <script src="<?php echo base_url(); ?>assets/js/jquery.gritter.min.js"></script>
        <script src="<?=base_url('assets/js/jquery-ui.js')?>"></script>
        <link href="<?=base_url('assets/css/pgwmodal.css')?>" rel="stylesheet" type="text/css">

        <script src="<?=base_url('assets/js/pgwmodal.min.js')?>"></script>
        <script language="javascript" type="text/javascript">
            <!--
            function afterclose() {
                window.location.reload()
            }

            // -->
        </script>

        <script type="text/javascript">

            function DropDown(el) {

                this.dd = el;


                this.initEvents();

            }

            function DropDownS(el) {
                this.dd_sub = el;

                this.initEvents();
            }

            DropDown.prototype = {
                initEvents: function() {

                    var obj = this;



                    obj.dd.on('click', function(event) {
                        $('div#dd_sub.wrapper-dropdown-2').removeClass('active');
                        $(this).toggleClass('active');
                        $(this).parent().parent().css('z-index', 9999);
                        $('div#dd_sub.wrapper-dropdown-2').parent().parent().css('z-index', 1);

                        event.stopPropagation();

                    });

/*                    obj.dd_sub.on('click', function(event) {

                        $(this).toggleClass('active');

                        event.stopPropagation();

                    });*/

                }

            }

            DropDownS.prototype = {
                initEvents: function() {

                    var obj = this;



                    obj.dd_sub.on('click', function(event) {
                        $('div#dd.wrapper-dropdown-2').removeClass('active');
                        $(this).toggleClass('active');

                        event.stopPropagation();

                    });

                    /*                    obj.dd_sub.on('click', function(event) {

                     $(this).toggleClass('active');

                     event.stopPropagation();

                     });*/

                }

            }

            $(function() {



                var dd = new DropDown($('#dd'));
                var dd_sub = new DropDownS($('#dd_sub'));


                $(document).click(function() {

                    // all dropdowns

                    $('.wrapper-dropdown-2').removeClass('active');

                });



            });

        </script>
    </head>
    <body class="home" data-twttr-rendered="true">
    <div class="container">
        <!--<start header>-->
        <header>
            <div class="wrapper">

                <div class="logo"><a href="<?=base_url()?>">
                        <img src="<?=base_url()?>assets/images/logo.png"></a></div>
                <div class="header_rt">
                    <div class="top-menu">

                        <?php if ($this->session->userdata['usertype_id'] == '4') { ?>
                            <nav>
                                <div class="responsiveMenuSelect">



                                    <div class="wrapper-demo">

                                        <!-----start-wrapper-dropdown-2---->

                                        <div tabindex="1" class="wrapper-dropdown-2" id="dd" style="background-color: rgb(38, 41, 46);">menu<span><img src="http://local.mingles.com/assets/images/icon-menu.png"></span>

                                            <ul class="dropdown">
                                                <li><a href="<?=base_url()?>">Home</a></li>
                                                <li><a href="#">Manage Master</a>
                                                    <ul class="sub-menu">
                                                        <li><a href="<?=base_url('admin/managecity')?>">Manage Cities</a></li>
                                                        <li><a href="<?=base_url('admin/managelevel')?>">Manage Level</a></li>
                                                        <li><a href="<?=base_url('admin/managelanguage')?>">Manage Language</a></li>
                                                        <li><a href="<?=base_url('admin/managesession')?>">Manage Session</a></li>
                                                        <li><a href="<?=base_url('admin/managepromocode')?>">Manage Promo Code</a></li>
                                                        <li><a href="<?=base_url('admin/managebones')?>">Manage Bones</a></li>
                                                    </ul>
                                                </li>
                                                <li><a href="#">Administration</a>
                                                    <ul class="sub-menu">
                                                        <li><a href="<?=base_url('admin/managepubs')?>">Manage Pubs</a> </li>
                                                        <li><a href="<?=base_url('admin/manageteacher')?>">Manage Teacher</a></li>
                                                        <li><a href="<?=base_url('admin/managestudent')?>">Manage Student</a></li>
                                                        <!--                                                <li><a href="ManageUsers.php">Manage Users</a></li>-->
                                                        <li><a href="<?=base_url('admin/manageclass')?>">Manage Class</a></li>
                                                        <li><a href="<?=base_url('admin/managegroup')?>">Manage Group</a></li>
                                                        <li><a href="<?=base_url('admin/email')?>">Email System</a></li>
                                                        <li><a href="<?=base_url('admin/pages/landing_list')?>">Landing Pages</a></li>
                                                        <li><a href="<?=base_url('admin/managegroup/export')?>">Export</a></li>
                                                        <li><a href="<?=base_url('admin/profile/rate_day')?>">Group of the day</a></li>
                                                    </ul>
                                                </li>
                                                <li><a href="<?=base_url()?>admin/dashboard/logout">Logout</a></li>
                                            </ul>
                                        </div>

                                    </div>




                                    <!-----end-wrapper-dropdown-2---->

                                    <!-----start-script---->



                                </div>
                                <ul class="nav">
                                    <li><a href="<?=base_url()?>">Home</a></li>
                                    <li><a href="#">Manage Master</a>
                                        <ul class="sub-menu">
                                            <li><a href="<?=base_url('admin/managecity')?>">Manage Cities</a></li>
                                            <li><a href="<?=base_url('admin/managelevel')?>">Manage Level</a></li>
                                            <li><a href="<?=base_url('admin/managelanguage')?>">Manage Language</a></li>
                                            <li><a href="<?=base_url('admin/managesession')?>">Manage Session</a></li>
                                            <li><a href="<?=base_url('admin/managepromocode')?>">Manage Promo Code</a></li>
                                            <li><a href="<?=base_url('admin/managebones')?>">Manage Bones</a></li>
                                        </ul>
                                    </li>

                                    <li><a href="#">Administration</a>
                                        <ul class="sub-menu">
                                            <li><a href="<?=base_url('admin/managepubs')?>">Manage Pubs</a> </li>
                                            <li><a href="<?=base_url('admin/manageteacher')?>">Manage Teacher</a></li>
                                            <li><a href="<?=base_url('admin/managestudent')?>">Manage Student</a></li>
                                            <!--                                                <li><a href="ManageUsers.php">Manage Users</a></li>-->
                                            <li><a href="<?=base_url('admin/manageclass')?>">Manage Class</a></li>
                                            <li><a href="<?=base_url('admin/managegroup')?>">Manage Group</a></li>
                                            <li><a href="<?=base_url('admin/email')?>">Email System</a></li>
                                            <li><a href="<?=base_url('admin/pages/landing_list')?>">Landing Pages</a></li>
                                            <li><a href="<?=base_url('admin/managegroup/export')?>">Export</a></li>
                                            <li><a href="<?=base_url('admin/profile/rate_day')?>">Group of the day</a></li>

                                            <!--     <li><a href="GroupandStudentDetails.php"> Group & Student </a></li>-->
                                            <!--<li><a href="StudentPaymentDetails.php">Payment Details</a></li>-->
                                        </ul>
                                    </li>
                                    <!--   <li><a href="#"><?php echo $this->session->userdata('user_fullname') ?></a>
                                                <ul class="sub-menu">
                                                    <li><a href="users_New.php?id=<?php echo $_SESSION['ses_UserID'] ?>" rel="gb_page_center[700, 400]" title="Manage Profile">Profile</a></li>
                                                    <li><a href="changePassword.php?id=<?php echo $_SESSION['ses_UserID'] ?>" rel="gb_page_center[600, 300]" title="Change Password">Change Password</a></li>
                                                </ul>
                                            </li>-->
                                    <li><a href="<?=base_url()?>admin/dashboard/logout">Logout</a></li>
                                </ul>
                            </nav>
                        <?php } ?>

                        <div class="height20"></div>
                    </div>
                    <div class="height15"></div>
                    <div class="social_icon"><a href="http://www.facebook.com/MinglesSpain" target="_blank">
                            <img src="<?=base_url()?>assets/images/facebook.png"></a><a href="http://twitter.com/MinglesSpain" target="_blank"><img src="<?=base_url()?>assets/images/twitter.png"></a><a href="http://www.youtube.com/channel/UCJN5c55Ik3iozb1HYZMqVIw" target="_blank"><img src="<?=base_url()?>assets/images/youtube.png"></a><a href="http://www.linkedin.com/company/mingles?trk=company_name" target="_blank"><img src="<?=base_url()?>assets/images/linkedin.png"></a><a href="http://gplus.to/Mingles" target="_blank"><img src="<?=base_url()?>assets/images/google_plus.png"></a></div>
                    <div class="height0"></div>
                    <nav>

                        <div class="responsiveMenuSelect">



                            <div class="wrapper-demo">

                                <!-----start-wrapper-dropdown-2---->

                                <div tabindex="1" class="wrapper-dropdown-2" id="dd_sub">menu #2<span><img src="http://local.mingles.com/assets/images/icon-menu.png"></span>

                                    <ul class="dropdown">
                                        <li><a href="#">IDIOMAS</a>
                                            <ul class="sub-menu">
                                                <li><a href="<?=base_url('whatsmingles')?>">¿Qué es?</a></li>
                                                <!--<li><a href="<?/*=base_url('metodo')*/?>">Nuestro Método</a> </li>-->
                                                <li><a href="http://www.mingles.es/page/spanish">#Spanish</a> </li>
                                                <li><a href="http://www.mingles.es/page/frenchmingles">#French</a> </li>
                                                <li><a href="http://www.mingles.es/page/portuguese">#Portuguese</a> </li>
                                            </ul>
                                        </li>
                                        <li><a href="<?=base_url('premiumcards')?>">PREMIUM CARD</a></li>
                                        <li><a href="#">Los Espacios</a>
                                            <ul class="sub-menu">
                                                <li><a href="<?=base_url('bares')?>">Locales Mingles</a></li>
                                                <li><a href="http://www.mingles.es/page/empresa">Empresas y Coworkings</a></li>
                                                <li><a href="http://www.mingles.es/page/womensmingles">En la Radio</a></li>
                                                <li><a href="http://www.mingles.es/page/kids">Kids Mingles</a></li>
                                                <li><a href="<?=base_url('eventos')?>">Eventos</a></li>
                                            </ul>

                                        </li>
                                        <li><a href="#">Blog</a>
                                            <ul class="sub-menu">
                                                <li><a href="http://www.mingles.es/blog/?cat=1">Topics</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="<?=base_url('bono')?>">BONO MINGLES</a></li>
                                    </ul>
                                </div>

                            </div>




                            <!-----end-wrapper-dropdown-2---->

                            <!-----start-script---->



                        </div>

                        <ul>
                            <li><a href="#">IDIOMAS</a>
                                <ul class="sub-menu">
                                                <li><a href="<?=base_url('whatsmingles')?>">¿Qué es?</a></li>
                                                <!--<li><a href="<?/*=base_url('metodo')*/?>">Nuestro Método</a> </li>-->
                                                <li><a href="http://www.mingles.es/page/spanish">#Spanish</a> </li>
                                                <li><a href="http://www.mingles.es/page/frenchmingles">#French</a> </li>
                                                <li><a href="http://www.mingles.es/page/portuguese">#Portuguese</a> </li>
                                </ul>
                            </li>
                            <li><a href="<?=base_url('premiumcards')?>">PREMIUM CARD</a></li>
                            <li><a href="#">Los Espacios</a>
                                <ul class="sub-menu">
                                    <li><a href="<?=base_url('bares')?>">Locales Mingles</a></li>
                                    <li><a href="http://www.mingles.es/page/empresa">Empresas y Coworkings</a></li>
                                    <li><a href="http://www.mingles.es/page/womensmingles">En la Radio</a></li>
                                    <li><a href="http://www.mingles.es/page/kids">Kids Mingles</a></li>
                                    <li><a href="<?=base_url('eventos')?>">Eventos</a></li>
                                </ul>

                            </li>
                            <li><a href="#">Blog</a>
                                <ul class="sub-menu">
                                    <li><a href="http://www.mingles.es/blog/?cat=1">Topics</a></li>
                                </ul>
                            </li>
                            <li><a href="<?=base_url('bono')?>">BONO MINGLES</a></li>
                        </ul>


                    </nav>
                </div>
        </header>
        <div class="height30">
        </div>

        <?php if($this->session->flashdata('notification-type') != "") { ?>
            <script>
                $.gritter.add({
                    // (string | mandatory) the heading of the notification
                    title: "<?php echo $this->session->flashdata('notification-title'); ?>",
                    // (string | mandatory) the text inside the notification
                    text: "<?php echo $this->session->flashdata('notification-text'); ?>",
                    // (bool | optional) if you want it to fade out on its own or just sit there
                    sticky: false,
                    // (int | optional) the time you want it to be alive for before fading out
                    time: '7500',
                    // (string | optional) the class name you want to apply to that specific message
                    class_name: "<?php echo $this->session->flashdata('notification-type'); ?>"
                });
            </script>
        <?php } ?>
