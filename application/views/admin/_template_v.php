<?php 
	$this->load->view('admin/components/header_v', @$data);

	$this->load->view($page_info['view_name'], @$data);

	$this->load->view('admin/components/footer_v', @$data);
?>