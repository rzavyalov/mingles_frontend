<script>
    $(function() {
        $('.change_status').click(function () {
            $.pgwModal({
                url: $(this).data('url'),
                loadingContent: '<span style="text-align:center">Loading in progress</span>',
                title: 'Edit class',
                closable: true,
                titleBar: false
            });
        });
    });

</script>
<div class="inner-content">
<div class="title_bgd">
    <div class="wrapper"><?=$this->lang->line('welcome_panel')?></div>
</div>
<input id="class_id" style="display: none" value="<?=$id?>">
<?php print $this->user_type?>

<?php switch($this->user_type) {
    case STUDENT:
        $this->load->view('admin/components/userDashBoardLeft');
        break;
    case ADMIN:
        $this->load->view('admin/components/admin_userDashBoardLeft');
        break;
    case TEACHER:
        $this->load->view('admin/components/teacherDashBoardLeft');
        break;
    case PUB:
        $this->load->view('admin/components/pubDashBoardLeft');
        $this->load->view('admin/pub_class');
        break;
}?>

<?php if($this->user_type != PUB) :?>
<div class="profile-right">

<h1>Mis Mingles</h1>
<!--        <p><div class="add-new-button">  <p><a href="../index.php#option" title="Add New Class">Add New Class</a></p></div>
        <div class="clear"></div>
        </p>-->
<?php if ($this->session->userdata('usertype_id') == 4) : ?>
    <a href="<?= base_url('admin/student/dashboard/'.$user_info['user_id']) ?>" class="btn btn-info">Back</a>
<?php else: ?>
<a href="<?= base_url('admin/dashboard') ?>" class="btn btn-info">Back</a>
<?php endif;?>
<!--<img src="<? /*= base_url() */ ?>assets/images/steps2.png" alt="" height="55px" width="58%">-->

<div class="price-box">
    <label><?=$this->lang->line('credit')?></label> <input type="text" name="price" value="<?= $user_info['credit'] ?>">
</div>
<br/>
<br/>

<p> Puedes ver qui&eacute;n es tu profesor de la sesi&oacute;n y los otros Minglers, los grupos son muy flexibles as&iacute;
    que esto puede cambiar hasta el &uacute;ltimo momento. Despu&eacute;s de tu sesi&oacute;n puedes puntuar y dejar tu
    opni&oacute;n sobre el local y sobre tu profesor, as&iacute; como de tu experiencia en general &iexcl;Seguimos
    mejorando por y par ti!</p>
<br/>

<h1><?=$this->lang->line('details_sessions')?></h1>
<?php if(!isset($no_teacher)):?>
<div style="float: left; padding-right: 20px">
    <div class="span3 center profile-pic">
												<span id="user_avatar_container"
                                                    <?php if (count($studentclass_details)) : ?>
                                                      <?php if (@$studentclass_details['user_id'] != TRUE) { ?>style="display:block;"<?php } ?>>
                                                                    <?php if (!file_exists(FCPATH . 'users/profile/' . $studentclass_details['user_id'] . '.jpg')) {
                                                                        ?>
                                                                        <img
                                                                            src="<?= base_url() ?>assets/images/defaultM.png"
                                                                            alt="" height="175px" width="175px">
                                                                    <?php } else { ?>
                                                                        <img
                                                                            src="<?= base_url() ?>users/profile/<?= $studentclass_details['user_id'] ?>.jpg"
                                                                            height="175px" width="175px"/>
                                                                    <?php } ?>
                                                    <?php endif; ?>
												</span>
    </div>
    <h2 style="color: #34a6fb;font-size: 17px;text-align: center"><?= @$studentclass_details['TeacherName'] ?></h2>
</div>
<?php endif;?>
<p>&nbsp;</p>

<div style="padding-top: 20px;font-size: 16px; float: left;">
    <p><strong><?=$this->lang->line('class')?>: </strong><?= $class_details['ClassName'] ?></p>

    <p><strong><?=$this->lang->line('date')?>: </strong><?= $class_details['ClassDate'] ?></p>

    <p><strong><?=$this->lang->line('time')?>: </strong><?= $class_details['ClassStartTime'] ?></p>

    <p><strong><?=$this->lang->line('place')?>: </strong><?= $class_details['PubName'] ?></p>

    <p><strong><?=$this->lang->line('city')?>: </strong><?= $class_details['CityName'] ?></p>

    <p><strong><?=$this->lang->line('language')?>: </strong><?= $class_details['LanguageName'] ?></p>

    <p><strong>Nivel: </strong><?= $level['LevelName'] ?></p>


    <?php if ($this->session->userdata('usertype_id') == 4) : ?>
        <p><strong>Estado: </strong><a data-url="<?=base_url('admin/student/change_transaction_status/'.$id.'/'.$user_info['user_id'])?>" class="change_status" href="#"><strong style="color:green"><?= ($is_payed) ? 'Paid' : 'not paid' ?></strong></a></p>
    <?php else :?>
        <p><strong>Estado: </strong><strong style="color:green"><?= ($is_payed) ? 'Paid' : 'not paid' ?></strong></p>
    <?php endif;?>
    <?php if ($can_cancel) : ?>
        <a class="btn btn-info cancelclass" href="#" data-name="<?= base_url('admin/userclass/' . $id) ?>" data-url="<?= base_url('admin/userclass/cancel/' . $id) ?>">Cancelar</a>
    <?php endif ?>
</div>

<?php if(!isset($no_group) && $is_payed && !isset($no_teacher)):?>
<div style="padding-top: 20px;padding-left: 20px;font-size: 16px; float: left;">
    <?php if (count($studentclass_details)) : ?>
        <?php if (!$has_rate) : ?>
            <form id="rate_form" action="<?= base_url('admin/teacher/updaterate') ?>" data-remote="true" accept-charset="UTF-8"
            method="POST">
            <input type="hidden" name="user_id" value="<?= @$studentclass_details['user_id'] ?>"/>
            <input type="hidden" name="class_id" value="<?= $id ?>"/>
            <input type="hidden" name="group_id" value="<?= $studentclass_details['GroupID'] ?>"/>

            <div>
                <p>Clarity of explanations</p>
                <input type="range" value="0" step="1" name="rate_explanation" id="backing1">

                <div class="rateit" data-rateit-backingfld="#backing1" data-rateit-resetable="false"
                     data-rateit-ispreset="true"
                     data-rateit-min="0" data-rateit-max="5">
                </div>
                <p>Error correction</p>
                <input type="range" value="0" step="1" name="rate_error_correction" id="backing2">

                <div class="rateit" data-rateit-backingfld="#backing2" data-rateit-resetable="false"
                     data-rateit-ispreset="true"
                     data-rateit-min="0" data-rateit-max="5">
                </div>
                <p>Knowledge of the topic</p>
                <input type="range" value="0" step="1" name="rate_punctuality" id="backing3">

                <div class="rateit" data-rateit-backingfld="#backing3" data-rateit-resetable="false"
                     data-rateit-ispreset="true"
                     data-rateit-min="0" data-rateit-max="5">
                </div>
                <p>Overall experience</p>
                <input type="range" value="0" step="1" name="rate_overal_experience" id="backing4">

                <div class="rateit" data-rateit-backingfld="#backing4" data-rateit-resetable="false"
                     data-rateit-ispreset="true"
                     data-rateit-min="0" data-rateit-max="5">
                </div>
            </div>
            <div>
                <p>Comment:</p>



                <textarea id="text_comment" name="comment" cols="20" rows="9" style="border: 1px solid;"></textarea>
                <span id="charsLeft"></span> characters left.
            </div>
<!--            <div>
                <input type="submit" class="btn btn-info" value="Save" name="update_profile">
            </div>-->
        <?php else: ?>
            <div style="width: 300px;float: left">
                <p><strong>Clarity of explanations</strong></p>

                <div class="rateit" data-rateit-value="<?= $rate_explanation ?>" min="0" max="5"
                     data-rateit-readonly="true" style="padding-left: 50px;"></div>
                <p><strong>Error correction</strong></p>

                <div class="rateit" data-rateit-value="<?= $rate_error_correction ?>" min="0" max="5"
                     data-rateit-readonly="true" style="padding-left: 50px;"></div>
                <p><strong>Knowledge of the topic</strong></p>

                <div class="rateit" data-rateit-value="<?= $rate_punctuality ?>" min="0" max="5"
                     data-rateit-readonly="true" style="padding-left: 50px;"></div>
                <p><strong>Overall experience</strong></p>

                <div class="rateit" data-rateit-value="<?= $rate_overal_experience ?>" min="0" max="5"
                     data-rateit-readonly="true" style="padding-left: 50px;"></div>
            </div>
            <?php if ($has_comment): ?>
                <div>
                    <div>
                        <p>Comentario (profesor):</p>
                    </div>
                    <div><b><?= $comment ?></b></div>
                </div>
            <?php else: ?>
                <form id="rate_form" action="<?= base_url('admin/student/updaterate') ?>" data-remote="true" accept-charset="UTF-8"
                      method="POST">
                    <input type="hidden" name="user_id" value="<?= @$studentclass_details['user_id'] ?>"/>
                    <input type="hidden" name="class_id" value="<?= $id ?>"/>

                    <div>
                        <p><strong>Comment:</strong></p>
                        <textarea id="pub_comment" name="comment" cols="45" rows="9" style="border: 1px solid;"></textarea>
                        <span id="PubCharsLeft"></span> characters left.
                    </div>
<!--                    <div>
                        <input type="submit" class="btn btn-info" value="Save" name="update_profile">
                    </div>-->
                </form>
            <?php endif; ?>
        <?php endif; ?>
        </form>
    <?php endif; ?>
</div>

<!--PUB rate-->
<div style="padding-top: 20px;padding-left: 20px;font-size: 16px; float: left;">
    <?php if (!$has_rate_pub) : ?>
        <form id="pub_form" action="<?= base_url('admin/pub/updaterate') ?>" data-remote="true" accept-charset="UTF-8" method="POST">
            <div style="width: 300px;float: left">
                <p><strong>Pub: </strong><?= $class_details['PubName'] ?></p>
                <input type="range" value="0" step="1" name="rate_pub" id="backing5">

                <div class="rateit" data-rateit-backingfld="#backing5" data-rateit-resetable="false"
                     data-rateit-ispreset="true"
                     data-rateit-min="0" data-rateit-max="5">
                </div>
                <input type="hidden" name="user_id" value="<?= @$studentclass_details['user_id'] ?>"/>
                <input type="hidden" name="pub_id" value="<?= @$class_details['PubID'] ?>"/>
                <input type="hidden" name="class_id" value="<?= $id ?>"/>
                <input type="hidden" name="group_id" value="<?= $studentclass_details['GroupID'] ?>"/>

                <div>
                    <p><strong>Comentario (Pub):</strong></p>
                    <textarea id="pub_comment" name="comment" cols="20" rows="9" style="border: 1px solid;"></textarea>

                    <span id="PubCharsLeft"></span> characters left.
                </div>
<!--                <div style="">
                    <input type="submit" class="btn btn-info" value="Save" name="update_profile">
                </div>-->
            </div>
        </form>
    <?php else: ?>
        <p><strong>Pub: </strong><?= $class_details['PubName'] ?></p>
        <div class="rateit" data-rateit-value="<?= $rate_pub ?>" min="0" max="5"
             data-rateit-readonly="true" style="padding-left: 50px;"></div>
        <p><strong>Comentario (Pub):</strong></p>
        <?php if($has_comment_pub):?>
        <div><b><?= $comment_pub ?></b></div>
            <?php else: ?>
            <form id="pub_form" action="<?= base_url('admin/pub/updaterate') ?>" data-remote="true" accept-charset="UTF-8" method="POST">
                <div style="width: 300px;float: left">
                    <input type="hidden" name="user_id" value="<?= @$studentclass_details['user_id'] ?>"/>
                    <input type="hidden" name="pub_id" value="<?= @$class_details['PubID'] ?>"/>
                    <input type="hidden" name="class_id" value="<?= $id ?>"/>
                    <input type="hidden" name="group_id" value="<?= $studentclass_details['GroupID'] ?>"/>

                    <div>
                        <textarea id="pub_comment" name="comment" cols="20" rows="9" style="border: 1px solid;"></textarea>
                        <span id="PubCharsLeft"></span> characters left.
                    </div>
<!--                    <div style="">
                        <input type="submit" class="btn btn-info" value="Save" name="update_profile">
                    </div>-->
                </div>
            </form>
            <?php endif;?>
    <?php endif; ?>

</div>
    <?php if(!$has_comment_pub || !$has_rate_pub || !$has_rate || !$has_comment) :?>
    <div class="clear"></div>
    <div>
        <button id="ratebutton" class="btn btn-info">Save</button>
    </div>
    <?php endif; ?>
    <?php elseif (isset($no_group)):?>
    <div><strong>You not assigned to group</strong></div>
    <?php elseif (isset($no_teacher)):?>
    <div><strong>Group not assigned teacher</strong></div>
<?php endif ?>
<?php if (!$is_payed) : ?>

    <form name="myForm" action="<?= base_url('paypal') ?>?sandbox=0" method="post">
        <input type="hidden" name="action" value="process"/>
        <input type="hidden" name="cmd" value="_cart"/> <?php // use _cart for cart checkout         ?>
        <input type="hidden" name="currency_code" value="EUR"/>
        <input type="hidden" name="invoice" value="<?php echo date("His") . rand(1234, 9632); ?>"/>

        <input type="hidden" name="name" value="<?= $this->session->userdata('user_fullname') ?>"/>
        <input type="hidden" name="email" value="<?= $this->session->userdata('email') ?>"/>
        <input type="hidden" name="ddlClass" value="<?= $class_details['ClassID'] ?>"/>
        <input type="hidden" name="ddlPub" value="<?= $class_details['PubID'] ?>"/>
        <input type="hidden" name="ddlLevel" value="<?= $class_details['LevelID'] ?>"/>
        <input type="hidden" name="ddlLanguage" value="<?= $class_details['LanguageID'] ?>"/>
        <input type="hidden" name="promoCode" value=""/>
        <!--<input type="hidden" name="ddlSession" value="" />-->

<!--        <select id="ddlSession" name="ddlSession">
            <?php /*foreach ($sessions as $session) : */?>
                <option value="<?/*= $session['SessionPaymentID'] */?>"><?/*= $session['SessionPaymentName'] */?></option>
            <?php /*endforeach; */?>
        </select>-->
        <input type="submit" value="Apuntarme" class="submit_bt" id="joinus" name="info">
    </form>
<?php endif; ?>

<p>&nbsp;</p>
<table border='0' cellpadding='0' class='tablefrom'>
    <tr>
        <th>Foto perfil</th>
        <th>Mingler</th>
    </tr>

    <?php if(!isset($no_group)):?>
    <?php foreach ($students as $student) : ?>
        <tr>
            <td>
                <?php if (!file_exists(FCPATH . 'users/profile/' . $student['uid'] . '.jpg')) {
                    ?>
                    <img src="<?= base_url() ?>assets/images/defaultM.png" alt="" height="175px" width="175px">
                <?php } else { ?>
                    <img src="<?= base_url() ?>users/profile/<?= $student['uid'] ?>.jpg" height="175px"
                         width="175px"/>
                <?php } ?>
            </td>
            <td><?= $student['user_fullname'] ?></td>
        </tr>
    <?php endforeach; ?>
    <?php endif;?>

</table>
</div>
</div>

<script src="<?=base_url('/assets/js/jquery.limit-1.2.source.js')?>"></script>



<script>
$(function(){
    $('#text_comment').limit('700','#charsLeft');
    $('#pub_comment').limit('700','#PubCharsLeft');

    $('.change_status').click(function() {
        $.pgwModal({
            url: $(this).data('url'),
            loadingContent: '<span style="text-align:center">Loading in progress</span>',
            title: 'Edit class',
            closable: true,
            titleBar: false
        });
    });
})




    $('.cancelclass').click(function () {
        var title_msg = $(this).attr('title');
        var url = $(this).data('url');

        var obj = $(this).data('name');
        dhtmlx.message({
            type: "confirm",
            text: "¿Estás seguro que quieres cancelar tu sesión Mingles?",
            title: title_msg,
            callback: function (e) {
                if (e) {
                    $.ajax({
                        url: url
                    }).done(function (e) {
                        window.location = obj;
                    });
                }
            }
        });
    })

    $('#ratebutton').click(function(e) {


            var title_msg = $(this).attr('title');
            var url = $(this).data('url');
            var obj = $(this).parent().parent();
            dhtmlx.message({
                type:"confirm",
                text: "Quieres guardar los comentarios sobre tu profesor y el local?",
                title: title_msg,
                callback: function(e) {
                    if(e)
                    {
                        if($('#rate_form').length) {
                            $.ajax({
                                url: '/admin/teacher/updaterate',
                                data: $('#rate_form').serialize(),
                                type: 'post',
                                async: false
                            });
                        }
                        if($('#pub_form').length) {
                            $.ajax({
                                url: '/admin/pub/updaterate',
                                data: $('#pub_form').serialize(),
                                type: 'post',
                                async: false
                            }).done(function () {
                                window.location.href = "/admin/userclass/" + $('#class_id').val();
                            });
                        }
                    }
                }
            });



    })
</script>

</body>
<?php endif;?>