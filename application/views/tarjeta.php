<div class="height30">
</div>
<!--<start top-section> -->
<div class="inner-content">
    <div class="title_bg">
        <div class="wrapper">TARJETA PREMIUM MINGLES</div>
    </div>
    <br>
    <p align="center">
        <img src="<?=base_url()?>assets/images/tarjeta_mingles.png">
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <img src="<?=base_url()?>assets/images/tarjeta_mingles2.png">
    </p>
    <br><br>
    <div class="wrapper">
         <h3> La tarjeta Premium Mingles te proporciona descuentos y promociones en los locales Mingles adem&aacute;s de
    ofertas exclusivas con nuestros partners. Consulta condiciones y &iexcl;pide ya tu tarjeta Premium Mingles!</h3>
        <br><br>
    <div class="title"> Descuentos el Locales Mingles </div>
        <br>
        <div class="container marketing">
            <div class="row">
                <div class="span4">
                    <div class="thumbnail">
                        <!-- <img data-src="js/holder/holder.js/300x200" alt="The Irish Rover">-->
                        <div class="img_inner2 fleft">
                            <img alt="The Irish Rover" src="<?=base_url()?>assets/images/irish1.jpg">
                        </div>
                        <div class="caption">
                            <address>
                                <strong>
                                    <h3>The Irish Rover</h3>
                                    Av. Brasil 7, Madrid</strong><br>
                            </address>
                            <p>Muy pronto estar&aacute;n disponibles los descuentos en The Irish Rover
                            </p>
                            <p>
                                <a class="btn btn-primary" href="http://www.theirishrover.com" target="_blank">M&aacute;s Info</a><a
                                    class="btn btn-default" href="index.php#option">Ap&uacute;ntate</a>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="span4">
                    <div class="thumbnail">
                        <div class="img_inner2 fleft">
                            <img alt="Libreria Ocho y Medio Cafe" src="<?=base_url()?>assets/images/ochoymedio.jpg">
                        </div>
                        <div class="caption">
                            <address>
                                <strong>
                                    <h3>Libreria Ocho y Medio Cafe</h3>
                                    <a href="https://www.google.es/maps/preview#!data=!1m4!1m3!1d6075!2d-3.713486!3d40.424376!4m13!3m12!1m0!1m1!1sOcho+Y+Medio+Libros+de+Cine%2C+Madrid!3m8!1m3!1d1519!2d-3.713682!3d40.424543!3m2!1i1024!2i768!4f13.1"
                                       target="_blank">C\ Martin de los Heros 11</a>, Madrid</strong><br>
                            </address>
                            <p>
                                Muy pronto estar&aacute;n disponibles los descuentos en El Ocho y Medio
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
                            <p>
                                <a class="btn btn-primary" href="http://www.ochoymedio.com/ochoymedio/quienes-somos"
                                   target="_blank">M&aacute;s Info</a><a class="btn btn-default" href="index.php#option">Ap&uacute;ntate</a>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="span4">
                    <div class="thumbnail">
                        <div class="img_inner2 fleft">
                            <img alt="Areia" src="<?=base_url()?>assets/images/areia.jpg">
                        </div>
                        <div class="caption">
                            <address>
                                <strong>
                                    <h3>Areia Chillout</h3>
                                    C\ Hortaleza, 92, Madrid</strong><br>
                            </address>
                            <p>
                                Muy pronto estar&aacute;n disponibles los descuentos en el Areia Chillout
                            </p>
                            <br>
                            <p>
                                <a class="btn btn-primary" href="http://www.areiachillout.com/" target="_blank">M&aacute;s
                                    Info</a><a class="btn btn-default" href="index.php#option">Ap&uacute;ntate</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <br>
            <div class="row">
                <div class="span4">
                    <div class="thumbnail">
                        <div class="img_inner2 fleft">
                            <img alt="Leka Leka" src="<?=base_url()?>assets/images/leka_leka.jpg">
                        </div>
                        <div class="caption">
                            <address>
                                <strong>
                                    <h3>Leka Leka Bar</h3>
                                    C\ San Bruno 3, Madrid</strong><br>
                            </address>
                            <p>
                                Muy pronto estar&aacute;n disponibles los descuentos en el Leka Leka
                            </p>
                            <p>
                                <a class="btn btn-primary" href="https://www.facebook.com/LekaLekaBar" target="_blank">M&aacute;s Info</a><a class="btn btn-default" href="index.php#option">Ap&uacute;ntate</a>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="span4">
                    <div class="thumbnail">
                        <div class="img_inner2 fleft">
                            <img alt="Flash Flash" src="<?=base_url()?>assets/images/flashflash.jpg">
                        </div>
                        <div class="caption">
                            <address>
                                <strong>
                                    <h3>Flash Flash Bar</h3>
                                    C\ Nu&ntilde;ez de Balboa 75, Madrid</strong><br>
                            </address>
                            <p>
                                Muy pronto estar&aacute;n disponibles los descuentos en Flash Flash
                            </p>
                            <br>
                            <p>
                                <a class="btn btn-primary" href="http://www.flashflashmadrid.com" target="_blank">M&aacute;s
                                    Info</a><a class="btn btn-default" href="index.php#option">Ap&uacute;ntate</a>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="span4">
                    <div class="thumbnail">
                        <div class="img_inner2 fleft">
                            <img alt="La Paca Bar" src="<?=base_url()?>assets/images/pub33.jpg">
                        </div>
                        <div class="caption">
                            <address>
                                <strong>
                                    <h3>The Thirty Three</h3>
                                    C/ Clara del Rey 33, Madrid</strong><br>
                            </address>
                            <p>
                                Muy pronto estar&aacute;n disponibles los descuentos en el Thirty Three
                            </p>
                            <br>
                            <p>
                                <a class="btn btn-primary" href="http://www.thethirtythree.es/" target="_blank">M&aacute;s Info</a><a class="btn btn-default" href="index.php#option">Ap&uacute;ntate</a>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="span4">
                    <div class="thumbnail">
                        <div class="img_inner2 fleft">
                            <img alt="La Bicicleta cafe" src="<?=base_url()?>assets/images/labicicleta.jpg">
                        </div>
                        <div class="caption">
                            <address>
                                <strong>
                                    <h3>La Bicicleta Cafe</h3>
                                    Plaza de San Ildefonso 9 (Malasa&ntilde;a), Madrid</strong><br>
                            </address>
                            <p>
                              Muy pronto estar&aacute;n disponibles los descuentos en la Bicicleta Caf&eacute;
                            </p>
                            <br>
                            <p>
                                <a class="btn btn-primary" href="http://www.labicicletacafe.com" target="_blank">M&aacute;s Info</a><a class="btn btn-default" href="index.php#option">Ap&uacute;ntate</a>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="span4">
                    <div class="thumbnail">
                        <div class="img_inner2 fleft">
                            <img alt="Cups & Kids" src="<?=base_url()?>assets/images/cupsandkids.jpg">
                        </div>
                        <div class="caption">
                            <address>
                                <strong>
                                    <h3>Cups & Kids</h3>
                                    Calle Alameda, 18 (BARRIO DE LAS LETRAS), Madrid</strong><br>
                            </address>
                            <p>
                              Muy pronto estar&aacute;n disponibles los descuentos en  Cups & Kids
                            </p>
                            <br>
                            <p>
                                <a class="btn btn-primary" href="http://cupsandkids.com/" target="_blank">M&aacute;s Info</a><a class="btn btn-default" href="index.php#option">Ap&uacute;ntate</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
