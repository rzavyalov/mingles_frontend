<?php 
	$this->load->view('components/header_page', @$data);

	$this->load->view($page_info['view_name'], @$data);

	$this->load->view('components/footer', @$data);
?>