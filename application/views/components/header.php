<?php
/*require 'common/config.php';
session_start();

if (isset($_SESSION['ses_UserDetailsID']) && (trim($_SESSION['ses_UserDetailsID']) != '')) {

    $dsn = "mysql:host=$server;dbname=$db";
    $pdo = new PDO($dsn, $user, $pass);
    $pdo->beginTransaction();
    try {
        $statement = $pdo->prepare("SELECT LD.UserID,LD.UserDetailsID, LD.EmailAddress, LD.UserPassword, LD.UserTypeID, UT.UserTypeName,UD.UserFullName
                                    FROM logindetails as LD
                                    LEFT JOIN  USERDetails AS UD ON UD.`UserDetailsID`=LD.`UserDetailsID`
                                    LEFT JOIN  USERTYPE AS UT ON UT.UserTypeID=LD.UserTypeID
                                    WHERE LD.`UserDetailsID`='" . trim($_SESSION['ses_UserDetailsID']) . "'");


        $statement->execute();
        $count = $statement->rowCount();
        if ($count != 0) {
            $UserDetails = $statement->fetch();
        }
    } catch (PDOException $e) {

    }

    unset($pdo);
}
*/?>

<!doctype html>
<html>
    <head>
        <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=UTF-8">
        <META name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
        <META name="apple-mobile-web-app-capable" content="yes" />
        <title><?php echo $page_info['page_title'] ?></title>
        <meta name="description" content="<?php echo $page_info['page_description'] ?>">
        <meta name="keywords" content="<?php echo $page_info['pageKeywords'] ?>">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css' />
        <!--        <link href="css/style.css" type="text/css" rel="stylesheet" />-->
        <link href="<?=base_url()?>assets/css/style.css?<?php echo time(); ?>" type="text/css" rel="stylesheet" />
        <link href="<?=base_url()?>assets/css/popup.css" type="text/css" rel="stylesheet" />

        <link rel="stylesheet" href="<?=site_url('assets/css/jquery-ui.css')?>">


        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="ico/apple-touch-icon-57-precomposed.png">
        <link rel="shortcut icon" href="ico/favicon.png">
        <link href="<?=base_url('assets/css/pgwmodal.css')?>" rel="stylesheet" type="text/css">
        <link href="<?=base_url()?>assets/css/admin/bootstrap-social-gh-pages/assets/css/font-awesome.css" rel="stylesheet">
        <link href="<?=base_url()?>assets/css/admin/bootstrap-social-gh-pages/assets/css/docs.css" rel="stylesheet" >
        <link rel="stylesheet" href="<?=base_url()?>assets/css/admin/bootstrap-social-gh-pages/bootstrap-social.css">

        <script src="<?=base_url('assets/js/jquery-1.10.2.js')?>"></script>
        <script src="<?=base_url('assets/js/jquery-ui.js')?>"></script>
<!--        <script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>-->
        <!--<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDY0kkJiTPVd2U7aTOAwhc9ySH6oHxOIYM&sensor=false">-->
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true"></script>
        </script>

        <link rel="stylesheet" href="<?=base_url()?>assets/js/jquery.bxslider.css" type="text/css" />
        <link href="<?=base_url()?>assets/css/polyglot-language-switcher-2.css" rel="stylesheet">
        <script type="text/javascript" src="<?=base_url()?>assets/js/jquery.bxslider.js"></script>
        <script type="text/javascript" src="<?=base_url()?>assets/js/scripts.js"></script>
        <script src="<?=base_url('assets/js/pgwmodal.min.js')?>"></script>
        <script src="<?=base_url('assets/js/jquery-polyglot.language.switcher.js')?>"></script>
        <script type="text/javascript">
            var loggedIn = <?php if ($this->session->userdata('user_id')) {
    echo $this->session->userdata('user_id');
} else{ ?> ""; <?php }?>;

        </script>
        <!-- Script by SumoMe -->
        <script src="//load.sumome.com/" data-sumo-site-id="5542afdce9f02e693c947c45bff4e27b43825a8cc2ab632e7a0cc0f2eeb67b34" async>
        </script>
        <!-- End of the script -->
        <script type="text/javascript">

                                    function DropDown(el) {

                                        this.dd = el;

                                        this.initEvents();

                                    }

                                    DropDown.prototype = {
                                        initEvents: function() {

                                            var obj = this;



                                            obj.dd.on('click', function(event) {

                                                $(this).toggleClass('active');

                                                event.stopPropagation();

                                            });

                                        }

                                    }

                                    $(function() {



                                        var dd = new DropDown($('#dd'));



                                        $(document).click(function() {

                                            // all dropdowns

                                            $('.wrapper-dropdown-2').removeClass('active');

                                        });



                                    });

                                </script>

        <script src="<?=base_url()?>assets/js/main.js"></script>

        <script type="text/javascript">




            window.onload = function() {
                document.getElementById("aboutus").onclick = function() {
                    var overlay = document.getElementById("overlay");
                    var popup = document.getElementById("popupAboutus");
                    overlay.style.display = "block";
                    popup.style.display = "block";
                };

                document.getElementById("CloseBtn").onclick = function() {

                    var overlay = document.getElementById("overlay");
                    var popup = document.getElementById("popupAboutus");
                    overlay.style.display = "none";
                    popup.style.display = "none";
                }


                document.getElementById("contactUS").onclick = function() {
                    var overlay = document.getElementById("overlay");
                    var popup = document.getElementById("popupContactus");
                    $('#Asunto').val('Contacta con nosotros');
                    overlay.style.display = "block";
                    popup.style.display = "block";
                };

                document.getElementById("teacher").onclick = function() {
                    var overlay = document.getElementById("overlay");
                    var popup = document.getElementById("popupContactus");
                    $('#Asunto').val('Eres un profesor');
                    overlay.style.display = "block";
                    popup.style.display = "block";
                };

                document.getElementById("bar").onclick = function() {
                    var overlay = document.getElementById("overlay");
                    var popup = document.getElementById("popupContactus");
                    $('#Asunto').val('Eres un bar/pub');
                    overlay.style.display = "block";
                    popup.style.display = "block";
                };

                document.getElementById("Company").onclick = function() {
                    var overlay = document.getElementById("overlay");
                    var popup = document.getElementById("popupContactus");
                    $('#Asunto').val('Eres una empresa');
                    overlay.style.display = "block";
                    popup.style.display = "block";
                };

                document.getElementById("btnclosemodalContactForm").onclick = function() {
                    var overlay = document.getElementById("overlay");
                    var popup = document.getElementById("popupContactus");
                    overlay.style.display = "none";
                    popup.style.display = "none";
                }

                document.getElementById("InfoNivel").onclick = function() {
                    var overlay = document.getElementById("overlay");
                    var popup = document.getElementById("popupInfoNivel");
                    overlay.style.display = "block";
                    popup.style.display = "block";
                };

                document.getElementById("informationConditionsTerms").onclick = function() {
                    var overlay = document.getElementById("overlay");
                    var popup = document.getElementById("popupInformationConditionsTerms");
                    overlay.style.display = "block";
                    popup.style.display = "block";
                };

                document.getElementById("btnClosepopupInfoNivel").onclick = function() {
                    var overlay = document.getElementById("overlay");
                    var popup = document.getElementById("popupInfoNivel");
                    overlay.style.display = "none";
                    popup.style.display = "none";
                }

                document.getElementById("btnInformationConditionsTerms").onclick = function() {
                    var overlay = document.getElementById("overlay");
                    var popup = document.getElementById("popupInformationConditionsTerms");
                    overlay.style.display = "none";
                    popup.style.display = "none";
                }

                document.getElementById("OnetoOne").onclick = function() {
                    var overlay = document.getElementById("overlay");
                    var popup = document.getElementById("popupOnetoOne");
                    overlay.style.display = "block";
                    popup.style.display = "block";
                };


                document.getElementById("btnClosepopupOnetoOne").onclick = function() {
                    var overlay = document.getElementById("overlay");
                    var popup = document.getElementById("popupOnetoOne");
                    overlay.style.display = "none";
                    popup.style.display = "none";
                }

/*                document.getElementById("tuOption").onclick = function() {
                    var overlay = document.getElementById("overlay");
                    var popup = document.getElementById("popupContactus");

                    overlay.style.display = "block";
                    popup.style.display = "block";

                    var Asunto = document.getElementById("Asunto");
                    Asunto.value = "Tu Opini?n nos Importa";
                    document.getElementById("Asunto").disabled = true;
                }*/

                document.getElementById("teacher").onclick = function() {
                    var overlay = document.getElementById("overlay");
                    var popup = document.getElementById("popupContactus");

                    overlay.style.display = "block";
                    popup.style.display = "block";

                    var Asunto = document.getElementById("Asunto");
                    Asunto.value = "Contacto de Profesor";
                    document.getElementById("Asunto").disabled = true;
                }


                document.getElementById("bar").onclick = function() {
                    var overlay = document.getElementById("overlay");
                    var popup = document.getElementById("popupContactus");

                    overlay.style.display = "block";
                    popup.style.display = "block";

                    var Asunto = document.getElementById("Asunto");
                    Asunto.value = "Contacto de Bar";
                    document.getElementById("Asunto").disabled = true;
                }

                document.getElementById("Company").onclick = function() {
                    var overlay = document.getElementById("overlay");
                    var popup = document.getElementById("popupContactus");

                    overlay.style.display = "block";
                    popup.style.display = "block";

                    var Asunto = document.getElementById("Asunto");
                    Asunto.value = "Quiero Mingles en mi ciudad";
                    document.getElementById("Asunto").disabled = true;
                }
            };

            $(function(){
                $("#modalContactForm").submit(function() {
                    $.ajax({
                        type: "post",
                        dataType: "",
                        url: "/main/sendemail",
                        data: $("#modalContactForm").serialize(),
                        success: function(response) {
                            $("#modalContactForm").html(response);
                        }
                    });
                    return false;
                });
            });



        </script>
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-43663746-1', 'auto');
            ga('send', 'pageview');

            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-43663746-1']);
            _gaq.push(['_trackPageview']);
            (function() {
                var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();

        </script>

        <script src="https://apis.google.com/js/platform.js" async defer>
            {lang: 'es'}
        </script>
    </head>
    <body class="home" data-twttr-rendered="true">
            <div class="container">
            <!--<start header>-->
            <header>
                <div class="wrapper">

                    <div class="logo"><a href="<?=base_url()?>">
                            <img src="<?=base_url()?>assets/images/logo.png"></a></div>
                    <div class="header_rt">
                        <div class="login">
                            <?php
                            if ($this->session->userdata('user_id') === FALSE) {
                                ?>
                                <a class="loginbtn" data-url="" href="#">Login / Register</a>
                                <?php
                            } else { ?>
                                    <a href="<?=base_url('admin/dashboard')?>"><?=$this->session->userdata('user_fullname')?></a> | <a href="<?=base_url('admin/dashboard/logout')?>">Logout</a>

                                <?php
                            }
                            ?>

                        </div>
                       <!-- <div class="top-menu"><a href="#">Espa&ntilde;ol - (ES)</a></div>-->
                        <div class="polyglot-language-switcher">
                            <ul style="display: none">
                                <!--<li><a href="<?/*=base_url('language/switchlanguage/en/'.uri_string())*/?>" title="English (US)"  data-lang-id="en_US">English - (US)</a></li>-->
                                <li><a href="<?=base_url('language/switchlanguage/es/'.uri_string())?>" title="Espa&ntilde;ol - (ES)" data-lang-id="es_ES">Espa&ntilde;ol - (ES)</a></li>
                            </ul>
                        </div>
                        <div class="select-city">
                            <select name="ddlCityHeader" id="ddlCityHeader">
                            </select>
                        </div>

                        <div class="height20"></div>
                        <div class="social_icon"><a href="http://www.facebook.com/MinglesSpain" target="_blank">
                                <img src="<?=base_url()?>assets/images/facebook.png"></a><a href="http://twitter.com/MinglesSpain" target="_blank"><img src="<?=base_url()?>assets/images/twitter.png"></a><a href="http://www.youtube.com/channel/UCJN5c55Ik3iozb1HYZMqVIw" target="_blank"><img src="<?=base_url()?>assets/images/youtube.png"></a><a href="http://www.linkedin.com/company/mingles?trk=company_name" target="_blank"><img src="<?=base_url()?>assets/images/linkedin.png"></a><a href="http://gplus.to/Mingles" target="_blank"><img src="<?=base_url()?>assets/images/google_plus.png"></a></div>
                        <div class="height0"></div>
                        <nav>
                            <div class="responsiveMenuSelect">



                                <div class="wrapper-demo">

                                    <!-----start-wrapper-dropdown-2---->

                                    <div id="dd" class="wrapper-dropdown-2" tabindex="1">menu<span><img src="<?=base_url()?>assets/images/icon-menu.png"/></span>

                                        <ul class="dropdown">
                                            <li><a href="#">IDIOMAS</a>
                                                <ul class="sub-menu">
                                                <li><a href="<?=base_url('whatsmingles')?>">¿Qué es?</a></li>
                                                <!--<li><a href="<?/*=base_url('metodo')*/?>">Nuestro Método</a> </li>-->
                                                <li><a href="http://www.mingles.es/page/spanish">#Spanish</a> </li>
                                                <li><a href="http://www.mingles.es/page/frenchmingles">#French</a> </li>
                                                <li><a href="http://www.mingles.es/page/portuguese">#Portuguese</a> </li>
                                                </ul>
                                            </li>
                                            <?php if($is_logged) : ?>
                                                <li><a href="<?=base_url('premiumcards')?>">PREMIUM CARD</a></li>
                                            <?php else : ?>
                                                <li><a class="loginbtn" data-url="premiumcards" href="#">PREMIUM CARD</a></li>
                                            <?php endif;?>
                                            <li><a href="#">Los Espacios</a>
                                                <ul class="sub-menu">
                                                    <li><a href="<?=base_url('bares')?>">Locales Mingles</a></li>
                                                    <li><a href="http://www.mingles.es/page/empresa">Empresas y Coworkings</a></li>
                                                    <li><a href="http://www.mingles.es/page/womensmingles">En la Radio</a></li>
                                                    <li><a href="http://www.mingles.es/page/kids">Kids Mingles</a></li>
                                                    <li><a href="<?=base_url('eventos')?>">Eventos</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="<?=base_url('bono')?>">BONO MINGLES</a></li>

                                        </ul>
                                    </div>

                                </div>

                                <!-----end-wrapper-dropdown-2---->

                                <!-----start-script---->



                            </div>


                            <ul>
                                <li><a href="#"><?=$this->lang->line('rmenu_languages')?></a>
                                                <ul class="sub-menu">
                                                <li><a href="<?=base_url('whatsmingles')?>"><?=$this->lang->line('rmenu_what_is')?></a></li>
                                                <!--<li><a href="<?/*=base_url('metodo')*/?>">Nuestro Método</a> </li>-->
                                                <li><a href="http://www.mingles.es/page/spanish">#Spanish</a> </li>
                                                <li><a href="http://www.mingles.es/page/frenchmingles">#French</a> </li>
                                                <li><a href="http://www.mingles.es/page/portuguese">#Portuguese</a> </li>
                                                </ul>
                                            </li>

                                    <?php if($is_logged) : ?>
                                        <li><a href="<?=base_url('premiumcards')?>"><?=$this->lang->line('rmenu_premium_card')?></a></li>
                                    <?php else : ?>
                                        <li><a class="loginbtn" data-url="premiumcards" href="#"><?=$this->lang->line('rmenu_premium_card')?></a></li>
                                    <?php endif;?>
                                    <li><a href="#"><?=$this->lang->line('rmenu_spaces')?></a>
                                        <ul class="sub-menu">
                                            <li><a href="<?=base_url('bares')?>"><?=$this->lang->line('rmenu_our_spaces')?></a></li>
                                            <li><a href="http://www.mingles.es/page/empresa"><?=$this->lang->line('rmenu_companies')?></a></li>
                                            <li><a href="http://www.mingles.es/page/womensmingles"><?=$this->lang->line('rmenu_radioshow')?></a></li>
                                            <li><a href="http://www.mingles.es/page/kids"><?=$this->lang->line('rmenu_kids')?></a></li>
                                            <li><a href="<?=base_url('eventos')?>"><?=$this->lang->line('rmenu_events')?></a></li>
                                        </ul>

                                    </li>
                                    <li><a href="<?=base_url('bono')?>"><?=$this->lang->line('rmenu_credits')?></a></li>
                            </ul>


                        </nav>

                    </div>

                    <div class="height15"></div>
                </div>

                <div class="banner">

                    <ul class="bxslider">
                        <li>
                            <!--<img src="images/banner/banner.jpg" alt="" id="Img1" class="bgwidth"></li>-->
                            <img src="assets/images/banner/MINGLES_INGLES_1.png" alt="" id="Img1" class="bgwidth"></li>
                        <li>
                            <!--<img src="images/banner/banner1.jpg" alt="" id="Img2" class="bgwidth"></li>-->
                            <img src="assets/images/banner/MINGLES_INGLES_2.png" alt="" id="Img2" class="bgwidth"></li>
                        <li>
                            <img src="assets/images/banner/banner2.jpg" alt="" id="Img3" class="bgwidth"></li>
                    </ul>

                </div>
                <script>
                    jQuery(document).ready(function ($) {
                        $('.polyglot-language-switcher').polyglotLanguageSwitcher({
                            openMode: 'click',
                            selectedLang: '<?=$this->session->userdata('site_lang')?>'
                        });
                    });
                </script>

            </header>
            <!--<end header>-->

