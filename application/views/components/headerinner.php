<!doctype html>
<html>
    <head>
        <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=UTF-8">
        <META name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
        <META name="apple-mobile-web-app-capable" content="yes" />
        <title><?php echo $page_info['page_title'] ?></title>
        <meta name="description" content="<?php echo $page_info['page_description'] ?>">
        <meta name="keywords" content="<?php echo $page_info['pageKeywords'] ?>">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css' />
        <!--        <link href="css/style.css" type="text/css" rel="stylesheet" />-->
        <link href="<?=base_url()?>assets/css/style.css?<?php echo time(); ?>" type="text/css" rel="stylesheet" />
        <link href="<?=base_url()?>assets/css/popup.css" type="text/css" rel="stylesheet" />

        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">



        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="ico/apple-touch-icon-57-precomposed.png">
        <link rel="shortcut icon" href="ico/favicon.png">
        <link href="<?=base_url('assets/css/pgwmodal.css')?>" rel="stylesheet" type="text/css">
        <link href="<?=base_url()?>assets/css/admin/bootstrap-social-gh-pages/assets/css/font-awesome.css" rel="stylesheet">
        <link href="<?=base_url()?>assets/css/admin/bootstrap-social-gh-pages/assets/css/docs.css" rel="stylesheet" >
        <link rel="stylesheet" href="<?=base_url()?>assets/css/admin/bootstrap-social-gh-pages/bootstrap-social.css">

        <script src="<?=base_url('assets/js/jquery-1.10.2.js')?>"></script>
        <script src="<?=base_url('assets/js/jquery-ui.js')?>"></script>
<!--        <script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>-->
        <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDY0kkJiTPVd2U7aTOAwhc9ySH6oHxOIYM&sensor=false">
        </script>
        <link rel="stylesheet" href="/resources/demos/style.css">

        <link rel="stylesheet" href="<?=base_url()?>assets/js/jquery.bxslider.css" type="text/css" />
        <link href="<?=base_url()?>assets/css/polyglot-language-switcher-2.css" rel="stylesheet">
        <script type="text/javascript" src="<?=base_url()?>assets/js/jquery.bxslider.js"></script>
        <script type="text/javascript" src="<?=base_url()?>assets/js/scripts.js"></script>
        <script src="<?=base_url('assets/js/pgwmodal.min.js')?>"></script>
        <script src="<?=base_url('assets/js/jquery-polyglot.language.switcher.js')?>"></script>
        <script type="text/javascript">
            var loggedIn = <?php if ($this->session->userdata('user_id')) {
    echo $this->session->userdata('user_id');
} else{ ?> ""; <?php }?>;

        </script>
        <script type="text/javascript">
            var loggedIn = <?php if (isset($_SESSION['ses_UserID'])) {
    echo $_SESSION['ses_UserID'];
} ?>;

        </script>
        
        <script type="text/javascript">

                                    function DropDown(el) {

                                        this.dd = el;

                                        this.initEvents();

                                    }

                                    DropDown.prototype = {
                                        initEvents: function() {

                                            var obj = this;



                                            obj.dd.on('click', function(event) {

                                                $(this).toggleClass('active');

                                                event.stopPropagation();

                                            });

                                        }

                                    }

                                    $(function() {



                                        var dd = new DropDown($('#dd'));



                                        $(document).click(function() {

                                            // all dropdowns

                                            $('.wrapper-dropdown-2').removeClass('active');

                                        });



                                    });

                                </script>

        <script src="<?=base_url()?>assets/js/main.js"></script>

        <script type="text/javascript">
            window.onload = function() {
                document.getElementById("aboutus").onclick = function() {
                    var overlay = document.getElementById("overlay");
                    var popup = document.getElementById("popupAboutus");
                    overlay.style.display = "block";
                    popup.style.display = "block";
                };

                document.getElementById("CloseBtn").onclick = function() {
                    var overlay = document.getElementById("overlay");
                    var popup = document.getElementById("popupAboutus");
                    overlay.style.display = "none";
                    popup.style.display = "none";
                }

                document.getElementById("contactUS").onclick = function() {
                    var overlay = document.getElementById("overlay");
                    var popup = document.getElementById("popupContactus");
                    overlay.style.display = "block";
                    popup.style.display = "block";
                };


                document.getElementById("btnclosemodalContactForm").onclick = function() {
                    var overlay = document.getElementById("overlay");
                    var popup = document.getElementById("popupContactus");
                    overlay.style.display = "none";
                    popup.style.display = "none";
                }

                document.getElementById("InfoNivel").onclick = function() {
                    var overlay = document.getElementById("overlay");
                    var popup = document.getElementById("popupInfoNivel");
                    overlay.style.display = "block";
                    popup.style.display = "block";
                };


                document.getElementById("btnClosepopupInfoNivel").onclick = function() {
                    var overlay = document.getElementById("overlay");
                    var popup = document.getElementById("popupInfoNivel");
                    overlay.style.display = "none";
                    popup.style.display = "none";
                }

                document.getElementById("OnetoOne").onclick = function() {
                    var overlay = document.getElementById("overlay");
                    var popup = document.getElementById("popupOnetoOne");
                    overlay.style.display = "block";
                    popup.style.display = "block";
                };


                document.getElementById("btnClosepopupOnetoOne").onclick = function() {
                    var overlay = document.getElementById("overlay");
                    var popup = document.getElementById("popupOnetoOne");
                    overlay.style.display = "none";
                    popup.style.display = "none";
                }

                document.getElementById("tuOption").onclick = function() {
                    var overlay = document.getElementById("overlay");
                    var popup = document.getElementById("popupContactus");

                    overlay.style.display = "block";
                    popup.style.display = "block";

                    var Asunto = document.getElementById("Asunto");
                    Asunto.value = "Tu Opinión nos Importa";
                    document.getElementById("Asunto").disabled = true;
                }

                document.getElementById("teacher").onclick = function() {
                    var overlay = document.getElementById("overlay");
                    var popup = document.getElementById("popupContactus");

                    overlay.style.display = "block";
                    popup.style.display = "block";

                    var Asunto = document.getElementById("Asunto");
                    Asunto.value = "Contacto de Profesor";
                    document.getElementById("Asunto").disabled = true;
                }


                document.getElementById("bar").onclick = function() {
                    var overlay = document.getElementById("overlay");
                    var popup = document.getElementById("popupContactus");

                    overlay.style.display = "block";
                    popup.style.display = "block";

                    var Asunto = document.getElementById("Asunto");
                    Asunto.value = "Contacto de Bar";
                    document.getElementById("Asunto").disabled = true;
                }

                document.getElementById("Company").onclick = function() {
                    var overlay = document.getElementById("overlay");
                    var popup = document.getElementById("popupContactus");

                    overlay.style.display = "block";
                    popup.style.display = "block";

                    var Asunto = document.getElementById("Asunto");
                    Asunto.value = "Quiero Mingles en mi ciudad";
                    document.getElementById("Asunto").disabled = true;
                }
            };


        </script>
    </head>
    <body class="home" data-twttr-rendered="true">
        <div class="container">
            <!--<start header>-->
            <header>
                <div class="wrapper">

                    <div class="logo"><a href="<?=base_url()?>">
                            <img src="<?=base_url()?>assets/images/logo.png"></a></div>
                    <div class="header_rt">
                        <div class="login">
                            <?php
                            if ($this->session->userdata('user_id') === FALSE) {
                                ?>
                                <a class="loginbtn" href="#">Login / Register</a>
                                <?php
                            } else {
                                ?>
                                <a href="<?=base_url('admin/dashboard')?>"><?=$this->session->userdata('user_fullname')?></a> | <a href="<?=base_url('admin/dashboard/logout')?>">Logout</a>
                                <?php
                            }
                            ?>

                        </div>
                        <!--<div class="top-menu"><a href="#">Espa&ntilde;ol - (ES)</a></div>-->
                        <div class="polyglot-language-switcher">
                            <ul style="display: none">
                                <!--<li><a href="<?/*=base_url('language/switchlanguage/en/'.uri_string())*/?>" title="English (US)"  data-lang-id="en_US">English - (US)</a></li>-->
                                <li><a href="<?=base_url('language/switchlanguage/es/'.uri_string())?>" title="Espa&ntilde;ol - (ES)" data-lang-id="es_ES">Espa&ntilde;ol - (ES)</a></li>
                            </ul>
                        </div>
                        <div class="select-city">
                            <select name="ddlCityHeader" id="ddlCityHeader">
                            </select>	
                        </div>

                        <div class="height20"></div>
                        <div class="social_icon"><a href="http://www.facebook.com/MinglesSpain" target="_blank">
                                <img src="<?=base_url()?>assets/images/facebook.png"></a><a href="http://twitter.com/MinglesSpain" target="_blank"><img src="<?=base_url()?>assets/images/twitter.png"></a><a href="http://www.youtube.com/channel/UCJN5c55Ik3iozb1HYZMqVIw" target="_blank"><img src="<?=base_url()?>assets/images/youtube.png"></a><a href="http://www.linkedin.com/company/mingles?trk=company_name" target="_blank"><img src="<?=base_url()?>assets/images/linkedin.png"></a><a href="http://gplus.to/Mingles" target="_blank"><img src="<?=base_url()?>assets/images/google_plus.png"></a></div>
                        <div class="height0"></div>
                        <nav>
                            <div class="responsiveMenuSelect">



                                <div class="wrapper-demo">

                                    <!-----start-wrapper-dropdown-2---->

                                    <div id="dd" class="wrapper-dropdown-2" tabindex="1">menu<span><img src="<?=base_url()?>assets/images/icon-menu.png"/></span>

                                        <ul class="dropdown">
                                            <li><a href="#">IDIOMAS</a>
                                                <ul class="sub-menu">
                                                <li><a href="<?=base_url('whatsmingles')?>">¿Qué es?</a></li>
                                                <!--<li><a href="<?/*=base_url('metodo')*/?>">Nuestro Método</a> </li>-->
                                                <li><a href="http://www.mingles.es/page/spanish">#Spanish</a> </li>
                                                <li><a href="http://www.mingles.es/page/frenchmingles">#French</a> </li>
                                                <li><a href="http://www.mingles.es/page/portuguese">#Portuguese</a> </li>
                                                </ul>
                                            </li>
                                            <?php if($this->session->userdata('user_id')) : ?>
                                                <li><a href="<?=base_url('premiumcards')?>">PREMIUM CARD</a></li>
                                            <?php else : ?>
                                                <li><a class="loginbtn" href="#">PREMIUM CARD</a></li>
                                            <?php endif;?>
                                            <li><a href="#">Los Espacios</a>
                                                <ul class="sub-menu">
                                                    <li><a href="<?=base_url('bares')?>">Locales Mingles</a></li>
                                                    <li><a href="http://www.mingles.es/page/empresa">Empresas y Coworkings</a></li>
                                                    <li><a href="http://www.mingles.es/page/womensmingles">En la Radio</a></li>
                                                    <li><a href="http://www.mingles.es/page/kids">Kids Mingles</a></li>
                                                    <li><a href="<?=base_url('eventos')?>">Eventos</a></li>
                                                </ul>

                                            </li>
                                            <li><a href="<?=base_url('bono')?>">BONO MINGLES</a></li>

                                        </ul>
                                    </div>

                                </div>

                                <!-----end-wrapper-dropdown-2---->

                                <!-----start-script---->

 

                            </div>


                            <ul>
                                <li><a href="#">IDIOMAS</a>
                                                <ul class="sub-menu">
                                                <li><a href="<?=base_url('whatsmingles')?>">¿Qué es?</a></li>
                                                <!--<li><a href="<?/*=base_url('metodo')*/?>">Nuestro Método</a> </li>-->
                                                <li><a href="http://www.mingles.es/page/spanish">#Spanish</a> </li>
                                                <li><a href="http://www.mingles.es/page/frenchmingles">#French</a> </li>
                                                <li><a href="http://www.mingles.es/page/portuguese">#Portuguese</a> </li>
                                                </ul>
                                            </li>
                                <?php if($this->session->userdata('user_id')) : ?>
                                    <li><a href="<?=base_url('premiumcards')?>">PREMIUM CARD</a></li>
                                <?php else : ?>
                                    <li><a class="loginbtn" href="#">PREMIUM CARD</a></li>
                                <?php endif;?>
                                    <li><a href="#">Los Espacios</a>
                                        <ul class="sub-menu">
                                            <li><a href="<?=base_url('bares')?>">Los Locales</a></li>
                                            <li><a href="http://www.mingles.es/page/empresa">Empresas y Coworkings</a></li>
                                            <li><a href="http://www.mingles.es/page/womensmingles">En la Radio</a></li>
                                            <li><a href="http://www.mingles.es/page/kids">Kids Mingles</a></li>
                                            <li><a href="<?=base_url('eventos')?>">Eventos</a></li>
                                        </ul>

                                    </li>
                                    <li><a href="<?=base_url('bono')?>">BONO MINGLES</a></li>
                            </ul>


                        </nav>

                    </div>

                    <div class="height15"></div>
                </div>
                <script>
                    jQuery(document).ready(function ($) {
                        $('.polyglot-language-switcher').polyglotLanguageSwitcher({
                            openMode: 'click',
                            selectedLang: '<?=$this->session->userdata('site_lang')?>'
                        });
                    });
                </script>

            </header>
            <!--<end header>-->
