<style>
    #pano img {
        border: none !important;
        max-width: none !important;
    }
    .streetlink {
        text-decoration: underline;
        color: blue;
        font-weight: bold;
    }
</style>
<script>
    function validateForm() {
        var error = false;
        if ($('#chkTerm').is(':checked')) {
            //var x = document.forms["myForm"]["name"].value;
            console.log(document.forms["myForm"]["promoCode"].value);
            if (document.forms["myForm"]["promoCode"].value != null && document.forms["myForm"]["promoCode"].value != '') {
                $.ajax({
                        url: 'paypal/check_promocode/' + document.forms["myForm"]["promoCode"].value,
                        global: false,
                        type: "POST",
                        async: false,
                        success: function(data){
                            if (data != 'true') {
                                alert(data);
                                document.forms["myForm"]["promoCode"].focus();
                                return false;
                            }
                        }
                    }
                );
/*                $.post('paypal/check_promocode/' + document.forms["myForm"]["promoCode"].value)
                    .done(function (data) {
                        //  console.log(data);
                        if (data != 'true') {
                            alert(data);
                            document.forms["myForm"]["email"].focus();
                            return false;
                        }
                    });*/
            }
            if (document.forms["myForm"]["ddlClass"].value != null && document.forms["myForm"]["ddlClass"].value != '') {
                $.ajax({
                        url: 'paypal/check_available_class/' + document.forms["myForm"]["ddlClass"].value,
                        global: false,
                        type: "POST",
                        async: false,
                        data: $('form[name="myForm"]').serialize(),
                        success: function (data) {
                            if (data != 'true') {
                                alert(data);
                                error = true;
/*                                if (data == 'Class not contain any groups') {
                                    alert(data);
                                    error = true;
                                }*/
                                document.forms["myForm"]["ddlClass"].focus();
                                return false;
                            }
                        }
                    }
                );
            }

            else
            {
                alert('Please choose available class');
                document.forms["myForm"]["ddlClass"].focus();
                return false;
            }

            if (document.forms["myForm"]["name"].value == null || document.forms["myForm"]["name"].value == "") {
                alert("First name must be filled out");
                document.forms["myForm"]["name"].focus();
                return false;
            }
            else if (document.forms["myForm"]["email"].value == null || document.forms["myForm"]["email"].value == "") {
                alert("Email must be filled out");
                document.forms["myForm"]["email"].focus();
                return false;
            }
        }
        else
        {
            alert('Tienes que marcar la casilla para aceptar los t?rminos y condiciones de Mingles.es');
            return false;
        }
        if(error)
        {
            return false;
        }
    }
</script>


<?php if(($is_logged&&$this->session->userdata('usertype_id') == 1) || !$is_logged):?>
    <script>
        $(function(){
            $("#joinus_user").bind( "click", function() {
                if($('#selectClass').val() == null || $('#selectClass').val() == "" || $('#selectClass').val() == "undefined")
                {
                    alert('Please choose Class');
                }
                else {
                    getSelectClass();
                    $('form[name="myForm"]').submit();
                }
            });
            $("#selectCity").change(function() {
                $("#ddlCityHeader").val($(this).val());
                getPubUsers()

            });
            $("#selectPub").change(function() {
                getClassByPub()

            });
            $("#selectClass").change(function() {
                getSelectClass();

            });
            getClassByPub();
        })

        function getSelectClass()
        {
            $("#ddlPub option[value='"+$('#selectPub').val()+"']").prop("selected", true);

            $.ajax({
                type: "POST",
                url: "/json/getclass",
                cache: false,
                data: {
                    "pubid": $('#selectPub').val()
                },
                dataType: 'json',
                async: false,
                success: function(data) //Si se ejecuta correctamente
                {
                    var ddlclass = $("#ddlClass");
                    ddlclass.empty();
                    $.each(data, function(index, array) {
                        ddlclass.append($("<option />").val(array.ClassID).text(array.ClassName));
                    });
                    $("#ddlClass option[value='"+$('#selectClass').val()+"']").prop("selected", true);
                },
                error: function(data)
                {
                    // alert('error');
                }
            });

            $("#promoCode").val($('#SelectpromoCode').val());
            $("#chkTerm").prop("checked",true);


        }

        function getPubUsers() {
            var cityid = $('#selectCity').val();


            $.ajax({
                type: "POST",
                url: "/json/getpub",
                cache: false,
                data: {
                    "cityid": cityid
                },
                dataType: 'json',
                success: function(data) //Si se ejecuta correctamente
                {
                    var pub = $("#selectPub");
                    pub.empty();
                    $.each(data, function(index, array) {
                        pub.append($("<option />").val(array.PubID).text(array.PubName));
                    });
                    getClassByPub();
                },
                error: function(data)
                {
                    //alert('error');
                }
            });
        }


        function getClassByPub() {
            var PubID = $("#selectPub").val();

            $.ajax({
                type: "POST",
                url: "/json/getclass",
                cache: false,
                data: {
                    "pubid": PubID
                },
                dataType: 'json',
                success: function(data) //Si se ejecuta correctamente
                {
                    var ddlclass = $("#selectClass");
                    ddlclass.empty();
                    $.each(data, function(index, array) {
                        ddlclass.append($("<option />").val(array.ClassID).text(array.ClassName));
                    });
/*                    getLabelbyClassID();
                    getLanguagebyClassID();*/
                },
                error: function(data)
                {
                    // alert('error');
                }
            });
        }

    </script>
<?php endif;?>
<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
$.src='//v2.zopim.com/?2JOllOvfoDPNG0R64KIiRYTlZDATMhp0';z.t=+new Date;$.
type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
</script>
<!--End of Zopim Live Chat Script-->

<!--<div class="height30">
</div>-->
<?php if($this->session->userdata('usertype_id') == 1||(!$is_logged)) : ?>
    <div class="top-section">
<div class="blue-box" style="margin-top: 0px; margin-bottom: 0px; padding-bottom: 0px;">
    <div class="wrapper">
<div class="" style="float: left;width: 180px;margin: 10px">
<select name="selectCity" id="selectCity" class="selectbg" >

    <?php foreach($cities as $city):?>
        <option value="<?=$city['CityID']?>"><?=$city['CityName']?></option>
    <?php endforeach;?>
    <option value="all">All</option>
</select>
    </div>
    <div class="" style="float: left;width: 180px;margin: 10px">
    <select name="selectPub" id="selectPub" class="selectbg">

        <?php foreach($pubs as $pub):?>
            <option value="<?=$pub['PubID']?>"><?=$pub['PubName']?></option>
        <?php endforeach;?>
    </select>

</div>
    <div class="" style="float: left;width: 180px;margin: 10px;">
        <select name="selectClass" id="selectClass" class="selectbg">
            <?php foreach($classes as $class):?>
                <option value="<?=$class['ClassID']?>"><?=$class['ClassName']?></option>
            <?php endforeach;?>
        </select>
    </div>
    <div class="" style="float: left">
        <input type="text" name="SelectpromoCode" id="SelectpromoCode" class="inputbg" placeholder="<?=$this->lang->line('select_promocode')?>" style="float: left;width: 100px;margin: 10px;height: 22px;">
        <?php if($is_logged&&$this->session->userdata('usertype_id') == 1) : ?>
        <input type="button" value="<?=$this->lang->line('subscribe_btn')?>" class="submit_bt" id="joinus_user" name="info" style="float: left;margin: 10px">
        <?php elseif(!$is_logged) : ?>
            <a class="submit_bt loginbtn" href="#" style="float: left;margin: 10px"><?=$this->lang->line('subscribe_btn')?></a>
        <?php endif;?>
    </div>
</div>
</div>
</div>
<?php endif;?>
<div class="height30">
</div>

<!--<start top-section> -->
<div class="top-section">
    <div class="wrapper">
        <div class="box1" >
            <div class="img">
                <img src="<?=base_url()?>assets/images/icon1.jpg" alt=""></div>
            <h2>
                <?=$this->lang->line('profesional_title')?></h2>
            <?=$this->lang->line('profesional_description')?>
        </div>
        <div class="box1">
            <div class="img">
                <img src="<?=base_url()?>assets/images/icon2.jpg" alt=""></div>
            <h2>
                <?=$this->lang->line('flexible_title')?></h2>
            <?=$this->lang->line('flexible_description')?>
        </div>
        <div class="box2">
            <div class="img">
                <img src="<?=base_url()?>assets/images/icon3.jpg" alt=""></div>
            <h2>
                <?=$this->lang->line('social_title')?></h2>
            <?=$this->lang->line('social_description')?>
        </div>
        <div class="height10">
        </div>
        <div class="content-section">
            <div class="registration_text">
                <?=$this->lang->line('main_promo_text')?>
                <div class="button">
                    <a href="#option">
                        <img src="<?=base_url()?>assets/images/register_bt_en.png" alt=""></a></div>
                <div class="nuestros">
                    <h2><?=$this->lang->line('meeting_places')?>!</h2>
                    <a href="http://www.ochoymedio.com/" target="_blank">
                        <img src="<?=base_url()?>assets/images/logobar1.png" alt="Ocho y Medio">
                    </a>
                    <a href="http://flashflashmadrid.com/" target="_blank">
                        <img src="<?=base_url()?>assets/images/logobar2.png" alt="Flash Flash">
                    </a>
                    <a href="http://www.theirishrover.com/" target="_blank">
                        <img src="<?=base_url()?>assets/images/logobar5.png" alt="The Irish Rover">
                    </a>
                    <a href="http://www.areiachillout.com/" target="_blank">
                        <img src="<?=base_url()?>assets/images/logobar3.png" alt="Areia Chillout">
                    </a>
                    <a href="https://es-la.facebook.com/LekaLekaBar/" target="_blank">
                        <img src="<?=base_url()?>assets/images/logobar4.png" alt="Leka Leka">
                    </a>
                    <!--<a href="http://www.thethirtythree.es/" target="_blank">
                        <img src="<?=base_url()?>assets/images/logobar9.png" alt="">
                    </a>-->
                    <!--<a href="http://cupsandkids.com/" target="_blank">
                        <img src="<?=base_url()?>assets/images/logo_cups.jpg" alt="Logo Cups">
                    </a>-->
                    <a href="http://prasad-ociosaludable.com/" target="_blank">
                        <img src="<?=base_url()?>assets/images/logobar12.png" alt="Prasad">
                    </a>
                     <a href="https://www.facebook.com/pages/Starbucks-Pedro-Teixeira/184290281602057" target="_blank">
                        <img src="<?=base_url()?>assets/images/logobar13.png" alt="Starbucks">
                    </a>
                    <a href="http://www.labicicletacafe.com/" target="_blank">
                        <img src="<?=base_url()?>assets/images/logobar6.png" alt="La Bicicleta">
                    </a>
                </div>
            </div>
            <div class="right-img">
                <!-- <iframe width="100%" height="299" src="//www.youtube.com/embed/KnVHkkT0Mo4" frameborder="0" allowfullscreen></iframe>-->
                <iframe width="100%" height="299" src="https://www.youtube.com/embed/KnVHkkT0Mo4" frameborder="0" allowfullscreen></iframe>

            </div>
            <div class="height20">
            </div>
        </div>
    </div>
</div>


<!--<start top-section> -->
<!--<start registration>-->
<?php // remove sandbox=1 for live transactions  ?>
<!--<form name="myForm" action="paypal/paypal.php?sandbox=1" onsubmit="return validateForm()" method="post">-->

<?php if($this->session->userdata('usertype_id') == 1 || !$is_logged):?>

<form name="myForm" action="<?=base_url('paypal')?>?sandbox=0" onsubmit="return validateForm()" method="post">
    <input type="hidden" name="action" value="process" />
    <input type="hidden" name="cmd" value="_cart" /> <?php // use _cart for cart checkout         ?>
    <input type="hidden" name="currency_code" value="EUR" />
    <input type="hidden" name="invoice" value="<?php echo date("His") . rand(1234, 9632); ?>" />

    <div class="blue-box">
        <div class="wrapper">
            <div class="regis-box1" id="option">
                <div class="number">
                    <img src="<?=base_url()?>assets/images/1.png"></div>
                <h2> <?=$this->lang->line('select_city')?> </h2>
                <select name="ddlCity" id="ddlCity" class="selectbg" ></select>

                <input type="hidden" name="hndCityID" id="hndCityID">
                <h2 style="padding-top:10px;">
                    <?=$this->lang->line('select_date')?></h2>
                <div id="datepicker" class="date"></div>
                <div id="event"> </div>
                <!--  <div class="date">
                <img src="images/calender.png" alt=""></div>-->
                <!--                <div class="map" id="map_canvas" style="width:304px;height:250px;">
                                </div>-->
                <div class="form-text">
                    <a href="#" class="showall" style="display: none;">
                        Show all</a> <br>
                </div>
                <div  id="map_canvas" style="width:304px;height:250px;"></div>
                <div id="pano" style="display:none;width:304px;height:250px;"></div>

            </div>
            <div class="border">
                <img src="<?=base_url()?>assets/images/border.png" alt=""></div>
            <div class="regis-box1" id="option1">
                <div class="number">
                    <img src="<?=base_url()?>assets/images/2.png"></div>
                <h2>
                    <?=$this->lang->line('select_session_data')?>:</h2>
                <div class="form">
                    <h3>
                        Mingles<br>
                        <!--                    <h2> <input type="label" name="selDate" id="selDate" value=""></h2><br>-->
                        <?=$this->lang->line('select_place')?>:</h3>
                    <select name="ddlPub" id="ddlPub" class="selectbg" >
                    </select>
                    <!--                <input type="submit" name="info" class="submit_bt" value="Info">-->
                    <div class="height20">
                    </div>
                    <h3><?=$this->lang->line('select_language')?></h3>
                    <select name="ddlLanguage" id="ddlLanguage" class="selectbg">
                    </select>
                    <h3><?=$this->lang->line('select_session')?>:</h3>
                    <select name="ddlClass" id="ddlClass" class="selectbg">
                    </select>
                    <h4>
                        <?=$this->lang->line('selected_price')?>:</h4>
                    <div class="submit_bt" id="price">


                    </div>
                    <div class="height20">
                    </div>
                    <h3>
                        <?=$this->lang->line('select_level')?>:</h3>
                    <select name="ddlLevel"  id="ddlLevel" class="selectbg">

                    </select>


                    <span id="InfoNivel" class="pointer submit_bt"><?=$this->lang->line('level_info')?></span> <span id="OnetoOne"
                                                                                           class="pointer submit_bt">one to one</span>
                </div>
            </div>
            <div class="border">
                <img src="<?=base_url()?>assets/images/border.png" alt=""></div>
            <div class="regis-box1" id="option2">
                <div class="number">
                    <img src="<?=base_url()?>assets/images/3.png"></div>
                <h2>
                    <?=$this->lang->line('user_data')?>:</h2>
                <div class="form">
                    <input type="text" name="name" id="name" class="inputbg" placeholder="<?=$this->lang->line('select_name')?>" value="<?php
                    if ($this->session->userdata('user_id')) {
                        echo $this->session->userdata('user_fullname');
                    }
                    ?>" >
                    <input type="text" name="email"  id="email" class="inputbg" placeholder="Email" value="<?php
                    if ($this->session->userdata('user_id')) {
                        echo $this->session->userdata('email');
                    }
                    ?>" >
                    <input type="text" name="promoCode" id="promoCode" class="inputbg" placeholder="<?=$this->lang->line('select_promocode')?>">
<!--                    <h3> Selecciona la cantidad a pagar:</h3>
                    <input type="hidden" name="ProductName" id="ProductName" value="" />
                    <select name="ddlSession" id="ddlSession" class="selectbg">
                    </select>-->
                    <div class="radio">
                        <p>
                            <input name="" type="checkbox" value="" id="chkTerm">
                            <a href="#modalInfoBar" style="color:#fff" id="informationConditionsTerms"><span><?=$this->lang->line('term_use')?></span></a>
                            </p>

                    </div>


                    <input type="submit" name="info" id="joinus" class="submit_bt" value="<?=$this->lang->line('subscribe_btn')?>" >
                    <div class="height15">
                    </div>

                    <div class="form-text">
                        <?=$this->lang->line('descr_1')?> <br>
                        <?=$this->lang->line('descr_2')?> <br>
                        <a href="<?=base_url('bono')?>">
                            <?=$this->lang->line('buy_credits')?>?</a> <br>
                    </div>
                </div>
            </div>
            <div class="height0">
            </div>
        </div>
    </div>

</form>
<?php endif;?>
<!--<end registration>-->

<!--<start testimonial>-->
<div class="testimonial">
    <div class="wrapper">
        <h2>
            <?=$this->lang->line('what_say_students')?></h2>
        <div class="testi-box">
            <img src="<?=base_url()?>assets/images/img.jpg" alt="">
            <p>
                <?=$this->lang->line('what_say_carlos')?></p>
        </div>
        <div class="testi-box">
            <img src="<?=base_url()?>assets/images/img1.jpg" alt="">
            <p>
                <?=$this->lang->line('what_say_orestes')?></p>
        </div>
        <div class="testi-box">
            <img src="<?=base_url()?>assets/images/img2.jpg" alt="">
            <p>
                <?=$this->lang->line('what_say_mariam')?></p>
        </div>
        <div class="height0">
        </div>
    </div>
</div>
<!--<end testimonial>-->
<script type="text/javascript">
    function controlcookies() {
        // si variable no existe se crea (al clicar en Aceptar)
        localStorage.controlcookie = (localStorage.controlcookie || 0);

        localStorage.controlcookie++; // incrementamos cuenta de la cookie
        cookie1.style.display='none'; // Esconde la pol?tica de cookies
    }
</script>
<!-- Politica de cookies -->
<div class="cookiesms" id="cookie1">
    Esta web utiliza cookies, puedes ver nuestra  <a href="<?=base_url('cookies.html')?>">la política de cookies, aquí</a>
    Si continuas navegando estás aceptándola
    <button onclick="controlcookies()">Aceptar</button>
    <div  class="cookies2" onmouseover="document.getElementById('cookie1').style.bottom = '0px';">Política de cookies + </div>
</div>
<script type="text/javascript">
    if (localStorage.controlcookie>0){
        document.getElementById('cookie1').style.bottom = '-50px';
    }
</script>


<!--- Codigo para la pol?tica de cookies-->


<style type="text/css">

    /* CSS para la animaci?n y localizaci?n de los DIV de cookies */

    @keyframes desaparecer
    {
        0%		{bottom: 0px;}
        80%		{bottom: 0px;}
        100%		{bottom: -50px;}
    }

    @-webkit-keyframes desaparecer /* Safari and Chrome */
    {
        0%		{bottom: 0px;}
        80%		{bottom: 0px;}
        100%		{bottom: -50px;}
    }

    @keyframes aparecer
    {
        0%		{bottom: -38px;}
        10%		{bottom: 0px;}
        90%		{bottom: 0px;}
        100%		{bottom: -38px;}
    }

    @-webkit-keyframes aparecer /* Safari and Chrome */
    {
        0%		{bottom: -38px;}
        10%		{bottom: 0px;}
        90%		{bottom: 0px;}
        100%		{bottom: -38px;}
    }
    #cookiesms1:target {
        display: none;
    }
    .cookiesms{
        width:100%;
        height:43px;
        margin:0 auto;
        padding-left:1%;
        padding-top:5px;
        font-size: 1.2em;
        clear:both;
        font-weight: strong;
        color: #333;
        bottom:0px;
        position:fixed;
        left: 0px;
        background-color: #FFF;
        opacity:0.7;
        filter:alpha(opacity=70); /* For IE8 and earlier */
        transition: bottom 1s;
        -webkit-transition:bottom 1s; /* Safari */
        -webkit-box-shadow: 3px -3px 1px rgba(50, 50, 50, 0.56);
        -moz-box-shadow:    3px -3px 1px rgba(50, 50, 50, 0.56);
        box-shadow:         3px -3px 1px rgba(50, 50, 50, 0.56);
        z-index:999999999;
    }

    .cookiesms:hover{
        bottom:0px;
    }
    .cookies2{
        background-color: #FFF;
        display:inline;
        opacity:0.95;
        filter:alpha(opacity=95);
        position:absolute;
        left:1%;
        top:-30px;
        font-size:15px;
        height:30px;
        padding-left:25px;
        padding-right:25px;
        -webkit-border-top-right-radius: 15px;
        -webkit-border-top-left-radius: 15px;
        -moz-border-radius-topright: 15px;
        -moz-border-radius-topleft: 15px;
        border-top-right-radius: 15px;
        border-top-left-radius: 15px;
        -webkit-box-shadow: 3px -3px 1px rgba(50, 50, 50, 0.56);
        -moz-box-shadow:    3px -3px 1px rgba(50, 50, 50, 0.56);
        box-shadow:         3px -3px 1px rgba(50, 50, 50, 0.56);
    }

    /* Fin del CSS para cookies */

</style>
