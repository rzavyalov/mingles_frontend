<div class="height30">
</div>
<!--<start top-section> -->
<div class="inner-content">

    <div class="title_bg">
        <div class="wrapper">BONO MINGLES</div>
    </div>

    <div class="wrapper">

        <div class="container marketing">
            <div class="row">
                <div class="span12">
                    <div class="row">
                        <p align="center">
                            <img src="<?=base_url()?>assets/images/bono.jpg" alt="Bono Mingles" />
                        </p>


                        <div class="searchbar">
                            <form align="center" name="myForm" action="<?=base_url('paypal/paypal_bono')?>?sandbox=0" onsubmit="return validateForm()" method="post"
                                  target="_top">
                                <input type="hidden" name="action" value="process" />
                                <input type="hidden" name="cmd" value="_cart" /> <?php // use _cart for cart checkout         ?>
                                <input type="hidden" name="currency_code" value="EUR" />
                                <input type="hidden" name="invoice" value="<?php echo date("His") . rand(1234, 9632); ?>" />
                                <table width="50%" align="center">
                                    <tr>
                                        <td>
                                            <input type="text" name="name" id="name" class="inputbg" placeholder="Full Name" value="<?php
                                            if ($this->session->userdata('user_id')) {
                                                echo $this->session->userdata('user_fullname');
                                            }
                                            ?>" >
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input type="text" name="email"  id="email" class="inputbg" placeholder="Email Address" value="<?php
                                            if ($this->session->userdata('user_id')) {
                                                echo $this->session->userdata('email');
                                            }
                                            ?>" >
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input type="hidden" name="on0" value="Selecciona el Bono">Selecciona el tipo de
                                            Bono
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <select name="bones_code" class="selectbg">
                                                <?php foreach($bones as $bono) :?>
                                                <option value="<?=$bono['bone_id']?>"><?=$bono['bone_name']?></option>
<!--                                                <option value="Bono OneToOne">Bono OneToOne €112,50 EUR</option>-->
                                                <?php endforeach;?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                    <input type="text" placeholder="Codigo Promocional " class="inputbg" id="promoCode" name="promoCode">
                                        </td>
                                    </tr>
                                </table>
                                <input type="hidden" name="currency_code" value="EUR">
                                <input type="Submit" name="paypal" class="submit_bt1" value="Comprar">
                            </form>
                        </div>

                    </div>
                    <br>
                    <br>
                    <div class="row" style="background-color: #36a9e1; border-radius: 10px; box-shadow: 10px 10px 5px #888888; color: #FFFFFF; padding: 50px 50px 50px 50px;">
                        <h2><?=$this->lang->line('benefits_title')?></h2>
                        <br>
                        <ul>
                            <b><?=$this->lang->line('benefits_discount')?></b>: <?=$this->lang->line('benefits_discount_body')?>
                            <br>
                            <br>
                            <b><?=$this->lang->line('benefits_no_expiration')?></b>: <?=$this->lang->line('benefits_no_expiration_body')?>
                            <br>
                            <br>
                            <b><?=$this->lang->line('benefits_flexibility')?></b>: <?=$this->lang->line('benefits_flexibility_body')?>
                            <br>
                            <br>
                            <b><?=$this->lang->line('benefits_social_aspect')?></b>: <?=$this->lang->line('benefits_social_aspect_body')?>
                            <br>
                            <br>
                            <b><?=$this->lang->line('benefits_preference')?></b>: <?=$this->lang->line('benefits_preference_body')?>
                            <br>
                            <br>
                            <b><?=$this->lang->line('benefits_online')?></b>: <?=$this->lang->line('benefits_online_body')?>
                        </ul>
                        <br>
                        <br>
                        <h2><?=$this->lang->line('type_title')?></h2>
                        <br>
                        <ul>
                            <b><?=$this->lang->line('type_group')?></b>: <?=$this->lang->line('type_group_body')?>
                            <br>
                            <br>
                            <b><?=$this->lang->line('type_one')?></b>: <?=$this->lang->line('type_one_body')?>
                        </ul>
                        <br>
                        <br>
                        <h2><?=$this->lang->line('payment_method_title')?></h2>
                        <br>
                        <ul>
                            <b><?=$this->lang->line('payment_method_paypal')?></b>: <?=$this->lang->line('payment_method_paypal_body')?>
                            <br>
                            <br>
                            <b><?=$this->lang->line('payment_method_wire')?></b>: <?=$this->lang->line('payment_method_wire_body')?>
                            <br>
                            <?=$this->lang->line('payment_method_wire_account')?>
                        </ul>
                    </div>
                    <br>
                    <br>
                </div>
            </div>
            <br>
            <br>
            <br>
            <p>
                Para más información escríbenos a <a href="mailto:info@mingles.es?subject='Contacto Web'">info@mingles.es</a>
            </p>
            <br>
        </div>
    </div>
</div>

<script>
    function validateForm() {
        var error = false;

            //var x = document.forms["myForm"]["name"].value;
            console.log(document.forms["myForm"]["promoCode"].value);
            if (document.forms["myForm"]["promoCode"].value != null && document.forms["myForm"]["promoCode"].value != '') {
                $.ajax({
                        url: 'paypal/check_bono_promocode/' + document.forms["myForm"]["promoCode"].value,
                        global: false,
                        type: "POST",
                        async: false,
                        success: function(data){
                            if (data != 'true') {
                                alert(data);
                                document.forms["myForm"]["promoCode"].focus();
                                error = true;
                                return false;
                            }
                        }
                    }
                );
                /*                $.post('paypal/check_promocode/' + document.forms["myForm"]["promoCode"].value)
                 .done(function (data) {
                 //  console.log(data);
                 if (data != 'true') {
                 alert(data);
                 document.forms["myForm"]["email"].focus();
                 return false;
                 }
                 });*/
            }


            if (document.forms["myForm"]["name"].value == null || document.forms["myForm"]["name"].value == "") {
                alert("First name must be filled out");
                document.forms["myForm"]["name"].focus();
                return false;
            }
            else if (document.forms["myForm"]["email"].value == null || document.forms["myForm"]["email"].value == "") {
                alert("Email must be filled out");
                document.forms["myForm"]["email"].focus();
                return false;
            }

        if(error)
        {
            return false;
        }
    }
</script>
