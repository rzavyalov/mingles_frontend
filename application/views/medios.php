<div class="height30">
</div>
<!--<start top-section> -->
<div class="inner-content">
    <div class="title_bg">
        <div class="wrapper">&iexcl;Consulta lo que dicen los medios de Mingles!</div>
    </div>
    <div class="wrapper">

        <div class="container marketing">
            <div class="row">
                <div class="span8">
                    <div class="medi_box">
                        <h3><a target="_blank" href="http://www.elreferente.es/mas-emprendedores/seguridad-en-ti-mismo-y-practicar-la-clave-para-un-pitch-ganador-28037">Entrevista a Amanda de @MinglesSpain</a></h3>
                        <small>El Referente</a></small>
                        <p>16/12/2014</p>
                    </div>
                </div>
            </div>
            <p class="divider"></p>
            <div class="row">
                <div class="span8">
                    <div class="medi_box">
                        <h3><a target="_blank" href="http://www.todostartups.com/experiencias_emprendedores_inversores/entrevista-amanda-mingles-finalista-mejor-startup-2014">Seguridad en ti mismo y practicar: La Clave para un Pitch Ganador</a></h3>
                        <small>TodoStartups</a></small>
                        <p>15/12/2014</p>
                    </div>
                </div>
            </div>
            <p class="divider"></p>
            <div class="row">
                <div class="span8">
                    <div class="medi_box">
                        <h3><a target="_blank" href="http://ocioparapeques.wordpress.com/2014/11/27/mingles-for-kids-aprender-ingles-es-cosa-de-ninos/">Mingles for Kids: aprender ingl&eacute;s es cosa de ni&ntilde;os</a></h3>
                        <small>Ocio para Peques - Mingles for Kids</a></small>
                        <p>27/11/2014</p>
                    </div>
                </div>
            </div>
            <p class="divider"></p>
            <div class="row">
                <div class="span8">
                    <div class="medi_box">
                        <h3><a target="_blank" href=" http://loogic.com/mingles-fomenta-una-nueva-forma-de-aprender-ingles/">Mingles fomenta una nueva forma de aprender ingl&eacute;s</a></h3>
                        <small>Loogic por Javier Mart&iacute;n</small>
                        <p>29/10/2014</p>
                    </div>
                </div>
            </div>
            <p class="divider"></p>
            <div class="row">
                <div class="span8">
                    <div class="medi_box">
                        <h3><a target="_blank" href="http://tubusinesscoach.es/gabriel-pazos/">Qu&eacute; hacer para conseguir que aprendas ingl&eacute;s y pases un buen rato,con Gabriel Pazos</a></h3>
                        <small>To Business Coach - Entrevista a Gabriel Pazos</a></small>
                        <p>13/10/2014</p>
                    </div>
                </div>
            </div>
            <p class="divider"></p>

            <div class="row">
                <div class="span8">
                    <div class="medi_box">
                        <h3><a href="http://www.consumocolaborativo.com/2014/04/08/educacion-p2p-espana-consumo-colaborativo-mingles/">&nbsp;Mingles: La nueva forma de aprender inglés</a></h3>
                        <small>Consumo Colaborativo</a></small>
                        <p>08/04/2014</p>
                    </div>
                </div>
            </div>
            <p class="divider">

            <div class="row">
                <div class="span8">
                    <div class="medi_box">
                        <h3><a target="_blank" href=" http://www.larazon.es/detalle_normal_economia/noticias/7538320/ingles-para-emprendedores-are-you-ready">Ingl&eacute;s para emprendedores: Are you ready?</a></h3>
                        <small>Diario La Razon</a></small>
                        <p>03/10/2014</p>
                    </div>
                </div>
            </div>
            <p class="divider"></p>

            <div class="row">
                <div class="span8">
                    <div class="medi_box">
                        <h3><a target="_blank" href="http://www.que.es/ultimas-noticias/sociedad/201409260800-nuevas-formas-aprender-ingles-clases.html">Las nuevas formas de aprender ingl&eacute;s: de las clases a la barra del bar</a></h3>
			             <small>Que.es</a></small>
                        <p>26/09/2014</p>
		            </div>
                </div>
            </div>
	        <p class="divider"></p>
            <div class="row">
		      <div class="span8">
                  <div class="medi_box">
			         <h3><a target="_blank" href="http://www.gestionaradio.com/12037-pulso-empresarial-ruben-gil-2-2014-09-12-190000-192kbps-mp3/">Mingles en gestiona radio</a></h3>
			         <small>Gestiona Radio - Pulso Empresarial</a></small>
			         <p>12/09/2014</p>
                  </div>
		      </div>
            </div>
	        <p class="divider"></p>

            <div class="row">
                <div class="span8">
                    <div class="medi_box">
                        <h3><a target="_blank" href="http://goo.gl/5SE76o">Entrevista con Gabriel Pazos de Mingles</a></h3>
			             <small>BlueMove</a></small>
                         <p>04/09/2014</p>
                    </div>
		       </div>
	       </div>
        <p class="divider"></p>
            <div class="row">
		      <div class="span8">
                 <div class="medi_box">
			         <h3><a target="_blank" href="http://babelanblog.blogspot.com.es/2014/09/practicar-ingles-en-bares-de-Madrid.html">Donde practicar inglés en bares de Madrid</a></h3>
			         <small>Babelean</a></small>
			         <p>03/09/2014</p>
                 </div>
                </div>
		    </div>
	 <p class="divider"></p>
     <div class="row">
		<div class="span8">
            <div class="medi_box">
			     <h3><a target="_blank" href="http://goo.gl/BAcDyV">La Linterna - Cadena COPE</a></h3>
			     <small>Cadena Cope</a></small>
			     <p>10/07/2014</p>
            </div>
		</div>
   	</div>
	<p class="divider"></p>
    <div class="row">
		<div class="span8">
            <div class="medi_box">
			     <h3><a target="_blank" href="http://www.cadenaser.com/local/audios/aje-conocemos-mingles-musica-angel-robledillo-2014/csrcsrpor/20140702csrcsrloc_6/Aes/">Hoy por Hoy - Cadena Ser</a></h3>
			     <small>Cadena Ser</a></small>
			     <p>02/07/2014</p>
            </div>
		</div>
   	</div>
	<p class="divider"></p>
    <div class="row">
		<div class="span8">
            <div class="medi_box">
			     <h3><a target="_blank" href="http://www.efeempresas.com/noticia/andres-pazos-en-mingles-queremos-ser-divertidos-y-diferentes-y-que-la-gente-quiera-aprender-con-nosotros/">EFE Emprende</a></h3>
			     <small>Agencia EFE</a></small>
			     <p>01/07/2014</p>
            </div>
		</div>
	 </div>
	 <p class="divider"></p>
     <div class="row">
	       <div class="span8">
                <div class="medi_box">
                    <h3><a target="_blank" href="http://www.dontstopmadrid.com/2014/06/mingles-clases.html">Mingles: una forma diferente de aprender idiomas</a></h3>
			         <small>DONTSTOP Madrid</a></small>
			         <p>01/06/2014</p>
                </div>
		      </div>
	   </div>
	 <p class="divider"></p>
	<div class="row">
		<div class="span8">
            <div class="medi_box">
			     <h3><a target="_blank" href="http://ondainversion.com/podcast/aprender-idiomas-con-amigos-es-posible">APRENDER IDIOMAS CON AMIGOS, ¿ES POSIBLE?</a></h3>
			     <small>Onda Inversión</a></small>
			     <p>23/05/2014</p>
            </div>
		</div>
   	</div>
	 <p class="divider"></p>
   <div class="row">
		<div class="span8">
            <div class="medi_box">
			     <h3><a target="_blank" href="http://www.rtve.es/alacarta/audios/tendencias/tendencias-mingles-29-05-14/2587931/">Tendencias - Mingles en RNE</a></h3>
			     <small>RNE - Tendencias</a></small>
			     <p>02/05/2014</p>
            </div>
		</div>
   	</div>
	 <p class="divider"></p>
	 <div class="row">
		<div class="span8">
            <div class="medi_box">
			     <h3><a target="_blank" href="http://entrepreneurship.blogs.ie.edu/2014/05/06/meet-the-startups-in-area-31-mingles/">Meet the Startups in Area 31: Mingles</a></h3>
			     <small>IE Entrepreneurship</a></small>
			     <p>06/05/2014</p>
            </div>
		</div>
	 </div>
	<p class="divider"></p>

            <div class="row">
                <div class="span8">
                    <div class="medi_box">
                        <h3><a href="http://www.tiempodehoy.com/sociedad/la-fiebre-del-ingles">&nbsp;La fiebre del Ingl&eacute;s</a>
                        </h3>
                        <small>Revista Tiempo</a></small>
                        <p>16/03/2014</p>
                    </div>
                </div>
            </div>
            <p class="divider">
            </p>
            <div class="row">
                <div class="span8">
                    <div class="medi_box">
                        <h3><a href="http://www.somoschueca.com/mingles-sexy-valentine-en-chueca/">Mingles Sexy Valentine in Chueca</a></h3>
                        <small>Somos Chueca</a></small>
                        <p>
                            11/02/2014
                        </p>
                    </div>
                </div>
            </div>
            <p class="divider">
            </p>
            <div class="row">
                <div class="span8">
                    <div class="medi_box">
                        <h3><a href="http://www.englishwarehouse.com/article_detail.asp?id=361&cat=features">Mingle
                                in English</a></h3>
                        <small>EnglishWareHouse</a></small>
                        <p>
                            10/02/2014
                        </p>
                    </div>
                </div>
            </div>
            <p class="divider">
            </p>
            <div class="row">
                <div class="span8">
                    <div class="medi_box">
                        <h3><a href="http://franciscobenito.es/2014/01/emprendimiento-ie-business-school/#more-834">Emprendimiento en el IE Business School</a></h3>
                        <small>Andrés Pazos, uno de los fundadores de Mingles nos habla del emprendimiento en el IE Business School en <a href="http://franciscobenito.es/">franciscobenito.es</a></small>
                        <p>
                            29/01/2014
                        </p>
                    </div>
                </div>
            </div>
            <p class="divider">
            </p>
            <div class="row">
                <div class="span8">
                    <div class="medi_box">
                        <h3><a href="http://carruselemprendedores.radio3w.com/derribando-barreras-nos-vamos-de-bares-en-ingles/">&nbsp;&nbsp; Derribando barreras, nos vamos de bares en Inglés</a></h3>
                        <small>Radio3W</a></small>
                        <p>
                            08/12/2013
                        </p>
                    </div>
                </div>
            </div>
            <p class="divider">
            </p>
            <div class="row">
                <div class="span8">
                    <div class="medi_box">
                        <h3><a href="http://unbuendiaenmadrid.com/mingles/">La nueva forma de aprender Inglés!</a></h3>
                        <small>Un Buen dia Madrid</a></small>
                        <p>
                            08/12/2013
                        </p>
                    </div>
                </div>
            </div>
            <p class="divider">
            </p>
            <div class="row">
                <div class="span8">
                    <div class="medi_box">
                        <h3><a href="http://www.cheapinmadrid.com/thanksgiving-con-mingles/">Cheap
                                Madrid. Thanksgiving con Mingles</a></h3>
                        <small>Cheap Madrid</a></small>
                        <p>
                            26/11/2013
                        </p>
                    </div>
                </div>
            </div>
            <p class="divider">
            </p>
            <div class="row">
                <div class="span8">
                    <div class="medi_box">
                        <h3><a href="http://www.justinmyhandbag.com/2013/11/mingles.html">Just in my handbag, ¿Te apetece practicar inglés?</a></h3>
                        <small>Just in my handbag</a></small>
                        <p>
                            18/11/2013
                        </p>
                    </div>
                </div>
            </div>
            <p class="divider"></p>
            <div class="row">
                <div class="span8">
                    <div class="medi_box">
                        <h3><a href="http://www.ochoymedio.com/en/noticias-eventos/on-the-road/mingles/">&nbsp;&nbsp;Mingles
                                en Ocho y Medio</a></h3>
                        <small>Ocho y Medio</a></small>
                        <p>16/11/2013</p>
                    </div>
                </div>
            </div>
            <p class="divider">
            </p>
            <div class="row">
                <div class="span8">
                    <div class="medi_box">
                        <h3><a href="http://www.dontstopmadrid.com/2013/10/mingles-clases-de-ingles.html">Don't Stop Madrid. Mingles, Aprende inglés en los bares más chulos de Madrid</a></h3>
                        <small>DONTSTOP Madrid</a></small>
                        <p>28/10/2013</p>
                    </div>
                </div>
            </div>
            <p class="divider">
            </p>
            <div class="row">
                <div class="span8">
                    <div class="medi_box">
                        <h3><a href="http://www.eae.es/news/2013/10/28/gabriel-pazos-alumno-de-eae-business-school-funda-la-empresa-mingles">&nbsp;&nbsp;Gabriel Pazos, alumno de EAE Business School, funda la empresa “Mingles”</a></h3>
                        <small>EAE Business School</a></small>
                        <p>
                            28/10/2013
                        </p>
                    </div>
                </div>
            </div>
            <p class="divider">
            </p>
            <div class="row">
                <div class="span8">
                    <div class="medi_box">
                        <h3><a href="http://elpais.com/elpais/2013/10/14/eps/1381753903_217752.html">&nbsp;&nbsp;El
                                Pais, Acceder antes que poseer</a></h3>
                        <small>El Pais</a></small>
                        <p>
                            14/10/2013
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
