<table border='0' cellpadding='0' class='tablefrom'>
    <tr>
        <th><?=$this->lang->line('class')?></th>
        <th><?=$this->lang->line('date')?></th>
        <th><?=$this->lang->line('time')?></th>
        <th><?=$this->lang->line('place')?></th>
        <th><?=$this->lang->line('city')?></th>
        <th><?=$this->lang->line('language')?></th>
        <th><?=$this->lang->line('details')?></th>
        <th><?=$this->lang->line('cancel')?></th>
        <th><?=$this->lang->line('share')?></th>
    </tr>

    <?php foreach ($classes as $row) : ?>
        <tr>
            <td><?= $row['ClassName'] ?></td>
            <td><?= $row['ClassDate'] ?></td>
            <td><?= $row['ClassStartTime'] . '-' . $row['ClassEndTime'] ?></td>
            <td><?= $row['PubName'] ?></td>
            <td><?= $row['CityName'] ?></td>
            <td><?= $row['LanguageName'] ?></td>
            <td>

                <?php if($this->session->userdata('usertype_id') == 4): ?>
                    <a href="<?= base_url() ?>admin/student/userclass/<?= $row['ClassID'] ?>/<?=$user_info['user_id']?>"> <img
                            src="<?= base_url() ?>assets/images/edit.png" style="height:24px;"></a>
                <?php else: ?>
                    <a href="<?= base_url() ?>admin/userclass/<?= $row['ClassID'] ?>"> <img
                            src="<?= base_url() ?>assets/images/edit.png" style="height:24px;"></a>
                <?php endif; ?>
            </td>
            <td><?php if ($row['can_cancel']) : ?><a class="cancelclass" href="#"
                                                     data-url="<?= base_url('admin/userclass/cancel/' . $row['ClassID']) ?>"
                                                     data-userid="<?=$row['UserDetailsID']?>"
                                                     title="Cancel class">
                    <img src="<?= base_url() ?>assets/images/delete.png" style="height:24px;">
                    </a><?php endif; ?></td>
            <td><a href="#" class="shareclass" data-url="<?=base_url('admin/share/popup/' . $row['ClassID'])?>">Share</a></td>
        </tr>
    <?php endforeach; ?>

</table>
<span class="ajax_pag"><?=$this->pagination->create_links()?> </span>


<script src="/assets/js/pagination.js"></script>
<script src="/assets/js/admin/deleteclass.js"></script>
<script src="/assets/js/admin/editclass.js"></script>
<script src="/assets/js/admin/newclass.js"></script>
<link rel="stylesheet" href="/assets/css/pagination.css">