<table border='0' cellpadding='0' class='tablefrom'>
    <tr>
        <th>Class ID</th>
        <th>Class Name</th>
        <th>Class Date</th>
        <th>Class Time</th>
        <th>Pub Name</th>
        <th>City</th>
        <th>Language</th>
        <th>Edit</th>
        <th>Delete</th>
    </tr>
    <?php foreach ($class_list as $class) :?>
        <tr>
            <td><?=$class['ClassID'] . '</td>'?>
            <td><?=$class['ClassName'] . '</td>'?>
            <td><?=$class['ClassDate'] . '</td>'?>
            <td><?=$class['ClassStartTime'] . '-' . $class['ClassEndTime'] . '</td>'?>
            <td><?=$class['PubName'] . '</td>'?>
            <td><?=$class['CityName'] . '</td>'?>
            <td><?=$class['LanguageName'] . '</td>'?>
            <td><a href="#" class="editmodal" data-id="<?=$class['ClassID']?>" data-url="<?=base_url('admin/manageclass/edit/'.$class['ClassID'])?>" title="Edit Class"> <img src="<?=base_url()?>assets/images/edit.png" style="height:24px;"></a></td>
            <td><a href="#" class="deleteclass" title="Delete Class" data-url="<?=base_url()?>admin/manageclass/delete/<?=$class['ClassID']?>" data-name="<?=$class['ClassID']?>"> <img src="<?=base_url()?>assets/images/delete.png" style="height:24px;"></a></td>
        </tr>
    <?php endforeach;?>
</table>
<span class="ajax_pag"><?=$this->pagination->create_links()?> </span>


<script src="/assets/js/pagination.js"></script>
<script src="/assets/js/admin/deleteclass.js"></script>
<script src="/assets/js/admin/editclass.js"></script>
<script src="/assets/js/admin/newclass.js"></script>
<link rel="stylesheet" href="/assets/css/pagination.css">