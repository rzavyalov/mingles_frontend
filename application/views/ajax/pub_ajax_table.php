<table border='0' cellpadding='0' class='tablefrom'>
    <tr>
        <th><?=$this->lang->line('class')?></th>
        <th><?=$this->lang->line('date')?></th>
        <th><?=$this->lang->line('time')?></th>
        <th><?=$this->lang->line('place')?></th>
        <th><?=$this->lang->line('city')?></th>
        <th>Total groups</th>
        <th>Total students</th>
        <th><?=$this->lang->line('language')?></th>
        <th><?=$this->lang->line('details')?></th>
        <th><?=$this->lang->line('cancel')?></th>
    </tr>

    <?php foreach ($classes as $row) : ?>
        <tr>
            <td><?= $row['ClassName'] ?></td>
            <td><?= $row['ClassDate'] ?></td>
            <td><?= $row['ClassStartTime'] . '-' . $row['ClassEndTime'] ?></td>
            <td><?= $pub_info['PubName'] ?></td>
            <td><?= $pub_info['CityName'] ?></td>
            <td><?= $row['groups'] ?></td>
            <td><?= count($row['students']) ?></td>
            <td><?= $row['LanguageName'] ?></td>
            <td>
                <?php if(count($row['students'])) : ?>
                    <a href="<?= base_url() ?>admin/userclass/<?= $row['ClassID'] ?>"> <img
                            src="<?= base_url() ?>assets/images/edit.png" style="height:24px;"></a>
                <?php endif;?>

            </td>
            <td><?php if ($row['can_cancel']) : ?><a class="cancelclass" href="#"
                                                     data-url="<?= base_url('admin/manageclass/delete/' . $row['ClassID']) ?>"
                                                     data-userid="<?=$row['ClassID']?>"
                                                     title="Cancel class">
                    <img src="<?= base_url() ?>assets/images/delete.png" style="height:24px;">
                    </a><?php endif; ?></td>
        </tr>
    <?php endforeach; ?>

</table>
<span class="ajax_pag"><?=$this->pagination->create_links()?> </span>

<script src="/assets/js/pagination.js"></script>
<link rel="stylesheet" href="/assets/css/pagination.css">