<table border='0' cellpadding='0' class='tablefrom'>
    <tr>
        <th>Pub Name</th>
        <th>City</th>
        <th>Date</th>
        <th>Class Name</th>
        <th>Group Name</th>
        <th>Teacher Name</th>
        <th>Level Name</th>
        <th>Student Name</th>
        <th>Edit</th>
        <th>Manage Student</th>
        <th>Delete</th>
    </tr>

    <?php foreach ($groups->result_array() as $group) : ?>
        <tr>
            <td><?=$group['PubName']?></td>
            <td><?=$group['CityName']?></td>
            <td><?=date_format(date_create($group['ClassDate']), 'Y-m-d');?></td>
            <td><?=$group['CLassName']?></td>
            <td><a href="<?=base_url('admin/managegroup/details/'.$group['GroupID'])?>"><?=$group['GroupName']?></a></td>
            <td><?=$group['TeacherName']?></td>
            <td><?=$group['LevelName']?></td>
            <td <?php if($group['UserFullName'] != ''):?> style="background-color: #ffff00" <?php endif;?>><?=$group['UserFullName']?></td>

            <td><a href="#" data-id="<?=$group['GroupID']?>" data-url="<?=base_url('admin/managegroup/edit/'.$group['GroupID'])?>" class="editgroupmodal" title="Edit Group"> <img src="<?=base_url()?>assets/images/edit.png" style="height:24px;"></a></td>
            <td><a href="#" data-id="<?=$group['GroupID']?>" data-url="<?=base_url('admin/managegroup/students/'.$group['GroupID'])?>" class="managestudentsmodal"" title="Manage Student Group"> <img src="<?=base_url()?>assets/images/edit.png" style="height:24px;"></a></td>
            <td><a href="#" class="deletegroup" title="Delete Group" data-name="<?=$group['PubName']?>" data-url="<?=base_url('admin/managegroup/delete/'.$group['GroupID'])?>" title="Delete Group"> <img src="<?=base_url()?>assets/images/delete.png" style="height:24px;"></a></td>
        </tr>
    <?php endforeach; ?>
</table>
<span class="ajax_pag"><?=$this->pagination->create_links()?> </span>


<script src="/assets/js/pagination.js"></script>

<script src="/assets/js/admin/deletegroup.js"></script>
<script src="/assets/js/admin/editgroup.js"></script>
<script src="/assets/js/admin/newgroup.js"></script>
<script src="/assets/js/admin/managestudents_modal.js"></script>
<link rel="stylesheet" href="/assets/css/pagination.css">
