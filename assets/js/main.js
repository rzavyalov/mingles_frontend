//Div lenguajes
var clic4 = 1;
function divlengua(){
  if(clic4==1){
    document.getElementById("navbar-apunta").style.marginTop = "-500px";
    document.getElementById("navbar-credito").style.marginTop = "-235px";
    document.getElementById("languajes").style.marginTop = "70px";
    clic4 = clic4 + 1;
   } else{
      document.getElementById("languajes").style.marginTop = "-235px";
      clic4 = 1;
   }
}

//Div creditos
var clic = 1;
function divcreditos(){
   if(clic==1){
    document.getElementById("languajes").style.marginTop = "-235px";
    document.getElementById("navbar-apunta").style.marginTop = "-500px";
    document.getElementById("navbar-credito").style.marginTop = "70px";
    clic = clic + 1;
   } else{
      document.getElementById("navbar-credito").style.marginTop = "-235px";
      clic = 1;
   }
}

//Div apuntate
var clic2 = 1;
function divapuntate(){
   if(clic2==1){
    document.getElementById("languajes").style.marginTop = "-235px";
    document.getElementById("navbar-credito").style.marginTop = "-235px";

    document.getElementById("navbar-apunta").style.marginTop = "70px";
    clic2 = clic2 + 1;
   } else{
      document.getElementById("navbar-apunta").style.marginTop = "-500px";
      clic2 = 1;
   }
}

//más y menos créditos
$(function() {
  $(".button").on("click", function() {
    var $button = $(this);
    var oldValue = $button.parent().find("input").val();
    if ($button.text() == "+") {
      var newVal = parseFloat(oldValue) + 1;
    } else {
        if (oldValue > 0) {
          var newVal = parseFloat(oldValue) - 1;
        } else {
            newVal = 0;
          }
      }
    $button.parent().find("input").val(newVal);
  });
});


//Boton tablet menu
function mostrar(enla,etik) {
  obj = document.getElementById(etik);
  obj.style.display = (obj.style.display == 'block') ? 'none' : 'block';
  obj.style.height = (obj.style.height == '100px') ? '0px' : '100px';
}

function toggleClass(element, className){
    if (!element || !className){
        return;
    }

    var classString = element.className;
    var nameIndex = classString.indexOf(className);

    if (nameIndex == -1) {
        classString += ' ' + className;
    } else {
        classString = classString.substr(0, nameIndex) + classString.substr(nameIndex+className.length);
    }

    element.className = classString;
}

document.getElementById('btn-menu-responsive').addEventListener('click', function() {
    toggleClass(this, 'close');
});

//Div movil menu apuntate

//Div apuntate movil
var clic2 = 1;
function divapuntatem(){
   if(clic2==1){
    document.getElementById("languajes").style.marginTop = "-235px";
    document.getElementById("navbar-credito").style.marginTop = "-235px";
    document.getElementById("fecha").style.marginTop = "-235px";
    document.getElementById("nivel").style.marginTop = "-235px";
    document.getElementById("precio").style.marginTop = "-235px";
    document.getElementById("lugar").style.marginTop = "-235px";
    document.getElementById("navbar-apunta-m").style.marginTop = "70px";
    clic2 = clic2 + 1;
   } else{
      document.getElementById("navbar-apunta-m").style.marginTop = "-500px";
      clic2 = 1;
   }
}
