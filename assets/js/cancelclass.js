$('.cancelclass').click(function (e) {
    e.preventDefault();
    var title_msg = $(this).attr('title');
    var url = $(this).data('url');
    var uid = $(this).data('userid');
    var obj = $(this).parent().parent();
    dhtmlx.message({
        type: "confirm",
        text: "¿Estás seguro que quieres cancelar tu sesión Mingles?",
        title: title_msg,
        callback: function (e) {
            if (e) {
                $.ajax({
                    url: url,
                    data: {'uid':uid},
                    type: "POST"
                }).done(function (e) {
                    Jsonobj = JSON.parse(e);
                    if(Jsonobj.status == 'error')
                    {
                        alert(Jsonobj.message);
                    }
                    else if(Jsonobj.status == 'OK') {
                        $('input[name="price"]').val(parseFloat($('input[name="price"]').val()) + parseFloat(Jsonobj.message));
                        obj.remove();
                    }
                });
            }
        }
    });
});
