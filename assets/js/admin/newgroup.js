$('.addgroupmodal').click(function() {
    $.pgwModal({
        url: $(this).data('url'),
        loadingContent: '<span style="text-align:center">Loading in progress</span>',
        title: $(this).attr('title'),
        closable: true,
        titleBar: false
    });
});