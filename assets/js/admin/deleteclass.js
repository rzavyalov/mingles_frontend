$('.deleteclass').click(function(e){
    e.preventDefault();
    var title_msg = $(this).attr('title');
    var url = $(this).data('url');
    var obj = $(this).parent().parent();
    dhtmlx.message({
        type:"confirm",
        text: "Do you want to delete the class?",
        title: title_msg,
        callback: function(e) {
            if(e)
            {
                $.ajax({
                    url: url
                }).done(function(e) {
                    obj.remove();
                });
            }
        }
    });
})