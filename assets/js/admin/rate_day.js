$(function(){
    $('.publinkinfoday').bind('click',function(e){
        e.preventDefault();
        $('.publinkinfoday').removeClass('active');
        $(this).addClass('active');
        var PubID = $(this).data('id');
        var curdate = $("#date").val();
        $.ajax({
            type: "POST",
            url: "/json/getclass/today",
            cache: false,
            data: {
                "pubid": PubID,
                "seldate": curdate
            },
            dataType: 'text',
            success: function(data) //Si se ejecuta correctamente
            {
                $('.classes').html(data);
                /*                    $.each(data, function(index, array) {
                 $('.classes').append('<div><a href="#">'+array.ClassName+'</a></div>');
                 });*/

                console.log(data);
            },
            error: function(data)
            {
                // alert('error');
            }
        });
    });
    $('.publinkinfoday').first().click();
});