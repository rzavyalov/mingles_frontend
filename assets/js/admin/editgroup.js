$('.editgroupmodal').click(function(e) {
    e.preventDefault();
    $.pgwModal({
        url: $(this).data('url'),
        loadingContent: '<span style="text-align:center">Loading in progress</span>',
        title: 'Edit Group',
        closable: true,
        titleBar: false
    });
});