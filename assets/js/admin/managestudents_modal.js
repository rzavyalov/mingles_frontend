$('.managestudentsmodal').click(function(event) {
    event.preventDefault();
    $.pgwModal({
        url: $(this).data('url'),
        loadingContent: '<span style="text-align:center">Loading in progress</span>',
        title: 'Manage Student Group',
        closable: true,
        titleBar: false
    });
});

