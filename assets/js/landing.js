var curDate;
var pubs;
var landingpage;
var sv = new google.maps.StreetViewService();
var panorama;
var gmarkers = [];
var map;
    $(function () {
        pubs = $('#pubs_ids').val();
        landingpage = $('#landingpage').val();
        //alert($('#pubs_ids').val());
        // disabledControls('#option1', false);
        //disabledControls('#option2', false);

        getCity();
        //getDatesClass();
        SelectCityChange();
        getsessionpaymentforHome();
        GetUserDetails();

        SelectThePaymentText();
        SelectPubChange();
        SelectLanguageChange();
        $('.showall').click(function (e) {
            e.preventDefault();
            $('#datepicker').datepicker('setDate', null);
            curDate = null;
            GenerateMap();
        })
    });

    function SelectPubChange() {
        $('#ddlPub').change(function () {

            curPub = $('#ddlPub :selected').val();
            curLanguage = $('#ddlLanguage :selected').val();

            sellers = gmarkers;
            for (sellerId in sellers) {
                if (curPub == sellers[sellerId].value) {
                    myclick(sellerId);
                    break;
                }
            }
        })
    }

    function SelectLanguageChange() {
        $('#ddlLanguage').change(function () {
            curPub = $('#ddlPub :selected').val();
            sellers = gmarkers;
            getClassLanguage(curDate);

        })
    }

    function updateScreetLinks() {
        var streetlink = $('.streetlink');
        streetlink.unbind('click');

        streetlink.click(function (e) {
            e.preventDefault();

//panorama = new google.maps.StreetViewPanorama(document.getElementById('pano'));
            $('#pano').show();
            google.maps.event.trigger(panorama, "resize");
        });
    }

    function updateScreetLinks() {
        var streetlink = $('.streetlink');
        streetlink.unbind('click');

        streetlink.click(function (e) {
            e.preventDefault();

//panorama = new google.maps.StreetViewPanorama(document.getElementById('pano'));
            $('#pano').show();
            google.maps.event.trigger(panorama, "resize");
        });
    }

    function getDatesClass() {
        var cityid = $('#ddlCity').val();
        //alert(cityid);
        $.ajax({
            type: "POST",
            url: "/json/dateclass/hidden",
            data: "cityid=" + cityid + "&pubs=" + pubs + "&landingpage=" + landingpage,
            cache: false,
            dataType: 'json',
            success: function (data) //Si se ejecuta correctamente
            {
                enabledDays = [];
                $.each(data, function (index, array) {
                    enabledDays.push(array.datestart);
                });
                $("#datepicker").datepicker("refresh");
                $("#datepicker").datepicker({
                    /*                changeMonth: true,
                     changeYear: true,*/
                    closeText: 'Cerrar',
                    isRTL: false,
                    prevText: '<Ant',
                    nextText: 'Sig>',
                    firstDay: 1,
                    currentText: 'Hoy',
                    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                    monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
                    dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
                    dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
                    dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
                    weekHeader: 'Sm',
                    onSelect: onSelectDatePicker,
                    beforeShowDay: enableAllTheseDays,
                    dateFormat: 'yy-mm-dd'
                });
            },
            error: function (data) {
                // alert('error');
            }
        });
    }


    var enabledDays = [];

    function enableAllTheseDays(date) {

        dmy = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) +
        '-' + ('0' + date.getDate()).slice(-2);
        //dmy = date.getFullYear() + "-" + (date.getMonth()+1) + "-" + date.getDate();
        if ($.inArray(dmy, enabledDays) != -1) {
            return [true, "ui-state-hover", ""];
        } else {
            return [false, "", ""];
        }
    }

    function onSelectDatePicker(dateText, inst) {
//    getBarsByDate();
//    $("#cursoshora").html(dateText.toString('dd, MM ,yyyy'));
//    $("#option2 img").attr("src", "img/2_filled-64.png");

        disabledControls('#option1', true);
        disabledControls('#option2', true);
        $("#selDate").html(dateText.toString('dd, MM ,yyyy'));
        curDate = dateText;
        getPub(dateText);
        $('.showall').show();
    }

    function disabledControls(divName, enabled) {
        var $div = $(divName);
        // let's select and cache all the fields
        var $inputs = $div.find("input, select, button, textarea, option, ul, li");
        if (enabled) {
            $inputs.removeAttr("disabled");
            $inputs.removeClass("disabled");
            $(divName + '> div').addClass("well-strong");
        }
        else {
            $inputs.prop("disabled", true);
        }
    }

    function getCity() {
        $.ajax({
            type: "POST",
            url: "/json/getcity",
            cache: false,
            dataType: 'json',
            data: {'pubs': pubs, 'landingpage': landingpage},
            success: function (data) //Si se ejecuta correctamente
            {
                var City = $("#ddlCity");
                $.each(data, function (index, array) {
                    City.append($("<option />").val(array.CityID).text(array.CityName));
                });
                var City1 = $("#ddlCityHeader");
                $.each(data, function (index, array) {
                    City1.append($("<option />").val(array.CityID).text(array.CityName));
                });
                getDatesClass();
                getPub();
                //GenerateMap();

            },
            error: function (data) {
                //alert('error');
            }
        });
    }

    function getPub(seldate) {
        var cityid = $('#ddlCity').val();
        var seldate = seldate;

        $.ajax({
            type: "POST",
            url: "/json/getpub",
            cache: false,
            async: false,
            data: {
                "cityid": cityid,
                "seldate": seldate,
                "pubs": pubs,
                "landingpage": landingpage
            },
            dataType: 'json',
            success: function (data) //Si se ejecuta correctamente
            {
                var pub = $("#ddlPub");
                pub.empty();
                $.each(data, function (index, array) {
                    pub.append($("<option />").val(array.PubID).text(array.PubName));
                });

                getClass(seldate);

                var latLng;
                if (data.length > 0) {

                    panorama = new google.maps.StreetViewPanorama(document.getElementById('pano'));

                    latLng = new google.maps.LatLng(data[0].Latitude, data[0].Longitude);
                    map = new google.maps.Map(document.getElementById("map_canvas"), {
                        center: latLng,
                        zoom: 11,
                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                        streetViewControl: false
                    });

                    for (var i = 0, len = data.length; i < len; i++) {
                        latLng = new google.maps.LatLng(data[i].Latitude, data[i].Longitude);
                        var marker = new google.maps.Marker({
                            position: latLng,
                            map: map,
                            title: data[i].PubName,
                            value: data[i].PubID
                        });

                        var infowindow = new google.maps.InfoWindow({
                            maxWidth: 150,
                            maxHeight: 50
                        });

                        google.maps.event.addListener(marker, 'click', (function (marker, i) {
                            return function () {
                                infowindow.setContent("<div style=\"color:red;width:auto;height:auto;\">" + data[i].PubName + '<br/>' +
                                '<a href="#" class="streetlink">' + data[i].PubAddress + "<a/></div>");
                                infowindow.open(map, marker);
                                map.panTo(this.getPosition());
                                map.setZoom(20);
                                sv.getPanoramaByLocation(this.getPosition(), 50, processSVData);
                                //getPub();
                                $("#ddlPub [value='" + data[i].PubID + "']").attr("selected", "selected");
                                // getClass(curDate);

                                google.maps.event.trigger(panorama, "resize");
                                updateScreetLinks();
                            }
                        })(marker, i));

                        google.maps.event.addListener(infowindow, 'closeclick', function () {
                            sv.getPanoramaByLocation(latLng, 50, processSVData);
                            $('#pano').hide();
                            map.setZoom(11);
                            map.panTo(latLng);
                            // then, remove the infowindows name from the array
                        });

                        gmarkers.push(marker);
//                    var infowindow = new google.maps.InfoWindow({
//                        content: data[i].PubName
//                    });

//                    google.maps.event.addListener(marker, 'click', function() {
//                        infowindow.open(map, marker);
//                    });
                    }


                    // sv.getPanoramaByLocation(berkeley, 50, processSVData);

                    /*                google.maps.event.addListener(map, 'click', function(event) {
                     sv.getPanoramaByLocation(event.latLng, 50, processSVData);
                     });*/

                }
            },
            error: function (data) {
                //alert('error');
            }
        });


    }

    function getClassLanguage(seldate) {
        var seldate = seldate;
        var curPub = $('#ddlPub :selected').val();
        var curLanguage = $('#ddlLanguage :selected').val();

        $.ajax({
            type: "POST",
            url: "/json/getclass/language",
            cache: false,
            data: {
                "pubid": curPub,
                "seldate": seldate,
                "lang": curLanguage
            },
            dataType: 'json',
            success: function (data) //Si se ejecuta correctamente
            {
                var ddlclass = $("#ddlClass");
                ddlclass.empty();
                $.each(data, function (index, array) {
                    ddlclass.append($("<option />").val(array.ClassID).text(array.ClassName));
                });

                updateprice();
                /*            getLabelbyClassID();
                 getAllLanguagebyPubID();
                 getLanguagebyClassID();*/
            },
            error: function (data) {
                // alert('error');
            }
        });

    }

    function getClass(seldate) {
        var PubID = $('#ddlPub').val();
        var seldate = seldate;

        $.ajax({
            type: "POST",
            url: "/json/getclass/hidden",
            cache: false,
            data: {
                "pubid": PubID,
                "seldate": seldate,
                "landing_page": landingpage
            },
            dataType: 'json',
            success: function (data) //Si se ejecuta correctamente
            {
                var ddlclass = $("#ddlClass");
                ddlclass.empty();
                $.each(data, function (index, array) {
                    ddlclass.append($("<option />").val(array.ClassID).text(array.ClassName));
                });
                getLabelbyClassID();
                getAllLanguagebyPubID(seldate);
                getLanguagebyClassID();
                updateprice();
            },
            error: function (data) {
                // alert('error');
            }
        });
    }

    function getLabelbyClassID() {
        var ClassID = $('#ddlClass').val();
        $.ajax({
            type: "POST",
            url: "/json/getlevel",
            cache: false,
            data: "classid=" + ClassID,
            dataType: 'json',
            success: function (data) //Si se ejecuta correctamente
            {
                var ddlLevel = $("#ddlLevel");
                ddlLevel.empty();
                $.each(data, function (index, array) {
                    ddlLevel.append($("<option />").val(array.LevelID).text(array.LevelName));
                });
                $.ajax({
                    type: "POST",
                    url: "/json/getlevel/get_current_level",
                    async: false,
                    cache: false,
                    success: function (data) //Si se ejecuta correctamente
                    {
                        $("#ddlLevel option[value='" + data + "']").prop("selected", true);
                    },
                    error: function (data) {
                        // alert('error');
                    }
                });

            },
            error: function (data) {
                // alert('error');
            }
        });
    }

    function getAllLanguagebyPubID(seldate) {
        var PubID = $('#ddlPub').val();
        var seldate = seldate;

        $.ajax({
            type: "POST",
            url: "/json/getlanguage/all",
            cache: false,
            data: "pubid=" + PubID,
            data: {
                "pubid": PubID,
                "seldate": seldate
            },
            async: false,
            dataType: 'json',
            success: function (data) //Si se ejecuta correctamente
            {
                var ddlLanguage = $("#ddlLanguage");
                ddlLanguage.empty();
                $.each(data, function (index, array) {
                    ddlLanguage.append($("<option />").val(array.LanguageID).text(array.LanguageName));
                });
            },
            error: function (data) {
                // alert('error');
            }
        });
    }

    function getLanguagebyClassID() {
        var ClassID = $('#ddlClass').val();
        $.ajax({
            type: "POST",
            url: "/json/getlanguage",
            cache: false,
            data: "classid=" + ClassID,
            dataType: 'json',
            success: function (data) //Si se ejecuta correctamente
            {
                var ddlLanguage = $("#ddlLanguage");

                $("#ddlLanguage option[value='" + data[0].LanguageID + "']").attr("selected", "selected");
                //ddlLanguage.val(data[0].LanguageID).change();
                /*            ddlLanguage.empty();
                 $.each(data, function(index, array) {
                 ddlLanguage.append($("<option />").val(array.LanguageID).text(array.LanguageName));
                 });*/
            },
            error: function (data) {
                // alert('error');
            }
        });
    }

    function getsessionpaymentforHome() {
        $.ajax({
            type: "POST",
            url: "/jsoncode/json_getsessionpaymentforHome.php",
            cache: false,
            dataType: 'json',
            success: function (data) //Si se ejecuta correctamente
            {
                var ddlSession = $("#ddlSession");
                ddlSession.empty();
                $.each(data, function (index, array) {
                    ddlSession.append($("<option />").val(array.SessionPaymentID).text(array.SessionPaymentName));
                });
                SelectThePaymentText();
            },
            error: function (data) {
                //  alert('error');
            }
        });
    }

    function SelectThePaymentText() {
//    $("#ddlSession").change(function() {
//        $('#ProductName').val = $("#ddlSession option:selected").text();
//    });

    }

    function SelectCityChange() {

        $("#ddlCity").change(function () {
            $("#ddlCityHeader").val($(this).val());
            getDatesClass();
            getPub();
            // GenerateMap();
        });
        $("#ddlCityHeader").change(function () {
            $("#ddlCity").val($(this).val());
            getDatesClass();
            getPub();
            GenerateMap();
        });
        $("#ddlPub").change(function () {
            getClass(curDate);
        });
        $("#ddlClass").change(function () {
            getLabelbyClassID();
            getLanguagebyClassID();
        });
    }

function updateprice()
{
    var price = $('#ddlClass :selected').data('price');
    var price_text = "0 EUR - FREELINGUAL";
    if(price > 0)
    {
        $("#price").text(price + " EUR");
    }
    else
    {
        $("#price").text(price_text);
    }
}
//function initializegooglemaps(latitude, longitude) {
//    alert(latitude);
//    var myLatlng = new google.maps.LatLng(latitude, longitude);
//    var mapOptions = {
//        zoom: 16,
//        center: myLatlng,
//        mapTypeId: google.maps.MapTypeId.ROADMAP
//    }
//    var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
//    var marker = new google.maps.Marker({
//        position: myLatlng,
//        map: map,
//        title: 'Hello World!'
//    });
//}
//var myCenter = new google.maps.LatLng(51.508742, -0.120850);
    function GenerateMap() {
        var cityid = $('#ddlCity').val();


        //alert(cityid);
        var location = [];
        $.ajax({
            type: "POST",
            url: "/json/getpub",
            cache: false,
            async: true,
            data: {
                "cityid": cityid,
                "pubs": pubs,
                "landingpage": landingpage
            },
            success: function (data) //Si se ejecuta correctamente
            {
                var latLng;
                if (data.length > 0) {

                    panorama = new google.maps.StreetViewPanorama(document.getElementById('pano'));

                    latLng = new google.maps.LatLng(data[0].Latitude, data[0].Longitude);
                    map = new google.maps.Map(document.getElementById("map_canvas"), {
                        center: latLng,
                        zoom: 11,
                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                        streetViewControl: false
                    });

                    for (var i = 0, len = data.length; i < len; i++) {
                        latLng = new google.maps.LatLng(data[i].Latitude, data[i].Longitude);
                        var marker = new google.maps.Marker({
                            position: latLng,
                            map: map,
                            title: data[i].PubName,
                            value: data[i].PubID
                        });

                        var infowindow = new google.maps.InfoWindow({
                            maxWidth: 150,
                            maxHeight: 50
                        });

                        google.maps.event.addListener(marker, 'click', (function (marker, i) {
                            return function () {
                                infowindow.setContent("<div style=\"color:red;width:auto;height:auto;\">" + data[i].PubName + '<br/>' +
                                '<a href="#" class="streetlink">' + data[i].PubAddress + "<a/></div>");
                                infowindow.open(map, marker);
                                map.panTo(this.getPosition());
                                map.setZoom(20);
                                sv.getPanoramaByLocation(this.getPosition(), 50, processSVData);
                                getPub();
                                $("#ddlPub [value='" + data[i].PubID + "']").attr("selected", "selected");
                                getClass(curDate);

                                google.maps.event.trigger(panorama, "resize");
                                updateScreetLinks();
                            }
                        })(marker, i));

                        google.maps.event.addListener(infowindow, 'closeclick', function () {
                            sv.getPanoramaByLocation(latLng, 50, processSVData);
                            $('#pano').hide();
                            map.setZoom(11);
                            map.panTo(latLng);
                            // then, remove the infowindows name from the array
                        });

                        gmarkers.push(marker);
//                    var infowindow = new google.maps.InfoWindow({
//                        content: data[i].PubName
//                    });

//                    google.maps.event.addListener(marker, 'click', function() {
//                        infowindow.open(map, marker);
//                    });
                    }


                    // sv.getPanoramaByLocation(berkeley, 50, processSVData);

                    /*                google.maps.event.addListener(map, 'click', function(event) {
                     sv.getPanoramaByLocation(event.latLng, 50, processSVData);
                     });*/

                }
            },
            error: function (data) {
                //alert('error');
            }
        });


//    var mapProp = {
//        center: new google.maps.LatLng(51.508742, -0.120850),
//        zoom: 5,
//        mapTypeId: google.maps.MapTypeId.ROADMAP
//    };
//
//    var map = new google.maps.Map(document.getElementById("map_canvas"), mapProp);
//
//    var marker = new google.maps.Marker({
//        position: new google.maps.LatLng(51.508742, -0.120850),
//    });
//
//
//    marker.setMap(map);
//
//    var infowindow = new google.maps.InfoWindow({
//        content: "<div>Hello World!112</div>"
//    });
//
//    google.maps.event.addListener(marker, 'click', function() {
//        infowindow.open(map, marker);
//    });


    }

    function myclick(i) {
        google.maps.event.trigger(gmarkers[i], "click");
    }

    function processSVData(data, status) {
        if (status == google.maps.StreetViewStatus.OK) {
            /*        var marker = new google.maps.Marker({
             position: data.location.latLng,
             map: map,
             title: data.location.description
             });*/

            panorama.setPano(data.location.pano);
            panorama.setPov({
                heading: 270,
                pitch: 0
            });
            panorama.setVisible(true);

            /*        google.maps.event.addListener(marker, 'click', function() {

             var markerPanoID = data.location.pano;
             // Set the Pano to use the passed panoID
             panorama.setPano(markerPanoID);
             panorama.setPov({
             heading: 270,
             pitch: 0
             });
             panorama.setVisible(true);
             });*/
        } else {
            alert('Street View data not found for this location.');
        }
    }

    function joinusaction() {
        if (!$('#joinus').hasClass("disabled")) {
            var form = $("#contact_form");
            var isValid;
            if (Modernizr.input.required) {
                isValid = form[0].checkValidity();
            }
            else {
                isValid = form.valid();
            }
            if (isValid) {
                // abort any pending request
                if (request) {
                    request.abort();
                }

                // setup some local variables
                var $form = $('#contact_form');
                // let's select and cache all the fields
                var $inputs = $form.find("input, select, button, textarea");
                // serialize the data in the form
                var serializedData = $form.serialize();

                // let's disable the inputs for the duration of the ajax request
                $inputs.prop("disabled", true);
                userNameData = $('#userName2').val();
                userEmailData = $('#userEmail2').val();
                idClassData = $('#horas').val();
                idPaymentData = $('#payment').val();
                levelData = $('#level_class option:selected').text();
                idlevelData = $('#level_class option:selected').val();
                idpromocode = $('#promo_code').val();
                var nameValue = document.getElementById("userName").value;
                $.getJSON("code/check_promocode.php", {promo_code: idpromocode}, function (data) {
                    var result = confirm(data.status);
                    if (result == true) {
                        if (data.promo_code == 0) {
                            idpromocode = 0;
                        }
                        request = $.ajax({
                            url: "code/insert_user_class.php",
                            type: "post",
                            data: {
                                userName: userNameData,
                                userEmail: userEmailData,
                                idClass: idClassData,
                                idPayment: idPaymentData,
                                level: levelData,
                                idlevel: idlevelData,
                                idpromocode: idpromocode
                            }
                        });


                        // callback handler that will be called on success
                        request.done(function (response, textStatus, jqXHR) {
                            // log a message to the console
                            $('#alert-success').fadeIn('slow');
                        });

                        // callback handler that will be called on failure
                        request.fail(function (jqXHR, textStatus, errorThrown) {
                            // log the error to the console
                            console.log("Error de registro en clase");
                            $('#alert-error').fadeIn('slow');
                        });

                        // callback handler that will be called regardless
                        // if the request failed or succeeded
                        request.always(function () {
                            // reenable the inputs
                            $inputs.prop("disabled", false);
                        });
                        // prevent default posting of form
                        event.preventDefault();
                    } ////==================confirm if true
                });
            } else {
                console.log("invalid form");
            }
        }
    }


    function GetUserDetails() {
        if (loggedIn != '') {
            $('#name').attr('readonly', 'readonly');
            $('#email').attr('readonly', 'readonly');
        }
        else {
            $('#name').removeAttr("readonly");
            $('#email').removeAttr("readonly");
        }
    }

    function RegisterUser() {
        if ($('#chkTerm').is(':checked')) {
            //alert(1);

//        var ClassID = $('#ddlClass').val();
//        var LevelID = $('#ddlLevel').val();
//        var LanguageID = $('#ddlLanguage').val();
//        var FullName = $('#name').val();
//        var EmailAddress = $('#email').val();
//        var PromoCode = $('#promoCode').val();
//        var PayMentAmount = $('#ddlSession').val();
//
//        //alert(ClassID +'-' +LevelID+'-' +LanguageID+'-' + FullName + '-' +EmailAddress +'-' +PromoCode +'-' +PayMentAmount);
//        // var UserDetailsID = loggedIn;
//
//        $.ajax({
//            type: "POST",
//            url: "jsoncode/json_paypal.php",
//            cache: false,
//            data: {
//                "ClassID": ClassID,
//                "LevelID": LevelID,
//                "LanguageID": LanguageID,
//                "FullName": FullName,
//                "EmailAddress": EmailAddress,
//                "PromoCode": PromoCode,
//                "PayMentAmount": PayMentAmount
//
//            },
//            dataType: 'json',
//            success: function(data) //Si se ejecuta correctamente
//            {
//                alert('Success From paypal');
//            },
//            error: function(data)
//            {
//                alert('errorfrom paypal');
//            }
//        });

        }
        else {
            alert('Tienes que marcar la casilla para aceptar los t?rminos y condiciones de Mingles.es');
            return false;

        }
    }


