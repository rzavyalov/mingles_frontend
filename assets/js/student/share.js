$('.shareclass').click(function() {
    $.pgwModal({
        url: $(this).data('url'),
        loadingContent: '<span style="text-align:center">Loading in progress</span>',
        title: 'Edit class',
        closable: true,
        titleBar: false,
        maxWidth: 800
    });
});